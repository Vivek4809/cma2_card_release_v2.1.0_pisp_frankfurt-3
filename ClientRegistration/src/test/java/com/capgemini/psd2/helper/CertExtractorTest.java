package com.capgemini.psd2.helper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.mockdata.DynamicClientMockData;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.nimbusds.jose.util.X509CertUtils;
@RunWith(SpringJUnit4ClassRunner.class)
public class CertExtractorTest {

	@InjectMocks
	CertExtractor certExtractor;
	
	@Mock
	RestClientSync restClientSync;
	@Test
	public void intermediateCertificateTest() throws CertificateException, IOException {
		X509Certificate cert = X509CertUtils.parse(DynamicClientMockData.certInStringFormat());
		Resource resource=new ByteArrayResource(DynamicClientMockData.certInStringFormat().getBytes());
		when(restClientSync.callForGet(any(), any(), any())).thenReturn(resource);
		Assert.assertNotNull(certExtractor.intermediateCertificate(cert));
	}

}
