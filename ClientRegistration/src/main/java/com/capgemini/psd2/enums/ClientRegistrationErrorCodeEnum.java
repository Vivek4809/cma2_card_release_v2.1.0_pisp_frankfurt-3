package com.capgemini.psd2.enums;

import com.capgemini.psd2.constant.ClientRegistarionConstants;

public enum ClientRegistrationErrorCodeEnum {

	/** The technical error. */
	TECHNICAL_ERROR("1100", ClientRegistarionConstants.TECHINICAL_ERROR, ClientRegistarionConstants.TECHINICAL_ERROR,
			"500"), SERVICE_UNAVAILABLE("1101", "Service Unavailable", "Service Unavailable", "503"),
	/** The authentication failure error. */
	AUTHENTICATION_FAILURE_ERROR("1102", "You are not authorized to use this service",
			"You are not authorized to use this service", "401"),

	SAML_USER_NULL("1103", ClientRegistarionConstants.SAML_USER_NULL, ClientRegistarionConstants.SAML_USER_NULL, "500"),

	/** Fetch User Profile Details **/
	INVALID_USER_ID("1104", ClientRegistarionConstants.INVALID_USER_ID, ClientRegistarionConstants.INVALID_USER_ID,
			"400"), INVALID_USER_DETAILS("1105", ClientRegistarionConstants.INVALID_USER_DETAILS,
					ClientRegistarionConstants.INVALID_USER_DETAILS, "400"), LDAP_ATTRIBUTE_NOT_FOUND("1106",
							ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
							ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, "400"),
	/** Update User Profile **/
	UPDATE_PROFILE_OPERATION_NOT_SUPPORTED("1107",
			"LDAP: error code 21 - When attempting to modify entry User profile information.",
			"LDAP: error code 21 - When attempting to modify entry User profile information.", "400"),
	ERROR_GENRIC("1113", ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			"500"),
	/** Download TestData **/
	ERROR_DOWNLOADING_TESTDATA("1114", ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, "400"),
	/** Valid Session **/
	USER_SESSION_EXPIRED("1115", ClientRegistarionConstants.SESSION_EXPIRED, ClientRegistarionConstants.SESSION_EXPIRED,
			"401"), APP_USER_SESSION_EXPIRED("1116", ClientRegistarionConstants.APP_SESSION_EXPIRED,
					ClientRegistarionConstants.APP_SESSION_EXPIRED, "401"),

	/** TPP Portal Error Codes **/
	TPP_ERROR_GENRIC("1200", ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,
			ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST, "500"),

	NO_PTC_DATA_AVAILABLE("1201", "No PTC Data Found", "No PTC Data Found", "400"),

	NO_TPP_DATA_AVAILABLE("1202", ClientRegistarionConstants.NO_TPP_DATA_FOUND, ClientRegistarionConstants.NO_TPP_DATA_FOUND, "400"),

	NO_CLIENT_APP_DATA_FOUND("1203", "No Client Data Found", "No Client Data Found", "400"),

	SSSEION_EXPIRED_OR_INVALID("1204", "Session has been invalidated now", "Session has been invalidated now", "400"),

	TIER_MORE_THAN_ONE("1205", ClientRegistarionConstants.INVALID_TIER, ClientRegistarionConstants.INVALID_TIER, "500"),

	NO_TPP_APPLICATION_AVAILABLE("1206", "No Application is registered for this TPP",
			"No Application is registered for this TPP", "500"),

	EXPIRED_SSA("1207", ClientRegistarionConstants.SSA_EXPIRED, ClientRegistarionConstants.SSA_EXPIRED, "400"),

	INVALID_SSA("1208", ClientRegistarionConstants.INVALID_SSA, ClientRegistarionConstants.INVALID_SSA, "400"),

	SSA_NOT_OF_SELECTED_ORG("1209", ClientRegistarionConstants.SSA_NOT_OF_SELECTED_ORG, ClientRegistarionConstants.SSA_NOT_OF_SELECTED_ORG,
			"400"),

	TPP_ORG_NOT_ACTIVE("1210", ClientRegistarionConstants.TPP_ORG_NOT_ACTIVE, ClientRegistarionConstants.TPP_ORG_NOT_ACTIVE, "400"),

	MULE_LOGIN_FAILURE("1211", ClientRegistarionConstants.MULE_LOGIN_FAILURE, ClientRegistarionConstants.MULE_LOGIN_FAILURE, "500"),

	MULE_API_REGISTERATION_CALL_FAILURE("1212", ClientRegistarionConstants.MULE_API_REGISTERATION_CALL_FAILURE,
			ClientRegistarionConstants.MULE_API_REGISTERATION_CALL_FAILURE, "500"),

	MULE_FETCH_LIST_OF_ALL_ORG_APIS_FAILURE("1213", ClientRegistarionConstants.MULE_FETCH_LIST_OF_ALL_ORG_APIS_FAILURE,
			ClientRegistarionConstants.MULE_FETCH_LIST_OF_ALL_ORG_APIS_FAILURE, "500"),

	MULE_FETCH_LIST_OF_TIERS_FOR_API_VERSIONS_FAILURE("1214",
			ClientRegistarionConstants.MULE_FETCH_LIST_OF_TIERS_FOR_API_VERSIONS_FAILURE,
			ClientRegistarionConstants.MULE_FETCH_LIST_OF_TIERS_FOR_API_VERSIONS_FAILURE, "500"),

	MULE_CREATE_APP_CONTRACT_FAILURE("1215", ClientRegistarionConstants.MULE_CREATE_APP_CONTRACT_FAILURE,
			ClientRegistarionConstants.MULE_CREATE_APP_CONTRACT_FAILURE, "500"),

	MULE_API_REGISTERATION_DELETE_CALL_FAILURE("1216", ClientRegistarionConstants.MULE_API_REGISTERATION_CALL_DELETE_FAILURE,
			ClientRegistarionConstants.MULE_API_REGISTERATION_CALL_DELETE_FAILURE, "500"),

	PF_REGISTER_API_CALL_FAILURE("1217", ClientRegistarionConstants.PF_REGISTER_API_CALL_FAILURE,
			ClientRegistarionConstants.PF_REGISTER_API_CALL_FAILURE, "500"),

	PD_TPP_ORG_UPDATE_OR_INSERT_FAILURE("1218", ClientRegistarionConstants.PD_TPP_ORG_UPDATE_OR_INSERT_FAILURE,
			ClientRegistarionConstants.PD_TPP_ORG_UPDATE_OR_INSERT_FAILURE, "500"),

	PD_TPP_APP_ASSOCIATION_FAILURE("1219", ClientRegistarionConstants.PD_TPP_APP_ASSOCIATION_FAILURE,
			ClientRegistarionConstants.PD_TPP_APP_ASSOCIATION_FAILURE, "500"),

	PD_CLIENT_APP_MAPPING_FAILURE("1220", ClientRegistarionConstants.PD_CLIENT_APP_MAPPING_FAILURE,
			ClientRegistarionConstants.PD_CLIENT_APP_MAPPING_FAILURE, "500"),

	PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE("1221", ClientRegistarionConstants.PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE,
			ClientRegistarionConstants.PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE, "500"),

	PD_TPP_APP_ASSOCIATION_DELETE_FAILURE("1222", ClientRegistarionConstants.PD_TPP_APP_ASSOCIATION_DELETE_FAILURE,
			ClientRegistarionConstants.PD_TPP_APP_ASSOCIATION_DELETE_FAILURE, "500"),

	PF_REGISTER_API_CALL_DELETE_FAILURE("1223", ClientRegistarionConstants.PF_REGISTER_API_CALL_DELETE_FAILURE,
			ClientRegistarionConstants.PF_REGISTER_API_CALL_DELETE_FAILURE, "500"),

	LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP("1224", ClientRegistarionConstants.LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP,
			ClientRegistarionConstants.LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP, "400"),

	PING_DIRECTORY_CALL_FAILED("1227", ClientRegistarionConstants.PING_DIRECTORY_CALL_FAILED,
			ClientRegistarionConstants.PING_DIRECTORY_CALL_FAILED, "500"),

	TPP_USER_SESSION_EXPIRED("1225", ClientRegistarionConstants.SESSION_EXPIRED, ClientRegistarionConstants.SESSION_EXPIRED, "401"),

	TPP_APP_USER_SESSION_EXPIRED("1226", ClientRegistarionConstants.APP_SESSION_EXPIRED, ClientRegistarionConstants.APP_SESSION_EXPIRED,
			"401"),

	LOGIN_ERROR("1228", ClientRegistarionConstants.LOGIN_ERROR, ClientRegistarionConstants.LOGIN_ERROR, "500"),

	NO_TPP_NAME_PROVIDED("1229", "No TPP name provided in the request", "No TPP name provided in the request", "400"),

	MULE_ORGANISATION_APIS_NOT_AVAILABLE("1230", ClientRegistarionConstants.MULE_ORGANISATION_APIS_NOT_AVAILABLE,
			ClientRegistarionConstants.MULE_ORGANISATION_APIS_NOT_AVAILABLE, "500"),

	PD_IS_TPP_EXIST_CALL_FAILED("1231", ClientRegistarionConstants.PD_IS_TPP_EXIST_CALL_FAILED,
			ClientRegistarionConstants.PD_IS_TPP_EXIST_CALL_FAILED, "500"),

	TPP_TECHNICAL_ERROR("1232", ClientRegistarionConstants.TECHINICAL_ERROR, ClientRegistarionConstants.TECHINICAL_ERROR, "500"),

	UNABLE_TO_PARSE("1233", ClientRegistarionConstants.UNABLE_TO_PARSE, ClientRegistarionConstants.UNABLE_TO_PARSE, "500"),

	TPP_CLIENTID_INVALID("1234", "TppId or ClientId is not valid in request",
			"TppId or ClientId is not valid in request", "400"),

	NO_TPP_CLIENTID_FOUND("1235", "TppId or ClientId is not found in request",
			"TppId or ClientId is not found in request", "400"),

	MULE_RESPONSE_NOT_FOUND("1236", "Response not found from Mule Api", "Response not found from Mule Api", "500"),

	TPP_BLOCKED("1237", "Unauthorised access.Tpp is blocked", "Unauthorised access.Tpp is blocked", "400"),

	ALGORITHM_NOT_SUPPORTED("1238", "Unsupported Algorithm", "Unsupported Algorithm", "500"),

	TOKEN_TYPE_INVALID("1239", "Token is of Invalid Type", "Token is of Invalid Type", "500"),
	
	CONTRACT_API_FLAG_INVALID("1240", ClientRegistarionConstants.CONTRACT_API_FLAG_INVALID, ClientRegistarionConstants.CONTRACT_API_FLAG_INVALID, "500"),
	
	INVALID_SSA_ISSUER("1241", "Issuer of the SSA token is invalid", "Issuer of the SSA token is invalid", "400"),
	
	INVALID_SSA_SIGNATURE("1242", "Signature of the SSA token is invalid", "Signature of the SSA token is invalid", "400"), 
	
	VALIDATION_ERROR("126", "Validation error found with the provided input", "Validation Error found in the parameter", "400"),
	
	NO_BLOCK_ACTION_PROVIDED("301", "No Block Action is provided", "No Block Action is provided", "400"),

	USER_ID_DOES_NOT_EXIST("302", "Invalid Request : User not found for User Id provided", "Invalid Request : User not found for User Id provided ", "400"),

	USER_ALREADY_DEACTIVATED("303", "The Deactivate action cannot be performed because User is already deactivated" , "The Deactivate action cannot be performed because User is already deactivated", "400"),

	USER_ALREADY_ACTIVATED("304", "The activation action cannot be performed because User is already activated", "The activation action cannot be performed because User is already activated", "400"),

	LDAP_ATTRIBUTE_NOT_FOUND_IN_DEVELOPER_PORTAL("305", "The ldap attribute not present in the developer Portal", "The ldap attribute not present in the developer Portal", "400"),

	NO_DESCRIPTIVE_MESSAGE_PROVIDED("306","Invalid Request : No Descriptive message  is provided","Invalid Request : No Descriptive message  is provided","400"),

	NO_SUCH_DEVELOPER_USER_RECORD("307","The specified User does not exist","“The specified User does not exist", "400"),

	NO_EMAIL_ID_PROVIDED("308","No Valid email-id provided","No Valid email-id provided","400"),
	
	
	/**
	 * Dynamic Client constants
	 */
	
	INVALID_CERTIFICATE("9000","invalid_certificate.","Invalid certificate.","400"),
	
	SSA_MISSING("9001","ssa_missing.","Software statement is missing in request.","400"),
	BAD_REQUEST("9002","ssa_missing.","Software statement is missing in request.","400"),

	
	SSA_CERTIFICATE_MISMATCH_CN("9003",ClientRegistarionConstants.INVALID_SSA_CERTIFICATE,"Software id in SSA is not matched with CN attribute of the certificate.","400"),
	
	SSA_CERTIFICATE_MISMATCH_OU("9004",ClientRegistarionConstants.INVALID_SSA_CERTIFICATE,"Org id in SSA is not matched with OU attribute of the certificate.","400"),

	INVALID_JWT("9005","invalid_jwt.","Registration JWT token is invalid.","400"),
	
	MANDATORY_REQUEST_PARAMS_MISSING("9006",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"The value of one of the mandatory client metadata fields is missing.","400"),
	
	AUD_MISMATCH("9007",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Aud of registration request is not matched with issuer in well known.","400"),
	
	REDIRECT_URIS_MISMATCH("9008","invalid_redirect_uri.","The value of one or more redirection URIs is invalid.","400"),
	AUTH_METHODS_NOT_SUPPORTED("9009",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Token endpoint auth method is not supported.","400"),
	GRANT_TYPES_NOT_SUPPORTED("9010",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Grant types is not supported.","400"),
	RESPONSE_TYPES_NOT_SUPPORTED("9011",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Response types is not supported.","400"),
	SOFTWARE_ID_MISMATCH("9012",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Software id in request must match the software id in SSA.","400"),
	SCOPES_NOT_SUPPORTED("9013",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Scopes in request is not supported.","400"),
	APP_TYPE_NOT_SUPPORTED("9014",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Application type must be WEB.","400"),
	ID_TOKEN_ALG_NOT_SUPPORTED("9015",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"ID token signing algorithm is not supported.","400"),
	REQUEST_OBJECT_ALG_NOT_SUPPORTED("9016",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Request object signing algorithm is not supported.","400"),
	INVALID_SSA_DYNAMIC("9017","invalid_software_statement.","The software statement presented is invalid.","400"),
	UNAPPROVED_SSA_ERROR("9018",ClientRegistarionConstants.UNAPPROVED_SSA,ClientRegistarionConstants.UNABLE_TO_PROCESS_YOUR_REQUEST,"400"),

	ISSUER_MISMATCH("9019",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Issuer of registration request is not matched with SSA software Id.","400"),
	UNSUPPORTED_VALIDATION_TYPE("9020",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Issuer of registration request is not matched with SSA software Id.","400"),
	PING_CERTIFICATE_IMPORT_FAILED("9021","cert_error","Certificate import error.","400"),
	TLS_CLIENT_AUTH_DN("9022",ClientRegistarionConstants.INVALID_CLIENT_METADATA,"Tls client auth dn must be present and matched with certificate dn.","400"),
	TPP_INACTIVE("9023",ClientRegistarionConstants.UNAPPROVED_SSA,"Invalid or Inactive TPP.","400"),
	PASSPORTING_CHECK_FAILED("9024",ClientRegistarionConstants.UNAPPROVED_SSA,"TPP is unauthorized for given member state.","400"),
	ERROR_IN_PASSPORTING_CHECK("9025",ClientRegistarionConstants.UNAPPROVED_SSA,"Error in Passporting check.","400"),
	INVALID_CERT_SSA("9026",ClientRegistarionConstants.INVALID_SSA_CERTIFICATE,ClientRegistarionConstants.INVALID_SSA_CERTIFICATE,"400"),
	INVALID_X5C_CERT("9027","Certificate in x5c claim is invalid","Certificate in x5c claim is invalid","400"),

	CLIENT_UPDATE_OP_FAILED("9028","Metadata present in the new SSA does not match with original SSA","Metadata present in the new SSA does not match with original SSA","400"),
	SSA_CERTIFICATE_MISMATCH_NCAID("9029",ClientRegistarionConstants.INVALID_SSA_CERTIFICATE,"NCA id in SSA is not matched with ncaid in the certificate.","400"),
	INVALID_EIDAS_CERTIFICATE("9030","Invalid Certificate.","The eidas certificate is invalid.","400"),
	SSA_SIGNATURE_FAILED("9031","Invalid SSA.","Signature verification of SSA failed.","400"),
	ORG_COMPETENT_AUTH_CLAIMS_MISSING("9032","Invalid SSA.","Roles in organisation competent authority claims is missing in SSA.","400"),
	
	INVALID_REDIRECT_URIS("9033","Invalid SSA.","Redirect URI is invalid.","400"),
	REDIRECT_URI_IS_MANDATORY("9034","Invalid SSA.","Redirect URI must be present in SSA.","400"),
	SELF_SIGNED_SSA_NOT_SUPPORTED("9035","Invalid SSA.","SSA is invalid.","400");


	/** The error code. */
	private String errorCode;

	/** The error message. */
	private String errorMessage;

	/** The detail error message. */
	private String detailErrorMessage;

	/** The status code. */
	private String statusCode;

	/**
	 * Instantiates a new error code enum.
	 *
	 * @param errorCode
	 *            the error code
	 * @param errorMesssage
	 *            the error messsage
	 * @param detailErrorMessage
	 *            the detail error message
	 * @param statusCode
	 *            the status code
	 */
	ClientRegistrationErrorCodeEnum(String errorCode, String errorMesssage, String detailErrorMessage, String statusCode) {
		this.errorCode = errorCode;
		this.errorMessage = errorMesssage;
		this.detailErrorMessage = detailErrorMessage;
		this.statusCode = statusCode;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Gets the detail error message.
	 *
	 * @return the detail error message
	 */
	public String getDetailErrorMessage() {
		return detailErrorMessage;
	}

	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public String getStatusCode() {
		return statusCode;
	}
	
	 public static ClientRegistrationErrorCodeEnum fromString(String errorCode) {
		    for (ClientRegistrationErrorCodeEnum b : ClientRegistrationErrorCodeEnum.values()) {
		      if (b.errorCode.equalsIgnoreCase(errorCode)) {
		        return b;
		      }
		    }
		    return null;
		  }
}
