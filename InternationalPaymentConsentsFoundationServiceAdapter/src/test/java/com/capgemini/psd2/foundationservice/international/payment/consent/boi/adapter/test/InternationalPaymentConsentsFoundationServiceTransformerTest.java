package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Agent;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ExchangeRateQuote;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentType;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PriorityCode;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Purpose;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.RateQuoteType;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.transformer.InternationalPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBPriority2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.stage.domain.Charge;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentConsentsFoundationServiceTransformerTest {
	@InjectMocks
	InternationalPaymentConsentsFoundationServiceTransformer transformer;
	
	@Mock
	private PSD2Validator psd2Validator;
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	 
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testtransformInternationalPaymentResponse11(){
	PaymentInstructionProposalInternational inputBalanceObj=new PaymentInstructionProposalInternational();
	Purpose p1= new Purpose();
	inputBalanceObj.setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setInstructionReference("boi");
	inputBalanceObj.setInstructionLocalInstrument("card");
	inputBalanceObj.setProposalCreationDatetime("03/14/2019");
	inputBalanceObj.setProposalStatus(ProposalStatus.Rejected);
	inputBalanceObj.setProposalStatusUpdateDatetime("02/15/2019");
	List<PaymentInstructionCharge> charges1 = new ArrayList<>();
	PaymentInstructionCharge pa= new PaymentInstructionCharge();
	pa.setChargeBearer(ChargeBearer.BORNEBYDEBTOR);
	pa.setType("vgju");
	charges1.add(pa);
	inputBalanceObj.setCharges(charges1);
	Charge c = new Charge();
	c.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
	c.setType("ABC");
	List<Charge> charges = new ArrayList<Charge>();
	charges.add(c);
	p1.setProprietaryPurpose("ABCD");
	inputBalanceObj.setPurpose(p1);
	Amount amt = new Amount();
	amt.setTransactionCurrency(12.00);
	pa.setAmount(amt);
	Currency cr = new Currency();
	cr.setIsoAlphaCode("a1s2");
	inputBalanceObj.setCurrencyOfTransfer(cr);
	pa.setCurrency(cr);
	FinancialEventAmount amount=new FinancialEventAmount();
	amount.setTransactionCurrency(23.0);
	inputBalanceObj.setFinancialEventAmount(amount);
	Currency currency= new Currency();
	currency.setIsoAlphaCode("iuhio");
	inputBalanceObj.setTransactionCurrency(currency);
	AccountInformation act=new AccountInformation();
	
	act.setAccountName("nam");
	act.setAccountIdentification("345");
	act.setSchemeName("debit");
	act.setSecondaryIdentification("678");
	inputBalanceObj.setAuthorisingPartyAccount(act);
	ProposingPartyAccount ppa=new ProposingPartyAccount();
	ppa.setAccountName("nam");
	ppa.setAccountNumber("987");
	ppa.setSchemeName("credit");
	ppa.setSecondaryIdentification("456");
	ppa.setAccountIdentification("c4d512");
	inputBalanceObj.setProposingPartyAccount(ppa);
	PartyBasicInformation partyBasicInformation = new PartyBasicInformation();
	partyBasicInformation.setPartyName("ABCD");
	inputBalanceObj.setProposingParty(partyBasicInformation);
	PaymentInstructionPostalAddress add=new PaymentInstructionPostalAddress();
	add.setTownName("pune");
	add.setDepartment("A");
	add.setCountrySubDivision("MH");
	add.setSubDepartment("ABC");
	add.setPostCodeNumber("600789");
	add.setGeoCodeBuildingName("capg");
	add.setGeoCodeBuildingNumber("1521");
	Country country=new Country();
	country.setIsoCountryAlphaTwoCode("IND");
	add.setAddressCountry(country);
	List<String> line= new ArrayList<>();
	line.add("Talwade");
	add.setAddressLine(line);
	add.setAddressType("POSTAL");
	inputBalanceObj.setProposingPartyPostalAddress(add);
	
	inputBalanceObj.setPaymentType(PaymentType.INTERNATIONAL);
	inputBalanceObj.setAuthorisationDatetime("12/12/2018");
	PaymentInstrumentRiskFactor ref = new PaymentInstrumentRiskFactor();
	ref .setMerchantCategoryCode("BUI");
	ref.setPaymentContextCode("BILLPAYMENT");
	ref.setMerchantCustomerIdentification("SBU");
	Address cpa=new Address();
	cpa.setFirstAddressLine("134");
	
	cpa.setThirdAddressLine("sector27");
	cpa.setFourthAddressLine("nigdi");
	
	cpa.setPostCodeNumber("600024");
	cpa.setGeoCodeBuildingNumber("1654");
	cpa.setGeoCodeBuildingName("Vinalay");
	cpa.setAddressCountry(country);
	Country country1=new Country();
	country1.setIsoCountryAlphaTwoCode("IND");
	cpa.setAddressCountry(country1);
	ref.setCounterPartyAddress(cpa);
	inputBalanceObj.setPaymentInstructionRiskFactorReference(ref);
	
	
	ExchangeRateQuote exchangeRateQuote=new ExchangeRateQuote();
	exchangeRateQuote.setRateQuoteType(RateQuoteType.Actual);
	exchangeRateQuote.setExchangeRate(2.0412);
	//exchangeRateQuote.setExpirationDateTime("12/06/2019");
	currency.setIsoAlphaCode("iuhio");
	exchangeRateQuote.setUnitCurrency(currency);
	
	inputBalanceObj.setExchangeRateQuote(exchangeRateQuote);
	inputBalanceObj.setPriorityCode(PriorityCode.Normal);
	//inputBalanceObj.setPurpose("dsfs");
	Agent agent=new Agent();
	agent.setAgentName("Msdzfc");
	agent.setPartyAddress(add);
	//agent.setSwiftBankIdentifierCodeBIC("5311241");
	inputBalanceObj.setProposingPartyAgent(agent);
	transformer.transformInternationalPaymentResponse(inputBalanceObj);
	transformer.transformInternationalConsentResponseFromFDToAPIForInsert(inputBalanceObj);
	inputBalanceObj.setProposalStatus(ProposalStatus.AwaitingAuthorisation);
	cpa.setFifthAddressLine("pune");
	inputBalanceObj.setAuthorisingPartyReference("ABCDEF");
	
	inputBalanceObj.setAuthorisingPartyUnstructuredReference("ANY TYPE");
	inputBalanceObj.setAuthorisationType(AuthorisationType.Any);
	cpa.setSecondAddressLine("pradhikaran");
	transformer.transformInternationalPaymentResponse(inputBalanceObj);
	transformer.transformInternationalConsentResponseFromFDToAPIForInsert(inputBalanceObj);
	}
	@Test
	public void testTransformDomesticConsentResponseFromAPIToFDForInsert(){
		
		
		CustomIPaymentConsentsPOSTRequest paymentConsentsRequest = new CustomIPaymentConsentsPOSTRequest();
		OBInternational1 oBInternational1 = new OBInternational1();
		OBDomestic1InstructedAmount amount=new OBDomestic1InstructedAmount();
		OBWriteDataInternationalConsent1 oBWriteDataInternationalConsent1 = new OBWriteDataInternationalConsent1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6(); 
		List<String> addressLine = new ArrayList<String>();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBBranchAndFinancialInstitutionIdentification3 creditoragent=new OBBranchAndFinancialInstitutionIdentification3();
		OBPartyIdentification43 creditor=new OBPartyIdentification43();
		Country addressCountry = new Country();
		OBExchangeRate1 oBExchangeRate1=new OBExchangeRate1();
		PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();

		OBCashAccountDebtor3 authorisingPartyAccount = new OBCashAccountDebtor3();
		Currency transactionCurrency = new Currency();
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		
		financialEventAmount.setTransactionCurrency(22.01);
		transactionCurrency.setIsoAlphaCode("NZD");
		authorisingPartyAccount.setSchemeName("pata nai");
		authorisingPartyAccount.setName("pata nai");
		authorisingPartyAccount.setIdentification("pata nai");
		authorisingPartyAccount.setSecondaryIdentification("pata nai");
		proposingPartyAccount.setSchemeName("pata nai");
		proposingPartyAccount.setAccountNumber("pata nai");
		proposingPartyAccount.setAccountName("pata nai");
		proposingPartyAccount.setSecondaryIdentification("pata nai");
		addressLine.add("FirstAddressLine");
		addressLine.add("SecondAddressLine");
		proposingPartyPostalAddress.setAddressType("permanent");
		proposingPartyPostalAddress.setSubDepartment("kuch bhi");
		proposingPartyPostalAddress.setDepartment("pata nai");
		proposingPartyPostalAddress.setGeoCodeBuildingName("pata nai");
		proposingPartyPostalAddress.setGeoCodeBuildingNumber("pata nai");
		proposingPartyPostalAddress.setTownName("pata nai");
		proposingPartyPostalAddress.setCountrySubDivision("pata nai");
		proposingPartyPostalAddress.setAddressCountry(addressCountry);
		proposingPartyPostalAddress.setAddressLine(addressLine);
		
		addressCountry.setIsoCountryAlphaTwoCode("pata nai");
		Address counterPartyAdress=new Address();
		counterPartyAdress.setAddressCountry(addressCountry);
		PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference=new PaymentInstrumentRiskFactor() ;
		paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAdress);
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setStreetName("abc");
		deliveryAddress.setBuildingNumber("1233");
		deliveryAddress.setPostCode("3111455");
		deliveryAddress.setTownName("pune");
		deliveryAddress.setCountrySubDivision("ABCD");
		deliveryAddress.setCountry("India");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setMerchantCategoryCode("pata nai");
		risk.setMerchantCustomerIdentification("pata nai");
		risk.setDeliveryAddress(deliveryAddress);
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		authorisation.completionDateTime(""+new Date());
		remittanceInformation.setUnstructured("pata nai");
		remittanceInformation.setReference("pata nai");
		
		addressLine.add("abc");
		addressLine.add("xyz");
		creditorPostalAddress.setAddressType(com.capgemini.psd2.pisp.domain.OBAddressTypeCode.POSTAL);
		creditorPostalAddress.setDepartment("pata nai");
		creditorPostalAddress.setSubDepartment("pata nai");
		creditorPostalAddress.setStreetName("pata nai");
		creditorPostalAddress.setBuildingNumber("pata nai");
		creditorPostalAddress.setPostCode("pata nai");
		creditorPostalAddress.setTownName("pata nai");
		creditorPostalAddress.setCountrySubDivision("pata nai");
		creditorPostalAddress.setCountry("India");
		creditorPostalAddress.setAddressLine(addressLine);
		debtorAccount.setName("pata nai");
		debtorAccount.setIdentification("pata nai");
		debtorAccount.setName("pata nai");
		debtorAccount.setSecondaryIdentification("pata nai");
		creditorAccount.setSchemeName("pata nai");
		creditorAccount.setIdentification("pata nai");
		creditorAccount.setName("pata nai");
		creditorAccount.setSecondaryIdentification("pata nai");
		
		amount.setAmount("29.01");
		amount.setCurrency("NZD");
		
		oBExchangeRate1.setContractIdentification("sfd");
		oBExchangeRate1.setExchangeRate(BigDecimal.valueOf(123.0));
		oBExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		oBExchangeRate1.setUnitCurrency("GBP");
		oBInternational1.setExchangeRateInformation(oBExchangeRate1);

	
		oBInternational1.setCreditorAccount(creditorAccount);
		creditoragent.setPostalAddress(creditorPostalAddress);
		oBInternational1.setCreditorAgent(creditoragent);
		creditor.setPostalAddress(creditorPostalAddress);
		oBInternational1.setCreditor(creditor);
		oBInternational1.setRemittanceInformation(remittanceInformation);
		oBInternational1.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
		oBWriteDataInternationalConsent1.setInitiation(oBInternational1);
		oBWriteDataInternationalConsent1.setAuthorisation(authorisation);
		
		paymentConsentsRequest.setData(oBWriteDataInternationalConsent1);
		paymentConsentsRequest.setRisk(risk);
		
	
		Map<String, String> params = new HashMap<String,String>();
		params.put("tenant_id", "BOIROI");
		params.put("X-BOI-CHANNEL", "channel");
		transformer.transformInternationalConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}
	
	
	@Test
	public void testTransformDomesticConsentResponseFromAPIToFDForInsert1(){
		
		
		CustomIPaymentConsentsPOSTRequest paymentConsentsRequest = new CustomIPaymentConsentsPOSTRequest();
		OBInternational1 oBInternational1 = new OBInternational1();
		OBDomestic1InstructedAmount amount=new OBDomestic1InstructedAmount();
		OBWriteDataInternationalConsent1 oBWriteDataInternationalConsent1 = new OBWriteDataInternationalConsent1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6(); 
		List<String> addressLine = new ArrayList<String>();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBBranchAndFinancialInstitutionIdentification3 creditoragent=new OBBranchAndFinancialInstitutionIdentification3();
		OBPartyIdentification43 creditor=new OBPartyIdentification43();
		Country addressCountry = new Country();
		OBExchangeRate1 oBExchangeRate1=new OBExchangeRate1();
		PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();

		OBCashAccountDebtor3 authorisingPartyAccount = new OBCashAccountDebtor3();
		Currency transactionCurrency = new Currency();
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		
		financialEventAmount.setTransactionCurrency(Double.valueOf(22));
		transactionCurrency.setIsoAlphaCode("NZD");
		authorisingPartyAccount.setSchemeName("pata nai");
		authorisingPartyAccount.setName("pata nai");
		authorisingPartyAccount.setIdentification("pata nai");
		authorisingPartyAccount.setSecondaryIdentification("pata nai");
		proposingPartyAccount.setSchemeName("pata nai");
		proposingPartyAccount.setAccountNumber("pata nai");
		proposingPartyAccount.setAccountName("pata nai");
		proposingPartyAccount.setSecondaryIdentification("pata nai");
		addressLine.add("FirstAddressLine");
		addressLine.add("SecondAddressLine");
		proposingPartyPostalAddress.setAddressType("permanent");
		proposingPartyPostalAddress.setSubDepartment("kuch bhi");
		proposingPartyPostalAddress.setDepartment("pata nai");
		proposingPartyPostalAddress.setGeoCodeBuildingName("pata nai");
		proposingPartyPostalAddress.setGeoCodeBuildingNumber("pata nai");
		proposingPartyPostalAddress.setTownName("pata nai");
		proposingPartyPostalAddress.setCountrySubDivision("pata nai");
		proposingPartyPostalAddress.setAddressCountry(addressCountry);
		proposingPartyPostalAddress.setAddressLine(addressLine);
		
		addressCountry.setIsoCountryAlphaTwoCode("pata nai");
		Address counterPartyAdress=new Address();
		counterPartyAdress.setAddressCountry(addressCountry);
		PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference=new PaymentInstrumentRiskFactor() ;
		paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAdress);
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setStreetName("abc");
		deliveryAddress.setBuildingNumber("1233");
		deliveryAddress.setPostCode("3111455");
		deliveryAddress.setTownName("pune");
		deliveryAddress.setCountrySubDivision("ABCD");
		deliveryAddress.setCountry("India");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setMerchantCategoryCode("pata nai");
		risk.setMerchantCustomerIdentification("pata nai");
		risk.setDeliveryAddress(deliveryAddress);
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		authorisation.completionDateTime(""+new Date());
		remittanceInformation.setUnstructured("pata nai");
		remittanceInformation.setReference("pata nai");
		
		addressLine.add("abc");
		creditorPostalAddress.setAddressType(com.capgemini.psd2.pisp.domain.OBAddressTypeCode.POSTAL);
		creditorPostalAddress.setDepartment("pata nai");
		creditorPostalAddress.setSubDepartment("pata nai");
		creditorPostalAddress.setStreetName("pata nai");
		creditorPostalAddress.setBuildingNumber("pata nai");
		creditorPostalAddress.setPostCode("pata nai");
		creditorPostalAddress.setTownName("pata nai");
		creditorPostalAddress.setCountrySubDivision("pata nai");
		creditorPostalAddress.setCountry("India");
		creditorPostalAddress.setAddressLine(addressLine);
		debtorAccount.setName("pata nai");
		debtorAccount.setIdentification("pata nai");
		debtorAccount.setSchemeName("pata nai");
		debtorAccount.setSecondaryIdentification("pata nai");
		creditorAccount.setSchemeName("pata nai");
		creditorAccount.setIdentification("pata nai");
		creditorAccount.setName("pata nai");
		creditorAccount.setSecondaryIdentification("pata nai");
		
		amount.setAmount("29");
		amount.setCurrency("NZD");
		
		oBExchangeRate1.setContractIdentification("sfd");
		oBExchangeRate1.setExchangeRate(BigDecimal.valueOf(123.0));
		oBExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		oBExchangeRate1.setUnitCurrency("GBP");
		oBInternational1.setExchangeRateInformation(oBExchangeRate1);
		oBInternational1.setInstructionPriority(OBPriority2Code.NORMAL);
		oBInternational1.setPurpose("fgds");
		oBInternational1.setCurrencyOfTransfer("asdasd");
		oBInternational1.setInstructionIdentification("pata nai");
		oBInternational1.setEndToEndIdentification("pata nai");
		oBInternational1.setLocalInstrument("pata nai");
		oBInternational1.setInstructedAmount(amount);
		oBInternational1.setDebtorAccount(debtorAccount);
		oBInternational1.setCreditorAccount(creditorAccount);
		creditoragent.setPostalAddress(creditorPostalAddress);
		oBInternational1.setCreditorAgent(creditoragent);
		creditor.setPostalAddress(creditorPostalAddress);
		oBInternational1.setCreditor(creditor);
		oBInternational1.setRemittanceInformation(remittanceInformation);
		
		oBWriteDataInternationalConsent1.setInitiation(oBInternational1);
		oBWriteDataInternationalConsent1.setAuthorisation(authorisation);
		
		paymentConsentsRequest.setData(oBWriteDataInternationalConsent1);
		paymentConsentsRequest.setRisk(risk);
		
	
		Map<String, String> params = new HashMap<String,String>();
		params.put("tenant_id", "BOIUK");
		transformer.transformInternationalConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}
}
