/*
 * Payment Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain;

import java.io.IOException;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Rate quote Type
 */
//@JsonAdapter(RateQuoteType.Adapter.class)
public enum RateQuoteType {
  
  Actual("Actual"),
  
  Agreed("Agreed"),
  
  Indicative("Indicative");

  private String value;

  RateQuoteType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  public static RateQuoteType fromValue(String text) {
    for (RateQuoteType b : RateQuoteType.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }

/*  public static class Adapter extends TypeAdapter<RateQuoteType> {
    @Override
    public void write(final JsonWriter jsonWriter, final RateQuoteType enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public RateQuoteType read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return RateQuoteType.fromValue(String.valueOf(value));
    }
  } */
}

