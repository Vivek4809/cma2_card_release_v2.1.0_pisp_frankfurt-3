
package com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;

@Component
public class IPaymentConsentsCoreSystemAdapterFactory
		implements ApplicationContextAware, IPaymentConsentsAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;

	@Override
	public InternationalPaymentStagingAdapter getInternationalPaymentSetupStagingInstance(String adapterName) {
		return (InternationalPaymentStagingAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}
}
