/*package com.capgemini.psd2.security.saas.test.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.security.web.WebAttributes;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.saas.controllers.SaaSController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;

@RunWith(SpringJUnit4ClassRunner.class)
public class SaaSControllerTest {
	
	private MockMvc mockMvc;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes; 
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	@Mock
	private SecurityRequestAttributes securityRequestAttributes;
	@Mock
	private AccountRequestAdapter accountRequestAdapter;
	
	@Mock
	private UIStaticContentUtilityController uiController;
	
	
	
	
	@InjectMocks
	private SaaSController SaaSController;
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(SaaSController).dispatchOptions(true).build();
	}
	
	@Test(expected=PSD2Exception.class)
	public void handleSessionTimeOutOnCancelTest() throws Exception{
		Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		when(SaaSController.handleSessionTimeOutOnCancel(model)).thenReturn(anyString());
	}
	
	@Test
	public void errorWithResumePathTest() throws Exception {
		Model model= mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		//PSD2AuthenticationException exception = new PSD2AuthenticationException("123", errorInfo);
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(null);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn("abcd");
		
		assertNotNull(SaaSController.error(model));
	}
	
	@Test
	public void errorWithOAuthURLParamTest() throws Exception {
		Model model= mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		PSD2AuthenticationException exception = new PSD2AuthenticationException("123", errorInfo);
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(exception);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("abcd");
		
		assertNotNull(SaaSController.error(model));
	}
	
	@Test(expected =PSD2Exception.class)
	public void Sessionerror() throws Exception {
		Model model= mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		String oAuthUrl = "Qxl7JY2bCzj/1O3i7r8U7FIGIeDodZEE7Yf70rLbH2hz8bK8PTtgSATm9ix61kE38Jpt/SS4Z1cXk+1RWeG1RCECy6u7FGgIzIdpco1MaE64vA7Pn3a11vv8ZmJ2y9skkVNcu9DXnL1fKzb2DylzGH9LWmVovRhU10at1NBRE0ERCB8dsT+xfuQ985E2MFrkWPUP/Kbz8iOa4dqY4WBxazQcy06dVUO7BQwmo9oYch8ubVW3ILdfbl7pfM7Z/wszl8cB8SPPTtLavvCfDNXRGUSZoYGGfK40F8TOvF/sYLeOx3E0HFNibWAx6f9JmhAZWO40a82nAYaY3BkTLY3uyQ8nZSQJ1oyW1VRa/U+Cig5lQ1W4BAH0KgYQzi9HkoF1KeP9ZE7WQlerJ2G8en9x5ewvgj11eF5L0zZ41oUNy41pso1lU+p+n0PCs6EngV+FImsx8pBLf6XN0rQ8LhW0z51I7MhKCBuyaeIw4zR6pWRBH9xcYKUgbkCvmLcM4MQRR7G+y0Jd5F/hLfhI60vTMZEntGexTanIZyOdEgPQnOQzTtqwmCmD8cQm8vxJIZGoi3xR36FWsvlLTbni1r9UaQZpB2P/tuRjxZtVveDuVeqZ2zfZ/M/xrMcpr6D9c7BSWiCs1PnVcDEWX4QwnZfk4FBD5Fl1NFI7b1fYsg0iZSkIU1L+bgt4RfZ8gGkzj9aiUx1f4OuzMu0m5A90tM2M8Q3YyKgNSzk3Ecji7t2o6rN3qkmYJihl/GhS+qTT5x9mT1FTtAQ/dQlQYReeKYA3LMP6f8SFEDGY+JvAjy3eb85BXCT1NEpjIz3oOd+RXh3bDUSKRAqhIRiPCZiGmQ/gnYJofJPYS3Q/WaeTnXA6KdUhLSz6AaNcf2datuut2mAgZdMQMRc4n9gGjn0tqOK/2eHy3rcs84ndOMhJY+pUGDE=#!/aisp-account";
	    when(request.getParameter(anyString())).thenReturn(oAuthUrl);
		when(request.getAttribute(PSD2SecurityConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getAttribute(PSD2SecurityConstants.OAUTH_URL_PARAM)).thenReturn(oAuthUrl);
		when(uiController.retrieveUiParamValue(anyString(),anyString())).thenReturn(oAuthUrl);
		ReflectionTestUtils.setField(SaaSController, "tokenSigningKey", "0123456789abcdef");
		assertNotNull(SaaSController.sessionError(model, request));
	}
	@Test
	public void cancelSetup() throws Exception{
		ModelAndView model = new ModelAndView();
        Map<String, String> map = new HashMap<>();
        map.put(PSD2SecurityConstants.REDIRECT_URI, "/test");
		when(securityRequestAttributes.getParamMap()).thenReturn(map);
		when(securityRequestAttributes.getRequestIdParamName()).thenReturn(ConsentTypeCodeEnum.AccountRequestId.name());
		when(accountRequestAdapter.updateAccountRequestResponse("123", StatusEnum.REJECTED, map)).thenReturn(null);
		ReflectionTestUtils.setField(SaaSController, "tokenSigningKey","0123456789abcdef");
		Map<String, String> paramsMap=new HashMap<>();
		String oAuthUrl="Qxl7JY2bCzj/1O3i7r8U7FIGIeDodZEE7Yf70rLbH2hz8bK8PTtgSATm9ix61kE38Jpt/SS4Z1cXk+1RWeG1RCECy6u7FGgIzIdpco1MaE64vA7Pn3a11vv8ZmJ2y9skkVNcu9DXnL1fKzb2DylzGH9LWmVovRhU10at1NBRE0ERCB8dsT+xfuQ985E2MFrkWPUP/Kbz8iOa4dqY4WBxazQcy06dVUO7BQwmo9oYch8ubVW3ILdfbl7pfM7Z/wszl8cB8SPPTtLavvCfDNXRGUSZoYGGfK40F8TOvF/sYLeOx3E0HFNibWAx6f9JmhAZWO40a82nAYaY3BkTLY3uyQ8nZSQJ1oyW1VRa/U+Cig5lQ1W4BAH0KgYQzi9HkoF1KeP9ZE7WQlerJ2G8en9x5ewvgj11eF5L0zZ41oUNy41pso1lU+p+n0PCs6EngV+FImsx8pBLf6XN0rQ8LhW0z51I7MhKCBuyaeIw4zR6pWRBH9xcYKUgbkCvmLcM4MQRR7G+y0Jd5F/hLfhI60vTMZEntGexTanIZyOdEgPQnOQzTtqwmCmD8cQm8vxJIZGoi3xR36FWsvlLTbni1r9UaQZpB2P/tuRjxZtVveDuVeqZ2zfZ/M/xrMcpr6D9c7BSWiCs1PnVcDEWX4QwnZfk4FBD5Fl1NFI7b1fYsg0iZSkIU1L+bgt4RfZ8gGkzj9aiUx1f4OuzMu0m5A90tM2M8Q3YyKgNSzk3Ecji7t2o6rN3qkmYJihl/GhS+qTT5x9mT1FTtAQ/dQlQYReeKYA3LMP6f8SFEDGY+JvAjy3eb85BXCT1NEpjIz3oOd+RXh3bDUSKRAqhIRiPCZiGmQ/gnYJofJPYS3Q/WaeTnXA6KdUhLSz6AaNcf2datuut2mAgZdMQMRc4n9gGjn0tqOK/2eHy3rcs84ndOMhJY+pUGDE=#!/aisp-account";
		assertNotNull(SaaSController.cancelSetUp(model, oAuthUrl, paramsMap, request, response));
	}
	
	@Test
	public void consentViewWithOAuthUrlParam() throws Exception {
	String encOAuthUrl = SaaSMockData.getMockEncOAuthUrl();
		String encOAuthUrl = "abcd";
	when(uiController.retrieveUiParamValue(anyString(), anyString())).thenReturn("12345");
	mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)
	.param("oAuthUrl", encOAuthUrl)).andExpect(status().isBadRequest())
	.andExpect(view().name("index"));
	}
	
	@Test
	public void consentViewWithNoOAuthUrlParam() throws Exception {
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}
	
	
	
	@Test
	public void signIntest() throws JSONException{
		//ModelAndView mv = mock(ModelAndView.class);
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		Enumeration<String> fsHeader =  new Enumeration<String>() {
			
			@Override
			public String nextElement() {
				return null;
			}
			
			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		SaaSController.signIn("abcd");
	}
}
*/