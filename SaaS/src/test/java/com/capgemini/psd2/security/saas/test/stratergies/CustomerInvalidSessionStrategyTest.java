/*package com.capgemini.psd2.security.saas.test.stratergies;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.security.saas.strategies.CustomInvalidSessionStrategy;

public class CustomerInvalidSessionStrategyTest {

	@InjectMocks
	private MockHttpServletRequest request = new MockHttpServletRequest();

	@Mock
	private MockHttpServletResponse response = new MockHttpServletResponse();

	@Mock
	CustomInvalidSessionStrategy customInvalidSessionStrategy = new CustomInvalidSessionStrategy();

	@Test
	public void onInvalidSessionDetectedTest() {
		try {
			customInvalidSessionStrategy.onInvalidSessionDetected(request, response);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void onInvalidSessionDetectedElseTest() {
		try {
			request.setMethod(RequestMethod.GET.toString());
			customInvalidSessionStrategy.onInvalidSessionDetected(request, response);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void onInvalidSessionDetectedSignInTest() {
		try {
			request.setServletPath("/signin");
			request.setQueryString("h");
			customInvalidSessionStrategy.onInvalidSessionDetected(request, response);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void onInvalidSessionDetectedSignInIfTest() {
		try {
			request.setServletPath("/signin");
			customInvalidSessionStrategy.onInvalidSessionDetected(request, response);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}

}
*/