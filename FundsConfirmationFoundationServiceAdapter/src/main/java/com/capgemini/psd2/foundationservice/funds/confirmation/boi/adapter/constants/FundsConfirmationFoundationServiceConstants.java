/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants;

/**
 * The Class FundsConfirmationFoundationServiceConstants.
 */
public class FundsConfirmationFoundationServiceConstants {

	/** The Constant ACCOUNT_ID. */
	public static final String ACCOUNT_ID = "accountId";
	public static final String CHANNEL_ID = "channelId";
	public static final String ACCOUNT_SUBTYPE = "account_subtype";
	public static final String CURRENT_ACCOUNT = "CurrentAccount";
	public static final String CREDIT_CARD = "CreditCard";
	public static final String SAVINGS = "Savings";
	public static final String PSD2API = "PSD2API";
	

}
