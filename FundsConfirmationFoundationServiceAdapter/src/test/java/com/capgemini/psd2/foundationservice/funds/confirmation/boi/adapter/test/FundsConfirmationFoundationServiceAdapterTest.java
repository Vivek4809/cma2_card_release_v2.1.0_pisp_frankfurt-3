package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.test;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.cisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationData1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.FundsConfirmationFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.client.FundsConfirmationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants.FundsConfirmationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.delegate.FundsConfirmationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFundsForWithdrawalAmount;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Balance;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BalanceAmount;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BalanceType;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Balanceresponse;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class FundsConfirmationFoundationServiceAdapterTest {

	//** The Funds Confirmation Foundation Service Adapter. */
	@InjectMocks
	private FundsConfirmationFoundationServiceAdapter fundsconfirmationFoundationServiceAdapter = new FundsConfirmationFoundationServiceAdapter();

	//** The Funds Confirmation Foundation Service Client. *//*
	@Mock
	private FundsConfirmationFoundationServiceClient fundsconfirmationFoundationServiceClient;

	//** The rest client. *//*
	@Mock
	private RestClientSyncImpl restClient;

	//** The Funds Confirmation Foundation Service Delegate. *//*
	@Mock
	private FundsConfirmationFoundationServiceDelegate fundsconfirmationFoundationServiceDelegate;
	
	
	
	OBFundsConfirmation1 obFundsConfirmation1= new OBFundsConfirmation1();

	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/*
	  Context loads.
	*/	
	@Test
	public void contextLoads() {
	}

	/*	
	Test Funds Confirmation FS
	*/
	
	@Test
	public void testFundsConfirmationFS1() {
		Balanceresponse balanceResponse = new Balanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balance= new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
	//	balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
	//	balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balance.setBalanceAmount(balanceAmount);
		balance.setBalanceCurrency(balanceCurrency);
		balance.setBalanceDate(balanceDate);
		balance.setBalanceType(BalanceType.AVAILABLE_LIMIT);
		
		balancesList.add(balance);
		balanceResponse.setBalancesList(balancesList);
		
		
		AccountDetails accDet = new AccountDetails();
		OBCashAccount3 account= new OBCashAccount3();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		account.setName("credit");
		account.setSecondaryIdentification("12345678");
		account.setIdentification("XXXXXXXXXXXX2000");
		accDet.setAccount(account);
		
		OBActiveOrHistoricCurrencyAndAmount instructedAmount = new OBActiveOrHistoricCurrencyAndAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("EUR");
		
		OBFundsConfirmationData1 data = new OBFundsConfirmationData1();
		data.setConsentId("12345");
		data.setInstructedAmount(instructedAmount);
		data.setReference("abc");
		
		obFundsConfirmation1.setData(data);

		Mockito.when(fundsconfirmationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/creditcards/12345678/acct1234/balance");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForCreditCardBalance(any(), any(), any()))
				.thenReturn(balanceResponse);
		Mockito.when(fundsconfirmationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any(),any()))
				.thenReturn(new OBFundsConfirmationResponse1());

		Map<String, String> params = new HashMap<String, String>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CREDITCARD");
		fundsconfirmationFoundationServiceAdapter.getFundsData(accDet,obFundsConfirmation1 , params);
	}
	
	@Test
	public void testFundsConfirmationFS2() {
		AvailableFunds availableFunds = new AvailableFunds();
		
		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("Euro");
		accountCurrency.setIsoAlphaCode("EUR");
		availableFunds.setAccountCurrency(accountCurrency);
		
		availableFunds.setAccountNumber("acct1234");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("nsc1234");
		
		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(801.77d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);

		
		AccountDetails accDet = new AccountDetails();
		OBCashAccount3 account= new OBCashAccount3();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		account.setName("credit");
		account.setSecondaryIdentification("12345678");
		account.setIdentification("XXXXXXXXXXXX2000");
		accDet.setAccount(account);
		
		OBActiveOrHistoricCurrencyAndAmount instructedAmount = new OBActiveOrHistoricCurrencyAndAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("EUR");
		
		OBFundsConfirmationData1 data = new OBFundsConfirmationData1();
		data.setConsentId("12345");
		data.setInstructedAmount(instructedAmount);
		data.setReference("abc");
		
		obFundsConfirmation1.setData(data);

		Mockito.when(fundsconfirmationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234/available-funds");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(any(), any(), any()))
				.thenReturn(availableFunds);
		Mockito.when(fundsconfirmationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any(),any()))
				.thenReturn(new OBFundsConfirmationResponse1());

		Map<String, String> params = new HashMap<String, String>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");
		fundsconfirmationFoundationServiceAdapter.getFundsData(accDet,obFundsConfirmation1 , params);
	}
	
@Test
	public void testFundsConfirmationFS3() {
	AvailableFunds availableFunds = new AvailableFunds();
	
	Currency accountCurrency = new Currency();
	accountCurrency.setCurrencyName("Euro");
	accountCurrency.setIsoAlphaCode("EUR");
	availableFunds.setAccountCurrency(accountCurrency);
	
	availableFunds.setAccountNumber("acct1234");
	OffsetDateTime date = OffsetDateTime.now();
	availableFunds.setAvailableFundsCheckDatetime(date);
	availableFunds.setParentNationalSortCodeNSCNumber("nsc1234");
	
	AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
	availableFundsForWithdrawalAmount.setTransactionCurrency(801.77d);
	availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);

	
	AccountDetails accDet = new AccountDetails();
	OBCashAccount3 account= new OBCashAccount3();
	accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
	accDet.setAccountId("12345");
	accDet.setAccountNSC("nsc1234");
	accDet.setAccountNumber("acct1234");
	account.setName("credit");
	account.setSecondaryIdentification("12345678");
	account.setIdentification("XXXXXXXXXXXX2000");
	accDet.setAccount(account);
	
	OBActiveOrHistoricCurrencyAndAmount instructedAmount = new OBActiveOrHistoricCurrencyAndAmount();
	instructedAmount.setAmount("500");
	instructedAmount.setCurrency("EUR");
	
	OBFundsConfirmationData1 data = new OBFundsConfirmationData1();
	data.setConsentId("12345");
	data.setInstructedAmount(instructedAmount);
	data.setReference("abc");
	
	obFundsConfirmation1.setData(data);

	Mockito.when(fundsconfirmationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
			.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234/available-funds");
	Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(any(), any(), any()))
			.thenReturn(availableFunds);
	Mockito.when(fundsconfirmationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any(),any()))
			.thenReturn(new OBFundsConfirmationResponse1());

	Map<String, String> params = new HashMap<String, String>();
	params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");
	fundsconfirmationFoundationServiceAdapter.getFundsData(accDet,obFundsConfirmation1 , params);
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void testcreateFundsConfirmationRequestHeaders() {
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("x-api-transaction-id", "header transactionId");
		httpHeaders.add("x-api-correlation-id", "header correlationMule");
		httpHeaders.add("x-api-source-system", "header sourceSystem");
		httpHeaders.add("x-api-source-user", "header sourceUser");
		httpHeaders.add("x-api-channel-Code", "header channelId");
		httpHeaders.add("x-api-party-source-id-number", "header partySourceId");

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc1234/acct1234/available-funds");
		Mockito.when(
				fundsconfirmationFoundationServiceDelegate.createFundsConfirmationRequestHeaders(anyObject(), anyObject(), anyMap()))
				.thenReturn(httpHeaders);
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channel123");
		httpHeaders = fundsconfirmationFoundationServiceDelegate.createFundsConfirmationRequestHeaders(requestInfo, accDet,
				params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionFilteredAccounts() {
/*		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");*/
		AccountDetails acc = new AccountDetails();
		Map<String,String> params = new HashMap<>();
		acc.setAccountId("12345");
		acc.setAccountNSC("nsc1234");
		acc.setAccountNumber("acct1234");
		fundsconfirmationFoundationServiceAdapter.getFundsData(acc,obFundsConfirmation1, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtils() {
		AccountDetails acc = new AccountDetails();
		Map<String,String> params = new HashMap<>();
		acc.setAccountId("12345");
		acc.setAccountNSC("nsc1234");
		acc.setAccountNumber("acct1234");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.getFundsData(acc,obFundsConfirmation1, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullParams() {
		AccountDetails acc = new AccountDetails();
		Map<String,String> params = new HashMap<>();
		acc.setAccountId("12345");
		acc.setAccountNSC("nsc1234");
		acc.setAccountNumber("acct1234");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.getFundsData(acc,obFundsConfirmation1, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil() {
		AccountDetails acc = new AccountDetails();
		Map<String,String> params = new HashMap<>();
		acc.setAccountId(null);
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.getFundsData(acc,obFundsConfirmation1, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullAccountDetails() {
		AccountDetails accountDet = null;
		Map<String, String> params = new HashMap<>();
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.getFundsData(accountDet,obFundsConfirmation1, params);
	}

/*	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForCreditCardBalance(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		Map<String, String> params = new HashMap<String, String>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		fundsconfirmationFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountMappingElse() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(null);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForCreditCardBalance(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.retrieveAccountBalance(accountMapping, new HashMap<String, String>());
	}*/

}