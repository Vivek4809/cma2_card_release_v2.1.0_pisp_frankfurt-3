package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * AssessmentRule
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class AssessmentRule   {
  @JsonProperty("assessmentRuleDescription")
  private String assessmentRuleDescription = null;

  @JsonProperty("assessmentRuleOutcomeTypeCode")
  private String assessmentRuleOutcomeTypeCode = null;

  @JsonProperty("assessmentRuleText")
  private String assessmentRuleText = null;

  @JsonProperty("assessmentRuleTypeCode")
  private String assessmentRuleTypeCode = null;

  public AssessmentRule assessmentRuleDescription(String assessmentRuleDescription) {
    this.assessmentRuleDescription = assessmentRuleDescription;
    return this;
  }

  /**
   * Get assessmentRuleDescription
   * @return assessmentRuleDescription
  **/
  @ApiModelProperty(value = "")


  public String getAssessmentRuleDescription() {
    return assessmentRuleDescription;
  }

  public void setAssessmentRuleDescription(String assessmentRuleDescription) {
    this.assessmentRuleDescription = assessmentRuleDescription;
  }

  public AssessmentRule assessmentRuleOutcomeTypeCode(String assessmentRuleOutcomeTypeCode) {
    this.assessmentRuleOutcomeTypeCode = assessmentRuleOutcomeTypeCode;
    return this;
  }

  /**
   * Get assessmentRuleOutcomeTypeCode
   * @return assessmentRuleOutcomeTypeCode
  **/
  @ApiModelProperty(value = "")


  public String getAssessmentRuleOutcomeTypeCode() {
    return assessmentRuleOutcomeTypeCode;
  }

  public void setAssessmentRuleOutcomeTypeCode(String assessmentRuleOutcomeTypeCode) {
    this.assessmentRuleOutcomeTypeCode = assessmentRuleOutcomeTypeCode;
  }

  public AssessmentRule assessmentRuleText(String assessmentRuleText) {
    this.assessmentRuleText = assessmentRuleText;
    return this;
  }

  /**
   * Get assessmentRuleText
   * @return assessmentRuleText
  **/
  @ApiModelProperty(value = "")


  public String getAssessmentRuleText() {
    return assessmentRuleText;
  }

  public void setAssessmentRuleText(String assessmentRuleText) {
    this.assessmentRuleText = assessmentRuleText;
  }

  public AssessmentRule assessmentRuleTypeCode(String assessmentRuleTypeCode) {
    this.assessmentRuleTypeCode = assessmentRuleTypeCode;
    return this;
  }

  /**
   * Get assessmentRuleTypeCode
   * @return assessmentRuleTypeCode
  **/
  @ApiModelProperty(value = "")


  public String getAssessmentRuleTypeCode() {
    return assessmentRuleTypeCode;
  }

  public void setAssessmentRuleTypeCode(String assessmentRuleTypeCode) {
    this.assessmentRuleTypeCode = assessmentRuleTypeCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AssessmentRule assessmentRule = (AssessmentRule) o;
    return Objects.equals(this.assessmentRuleDescription, assessmentRule.assessmentRuleDescription) &&
        Objects.equals(this.assessmentRuleOutcomeTypeCode, assessmentRule.assessmentRuleOutcomeTypeCode) &&
        Objects.equals(this.assessmentRuleText, assessmentRule.assessmentRuleText) &&
        Objects.equals(this.assessmentRuleTypeCode, assessmentRule.assessmentRuleTypeCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(assessmentRuleDescription, assessmentRuleOutcomeTypeCode, assessmentRuleText, assessmentRuleTypeCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AssessmentRule {\n");
    
    sb.append("    assessmentRuleDescription: ").append(toIndentedString(assessmentRuleDescription)).append("\n");
    sb.append("    assessmentRuleOutcomeTypeCode: ").append(toIndentedString(assessmentRuleOutcomeTypeCode)).append("\n");
    sb.append("    assessmentRuleText: ").append(toIndentedString(assessmentRuleText)).append("\n");
    sb.append("    assessmentRuleTypeCode: ").append(toIndentedString(assessmentRuleTypeCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

