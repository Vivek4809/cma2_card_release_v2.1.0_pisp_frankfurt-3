package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Organisation object
 */
@ApiModel(description = "Organisation object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class Organisation4   {
  @JsonProperty("organisationInformation")
  private Organisation organisationInformation = null;

  @JsonProperty("contactName")
  private String contactName = null;

  public Organisation4 organisationInformation(Organisation organisationInformation) {
    this.organisationInformation = organisationInformation;
    return this;
  }

  /**
   * Get organisationInformation
   * @return organisationInformation
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public Organisation getOrganisationInformation() {
    return organisationInformation;
  }

  public void setOrganisationInformation(Organisation organisationInformation) {
    this.organisationInformation = organisationInformation;
  }

  public Organisation4 contactName(String contactName) {
    this.contactName = contactName;
    return this;
  }

  /**
   * Get contactName
   * @return contactName
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getContactName() {
    return contactName;
  }

  public void setContactName(String contactName) {
    this.contactName = contactName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Organisation4 organisation4 = (Organisation4) o;
    return Objects.equals(this.organisationInformation, organisation4.organisationInformation) &&
        Objects.equals(this.contactName, organisation4.contactName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(organisationInformation, contactName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Organisation4 {\n");
    
    sb.append("    organisationInformation: ").append(toIndentedString(organisationInformation)).append("\n");
    sb.append("    contactName: ").append(toIndentedString(contactName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

