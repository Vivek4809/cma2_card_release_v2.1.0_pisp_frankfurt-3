package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * AssessmentModel
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class AssessmentModel   {
  @JsonProperty("assessmentModelApprovalStatusCode")
  private AssessmentModelApprovalStatusCode assessmentModelApprovalStatusCode = null;

  @JsonProperty("assessmentModelName")
  private String assessmentModelName = null;

  @JsonProperty("assessmentModelValidationResultCode")
  private AssessmentModelValidationResultCode assessmentModelValidationResultCode = null;

  public AssessmentModel assessmentModelApprovalStatusCode(AssessmentModelApprovalStatusCode assessmentModelApprovalStatusCode) {
    this.assessmentModelApprovalStatusCode = assessmentModelApprovalStatusCode;
    return this;
  }

  /**
   * Get assessmentModelApprovalStatusCode
   * @return assessmentModelApprovalStatusCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AssessmentModelApprovalStatusCode getAssessmentModelApprovalStatusCode() {
    return assessmentModelApprovalStatusCode;
  }

  public void setAssessmentModelApprovalStatusCode(AssessmentModelApprovalStatusCode assessmentModelApprovalStatusCode) {
    this.assessmentModelApprovalStatusCode = assessmentModelApprovalStatusCode;
  }

  public AssessmentModel assessmentModelName(String assessmentModelName) {
    this.assessmentModelName = assessmentModelName;
    return this;
  }

  /**
   * Get assessmentModelName
   * @return assessmentModelName
  **/
  @ApiModelProperty(value = "")


  public String getAssessmentModelName() {
    return assessmentModelName;
  }

  public void setAssessmentModelName(String assessmentModelName) {
    this.assessmentModelName = assessmentModelName;
  }

  public AssessmentModel assessmentModelValidationResultCode(AssessmentModelValidationResultCode assessmentModelValidationResultCode) {
    this.assessmentModelValidationResultCode = assessmentModelValidationResultCode;
    return this;
  }

  /**
   * Get assessmentModelValidationResultCode
   * @return assessmentModelValidationResultCode
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AssessmentModelValidationResultCode getAssessmentModelValidationResultCode() {
    return assessmentModelValidationResultCode;
  }

  public void setAssessmentModelValidationResultCode(AssessmentModelValidationResultCode assessmentModelValidationResultCode) {
    this.assessmentModelValidationResultCode = assessmentModelValidationResultCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AssessmentModel assessmentModel = (AssessmentModel) o;
    return Objects.equals(this.assessmentModelApprovalStatusCode, assessmentModel.assessmentModelApprovalStatusCode) &&
        Objects.equals(this.assessmentModelName, assessmentModel.assessmentModelName) &&
        Objects.equals(this.assessmentModelValidationResultCode, assessmentModel.assessmentModelValidationResultCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(assessmentModelApprovalStatusCode, assessmentModelName, assessmentModelValidationResultCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AssessmentModel {\n");
    
    sb.append("    assessmentModelApprovalStatusCode: ").append(toIndentedString(assessmentModelApprovalStatusCode)).append("\n");
    sb.append("    assessmentModelName: ").append(toIndentedString(assessmentModelName)).append("\n");
    sb.append("    assessmentModelValidationResultCode: ").append(toIndentedString(assessmentModelValidationResultCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

