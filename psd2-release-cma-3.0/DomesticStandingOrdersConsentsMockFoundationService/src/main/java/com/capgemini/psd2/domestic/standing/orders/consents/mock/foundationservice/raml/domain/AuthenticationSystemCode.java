package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Gets or Sets authenticationSystemCode
 */
public enum AuthenticationSystemCode {
  
  BOL("BOL"),
  
  B365("B365");

  private String value;

  AuthenticationSystemCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static AuthenticationSystemCode fromValue(String text) {
    for (AuthenticationSystemCode b : AuthenticationSystemCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

