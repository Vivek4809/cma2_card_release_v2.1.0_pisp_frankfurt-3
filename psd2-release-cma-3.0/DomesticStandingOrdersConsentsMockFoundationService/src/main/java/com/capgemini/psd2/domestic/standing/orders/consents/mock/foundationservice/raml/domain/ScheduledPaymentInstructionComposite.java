package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Scheduled Payment Instruction-Proposal composite object
 */
@ApiModel(description = "Scheduled Payment Instruction-Proposal composite object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class ScheduledPaymentInstructionComposite   {
  @JsonProperty("paymentInstructionProposal")
  private ScheduledPaymentInstructionProposal paymentInstructionProposal = null;

  @JsonProperty("paymentInstruction")
  private ScheduledPaymentInstructionProposal2 paymentInstruction = null;

  public ScheduledPaymentInstructionComposite paymentInstructionProposal(ScheduledPaymentInstructionProposal paymentInstructionProposal) {
    this.paymentInstructionProposal = paymentInstructionProposal;
    return this;
  }

  /**
   * Get paymentInstructionProposal
   * @return paymentInstructionProposal
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ScheduledPaymentInstructionProposal getPaymentInstructionProposal() {
    return paymentInstructionProposal;
  }

  public void setPaymentInstructionProposal(ScheduledPaymentInstructionProposal paymentInstructionProposal) {
    this.paymentInstructionProposal = paymentInstructionProposal;
  }

  public ScheduledPaymentInstructionComposite paymentInstruction(ScheduledPaymentInstructionProposal2 paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
    return this;
  }

  /**
   * Get paymentInstruction
   * @return paymentInstruction
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public ScheduledPaymentInstructionProposal2 getPaymentInstruction() {
    return paymentInstruction;
  }

  public void setPaymentInstruction(ScheduledPaymentInstructionProposal2 paymentInstruction) {
    this.paymentInstruction = paymentInstruction;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ScheduledPaymentInstructionComposite scheduledPaymentInstructionComposite = (ScheduledPaymentInstructionComposite) o;
    return Objects.equals(this.paymentInstructionProposal, scheduledPaymentInstructionComposite.paymentInstructionProposal) &&
        Objects.equals(this.paymentInstruction, scheduledPaymentInstructionComposite.paymentInstruction);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionProposal, paymentInstruction);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ScheduledPaymentInstructionComposite {\n");
    
    sb.append("    paymentInstructionProposal: ").append(toIndentedString(paymentInstructionProposal)).append("\n");
    sb.append("    paymentInstruction: ").append(toIndentedString(paymentInstruction)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

