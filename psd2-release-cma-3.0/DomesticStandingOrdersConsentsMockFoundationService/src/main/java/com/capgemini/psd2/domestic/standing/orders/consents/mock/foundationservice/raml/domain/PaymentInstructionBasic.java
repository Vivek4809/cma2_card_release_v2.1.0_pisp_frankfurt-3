package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Payment Instruction Basic object
 */
@ApiModel(description = "Payment Instruction Basic object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentInstructionBasic   {
  @JsonProperty("paymentInstructionNumber")
  private String paymentInstructionNumber = null;

  @JsonProperty("paymentInstructionStatusCode")
  private String paymentInstructionStatusCode = null;

  public PaymentInstructionBasic paymentInstructionNumber(String paymentInstructionNumber) {
    this.paymentInstructionNumber = paymentInstructionNumber;
    return this;
  }

  /**
   * The identifying number of the Payment Instruction in the Banking System
   * @return paymentInstructionNumber
  **/
  @ApiModelProperty(required = true, value = "The identifying number of the Payment Instruction in the Banking System")
  @NotNull


  public String getPaymentInstructionNumber() {
    return paymentInstructionNumber;
  }

  public void setPaymentInstructionNumber(String paymentInstructionNumber) {
    this.paymentInstructionNumber = paymentInstructionNumber;
  }

  public PaymentInstructionBasic paymentInstructionStatusCode(String paymentInstructionStatusCode) {
    this.paymentInstructionStatusCode = paymentInstructionStatusCode;
    return this;
  }

  /**
   * Signals if the Payment Instruction is on hold or similar state
   * @return paymentInstructionStatusCode
  **/
  @ApiModelProperty(value = "Signals if the Payment Instruction is on hold or similar state")


  public String getPaymentInstructionStatusCode() {
    return paymentInstructionStatusCode;
  }

  public void setPaymentInstructionStatusCode(String paymentInstructionStatusCode) {
    this.paymentInstructionStatusCode = paymentInstructionStatusCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstructionBasic paymentInstructionBasic = (PaymentInstructionBasic) o;
    return Objects.equals(this.paymentInstructionNumber, paymentInstructionBasic.paymentInstructionNumber) &&
        Objects.equals(this.paymentInstructionStatusCode, paymentInstructionBasic.paymentInstructionStatusCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(paymentInstructionNumber, paymentInstructionStatusCode);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstructionBasic {\n");
    
    sb.append("    paymentInstructionNumber: ").append(toIndentedString(paymentInstructionNumber)).append("\n");
    sb.append("    paymentInstructionStatusCode: ").append(toIndentedString(paymentInstructionStatusCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

