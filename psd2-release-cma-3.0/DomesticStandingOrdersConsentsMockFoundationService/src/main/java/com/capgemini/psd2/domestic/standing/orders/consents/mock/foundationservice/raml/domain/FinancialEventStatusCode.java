package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Classifies a Financial Event in terms of its progression through a life cycle. Not all statuses would necessarily be applicable to all Financial Events.E.g. Planned/Pending; Executed; Booked; Cleared
 */
public enum FinancialEventStatusCode {
  
  BOOKED("Booked"),
  
  PENDING("Pending");

  private String value;

  FinancialEventStatusCode(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static FinancialEventStatusCode fromValue(String text) {
    for (FinancialEventStatusCode b : FinancialEventStatusCode.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}

