package com.capgemini.psd2.domestic.payments.consents.mock.foundationservice.domain;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * This is the counterparty (payee) account for a payment instruction
 */
@ApiModel(description = "This is the counterparty (payee) account for a payment instruction")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-01-16T16:32:31.691+05:30")

public class PaymentInstructionCounterpartyAccountBasic   {
  @JsonProperty("counterpartyNationalSortCodeNSCNumber")
  private String counterpartyNationalSortCodeNSCNumber = null;

  @JsonProperty("counterpartyAccountNumber")
  private String counterpartyAccountNumber = null;

  @JsonProperty("counterpartySWIFTBankIdentifierCodeBIC")
  private String counterpartySWIFTBankIdentifierCodeBIC = null;

  @JsonProperty("counterpartyInternationalBankAccountNumberIBAN")
  private String counterpartyInternationalBankAccountNumberIBAN = null;

  @JsonProperty("counterpartyABARTNRoutingTransitNumber")
  private String counterpartyABARTNRoutingTransitNumber = null;

  @JsonProperty("counterpartyName")
  private String counterpartyName = null;

  @JsonProperty("counterpartyReferenceText")
  private String counterpartyReferenceText = null;

  @JsonProperty("instructingPartyNarrativeText")
  private String instructingPartyNarrativeText = null;

  public PaymentInstructionCounterpartyAccountBasic counterpartyNationalSortCodeNSCNumber(String counterpartyNationalSortCodeNSCNumber) {
    this.counterpartyNationalSortCodeNSCNumber = counterpartyNationalSortCodeNSCNumber;
    return this;
  }

  /**
   * The National Sort Code (NSC) of the bank account of the counterparty to the financial Event or, if not applicable, as in the case of a non-UK/Irish bank, then the Swift Code(BIC) or any other unique bank identifier.
   * @return counterpartyNationalSortCodeNSCNumber
  **/
  @ApiModelProperty(value = "The National Sort Code (NSC) of the bank account of the counterparty to the financial Event or, if not applicable, as in the case of a non-UK/Irish bank, then the Swift Code(BIC) or any other unique bank identifier.")


  public String getCounterpartyNationalSortCodeNSCNumber() {
    return counterpartyNationalSortCodeNSCNumber;
  }

  public void setCounterpartyNationalSortCodeNSCNumber(String counterpartyNationalSortCodeNSCNumber) {
    this.counterpartyNationalSortCodeNSCNumber = counterpartyNationalSortCodeNSCNumber;
  }

  public PaymentInstructionCounterpartyAccountBasic counterpartyAccountNumber(String counterpartyAccountNumber) {
    this.counterpartyAccountNumber = counterpartyAccountNumber;
    return this;
  }

  /**
   * The identifier of the bank account of the counterparty to the financial Event.
   * @return counterpartyAccountNumber
  **/
  @ApiModelProperty(value = "The identifier of the bank account of the counterparty to the financial Event.")

@Pattern(regexp="[A-Za-z0-9/?:().,'+ -]{0,34}") 
  public String getCounterpartyAccountNumber() {
    return counterpartyAccountNumber;
  }

  public void setCounterpartyAccountNumber(String counterpartyAccountNumber) {
    this.counterpartyAccountNumber = counterpartyAccountNumber;
  }

  public PaymentInstructionCounterpartyAccountBasic counterpartySWIFTBankIdentifierCodeBIC(String counterpartySWIFTBankIdentifierCodeBIC) {
    this.counterpartySWIFTBankIdentifierCodeBIC = counterpartySWIFTBankIdentifierCodeBIC;
    return this;
  }

  /**
   * The Bank Identifier Code (BIC) for the counterparty account
   * @return counterpartySWIFTBankIdentifierCodeBIC
  **/
  @ApiModelProperty(value = "The Bank Identifier Code (BIC) for the counterparty account")


  public String getCounterpartySWIFTBankIdentifierCodeBIC() {
    return counterpartySWIFTBankIdentifierCodeBIC;
  }

  public void setCounterpartySWIFTBankIdentifierCodeBIC(String counterpartySWIFTBankIdentifierCodeBIC) {
    this.counterpartySWIFTBankIdentifierCodeBIC = counterpartySWIFTBankIdentifierCodeBIC;
  }

  public PaymentInstructionCounterpartyAccountBasic counterpartyInternationalBankAccountNumberIBAN(String counterpartyInternationalBankAccountNumberIBAN) {
    this.counterpartyInternationalBankAccountNumberIBAN = counterpartyInternationalBankAccountNumberIBAN;
    return this;
  }

  /**
   * The International Bank Account Number (IBAN) of the Account.
   * @return counterpartyInternationalBankAccountNumberIBAN
  **/
  @ApiModelProperty(value = "The International Bank Account Number (IBAN) of the Account.")


  public String getCounterpartyInternationalBankAccountNumberIBAN() {
    return counterpartyInternationalBankAccountNumberIBAN;
  }

  public void setCounterpartyInternationalBankAccountNumberIBAN(String counterpartyInternationalBankAccountNumberIBAN) {
    this.counterpartyInternationalBankAccountNumberIBAN = counterpartyInternationalBankAccountNumberIBAN;
  }

  public PaymentInstructionCounterpartyAccountBasic counterpartyABARTNRoutingTransitNumber(String counterpartyABARTNRoutingTransitNumber) {
    this.counterpartyABARTNRoutingTransitNumber = counterpartyABARTNRoutingTransitNumber;
    return this;
  }

  /**
   * Bank Identifier for US Banks
   * @return counterpartyABARTNRoutingTransitNumber
  **/
  @ApiModelProperty(value = "Bank Identifier for US Banks")


  public String getCounterpartyABARTNRoutingTransitNumber() {
    return counterpartyABARTNRoutingTransitNumber;
  }

  public void setCounterpartyABARTNRoutingTransitNumber(String counterpartyABARTNRoutingTransitNumber) {
    this.counterpartyABARTNRoutingTransitNumber = counterpartyABARTNRoutingTransitNumber;
  }

  public PaymentInstructionCounterpartyAccountBasic counterpartyName(String counterpartyName) {
    this.counterpartyName = counterpartyName;
    return this;
  }

  /**
   * Name of the counterparty
   * @return counterpartyName
  **/
  @ApiModelProperty(value = "Name of the counterparty")


  public String getCounterpartyName() {
    return counterpartyName;
  }

  public void setCounterpartyName(String counterpartyName) {
    this.counterpartyName = counterpartyName;
  }

  public PaymentInstructionCounterpartyAccountBasic counterpartyReferenceText(String counterpartyReferenceText) {
    this.counterpartyReferenceText = counterpartyReferenceText;
    return this;
  }

  /**
   * Payment narrative for the counterparty
   * @return counterpartyReferenceText
  **/
  @ApiModelProperty(value = "Payment narrative for the counterparty")


  public String getCounterpartyReferenceText() {
    return counterpartyReferenceText;
  }

  public void setCounterpartyReferenceText(String counterpartyReferenceText) {
    this.counterpartyReferenceText = counterpartyReferenceText;
  }

  public PaymentInstructionCounterpartyAccountBasic instructingPartyNarrativeText(String instructingPartyNarrativeText) {
    this.instructingPartyNarrativeText = instructingPartyNarrativeText;
    return this;
  }

  /**
   * Free text provided by the Instructing Party of the relevant Payment Instruction which is to be included on the resulting Transaction and which then can be viewed by the Counterparty
   * @return instructingPartyNarrativeText
  **/
  @ApiModelProperty(value = "Free text provided by the Instructing Party of the relevant Payment Instruction which is to be included on the resulting Transaction and which then can be viewed by the Counterparty")


  public String getInstructingPartyNarrativeText() {
    return instructingPartyNarrativeText;
  }

  public void setInstructingPartyNarrativeText(String instructingPartyNarrativeText) {
    this.instructingPartyNarrativeText = instructingPartyNarrativeText;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentInstructionCounterpartyAccountBasic paymentInstructionCounterpartyAccountBasic = (PaymentInstructionCounterpartyAccountBasic) o;
    return Objects.equals(this.counterpartyNationalSortCodeNSCNumber, paymentInstructionCounterpartyAccountBasic.counterpartyNationalSortCodeNSCNumber) &&
        Objects.equals(this.counterpartyAccountNumber, paymentInstructionCounterpartyAccountBasic.counterpartyAccountNumber) &&
        Objects.equals(this.counterpartySWIFTBankIdentifierCodeBIC, paymentInstructionCounterpartyAccountBasic.counterpartySWIFTBankIdentifierCodeBIC) &&
        Objects.equals(this.counterpartyInternationalBankAccountNumberIBAN, paymentInstructionCounterpartyAccountBasic.counterpartyInternationalBankAccountNumberIBAN) &&
        Objects.equals(this.counterpartyABARTNRoutingTransitNumber, paymentInstructionCounterpartyAccountBasic.counterpartyABARTNRoutingTransitNumber) &&
        Objects.equals(this.counterpartyName, paymentInstructionCounterpartyAccountBasic.counterpartyName) &&
        Objects.equals(this.counterpartyReferenceText, paymentInstructionCounterpartyAccountBasic.counterpartyReferenceText) &&
        Objects.equals(this.instructingPartyNarrativeText, paymentInstructionCounterpartyAccountBasic.instructingPartyNarrativeText);
  }

  @Override
  public int hashCode() {
    return Objects.hash(counterpartyNationalSortCodeNSCNumber, counterpartyAccountNumber, counterpartySWIFTBankIdentifierCodeBIC, counterpartyInternationalBankAccountNumberIBAN, counterpartyABARTNRoutingTransitNumber, counterpartyName, counterpartyReferenceText, instructingPartyNarrativeText);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentInstructionCounterpartyAccountBasic {\n");
    
    sb.append("    counterpartyNationalSortCodeNSCNumber: ").append(toIndentedString(counterpartyNationalSortCodeNSCNumber)).append("\n");
    sb.append("    counterpartyAccountNumber: ").append(toIndentedString(counterpartyAccountNumber)).append("\n");
    sb.append("    counterpartySWIFTBankIdentifierCodeBIC: ").append(toIndentedString(counterpartySWIFTBankIdentifierCodeBIC)).append("\n");
    sb.append("    counterpartyInternationalBankAccountNumberIBAN: ").append(toIndentedString(counterpartyInternationalBankAccountNumberIBAN)).append("\n");
    sb.append("    counterpartyABARTNRoutingTransitNumber: ").append(toIndentedString(counterpartyABARTNRoutingTransitNumber)).append("\n");
    sb.append("    counterpartyName: ").append(toIndentedString(counterpartyName)).append("\n");
    sb.append("    counterpartyReferenceText: ").append(toIndentedString(counterpartyReferenceText)).append("\n");
    sb.append("    instructingPartyNarrativeText: ").append(toIndentedString(instructingPartyNarrativeText)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

