package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.test;

import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.AccountSchedulePaymentsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.client.AccountSchedulePaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.delegate.AccountSchedulePaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PartiesOnceOffPaymentScheduleScheduleItemsresponse;
import com.capgemini.psd2.logger.PSD2Constants;

public class AccountSchedulePaymentsFoundationServiceAdapterTest {
	
	@InjectMocks
	private AccountSchedulePaymentsFoundationServiceAdapter accountSchedulePaymentsFoundationServiceAdapter;
	/** The account information foundation service delegate. */
	@Mock
	private AccountSchedulePaymentsFoundationServiceDelegate accountSchedulePaymentsFoundationServiceDelegate;
	
	/** The account information foundation service client. */
	@Mock
	private AccountSchedulePaymentsFoundationServiceClient accountSchedulePaymentsFoundationServiceClient;
	
	/** The rest client. */
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	@Test(expected=AdapterException.class)
	public void retrieveAccountSchedulePayments4(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accountMapping.setAccountDetails(accList);
		//accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOnceOffPaymentScheduleScheduleItemsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	@Test
	public void retrieveAccountSchedulePayments(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOnceOffPaymentScheduleScheduleItemsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	@Test
	public void retrieveAccountSchedulePayments1(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		//params.put("cmaVersion", "2.0");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		
		accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(),any())).thenReturn(new PartiesOnceOffPaymentScheduleScheduleItemsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	@Test(expected=AdapterException.class)
	public void retrieveAccountSchedulePayments5(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(null);
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		//params.put("accountId", null);
		//params.put("account_subtype",null);
		//params.put("cmaVersion", null);
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		
		accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(),any())).thenReturn(new PartiesOnceOffPaymentScheduleScheduleItemsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	@Test(expected=AdapterException.class)
	public void retrieveAccountSchedulePayments6(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		/*Map<String, String> params = new HashMap<>();
		params.put("accountId", null);
		params.put("account_subtype",null);
		//params.put("cmaVersion", null);
*/		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		
		accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOnceOffPaymentScheduleScheduleItemsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, null);
	}
	@Test
	public void retrieveAccountSchedulePayments2(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		//params.put("cmaVersion", "2.0");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		//accDet.setAccountSubType(AccountSubTypeEnum.SAVINGS);
		
		//accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOnceOffPaymentScheduleScheduleItemsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
	@Test
	public void retrieveAccountSchedulePayments3(){
		PlatformAccountSchedulePaymentsResponse response = new PlatformAccountSchedulePaymentsResponse();
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("1234");
		List<AccountDetails> accList= new ArrayList();
		AccountDetails accDet = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("account_subtype","account_subtype");
		params.put(PSD2Constants.CMAVERSION, "2.0");
		accountMapping.setCorrelationId("1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		
		//accDet.setAccountType("CurrentAccount");
		accountMapping.setAccountDetails(accList);
		accList.add(0, accDet);
		accountMapping.setAccountDetails(accList);
		Mockito.when(accountSchedulePaymentsFoundationServiceClient.restTransportForScheduleItems(any(), any(), any(), any())).thenReturn(new PartiesOnceOffPaymentScheduleScheduleItemsresponse());
		Mockito.when(accountSchedulePaymentsFoundationServiceDelegate.createRequestHeaders(any(), any(), any())).thenReturn(null);
		accountSchedulePaymentsFoundationServiceAdapter.retrieveAccountSchedulePayments(accountMapping, params);
	}
}
