package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.CustomerAccountProfileFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.client.CustomerAccountProfileFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate.CustomerAccountProfileFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.ChannelProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountProfileFoundationServiceAdapterApplicationTests {

	@InjectMocks
	private CustomerAccountProfileFoundationServiceAdapter customerAccProFoundServiceAdapter;

	@Mock
	private CustomerAccountProfileFoundationServiceDelegate cusAccProFoundServiceDelegate;

	@Mock
	private RestClientSyncImpl restClient;

	@Mock
	private PSD2Validator validator;
	
    @Mock
    private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;
    
    @Mock
    CustomerAccountProfileFoundationServiceTransformer customerAccProFoundServiceTransformer;
    
    @Mock
    CustomerAccountProfileFoundationServiceClient customerAccountProfileFoundationServiceClient;
    
    @Mock
	private AdapterFilterUtility adapterFilterUtility;
	
	
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test()
	public void testCustomerAccountInfoList() {

		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt account1 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		account1.setAccountName("test");
		DigitalUserProfile digitalUserProfile= new DigitalUserProfile();
		//digitalUserProfile.getAccount().add(account1);
		
		/** Mock all methods to test retrieve Customer Account List */
        Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(digitalUserProfile);
        OBReadAccount2 accGETRes=new OBReadAccount2();
		Mockito.when(cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(accGETRes);
		OBReadAccount2 accGETRes1 = customerAccProFoundServiceAdapter
				.retrieveCustomerAccountList("test", new HashMap<String, String>());
	}

	@Test(expected = AdapterException.class)
	public void testCustomerAccountInfoListParamsNull() {
	/** Test account list */
	/** Creating First Account */
		Accnt accnt = new Accnt();
		accnt.setAccountNumber("acct8895");
		accnt.setAccountNSC("123456");
		accnt.setAccountName("darshan");
		accnt.setBic("SC802001");
		accnt.setIban("1022675");
		accnt.setCurrency("GBP");
		accnt.setAccountPermission("V");
	/** Creating Second Account */
		Accnt accnt1 = new Accnt();
		accnt1.setAccountNumber("acct4568");
		accnt1.setAccountNSC("26365678");
		accnt1.setAccountName("amit");
		accnt1.setBic("LOYDGB21");
		accnt1.setIban("GB19LOYD30961799709943");
		accnt1.setCurrency("EUR");
		accnt1.setAccountPermission("A");
	/** Creating List Of Accounts */
		Accnts accounts = new Accnts();
		accounts.getAccount().add(accnt1);
		accounts.getAccount().add(accnt);
	/** Creating Channel Profile Object */
		ChannelProfile channelProfile = new ChannelProfile();
		channelProfile.setAccounts(accounts);
		channelProfile.setId("BOI123");

	/** Creating Customer Account Info List */
		CustomerAccountInfo cusInfo = new CustomerAccountInfo();
		cusInfo.setAccountName("Test");
		cusInfo.setAccountNSC("123456");
		cusInfo.setAccountNumber("BOI123");
		cusInfo.setUserId("BOI123");
		List<CustomerAccountInfo> cusInfoList = new ArrayList<CustomerAccountInfo>();
		cusInfoList.add(cusInfo);
		OBReadAccount2 accGETRes=new OBReadAccount2();
	
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenThrow(AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_DETAILS_NOT_FOUND));
		Mockito.when(cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(accGETRes);
		OBReadAccount2 accGETRes1 = customerAccProFoundServiceAdapter
				.retrieveCustomerAccountList(channelProfile.getId(), null);
		assertNotNull(accGETRes1);
		
	}

	/**
	 * Test customer account information UserId as null.
	 */
	@Test(expected = AdapterException.class)
	public void testCustomerAccountInformationUserIdAsNull() {
		customerAccProFoundServiceAdapter.retrieveCustomerAccountList(null, new HashMap<String, String>());
	}

	
	@Test
	public void testCustomerPersonalInformationUserIdAsNull() {
		customerAccProFoundServiceAdapter.retrieveCustomerInfo(null, new HashMap<String, String>());
	}
	
	@Test()
	public void testCustomerInfoList() {

		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt account1 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		account1.setAccountName("test");
		DigitalUserProfile digitalUserProfile= new DigitalUserProfile();
		com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile channelProfile=new com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile();
		List<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt> accountList=new ArrayList<>();
		accountList.add(account1);
		
		/** Mock all methods to test retrieve Customer Account List */
        Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(digitalUserProfile);
        Mockito.when(customerAccountProfileFoundationServiceClient.restTransportForParty(anyObject(), anyObject(),anyObject())).thenReturn(channelProfile);
		OBReadAccount2 accGETRes=new OBReadAccount2();
		Mockito.when(cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(accGETRes);
		OBReadAccount2 accGETRes1 = customerAccProFoundServiceAdapter
				.retrieveCustomerAccountList("BOI123", new HashMap<String, String>());
	}

	@Test
	public void testCustomerInfoListwhenFilteredAccountNull() {

		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt account1 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		account1.setAccountName("test");
		DigitalUserProfile filteredAccounts= new DigitalUserProfile();
		com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile channelProfile=new com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile();
		List<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt> accountList=new ArrayList<>();
		accountList.add(account1);
		
		/** Mock all methods to test retrieve Customer Account List */
        Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(filteredAccounts);
        Mockito.when(customerAccountProfileFoundationServiceClient.restTransportForParty(anyObject(), anyObject(),anyObject())).thenReturn(channelProfile);
		OBReadAccount2 accGETRes=new OBReadAccount2();
		Mockito.when(cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(accGETRes);
		OBReadAccount2 accGETRes1 = customerAccProFoundServiceAdapter
				.retrieveCustomerAccountList("BOI123", new HashMap<String, String>());
	}
	
	@Test
	public void testCustomerAccntProfileFoundSerClient()
	{
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile channelProfile=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.ChannelProfile();
		when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(channelProfile);
		customerAccountProfileFoundationServiceClient.restTransportForParty(new RequestInfo(), com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile.class, new HttpHeaders());
	}
	

	@Test(expected = AdapterException.class)
	public void testCustomerInfoListParamNull() {

		
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt account1 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		account1.setAccountName("test");
		DigitalUserProfile filteredAccounts= new DigitalUserProfile();
		com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile channelProfile=new com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile();
		List<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt> accountList=new ArrayList<>();
		accountList.add(account1);
		
		/** Mock all methods to test retrieve Customer Account List */
        Mockito.when(commonFilterUtility.retrieveCustomerAccountList(anyObject(), anyObject())).thenReturn(filteredAccounts);
        Mockito.when(customerAccountProfileFoundationServiceClient.restTransportForParty(anyObject(), anyObject(),anyObject())).thenReturn(channelProfile);
		OBReadAccount2 accGETRes=new OBReadAccount2();
		Mockito.when(cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(accGETRes);
		OBReadAccount2 accGETRes1 = customerAccProFoundServiceAdapter
				.retrieveCustomerAccountList("BOI123", null);
	}
}