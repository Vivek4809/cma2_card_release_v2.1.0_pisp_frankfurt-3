package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.transformer.DomesticPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class DomesticPaymentConsentsFoundationServiceDelegate {

	@Autowired
	private DomesticPaymentConsentsFoundationServiceTransformer domesticPaymentConsentsFoundationServiceTransformer;

	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;

	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;

	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;

	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourcesystem;

	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;

	@Value("${foundationService.domesticPaymentConsentBaseURL}")
	private String domesticPaymentConsentBaseURL;

	@Value("${foundationService.domesticPaymentConsentPostBaseURL}")
	private String domesticPaymentConsentPostBaseURL;

	@Value("${foundationService.apiChannelCode:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceId;

	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;

	@Value("${foundationService.domesticPaymentConsentSetupVersion}")
	private String domesticPaymentConsentSetupVersion;

	public HttpHeaders createPaymentRequestHeaders(CustomPaymentStageIdentifiers customStageIdentifiers,
			RequestInfo requestInfo, Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(sourcesystem, "PSD2API");
		httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		if (customStageIdentifiers.getPaymentSetupVersion().equalsIgnoreCase("1.0")) {
			httpHeaders.add(systemApiVersion, "1.1");
		} else {
			httpHeaders.add(systemApiVersion, "3.0");
		}
		return httpHeaders;
	}

	public String getPaymentFoundationServiceURL(String version, String PaymentInstuctionProposalId) {
		return domesticPaymentConsentBaseURL + "/" + "v" + domesticPaymentConsentSetupVersion + "/"
				+ "domestic/payment-instruction-proposals" + "/" + PaymentInstuctionProposalId;
	}

	public HttpHeaders createPaymentRequestHeadersPost(CustomPaymentStageIdentifiers customStageIdentifiers,RequestInfo requestInfo, Map<String, String> params) {

		HttpHeaders httpHeaders = new HttpHeaders();

		httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add("X-API-PARTY-SOURCE-ID-NUMBER", "");
		httpHeaders.add(sourcesystem, "PSD2API");
		if (customStageIdentifiers.getPaymentSetupVersion().equalsIgnoreCase("1.0")) {
			httpHeaders.add(systemApiVersion, "1.1");
		} else {
			httpHeaders.add(systemApiVersion, "3.0");
		}
		// httpHeaders.add("Content-Type","application/json");

		return httpHeaders;
	}

	public String postPaymentFoundationServiceURL(String paymentSetupVersion) {

		return domesticPaymentConsentPostBaseURL + "/" + "v" + domesticPaymentConsentSetupVersion
				+ "/domestic/payment-instruction-proposals";
	}

	public PaymentInstructionProposal transformDomesticConsentResponseFromAPIToFDForInsert(
			CustomDPaymentConsentsPOSTRequest paymentConsentsRequest, Map<String, String> params) {

		return domesticPaymentConsentsFoundationServiceTransformer
				.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}

	public CustomDPaymentConsentsPOSTResponse transformDomesticConsentResponseFromFDToAPIForInsert(
			PaymentInstructionProposal paymentInstructionProposalResponse, Map<String, String> params) {

		return domesticPaymentConsentsFoundationServiceTransformer
				.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse, params);
	}

}
