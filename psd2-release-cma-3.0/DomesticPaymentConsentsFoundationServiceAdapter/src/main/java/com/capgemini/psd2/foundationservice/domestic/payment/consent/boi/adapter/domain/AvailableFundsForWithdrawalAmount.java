package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.domain;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AvailableFundsForWithdrawalAmount
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-08T13:41:31.197+05:30")

public class AvailableFundsForWithdrawalAmount   {
  @JsonProperty("transactionCurrency")
  private Double transactionCurrency = null;

  public AvailableFundsForWithdrawalAmount transactionCurrency(Double transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
    return this;
  }

  /**
   * The amount of funds that is available for withdrawal.
   * @return transactionCurrency
  **/
  @ApiModelProperty(required = true, value = "The amount of funds that is available for withdrawal.")
  @NotNull


  public Double getTransactionCurrency() {
    return transactionCurrency;
  }

  public void setTransactionCurrency(Double transactionCurrency) {
    this.transactionCurrency = transactionCurrency;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = (AvailableFundsForWithdrawalAmount) o;
    return Objects.equals(this.transactionCurrency, availableFundsForWithdrawalAmount.transactionCurrency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(transactionCurrency);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AvailableFundsForWithdrawalAmount {\n");
    
    sb.append("    transactionCurrency: ").append(toIndentedString(transactionCurrency)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

