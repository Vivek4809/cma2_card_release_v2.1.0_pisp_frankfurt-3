package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.delegate.DomesticScheduledPaymentConsentsFoundationServiceDelegate;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticScheduledPaymentConsentsFoundationServiceDelegateTest {

	@InjectMocks
	DomesticScheduledPaymentConsentsFoundationServiceDelegate delegate;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testcreatePaymentRequestHeaders() {
		ReflectionTestUtils.setField(delegate, "transactionReqHeader", "transactionReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partySourceId", "sourceid");
		ReflectionTestUtils.setField(delegate, "domesticScheduledPaymentConsentBaseURL", "baseUrl");
//		ReflectionTestUtils.setField(delegate, "domesticScheduledPaymentConsentPostBaseURL", "baseUrl");

		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();
		params.put("transactionReqHeader", "boi");
		HttpHeaders header = delegate.createPaymentRequestHeaders(requestInfo, params);
		HttpHeaders header1 = delegate.createPaymentRequestHeadersPost(requestInfo, params);

		assertNotNull(header);

	}

	@Test
	public void testgetPaymentFoundationServiceURL() {
		String version = "1.0";
		String PaymentInstuctionProposalId = "1007";
		delegate.getPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test
	public void testgetPaymentFoundationServiceURL1() {
		String version = "1.0";
		String PaymentInstuctionProposalId = "1007";
		delegate.postPaymentFoundationServiceURL(version);
		assertNotNull(delegate);
	}

}
