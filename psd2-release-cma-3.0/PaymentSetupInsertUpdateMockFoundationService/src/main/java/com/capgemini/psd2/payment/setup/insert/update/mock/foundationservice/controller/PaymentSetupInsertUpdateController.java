package com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;
import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.domain.PaymentInstruction;
import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.service.PaymentSetupInsertUpdateService;

@RestController
@RequestMapping("/fs-payment-web-service/services/stagePayment")
public class PaymentSetupInsertUpdateController {
	@Autowired
	PaymentSetupInsertUpdateService paymentSetupCreateService;
	
	@RequestMapping(method = RequestMethod.POST, consumes = { MediaType.APPLICATION_XML_VALUE }, produces = {MediaType.APPLICATION_XML_VALUE})
	@ResponseBody
	public ResponseEntity<ValidationPassed> stagePaymentInsertion(
			@RequestBody PaymentInstruction paymentInstruction,
			@RequestHeader(required = false,value="X-BOI-USER") String boiUser,
			@RequestHeader(required = false,value="X-BOI-CHANNEL") String boiChannel,
			@RequestHeader(required = false,value="X-BOI-PLATFORM") String boiPlatform,
			@RequestHeader(required = false,value="X-CORRELATION-ID") String correlationID) throws Exception {
		
			
			if("I".equalsIgnoreCase(paymentInstruction.getPayment().getRequestType())){
				if(NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
					throw MockFoundationServiceException
						.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_PMI);
				}
			}else if("U".equalsIgnoreCase(paymentInstruction.getPayment().getRequestType())){
				if(NullCheckUtils.isNullOrEmpty(boiUser) || NullCheckUtils.isNullOrEmpty(boiChannel)
						|| NullCheckUtils.isNullOrEmpty(boiPlatform) || NullCheckUtils.isNullOrEmpty(correlationID)) {
					throw MockFoundationServiceException
						.populateMockFoundationServiceException(ErrorCodeEnum.AUTHENTICATION_FAILURE_PMU);
				}
			}else{
				throw MockFoundationServiceException
				.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PMI);
			}
			
		
		ValidationPassed validationPassed =	 paymentSetupCreateService.stagePaymentInsertion(paymentInstruction);
		if("I".equalsIgnoreCase(paymentInstruction.getPayment().getRequestType())){
			return new ResponseEntity<ValidationPassed>(validationPassed, HttpStatus.CREATED);
		}else{
			return new ResponseEntity<ValidationPassed>(validationPassed, HttpStatus.OK);
		}
	}

}
