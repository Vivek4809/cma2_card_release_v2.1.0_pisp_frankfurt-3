package com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.payment.setup.insert.update.mock.foundationservice.domain.PaymentInstruction;


public interface PaymentSetupInsertUpdateRepository extends MongoRepository<PaymentInstruction,String> {

	public PaymentInstruction findByPaymentPaymentId(String paymentId);
}
