package com.capgemini.psd2.account.product.mongo.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountProductCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountProductMockData {

	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static OBReadProduct2 getBlankResponse() {
		OBReadProduct2 ProductResponse = new OBReadProduct2();
		OBReadProduct2Data data = new OBReadProduct2Data();
		List<OBProduct2> product = new ArrayList<>();
		data.setProduct(product);
		ProductResponse.setData(data);
		return ProductResponse;
	}

	public static AccountProductCMA2 getMockAccountDetails() {
		/*
		 * AccountProductCMA2 accountDetails = new AccountProductCMA2();
		 * accountDetails.setAccountNumber("76528776");
		 * accountDetails.setAccountNSC("903779");
		 */
		// return accountDetails;
		return new AccountProductCMA2();
	}

	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}

	public static OBReadProduct2 getMockExpectedAccountProductResponse() {
		PlatformAccountProductsResponse platformAccountProductsResponse = new PlatformAccountProductsResponse();
		OBReadProduct2Data oBReadProduct2Data = new OBReadProduct2Data();
		OBReadProduct2 obReadProduct2 = new OBReadProduct2();
		AccountProductCMA2 mockProduct = new AccountProductCMA2();

		mockProduct.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");

		List<OBProduct2> productList = new ArrayList<>();
		productList.add(mockProduct);
		oBReadProduct2Data.setProduct(productList);
		obReadProduct2.setData(oBReadProduct2Data);
		platformAccountProductsResponse.setoBReadProduct2(obReadProduct2);
		return obReadProduct2;
	}
}
