package com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.transformer.ValidatePaymentFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.utility.ValidatePaymentFoundationServiceUtility;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class ValidatePaymentFoundationServiceUtilityTest {
	
	@InjectMocks
	private ValidatePaymentFoundationServiceUtility validatePaymentFoundationServiceUtility;
	
	@Mock
	private ValidatePaymentFoundationServiceTransformer validatePaymentFSTransformer;
	
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	/**
	 * Test transform request from APIToFS.
	 */
	@Test
	public void transformRequestFromAPIToFSTest(){
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse= new CustomPaymentSetupPOSTResponse();
		HashMap<String, String> params = new HashMap<>();
		PaymentInstruction paymentInstruction= new PaymentInstruction();
		
		Mockito.when(validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(paymentInstruction);
		paymentInstruction = validatePaymentFoundationServiceUtility.transformRequestFromAPIToFS(paymentSetupPOSTResponse, params);
		assertNotNull(paymentInstruction);
	}
	
	/**
	 * Test transform response from FSToAPI.
	 */
	@Test
	public void transformResponseFromFSToAPITest(){
		
		ValidationPassed validationPassed= new ValidationPassed();
		PaymentSetupValidationResponse paymentSetupValidationResponse= new PaymentSetupValidationResponse();
		
		Mockito.when(validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(paymentSetupValidationResponse);
		paymentSetupValidationResponse= validatePaymentFoundationServiceUtility.transformResponseFromFSToAPI(validationPassed);
		assertNotNull(paymentSetupValidationResponse);
	}

	
	/**
	 * Test request headers.
	 */
	@Test
	public void createRequestHeadersTest(){
		
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "platformInReqHeader", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "correlationReqHeader", "X-CORRELATION-ID");
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "platform", "PSD2API");
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "clientIdReqHeader_Payments", "client_id");
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "clientSecretReqHeader_Payments", "client_secret");
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "clientIdReqHeaderValue","clientIdReqHeaderValue");
		ReflectionTestUtils.setField(validatePaymentFoundationServiceUtility, "clientSecretReqHeaderValue", "clientSecretReqHeaderValue");



		HashMap<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "testUser");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "testChannel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "testCorrelation");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "testPlatform");
		
		CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse= new CustomPaymentSetupPOSTResponse();
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders = validatePaymentFoundationServiceUtility.createRequestHeaders(requestInfo, paymentSetupPOSTResponse, params);
		assertNotNull(httpHeaders);
	}
}
