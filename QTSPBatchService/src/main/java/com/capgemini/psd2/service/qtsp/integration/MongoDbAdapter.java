package com.capgemini.psd2.service.qtsp.integration;

import java.util.List;

import com.capgemini.psd2.model.qtsp.QTSPResource;

public interface MongoDbAdapter {

	public List<QTSPResource> saveQTSPs(List<QTSPResource> qtsps);

	public List<QTSPResource> retrieveQTSPs();

	public void removeQTSP(String id);

}
