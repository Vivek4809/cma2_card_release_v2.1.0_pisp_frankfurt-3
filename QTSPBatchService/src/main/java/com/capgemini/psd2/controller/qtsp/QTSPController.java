package com.capgemini.psd2.controller.qtsp;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.service.qtsp.config.QTSPServiceConfig;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;

@RestController
public class QTSPController {

	@Lazy
	@Autowired
	private JobLauncher jobLauncher;

	@Lazy
	@Autowired
	private Job job;

	@Autowired
	private QTSPServiceConfig qtspServiceConfig;

	@Autowired
	private QTSPServiceUtility utility;

	@GetMapping(value = "/process-qtsp-job", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<String> processQTSPJob() throws JobExecutionException {
		qtspServiceConfig.setBatchJobState(false);
		utility.getFailures().clear();
		JobParameters params = new JobParametersBuilder().addString("job1", String.valueOf(System.currentTimeMillis()))
				.toJobParameters();
		jobLauncher.run(job, params);

		return new ResponseEntity<>(HttpStatus.OK);
	}

}