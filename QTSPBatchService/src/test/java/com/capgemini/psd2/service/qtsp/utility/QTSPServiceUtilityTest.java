package com.capgemini.psd2.service.qtsp.utility;

import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.model.qtsp.FailureInfo;
import com.capgemini.psd2.service.qtsp.stub.QTSPStub;
import com.nimbusds.jose.util.X509CertUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class QTSPServiceUtilityTest {

	@InjectMocks
	private QTSPServiceUtility qtspServiceUtility;

	private static String x509Cert;

	private static final String beginMarker = "-----BEGIN CERTIFICATE-----";

	private static final String endMarker = "-----END CERTIFICATE-----";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		x509Cert = "MIIFzjCCBLagAwIBAgIGSUEs5ABFMA0GCSqGSIb3DQEBCwUAMIGnMQswCQYDVQQGEwJIVTERMA8G\r\n"
				+ "A1UEBwwIQnVkYXBlc3QxFTATBgNVBAoMDE5ldExvY2sgS2Z0LjE3MDUGA1UECwwuVGFuw7pzw610\r\n"
				+ "dsOhbnlraWFkw7NrIChDZXJ0aWZpY2F0aW9uIFNlcnZpY2VzKTE1MDMGA1UEAwwsTmV0TG9jayBB\r\n"
				+ "cmFueSAoQ2xhc3MgR29sZCkgRsWRdGFuw7pzw610dsOhbnkwHhcNMTYwOTI4MTcyODMzWhcNMjgw\r\n"
				+ "NzA3MTcyODMzWjBcMQswCQYDVQQGEwJIVTERMA8GA1UEBwwIQnVkYXBlc3QxFTATBgNVBAoMDE5F\r\n"
				+ "VExPQ0sgTHRkLjEjMCEGA1UEAwwaTkVUTE9DSyBUcnVzdCBRdWFsaWZpZWQgQ0EwggEiMA0GCSqG\r\n"
				+ "SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDo0wKRbbCHmWOD64vZj7wvwlmI+6KwJNmA8xDjdgVbvlPz\r\n"
				+ "j/paQLT46a+x4tQ/ZvbQn7zqdtim7Q7cxoAPEAX2kVraY4HKiLhi0FA03AuAFeSUkK6Lg6sESpPn\r\n"
				+ "fcZ6LTMoRyKYodpFcQqkbUDFFfLoV701ICy+cjXKnWpb8J3vYaOw5LJ1UOPTybd3vlDXQzQPhrpV\r\n"
				+ "1+Or+NEuJ/yaLegPeqoJ4nK7zE5/Ln1wk+wfqiyzrPaWmUh+stKRqxW4hiHk1BX9M8yeHLAzOVtz\r\n"
				+ "vg0GfHYRwWF+pa3FtyGHGJ/ookvZn/cN3e92RLHXFoQU7f1dCswLZv83QQAmomQgKfhHAgMA8wGj\r\n"
				+ "ggJIMIICRDAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBBjAfBgNVHSMEGDAWgBTM+meT\r\n"
				+ "8La40KXAHvNT/YxT34PXljAdBgNVHQ4EFgQUUjpVthlqi3NyxV08C2CHlFDwbOcwggE+BggrBgEF\r\n"
				+ "BQcBAQSCATAwggEsMCwGCCsGAQUFBzABhiBodHRwOi8vb2NzcDEubmV0bG9jay5odS9nb2xkLmNn\r\n"
				+ "aTAsBggrBgEFBQcwAYYgaHR0cDovL29jc3AyLm5ldGxvY2suaHUvZ29sZC5jZ2kwLAYIKwYBBQUH\r\n"
				+ "MAGGIGh0dHA6Ly9vY3NwMy5uZXRsb2NrLmh1L2dvbGQuY2dpMDQGCCsGAQUFBzAChihodHRwOi8v\r\n"
				+ "YWlhMS5uZXRsb2NrLmh1L2luZGV4LmNnaT9jYT1nb2xkMDQGCCsGAQUFBzAChihodHRwOi8vYWlh\r\n"
				+ "Mi5uZXRsb2NrLmh1L2luZGV4LmNnaT9jYT1nb2xkMDQGCCsGAQUFBzAChihodHRwOi8vYWlhMy5u\r\n"
				+ "ZXRsb2NrLmh1L2luZGV4LmNnaT9jYT1nb2xkMIGeBgNVHR8EgZYwgZMwL6AtoCuGKWh0dHA6Ly9j\r\n"
				+ "cmwxLm5ldGxvY2suaHUvaW5kZXguY2dpP2NybD1nb2xkMC+gLaArhilodHRwOi8vY3JsMi5uZXRs\r\n"
				+ "b2NrLmh1L2luZGV4LmNnaT9jcmw9Z29sZDAvoC2gK4YpaHR0cDovL2NybDMubmV0bG9jay5odS9p\r\n"
				+ "bmRleC5jZ2k/Y3JsPWdvbGQwDQYJKoZIhvcNAQELBQADggEBAAZKg60kuK/y8TGXo/gkZAwVIQFP\r\n"
				+ "hD+3TLmXjKtde3y5VwloBilf9OuA+pUznUHj2pZ23w7HyRiPp8FNOX7ZVBq+4S9me0kaZ+JWEOmS\r\n"
				+ "tJDa5HuwqitVFUFAcR+HJvMncFccEFIVYzSzNLJWADKnibmou3xO8C7b+6cmvYg/osVWLx1uVI2X\r\n"
				+ "Pm/ij/Ndp5PaV2kqLkb09f7WMgsIat34ISb9DchEZ7k2tYVoS+fpz2+8pvlDn6LM3mRfI0wxg9wi\r\n"
				+ "kqzERsx5GOYRyfyUdU1q0Axh0CRC++6mSmjIOomBK235tdHBtHh3+3J8A1OFLruRoNZA2atJZADy\r\n" + "/1yURQRKS/8=";
	}

	@Test
	public void parseForPFTest_empty() {
		QTSPServiceUtility.parseForPF("");
	}

	@Test
	public void parseForPFTest() {

		String cert = beginMarker + x509Cert + endMarker;
		QTSPServiceUtility.parseForPF(cert);
	}

	@Test
	public void parseForNSTest_empty() {
		QTSPServiceUtility.parseForNS("");
	}

	@Test
	public void parseForNSTest() {

		String cert = beginMarker + x509Cert + endMarker;
		QTSPServiceUtility.parseForNS(cert);
	}

	@Test
	public void parseForNSTest_noMarkers() {

		String cert = x509Cert;
		QTSPServiceUtility.parseForNS(cert);
	}

	@Test
	public void extractSha1Thumbprint() {
		X509Certificate cert = X509CertUtils.parse(beginMarker + x509Cert + endMarker);
		try {
			QTSPServiceUtility.extractSha1Thumbprint(cert);
		} catch (CertificateEncodingException | NoSuchAlgorithmException e) {
		}
	}

	@Test
	public void getFailuresTest() {
		qtspServiceUtility.getFailures();
	}

	@Test
	public void setFailuresTest() {
		List<FailureInfo> failures = new ArrayList<>();
		FailureInfo failureInfo = new FailureInfo(QTSPStub.getCerts().get(0), "ns1", "abc", "NS_ADD");
		failures.add(failureInfo);
		qtspServiceUtility.setFailures(failures);
	}
}
