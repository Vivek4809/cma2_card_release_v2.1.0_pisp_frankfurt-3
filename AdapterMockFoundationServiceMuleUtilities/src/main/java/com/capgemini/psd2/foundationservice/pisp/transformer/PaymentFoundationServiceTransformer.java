package com.capgemini.psd2.foundationservice.pisp.transformer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.pisp1.domain.PayerInformation;
import com.capgemini.psd2.foundationservice.pisp1.domain.Payment;
import com.capgemini.psd2.foundationservice.pisp1.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.pisp3.domain.Address;
import com.capgemini.psd2.foundationservice.pisp3.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.pisp3.domain.Country;
import com.capgemini.psd2.foundationservice.pisp3.domain.Currency;
import com.capgemini.psd2.foundationservice.pisp3.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstructionStatusCode;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.pisp3.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.pisp3.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;




@Component
public class PaymentFoundationServiceTransformer {
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	
	public PaymentInstructionProposal transformPaymentInstructionConsent(PaymentInstruction paymentInstruction){
		
		System.out.println("Inside transformer "+paymentInstruction.toString());
		
		Payment payment = paymentInstruction.getPayment();
		
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();		
		paymentInstructionProposal.setPaymentInstructionProposalId(payment.getPaymentId());		
		
		Date date = timeZoneDateTimeAdapter.parseDateTimeFS(payment.getCreatedOn());
		
		System.out.println("Date: "+date.toString());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");		
		paymentInstructionProposal.setProposalCreationDatetime(format.format(date));
		
		
		if(payment.getPaymentInformation().getStatus().equals("AcceptedTechnicalValidation")){		
		paymentInstructionProposal.setProposalStatus(ProposalStatus.AcceptedTechnicalValidation);
		} else {
		paymentInstructionProposal.setProposalStatus(ProposalStatus.Rejected);	
		}
		paymentInstructionProposal.setInstructionReference(payment.getPaymentInformation().getInstructionIdentification());
		paymentInstructionProposal.setInstructionEndToEndReference(paymentInstruction.getEndToEndIdentification());
		
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		financialEventAmount.setTransactionCurrency(payment.getPaymentInformation().getAmount().doubleValue());	
		paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode(payment.getPayeeInformation().getBeneficiaryCurrency());
		paymentInstructionProposal.setTransactionCurrency(currency);
		
		//DebtorAccount
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();		
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getIban())){ 		
			authorisingPartyAccount.setSchemeName("UK.OBIE.IBAN");
			authorisingPartyAccount.setAccountIdentification(payment.getPayerInformation().getIban());
			authorisingPartyAccount.setAccountName(payment.getPayerInformation().getAccountName());
			authorisingPartyAccount.setSecondaryIdentification(payment.getPayerInformation().getAccountSecondaryID());		
	     }
		else if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getAccountNumber()) && !NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getNsc())){ 					
			authorisingPartyAccount.setSchemeName("UK.OBIE.SortCodeAccountNumber");
			authorisingPartyAccount.setAccountIdentification(payment.getPayerInformation().getNsc()+payment.getPayerInformation().getAccountNumber());
			authorisingPartyAccount.setAccountName(payment.getPayerInformation().getAccountName());
			authorisingPartyAccount.setSecondaryIdentification(payment.getPayerInformation().getAccountSecondaryID());
	     }		
		paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		
		//CreditorAccount		
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getIban())){
			proposingPartyAccount.setSchemeName("UK.OBIE.IBAN");
			proposingPartyAccount.setAccountIdentification(payment.getPayeeInformation().getIban());
			proposingPartyAccount.setAccountName(payment.getPayeeInformation().getBeneficiaryName());
			proposingPartyAccount.setSecondaryIdentification(payment.getPayeeInformation().getAccountSecondaryID());
		}
		else if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getBeneficiaryAccountNumber()) && !NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getBeneficiaryNsc())){
			proposingPartyAccount.setSchemeName("UK.OBIE.SortCodeAccountNumber");
			proposingPartyAccount.setAccountIdentification(payment.getPayeeInformation().getBeneficiaryNsc()+payment.getPayeeInformation().getBeneficiaryAccountNumber());
			proposingPartyAccount.setAccountName(payment.getPayeeInformation().getBeneficiaryName());
			proposingPartyAccount.setSecondaryIdentification(payment.getPayeeInformation().getAccountSecondaryID());
		}		
		paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);		
		paymentInstructionProposal.setAuthorisingPartyUnstructuredReference(payment.getPaymentInformation().getUnstructured());
		paymentInstructionProposal.setAuthorisingPartyReference(payment.getPaymentInformation().getBeneficiaryReference());
		
		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = new PaymentInstrumentRiskFactor();
		paymentInstrumentRiskFactor.setPaymentContextCode(payment.getPaymentInformation().getPaymentContextCode());
		paymentInstrumentRiskFactor.setMerchantCategoryCode(payment.getPayeeInformation().getMerchentCategoryCode());
		paymentInstrumentRiskFactor.setMerchantCustomerIdentification(payment.getPayerInformation().getMerchantCustomerIdentification());
		
		Address address = new Address();
		address.setFirstAddressLine(payment.getPayeeInformation().getDeliveryAddress().getAddressLine1());
		address.setSecondAddressLine(payment.getPayeeInformation().getDeliveryAddress().getAddressLine2());
		address.setGeoCodeBuildingName(payment.getPayeeInformation().getDeliveryAddress().getStreetName());
		address.setGeoCodeBuildingNumber(payment.getPayeeInformation().getDeliveryAddress().getBuildingNumber());
		address.setPostCodeNumber(payment.getPayeeInformation().getDeliveryAddress().getPostCode());
		address.setThirdAddressLine(payment.getPayeeInformation().getDeliveryAddress().getTownName());
		address.setFourthAddressLine(payment.getPayeeInformation().getDeliveryAddress().getCountrySubDivision1());
		address.setFifthAddressLine(payment.getPayeeInformation().getDeliveryAddress().getCountrySubDivision2());
		Country country = new Country();
		country.setIsoCountryAlphaTwoCode(payment.getPayeeInformation().getDeliveryAddress().getCountry());	
		address.setAddressCountry(country);		
		paymentInstrumentRiskFactor.setCounterPartyAddress(address);
		paymentInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		
		System.out.println("Out");
		return paymentInstructionProposal;
		
		
		
	}
	
	
	public PaymentInstruction transformPaymentInstructionUpdate(PaymentInstruction paymentInstruction, PaymentInstructionProposal paymentInstructionProposal ) {
		
		System.out.println("Inside Update");
		System.out.println(paymentInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
		
		PayerInformation payerInfo = null;
		if(paymentInstructionProposal.getAuthorisingPartyAccount().getSchemeName().equals("UK.OBIE.IBAN")){ 				
			System.out.println("Inside debtor Update");
			payerInfo = new PayerInformation();
			payerInfo.setIban(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountIdentification());
	        payerInfo.setAccountName(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountName());
	        payerInfo.setAccountSecondaryID(paymentInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification());
	        payerInfo.setMerchantCustomerIdentification(paymentInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
	        payerInfo.setBic(paymentInstruction.getPayment().getPayerInformation().getBic());
	        String Iban = paymentInstructionProposal.getAuthorisingPartyAccount().getAccountIdentification();
			if(Iban.substring(0, 2).equalsIgnoreCase("IE")){
	        payerInfo.setNsc(Iban.substring(8, 14));
	        payerInfo.setAccountNumber(Iban.substring(14, 22));
			}
		} else {
			payerInfo = new PayerInformation();
			payerInfo.setNsc(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountIdentification().substring(0, 6));
        	payerInfo.setAccountNumber(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountIdentification().substring(6, 14));
			payerInfo.setAccountName(paymentInstructionProposal.getAuthorisingPartyAccount().getAccountName());
			payerInfo.setAccountSecondaryID(paymentInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification());
			payerInfo.setMerchantCustomerIdentification(paymentInstructionProposal.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
		}
		paymentInstruction.getPayment().setPayerInformation(payerInfo);
		
		System.out.println(paymentInstructionProposal.getProposalStatus().toString());
		
		paymentInstruction.getPayment().getPaymentInformation().setStatus(paymentInstructionProposal.getProposalStatus().toString());
		
		return paymentInstruction;
	}
	
	public PaymentInstructionProposalComposite transformPaymentInstructionOrder(PaymentInstruction paymentInstruction){
		
		PaymentInstructionProposalComposite paymentInstructionProposalComposite = new PaymentInstructionProposalComposite();
		
		Payment payment = paymentInstruction.getPayment();
			
		//DomesticPaymentId
		com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstruction1 paymentInstruction3 = new com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstruction1();
		paymentInstruction3.paymentInstructionNumber(payment.getPaymentInformation().getSubmissionID());
		
		PaymentInstructionProposal paymentInstructionProposal = new PaymentInstructionProposal();
		//ConsentId
		paymentInstructionProposal.setPaymentInstructionProposalId(payment.getPaymentId());	
		
		//Date date = timeZoneDateTimeAdapter.parseDateTimeFS(payment.getCreatedOn());
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//CreationDateTime
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		paymentInstruction3.setInstructionIssueDate(payment.getCreatedOn());
			
		//Status
		if(payment.getPaymentInformation().getStatus().equals("AcceptedSettlementInProcess")){		
			paymentInstruction3.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.AcceptedSettlementInProcess);
		} else {
			paymentInstruction3.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.Rejected);	
		}
		
		//Initiation
		paymentInstructionProposal.setInstructionReference(payment.getPaymentInformation().getInstructionIdentification());
		paymentInstructionProposal.setInstructionEndToEndReference(paymentInstruction.getEndToEndIdentification());
		
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		financialEventAmount.setTransactionCurrency(payment.getPaymentInformation().getAmount().doubleValue());	
		paymentInstructionProposal.setFinancialEventAmount(financialEventAmount);
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode(payment.getPayeeInformation().getBeneficiaryCurrency());
		paymentInstructionProposal.setTransactionCurrency(currency);
		
		//DebtorAccount
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();		
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getIban())){ 		
			authorisingPartyAccount.setSchemeName("UK.OBIE.IBAN");
			authorisingPartyAccount.setAccountIdentification(payment.getPayerInformation().getIban());
			authorisingPartyAccount.setAccountName(payment.getPayerInformation().getAccountName());
			authorisingPartyAccount.setSecondaryIdentification(payment.getPayerInformation().getAccountSecondaryID());		
	     }
		else if(!NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getAccountNumber()) && !NullCheckUtils.isNullOrEmpty(payment.getPayerInformation().getNsc())){ 					
			authorisingPartyAccount.setSchemeName("UK.OBIE.SortCodeAccountNumber");
			authorisingPartyAccount.setAccountIdentification(payment.getPayerInformation().getNsc()+payment.getPayerInformation().getAccountNumber());
			authorisingPartyAccount.setAccountName(payment.getPayerInformation().getAccountName());
			authorisingPartyAccount.setSecondaryIdentification(payment.getPayerInformation().getAccountSecondaryID());
	     }		
		paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		
		//CreditorAccount		
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getIban())){
			proposingPartyAccount.setSchemeName("UK.OBIE.IBAN");
			proposingPartyAccount.setAccountIdentification(payment.getPayeeInformation().getIban());
			proposingPartyAccount.setAccountName(payment.getPayeeInformation().getBeneficiaryName());
			proposingPartyAccount.setSecondaryIdentification(payment.getPayeeInformation().getAccountSecondaryID());
		}
		else if(!NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getBeneficiaryAccountNumber()) && !NullCheckUtils.isNullOrEmpty(payment.getPayeeInformation().getBeneficiaryNsc())){
			proposingPartyAccount.setSchemeName("UK.OBIE.SortCodeAccountNumber");
			proposingPartyAccount.setAccountIdentification(payment.getPayeeInformation().getBeneficiaryNsc()+payment.getPayeeInformation().getBeneficiaryAccountNumber());
			proposingPartyAccount.setAccountName(payment.getPayeeInformation().getBeneficiaryName());
			proposingPartyAccount.setSecondaryIdentification(payment.getPayeeInformation().getAccountSecondaryID());
		}		
		paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);	
		
		//RemittanceInformation
		paymentInstructionProposal.setAuthorisingPartyUnstructuredReference(payment.getPaymentInformation().getUnstructured());
		paymentInstructionProposal.setAuthorisingPartyReference(payment.getPaymentInformation().getBeneficiaryReference());
				
		paymentInstructionProposalComposite.setPaymentInstructionProposal(paymentInstructionProposal);
		paymentInstructionProposalComposite.setPaymentInstruction(paymentInstruction3);		
		
		return paymentInstructionProposalComposite;		
	}
	
	
	
	

}
