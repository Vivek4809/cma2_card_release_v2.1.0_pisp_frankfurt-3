package com.capgemini.psd2.fraudsystem.routing.adapter.routing;

import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;

public interface FraudSystemAdapterFactory {
	public FraudSystemAdapter getAdapterInstance(String coreSystemName);
}
