package com.capgemini.psd2.pisp.validation.adapter.impl;

import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;


@Lazy
@Component("internationalStandingOrderValidator")
@ConfigurationProperties("app")
public class InternationalStandingOrderValidatorImpl implements
		PaymentConsentsCustomValidator<CustomIStandingOrderConsentsPOSTRequest, CustomIStandingOrderConsentsPOSTResponse>,
		PaymentSubmissionCustomValidator<CustomIStandingOrderPOSTRequest, CustomIStandingOrderPOSTResponse> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.regex.paymentId:#{null}}")
	private String paymentIdRegexValidator;

	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Value("${app.pattern.merchantCategoryCode:\\d+}")
	private String merchantCategoryCodeRegexValidator;

	@Autowired
	private CommonPaymentValidations commonPaymentValidations;

	// Implementation Methods for PaymentConsentsCustomValidator
	@Override
	public boolean validateConsentRequest(
			CustomIStandingOrderConsentsPOSTRequest customInternationalStandingOrderPOSTRequest) {

		commonPaymentValidations.validateHeaders();
		if (reqValidationEnabled)
			executeInternationalConsentsRequestSwaggerValidations(customInternationalStandingOrderPOSTRequest);
		executeInternationalConsentsRequestCustomValidations(customInternationalStandingOrderPOSTRequest);
		return Boolean.TRUE;
	}

	@Override
	public boolean validateConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		// validate request parameter payment consent id
		psd2Validator.validate(paymentRetrieveRequest);
		return Boolean.TRUE;
	}

	@Override
	public boolean validatePaymentConsentResponse(
			CustomIStandingOrderConsentsPOSTResponse customInternationalStandingOrderPOSTResponse) {

		// validate request parameter domestic standing order id
		if (resValidationEnabled)
			executeInternationalConsentsResponseSwaggerValidations(customInternationalStandingOrderPOSTResponse);
		executeInternationalConsentsResponseCustomValidations(customInternationalStandingOrderPOSTResponse);
		return true;
	}

	// Implementation Methods for PaymentSubmissionCustomValidator
	@Override
	public void validatePaymentsPOSTRequest(CustomIStandingOrderPOSTRequest submissionRequest) {
		executeInternationalSubmissionRequestCustomValidation(submissionRequest);
	}

	@Override
	public void validatePaymentsResponse(CustomIStandingOrderPOSTResponse submissionResponse) {
		if (resValidationEnabled)
			executeInternationalSubmissionResponseSwaggerValidations(submissionResponse);
		executeInternationalSubmissionResponseCustomValidations(submissionResponse);
	}

	private void executeInternationalConsentsRequestCustomValidations(
			CustomIStandingOrderConsentsPOSTRequest customInternationalStandingOrderPOSTRequest) {
		// validate Currency
		// InstructedAmountCurrencyValidation
		commonPaymentValidations.isValidCurrency(customInternationalStandingOrderPOSTRequest.getData().getInitiation()
				.getInstructedAmount().getCurrency(), null);

		// validate first Payment date time
		String firstPaymentDateTime = customInternationalStandingOrderPOSTRequest.getData().getInitiation()
				.getFirstPaymentDateTime();
		if (!NullCheckUtils.isNullOrEmpty(firstPaymentDateTime))
			commonPaymentValidations.validateAndParseDateTimeFormat(firstPaymentDateTime);

		// validate final payment date time
		String finalPaymentDateTime = customInternationalStandingOrderPOSTRequest.getData().getInitiation()
				.getFinalPaymentDateTime();
		if (!NullCheckUtils.isNullOrEmpty(finalPaymentDateTime))
			commonPaymentValidations.validateAndParseDateTimeFormat(finalPaymentDateTime);

		if (!NullCheckUtils.isNullOrEmpty(customInternationalStandingOrderPOSTRequest.getData().getAuthorisation())
				&& !NullCheckUtils.isNullOrEmpty(customInternationalStandingOrderPOSTRequest.getData()
						.getAuthorisation().getCompletionDateTime())) {
			commonPaymentValidations.validateAndParseDateTimeFormat(
					customInternationalStandingOrderPOSTRequest.getData().getAuthorisation().getCompletionDateTime());
		}

		checkIfLessthanRequiredDate(firstPaymentDateTime, OffsetDateTime.now().toString());

		if (!NullCheckUtils.isNullOrEmpty(finalPaymentDateTime)) {
			checkIfLessthanRequiredDate(finalPaymentDateTime, OffsetDateTime.now().toString());
			checkIfLessthanAndEqualRequiredDate(finalPaymentDateTime, firstPaymentDateTime);

			if (!NullCheckUtils.isNullOrEmpty(
					customInternationalStandingOrderPOSTRequest.getData().getInitiation().getNumberOfPayments())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_UNEXPECTED,
						ErrorMapKeys.INVALID_FIELDS_COMBINATION));
			}
		}

		// validate creditorAccount SchemeName Identification
		if (customInternationalStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					customInternationalStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount()
							.getSchemeName(),
					customInternationalStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount()
							.getIdentification(),
					null);

			if (!NullCheckUtils.isNullOrEmpty(customInternationalStandingOrderPOSTRequest.getData().getInitiation()
					.getCreditorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						customInternationalStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount()
								.getSchemeName(),
						customInternationalStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate debtorAccount SchemeName Identification
		if (customInternationalStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					customInternationalStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount()
							.getSchemeName(),
					customInternationalStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount()
							.getIdentification(),
					null);

			if (!NullCheckUtils.isNullOrEmpty(customInternationalStandingOrderPOSTRequest.getData().getInitiation()
					.getDebtorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						customInternationalStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount()
								.getSchemeName(),
						customInternationalStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate Payment Context
		commonPaymentValidations.validateDomesticPaymentContext(customInternationalStandingOrderPOSTRequest.getRisk());

		// validate Risk Delivery Address fields
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				customInternationalStandingOrderPOSTRequest.getRisk().getDeliveryAddress(), null);

		// validate authorisation completion date time
		if (null != customInternationalStandingOrderPOSTRequest.getData().getAuthorisation()
				&& null != customInternationalStandingOrderPOSTRequest.getData().getAuthorisation()
						.getCompletionDateTime()) {
			commonPaymentValidations.validateDateTime(
					customInternationalStandingOrderPOSTRequest.getData().getAuthorisation().getCompletionDateTime());
		}

		// Validate Merchant Category Code
		if (null != customInternationalStandingOrderPOSTRequest.getRisk()
				&& null != customInternationalStandingOrderPOSTRequest.getRisk().getMerchantCategoryCode()
				&& null != merchantCategoryCodeRegexValidator) {
			commonPaymentValidations.validateMerchantCategoryCode(
					customInternationalStandingOrderPOSTRequest.getRisk().getMerchantCategoryCode(),
					merchantCategoryCodeRegexValidator);
		}
	}

	private void checkIfLessthanRequiredDate(String incomingDate, String comparedDate) {
		OffsetDateTime incomingDateTime = OffsetDateTime.parse(incomingDate);
		if (incomingDateTime.isBefore(OffsetDateTime.parse(comparedDate))) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE, ErrorMapKeys.INVALID_DATE));
		}
	}

	private void checkIfLessthanAndEqualRequiredDate(String incomingDate, String comparedDate) {
		OffsetDateTime incomingDateTime = OffsetDateTime.parse(incomingDate);
		if (!incomingDateTime.isAfter(OffsetDateTime.parse(comparedDate))) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE, ErrorMapKeys.INVALID_DATE));
		}
	}

	private void executeInternationalConsentsRequestSwaggerValidations(
			CustomIStandingOrderConsentsPOSTRequest customInternationalStandingOrderPOSTRequest) {
		psd2Validator.validateRequestWithSwagger(customInternationalStandingOrderPOSTRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
	}

	private void executeInternationalConsentsResponseCustomValidations(
			CustomIStandingOrderConsentsPOSTResponse paymentConsentsResponse) {

		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				paymentConsentsResponse.getRisk().getDeliveryAddress(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// validate debtorAccount SchemeName Identification
		if (paymentConsentsResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentConsentsResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// validate creditor SchemeName Identification
		if (paymentConsentsResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentConsentsResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		if (paymentConsentsResponse.getData().getCharges() != null
				&& !paymentConsentsResponse.getData().getCharges().isEmpty()) {

			for (OBCharge1 charges : paymentConsentsResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}
		// validate Currency
		// FirstPaymentAmountCurrencyValidation
		commonPaymentValidations.isValidCurrency(
				paymentConsentsResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// Validate Merchant Category Code
		if (null != paymentConsentsResponse.getRisk()
				&& null != paymentConsentsResponse.getRisk().getMerchantCategoryCode()
				&& null != merchantCategoryCodeRegexValidator) {
			commonPaymentValidations.validateMerchantCategoryCode(
					paymentConsentsResponse.getRisk().getMerchantCategoryCode(), merchantCategoryCodeRegexValidator);
		}

		// validate Payment Context
		commonPaymentValidations.validateDomesticPaymentContextforResponse(paymentConsentsResponse.getRisk());

		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				paymentConsentsResponse.getRisk().getDeliveryAddress(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// validate authorisation completion date time
		if (null != paymentConsentsResponse.getData().getAuthorisation()
				&& null != paymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime());

		}

		commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
				paymentConsentsResponse.getData().getInitiation().getFirstPaymentDateTime());

		if (paymentConsentsResponse.getData().getInitiation().getFinalPaymentDateTime() != null) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getInitiation().getFinalPaymentDateTime());
		}

		if (paymentConsentsResponse.getData().getCutOffDateTime() != null) {
			commonPaymentValidations
					.validateAndParseDateTimeFormatForResponse(paymentConsentsResponse.getData().getCutOffDateTime());
		}
	}

	private void executeInternationalConsentsResponseSwaggerValidations(
			CustomIStandingOrderConsentsPOSTResponse customInternationalStandingOrderPOSTResponse) {
		psd2Validator.validate(customInternationalStandingOrderPOSTResponse);
	}

	private void executeInternationalSubmissionRequestCustomValidation(
			CustomIStandingOrderPOSTRequest submissionRequest) {
		// validate consent id
		validateInternationalPaymentId(submissionRequest.getData().getConsentId(), null, null);

		

	}

	private void validateInternationalPaymentId(String paymentId, OBErrorCodeEnum errorCodeEnum, String keys) {
		if (!paymentId.matches(paymentIdRegexValidator)) {
			if (NullCheckUtils.isNullOrEmpty(errorCodeEnum)) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_PAYMENT_ID));
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(errorCodeEnum, keys));
			}
		}
	}

	private void executeInternationalSubmissionResponseCustomValidations(
			CustomIStandingOrderPOSTResponse paymentSubmissionResponse) {

		validateInternationalPaymentId(paymentSubmissionResponse.getData().getConsentId(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, InternalServerErrorMessage.PISP_INVALID_ID);
		validateInternationalPaymentId(paymentSubmissionResponse.getData().getInternationalStandingOrderId(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, InternalServerErrorMessage.PISP_INVALID_ID);

		if (paymentSubmissionResponse.getData().getConsentId()
				.equals(paymentSubmissionResponse.getData().getInternationalStandingOrderId())) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_INVALID_ID));
		}

		// validate debtorAccount SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// validate creditor SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		if (paymentSubmissionResponse.getData().getCharges() != null
				&& !paymentSubmissionResponse.getData().getCharges().isEmpty()) {

			for (OBCharge1 charges : paymentSubmissionResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}

		// validate MultiAuth
		commonPaymentValidations.validateMultiAuth(paymentSubmissionResponse.getData().getMultiAuthorisation());

		OBMultiAuthorisation1 multiAuthBlock = paymentSubmissionResponse.getData().getMultiAuthorisation();
		if (!NullCheckUtils.isNullOrEmpty(multiAuthBlock)) {
			if (multiAuthBlock.getStatus().equals(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)) {
				if (paymentSubmissionResponse.getData().getStatus().equals(OBExternalStatus1Code.INITIATIONCOMPLETED)) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.PISP_INVALID_STATUS));
				}
			}
		}

		// validate Currency
		commonPaymentValidations.isValidCurrency(
				paymentSubmissionResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// Validate first payment date time
		if (paymentSubmissionResponse.getData().getInitiation().getFirstPaymentDateTime() != null) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getInitiation().getFirstPaymentDateTime());
		}

		// Validate final payment date time
		if (paymentSubmissionResponse.getData().getInitiation().getFinalPaymentDateTime() != null) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getInitiation().getFinalPaymentDateTime());
			if (!NullCheckUtils
					.isNullOrEmpty(paymentSubmissionResponse.getData().getInitiation().getNumberOfPayments())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PISP_INVALID_COMBINATION));
			}
		}

	}

	private void executeInternationalSubmissionResponseSwaggerValidations(
			CustomIStandingOrderPOSTResponse paymentSubmissionResponse) {
		psd2Validator.validate(paymentSubmissionResponse);
	}

}
