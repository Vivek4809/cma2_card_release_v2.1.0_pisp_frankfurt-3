
package com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.client;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class SCAAuthenticationRetrieveFoundationServiceClient {

	@Autowired
	@Qualifier("restClientSecurityFoundation")
	private RestClientSync restClient;

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${foundationService.maskRequest:#{false}}")
	private boolean maskRequest;
	
	@Value("${foundationService.maskResponse:#{false}}")
	private boolean maskResponse;
	
	@Autowired
	private DataMask dataMask;
	
	@PostConstruct
	private void init() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        converter.setObjectMapper(mapper);
        restTemplate.getMessageConverters().add(0,converter);
	}
	
	public AuthenticationParameters restTransportForRetrieveAuthenticationService(RequestInfo requestInfo, 
			Class<AuthenticationParameters> authenticationParametersResponse, MultiValueMap<String, String> params, HttpHeaders httpHeaders) {
		AuthenticationParameters authenticationParameters = new AuthenticationParameters();
		requestInfo.setUrl(UriComponentsBuilder.fromHttpUrl(requestInfo.getUrl()).queryParams(params).build().toString());
		authenticationParameters = restClient.callForGet(requestInfo, authenticationParametersResponse, httpHeaders);
		if(maskResponse){
			authenticationParameters = dataMask.maskResponse(authenticationParameters, "restTransportForRetrieveAuthenticationService");
		System.out.println("authenticationParameters:\n"+authenticationParameters);
	}
		
		return authenticationParameters;
	}

}
