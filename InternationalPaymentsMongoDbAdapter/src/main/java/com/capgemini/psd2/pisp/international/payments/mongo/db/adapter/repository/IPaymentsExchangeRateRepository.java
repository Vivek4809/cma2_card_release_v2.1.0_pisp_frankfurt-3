package com.capgemini.psd2.pisp.international.payments.mongo.db.adapter.repository;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.pisp.international.payments.mongo.db.adapter.domain.ExchangeRateDetails;

public interface IPaymentsExchangeRateRepository extends MongoRepository<ExchangeRateDetails, String> {
	public List<ExchangeRateDetails> findAll();
}
