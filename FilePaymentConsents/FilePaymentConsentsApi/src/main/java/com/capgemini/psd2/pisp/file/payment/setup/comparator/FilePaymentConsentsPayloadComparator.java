package com.capgemini.psd2.pisp.file.payment.setup.comparator;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteFileConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class FilePaymentConsentsPayloadComparator implements Comparator<OBWriteFileConsentResponse1> {

	@Override
	public int compare(OBWriteFileConsentResponse1 response,
			OBWriteFileConsentResponse1 adaptedPaymentConsentsResponse) {
		int value = 1;

		if (Double.compare(Double.parseDouble(response.getData().getInitiation().getControlSum().toString()) ,Double
				.parseDouble(adaptedPaymentConsentsResponse.getData().getInitiation().getControlSum().toString()))==0) {
			adaptedPaymentConsentsResponse.getData().getInitiation()
					.setControlSum(new BigDecimal(response.getData().getInitiation().getControlSum().toString()));
		}

		if (response.getData().getInitiation().equals(adaptedPaymentConsentsResponse.getData().getInitiation()))
			value = 0;

		OBAuthorisation1 responseAuth = response.getData().getAuthorisation();
		OBAuthorisation1 adaptedResponseAuth = adaptedPaymentConsentsResponse.getData().getAuthorisation();

		if (!NullCheckUtils.isNullOrEmpty(responseAuth) && !NullCheckUtils.isNullOrEmpty(adaptedResponseAuth)
				&& value != 1) {
			if (responseAuth.equals(adaptedResponseAuth))
				value = 0;
			else
				value = 1;
		}

		return value;

	}

	public int compareFilePaymentDetails(CustomFilePaymentConsentsPOSTResponse response,
			CustomFilePaymentSetupPOSTRequest request, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		String strPaymentConsentsRequest = JSONUtilities.getJSONOutPutFromObject(request);

		CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), strPaymentConsentsRequest,
				CustomFilePaymentConsentsPOSTResponse.class);

		if (!validateDebtorDetails(response.getData().getInitiation(),
				adaptedPaymentConsentsResponse.getData().getInitiation(), paymentSetupPlatformResource))
			return 1;

		checkAndModifyEmptyFields(adaptedPaymentConsentsResponse, response);

		if (!NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation())
				&& (NullCheckUtils.isNullOrEmpty(adaptedPaymentConsentsResponse.getData().getAuthorisation()))) {
			return 1;
		}

		if (NullCheckUtils.isNullOrEmpty(response.getData().getAuthorisation())
				&& !NullCheckUtils.isNullOrEmpty(adaptedPaymentConsentsResponse.getData().getAuthorisation())) {
			return 1;
		}

		return compare(response, adaptedPaymentConsentsResponse);

	}

	private boolean validateDebtorDetails(OBFile1 obj1, OBFile1 obj2,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {
			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be
			 * true, then name will participate in comparison else product will
			 * set foundation response DebtorAccount.Name as null so that it can
			 * be passed in comparison. Both fields would have the value as
			 * 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails())) {
				obj1.getDebtorAccount().setName(null);
			}
			return Objects.equals(obj1.getDebtorAccount(), obj2.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()) && obj2.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()) && obj1.getDebtorAccount() != null) {
			obj1.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	private void checkAndModifyEmptyFields(CustomFilePaymentConsentsPOSTResponse adaptedPaymentConsentsResponse,
			CustomFilePaymentConsentsPOSTResponse response) {

		checkAndModifyRemittanceInformation(adaptedPaymentConsentsResponse.getData().getInitiation(), response);
	}

	private void checkAndModifyRemittanceInformation(OBFile1 obFile1, CustomFilePaymentConsentsPOSTResponse response) {

		OBRemittanceInformation1 remittanceInformation = obFile1.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			obFile1.setRemittanceInformation(null);
		}
	}

}
