package com.capgemini.psd2.pisp.file.payment.test.setup.utilities;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.impl.FPaymentConsentsMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.repository.FPaymentConsentsChargesDataRepository;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.repository.FPaymentConsentsDataRepository;
import com.capgemini.psd2.pisp.file.payment.setup.utilities.FilePaymentConsentsUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentConsentsUtilitiesTest {

	private MockMvc mockMvc;

	@InjectMocks
	private FilePaymentConsentsUtilities utilities;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(utilities).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testBase64EncodeHash() throws Exception {

		byte[] hash = new byte[100];
		utilities.base64EncodeHash(hash);
	}

	@Test
	public void testBase64DecodeHash() throws Exception {

		utilities.base64DecodeHash("wkjrbkw823rneanf932");
	}

	/*@Test
	public void testGenerateFileHash() throws Exception {

		MultipartFile paymentFile = new MultipartFile() {

			@Override
			public void transferTo(File dest) throws IOException, IllegalStateException {
				// TODO Auto-generated method stub

			}

			@Override
			public boolean isEmpty() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public long getSize() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String getOriginalFilename() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getName() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getContentType() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public byte[] getBytes() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
		};

		byte[] value = new byte[100];
		//Mockito.when(DigestUtils.sha256(paymentFile.getBytes())).thenReturn(value);
		//doNothing().when(paymentFile.getBytes());

		utilities.generateFileHash(paymentFile);
	}
*/
	
}
