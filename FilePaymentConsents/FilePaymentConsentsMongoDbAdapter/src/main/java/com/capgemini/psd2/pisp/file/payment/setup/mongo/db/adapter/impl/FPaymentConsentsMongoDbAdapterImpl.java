package com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus2Code;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.config.PaymentConsentAwsObject;
import com.capgemini.psd2.pisp.file.payment.setup.mongo.db.adapter.repository.FPaymentConsentsDataRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationConfig;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("filePaymentConsentsStagingMongoDbAdapter")
public class FPaymentConsentsMongoDbAdapterImpl implements FilePaymentConsentsAdapter {

	@Autowired
	private FPaymentConsentsDataRepository fPaymentConsentsDataRepository;

	@Autowired
	private ClientConfiguration clientConfig;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private SandboxValidationConfig sandboxValidation;

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;
	
	@Value("${app.awsRegion:null}")
	private String awsRegion;

	@Value("${app.awsS3BucketName:null}")
	private String awsS3BucketName;
	
	@Value("${app.awsS3BucketFilePath:null}")
	private String fileFolder;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Autowired
	private PaymentConsentAwsObject awsObject;

	@Value("${app.sandboxValidationPolicies.charges.borneByCreditor:null}")
	private String borneByCreditorRegex;

	@Value("${app.sandboxValidationPolicies.charges.borneByDebtor:null}")
	private String borneByDebtorRegex;

	@Value("${app.sandboxValidationPolicies.charges.followingServiceLevel:null}")
	private String followServiceLevelRegex;

	@Value("${app.sandboxValidationPolicies.charges.shared:null}")
	private String sharedRegex;

	@Value("${app.sandboxValidationPolicies.chargeAmount:null}")
	private String chargeAmount;
	
	private String decimalPlaces = "%.2f";

	@Override
	public CustomFilePaymentConsentsPOSTResponse createStagingFilePaymentConsents(
			CustomFilePaymentSetupPOSTRequest customRequest, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {

		CustomFilePaymentConsentsPOSTResponse paymentConsentsBankResource = transformFilePaymentConsentsToBankResource(
				customRequest);
		String initiationAmount = null;
		String currency = (sandboxValidation != null && sandboxValidation.getChargeCurrency() != null)
				? sandboxValidation.getChargeCurrency()
				: "EUR";

		if (customRequest.getData().getInitiation().getControlSum() != null)
			initiationAmount = customRequest.getData().getInitiation().getControlSum().toString();

		// Sandbox Validation for POST

		if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_POST_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		String consentId = null;
		String cutOffDateTime = utility.getMockedCutOffDtTm();
		ProcessConsentStatusEnum processConsentStatusEnum = utility.getMockedConsentProcessExecStatus(currency,
				initiationAmount);
		

		if (processConsentStatusEnum == ProcessConsentStatusEnum.PASS
				|| processConsentStatusEnum == ProcessConsentStatusEnum.FAIL)
			consentId = UUID.randomUUID().toString();

		paymentConsentsBankResource.getData().setCreationDateTime(customRequest.getCreatedOn());
		paymentConsentsBankResource.getData().setConsentId(consentId);
		
		// removing charges block as this is not returned for BOI on domestic payment APIs
		if(!isSandboxEnabled){
			List<OBCharge1> charges = getMockedFilePaymentsCharges(currency, initiationAmount);
			paymentConsentsBankResource.getData().setCharges(charges);
		}
		
		paymentConsentsBankResource.getData().setCutOffDateTime(cutOffDateTime);
		paymentConsentsBankResource.setConsentProcessStatus(processConsentStatusEnum);

		if (consentId != null) {
			OBExternalConsentStatus2Code status = calculateCMAStatus(processConsentStatusEnum);
			paymentConsentsBankResource.getData().setStatus(status);
			paymentConsentsBankResource.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			return fPaymentConsentsDataRepository.save(paymentConsentsBankResource);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_TECHNICAL_ERROR_FS_PRE_STAGE_INSERTION));
		}
	}

	@Override
	public CustomFilePaymentConsentsPOSTResponse fetchFilePaymentMetadata(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {
		try {

			CustomFilePaymentConsentsPOSTResponse customFilePaymentConsentsPOSTResponse = fPaymentConsentsDataRepository
					.findOneByDataConsentId(stageIdentifiers.getPaymentConsentId());

			// Sandbox Mocking for GET
			if (reqAttributes.getMethodType().equals("GET") && utility.isValidAmount(
					customFilePaymentConsentsPOSTResponse.getData().getInitiation().getControlSum().toString(),
					PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}

			return customFilePaymentConsentsPOSTResponse;

		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE));
		}
	}

	private CustomFilePaymentConsentsPOSTResponse transformFilePaymentConsentsToBankResource(
			CustomFilePaymentSetupPOSTRequest paymentConsentsRequest) {
		String paymentFoundationRequest = JSONUtilities.getJSONOutPutFromObject(paymentConsentsRequest);
		return JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(), paymentFoundationRequest,
				CustomFilePaymentConsentsPOSTResponse.class);
	}

	private List<OBCharge1> getMockedFilePaymentsCharges(String currency, String controlSum) {
		List<OBCharge1> charges = new ArrayList<>();
		OBCharge1 obCharge1 = new OBCharge1();
		OBChargeBearerType1Code obj = null;
		List<OBCharge1> charges1 =null;
		if (controlSum != null)
			if (Pattern.compile(borneByCreditorRegex).matcher(controlSum).find()) {
				obj = OBChargeBearerType1Code.BORNEBYCREDITOR;
			} else if (Pattern.compile(borneByDebtorRegex).matcher(controlSum).find()) {
				obj = OBChargeBearerType1Code.BORNEBYDEBTOR;
			} else if (Pattern.compile(followServiceLevelRegex).matcher(controlSum).find()) {
				obj = OBChargeBearerType1Code.FOLLOWINGSERVICELEVEL;
			} else if (Pattern.compile(sharedRegex).matcher(controlSum).find()) {
				obj = OBChargeBearerType1Code.SHARED;
			}

		if (obj != null) {
			double charge = Double.parseDouble(chargeAmount) / 100;
			double chargeLimit = Double.parseDouble(sandboxValidation.getChargeAmountLimit());
			Double d = Double.parseDouble(controlSum) * charge;
			String chargeAmount = null;
			if( d > chargeLimit){
				chargeAmount = Double.toString(chargeLimit);
			}else{
				chargeAmount = String.format(decimalPlaces, d);
			}
			OBCharge1Amount amount = new OBCharge1Amount();
			amount.setAmount(chargeAmount);
			amount.setCurrency(sandboxValidation.getChargeCurrency());
			obCharge1.setAmount(amount);
			obCharge1.setType("Type1");
            obCharge1.setChargeBearer(obj);
			charges.add(obCharge1);
		} else {
			return charges1;
		}
		return charges;
	}

	@Override
	public String uploadFilePaymentConsents(MultipartFile paymentFile, String consentId) {

		try {

			AmazonS3 s3client = null;

			if (clientConfig != null) {
				s3client = awsObject.getPaymentAwsClient(null, clientConfig, awsRegion);
			} else {
				/*s3client = AmazonS3ClientBuilder.standard()
						.withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).withRegion(awsRegion)
						.build();*/
				s3client = AmazonS3ClientBuilder.standard()
						.withCredentials(DefaultAWSCredentialsProviderChain.getInstance()).withRegion(awsRegion)
						.build();
				
			}

			String fileName=fileFolder.concat(consentId);
			
			ObjectMetadata data = new ObjectMetadata();
			data.setContentType(paymentFile.getContentType());
			data.setContentLength(paymentFile.getSize());

			s3client.putObject(new PutObjectRequest(awsS3BucketName, fileName, paymentFile.getInputStream(), data));

			return consentId;
		} catch (Exception exp) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_PAYMENT_SETUP_CREATION_FAILED));
		}

	}

	@Override
	public CustomFilePaymentConsentsPOSTResponse updateStagingFilePaymentConsents(
			CustomFilePaymentConsentsPOSTResponse response) {

		try {
			return fPaymentConsentsDataRepository.save(response);
		} catch (Exception exp) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_PAYMENT_SETUP_CREATION_FAILED));
		}

	}

	@Override
	public PlatformFilePaymentsFileResponse downloadFilePaymentConsents(
			CustomPaymentStageIdentifiers paymentStageIdentifiers) {

		try {

			String consentId = paymentStageIdentifiers.getPaymentConsentId();
			PlatformFilePaymentsFileResponse platformFilePaymentsFileResponse = new PlatformFilePaymentsFileResponse();
			S3Object fullObject = null;
			AmazonS3 s3client = null;

			if (clientConfig != null) {
				s3client = awsObject.getPaymentAwsClient(null, clientConfig, awsRegion);
			} else {
				s3client = AmazonS3ClientBuilder.standard()
						.withCredentials(DefaultAWSCredentialsProviderChain.getInstance()).withRegion(awsRegion)
						.build();
			}

			String fileName=fileFolder.concat(consentId);
			
			fullObject = awsObject.getPaymentAwsObjectS3Object(s3client, awsS3BucketName, fileName);

			if (fullObject != null && fullObject.getObjectContent() != null && fullObject.getObjectMetadata() != null) {

				byte[] targetArray = getStringFromInputStream(fullObject.getObjectContent());
				platformFilePaymentsFileResponse.setFileByteArray(targetArray);
				platformFilePaymentsFileResponse.setFileName(consentId);
				platformFilePaymentsFileResponse.setFileType(fullObject.getObjectMetadata().getContentType());

				return platformFilePaymentsFileResponse;
			} else {
				return null;
			}

		} catch (Exception exp) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE));
		}

	}

	private byte[] getStringFromInputStream(InputStream input) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE));
		}

		return sb.toString().getBytes();
	}

	private OBExternalConsentStatus2Code calculateCMAStatus(ProcessConsentStatusEnum processConsentStatusEnum) {
		return (processConsentStatusEnum == ProcessConsentStatusEnum.PASS)
				? OBExternalConsentStatus2Code.AWAITINGUPLOAD
				: OBExternalConsentStatus2Code.REJECTED;
	}
}