package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;

public interface IPaymentConsentsFoundationRepository extends MongoRepository<CustomIPaymentConsentsPOSTResponse, String>{

	  public CustomIPaymentConsentsPOSTResponse findOneByDataConsentId(String consentId );	
}
