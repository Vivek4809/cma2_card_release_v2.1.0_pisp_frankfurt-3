package com.capgemini.psd2.revoke.consent.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.OBExternalPermissions1Code;
import com.capgemini.psd2.aisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1;
import com.capgemini.psd2.aisp.domain.OBReadDataResponse1.PermissionsEnum;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.internal.apis.adapter.RevokeConsentAdapter;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeAccountRequestCMA3Repository;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeAccountRequestRepository;
import com.capgemini.psd2.utilities.DateUtilites;
import com.mongodb.WriteResult;

@Component("revokeConsentMongoDbAdaptor")
public class RevokeConsentMongoDbAdaptorImpl implements RevokeConsentAdapter {

	@Autowired
	private RevokeAccountRequestCMA3Repository accountRequestRepository;

	@Autowired
	private RevokeAccountRequestRepository accountRequestRepositoryV2;

	@Autowired
	private AispConsentAdapterImpl aispConsentAdapter;

	@Autowired
	MongoTemplate mongoTemplate;

	private Boolean resourceLocated;

	@Override
	public void revokeConsentRequest(String consentId, String cId) {
		resourceLocated = false;
		resourceLocated = fetchAndUpdateAisp(consentId, cId, resourceLocated);
		if (resourceLocated == false)
			resourceLocated = fetchAndUpdatePisp(consentId, cId, resourceLocated);
		if (resourceLocated == false)
			resourceLocated = fetchAndUpdateCisp(consentId, cId, resourceLocated);

		if (resourceLocated == false) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.NO_CONSENT_ID_FOUND));
		}

	}

	private Boolean fetchAndUpdateCisp(String consentId, String cId, Boolean resourceLocated2) {
		if (resourceLocated2)
			return true;

		Query query = new Query(Criteria.where("consentId").is(consentId));
		Update update = new Update();
		update.set("status", com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code.REVOKED);
		update.set("statusUpdateDateTime", DateUtilites.getCurrentDateInISOFormat());

		WriteResult result = mongoTemplate.updateFirst(query, update, OBFundsConfirmationConsentDataResponse1.class);

		if (result.getN() != 0) {
			resourceLocated = true;
			query = new Query(Criteria.where("fundsIntentId").is(consentId));
			update = new Update();
			update.set("status", ConsentStatusEnum.REVOKED);

			mongoTemplate.updateFirst(query, update, CispConsent.class);
		}
		return resourceLocated;
	}

	private Boolean fetchAndUpdatePisp(String consentId, String cId, Boolean resourceLocated2) {
		if (resourceLocated2)
			return true;

		Query query = new Query();
		Criteria criteria = new Criteria().orOperator(Criteria.where("paymentConsentId").is(consentId),
				Criteria.where("paymentId").is(consentId));
		query.addCriteria(criteria);

		Update update = new Update();
		update.set("status", OBExternalConsentStatus1Code.REJECTED);
		update.set("statusUpdateDateTime", DateUtilites.getCurrentDateInISOFormat());
		update.set("statusUpdatedDescription", PSD2Constants.REVOKED_REASON);

		WriteResult result = mongoTemplate.updateFirst(query, update, PaymentConsentsPlatformResource.class);

		if (result.getN() != 0) {
			resourceLocated = true;
			query = new Query(Criteria.where("paymentId").is(consentId));
			update = new Update();
			update.set("status", ConsentStatusEnum.REVOKED);

			mongoTemplate.updateFirst(query, update, PispConsent.class);

		}
		return resourceLocated;
	}

	private Boolean fetchAndUpdateAisp(String consentId, String cId, Boolean resourceLocated2) {
		if (resourceLocated2) {
			return true;
		} else {
			OBReadConsentResponse1Data data = fetchAndUpdateAispSetup(consentId, cId);
			if (data != null) {
				/* Update status in AISP Consent */
				fetchAndUpdateAispConsent(consentId, cId);
				resourceLocated = true;
			}
		}
		return resourceLocated;
	}

	@SuppressWarnings("unused")
	private OBReadConsentResponse1Data fetchAndUpdateAispSetup(String consentId, String cid) {
		OBReadConsentResponse1Data data = null;
		OBReadDataResponse1 dataV2 = null;
		try {
			data = accountRequestRepository.findByConsentId(consentId);

			if (data != null)
				resourceLocated = Boolean.TRUE;
			if(data != null && data.getStatus() == OBExternalRequestStatus1Code.REVOKED)
				return null;
			else if (data == null && !resourceLocated) {
				dataV2 = accountRequestRepositoryV2.findByAccountRequestId(consentId);
				if (dataV2 != null) {
					data = transformAccountInformation(dataV2, new OBReadConsentResponse1Data(), null);
					resourceLocated = Boolean.TRUE;
				}
			}
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		if (resourceLocated) {
			if(data != null) {
			data.setStatus(OBExternalRequestStatus1Code.REVOKED);
			if (data.getCmaVersion() != null)
				data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
			}
			try {
				data = accountRequestRepository.save(data);
			} catch (DataAccessResourceFailureException e) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CONNECTION_ERROR));
			}
		}
		return data;
	}

	private void fetchAndUpdateAispConsent(String consentId, String cid) {
		AispConsent consentData = null;
		consentData = aispConsentAdapter.retrieveConsentByAccountRequestId(consentId);

		if (consentData != null && !ConsentStatusEnum.REVOKED.equals(consentData.getStatus())) {
			consentData.setStatus(ConsentStatusEnum.REVOKED);
			aispConsentAdapter.updateConsent(consentData);
		}
	}

	private <T> OBReadConsentResponse1Data transformAccountInformation(T source, OBReadConsentResponse1Data destination,
			Map<String, String> params) {
		OBReadDataResponse1 accCma2Response = (OBReadDataResponse1) source;
		destination.setCmaVersion(accCma2Response.getCmaVersion());
		destination.setConsentId(accCma2Response.getAccountRequestId());
		destination.setCreationDateTime(accCma2Response.getCreationDateTime());
		destination.setExpirationDateTime(accCma2Response.getExpirationDateTime());
		destination.setStatusUpdateDateTime(accCma2Response.getStatusUpdateDateTime());
		destination.setTransactionFromDateTime(accCma2Response.getTransactionFromDateTime());
		destination.setTransactionToDateTime(accCma2Response.getTransactionToDateTime());
		List<OBExternalPermissions1Code> persmissions = new ArrayList<>();
		for (PermissionsEnum permission : accCma2Response.getPermissions()) {
			persmissions.add(OBExternalPermissions1Code.fromValue(permission.toString()));
		}
		destination.setPermissions(persmissions);
		destination.setStatus(OBExternalRequestStatus1Code.fromValue(accCma2Response.getStatus().toString()));
		return destination;
	}

}