package com.capgemini.psd2.international.payments.consents.mock.foundationservice.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.constants.InternationalPaymentConsentsFoundationConstants;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.domain.PaymentInstructionCharge;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.domain.ProposalStatus;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.repository.InternationalPaymentConsentsRepository;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.service.InternationalPaymentConsentsService;

@Service
public class InternationalPaymentConsentsServiceImpl implements InternationalPaymentConsentsService {
	/** The repository. */
	@Autowired
	private InternationalPaymentConsentsRepository repository;
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Override
	public PaymentInstructionProposalInternational retrieveInternationalPaymentConsentsResource(
			String paymentInstructionProposalId) throws Exception {
		PaymentInstructionProposalInternational response = repository
				.findByPaymentInstructionProposalId(paymentInstructionProposalId);
		if (null == response) {
			throw new RecordNotFoundException(InternationalPaymentConsentsFoundationConstants.RECORD_NOT_FOUND);
		}
		return response;
	}

	@Override
	public PaymentInstructionProposalInternational createInternationalPaymentConsentsResource(
			PaymentInstructionProposalInternational paymentInstProposalReq) throws Exception {
		String consentID = null;
		if (paymentInstProposalReq == null) {
			throw new RecordNotFoundException(InternationalPaymentConsentsFoundationConstants.RECORD_NOT_FOUND);
		}

		consentID = UUID.randomUUID().toString();
		paymentInstProposalReq.setPaymentInstructionProposalId(consentID);

		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		// Amount amount = new Amount();
		// charge.setChargeBearer(ChargeBearer.BORNEBYDEBTOR);
		// charge.setType("UK.OBIE.ChapsOut"); amount.setTransactionCurrency(500.01);
		// charge.setAmount(amount); Currency currency= new Currency();
		/// currency.setIsoAlphaCode("GBP"); charge.setCurrency(currency);
		// List<PaymentInstructionCharge> List= new ArrayList(); List.add(charge);

		// paymentInstProposalReq.setCharges(List);
		if (paymentInstProposalReq.getExchangeRateQuote() != null
				&& (paymentInstProposalReq.getExchangeRateQuote().getRateQuoteType() != null) && (paymentInstProposalReq
						.getExchangeRateQuote().getRateQuoteType().getValue().equalsIgnoreCase("Agreed"))) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PMPSV);
		}

		try {
			Date date = Calendar.getInstance().getTime();
			String strDate = timeZoneDateTimeAdapter.parseDateTimeCMA(date);
			paymentInstProposalReq.setProposalCreationDatetime(strDate);
			paymentInstProposalReq.setProposalStatusUpdateDatetime(strDate);
		} catch (Exception e) {
		}

		// Simulation for Rejected Status
		if (paymentInstProposalReq.getInstructionReference() != null && paymentInstProposalReq.getInstructionReference()
				.equalsIgnoreCase(InternationalPaymentConsentsFoundationConstants.INSTRUCTION_ENDTOEND_REF)) {
			paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
		} else {
			paymentInstProposalReq.setProposalStatus(ProposalStatus.AwaitingAuthorisation);
			if (null != paymentInstProposalReq.getChannel()
					&& null != paymentInstProposalReq.getChannel().getBrandCode()) {

				if (paymentInstProposalReq.getChannel().getBrandCode().toString().equalsIgnoreCase("BOI-ROI")) {
					if (paymentInstProposalReq.getProposingPartyAccount().getSchemeName()
							.equalsIgnoreCase("UK.OBIE.SortCodeAccountNumber")
							|| paymentInstProposalReq.getProposingPartyAccount().getSchemeName()
									.equalsIgnoreCase("SortCodeAccountNumber")) {
						paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
					} else if (null != paymentInstProposalReq.getAuthorisingPartyAccount()
							&& null != paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()) {
						if (paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()
								.equalsIgnoreCase("UK.OBIE.SortCodeAccountNumber")
								|| paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()
										.equalsIgnoreCase("SortCodeAccountNumber")) {
							paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
						}

					}
				}

			}
		}

		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;
	}

	@Override
	public PaymentInstructionProposalInternational retrieveAccountInformation(String paymentInstructionProposalId)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
