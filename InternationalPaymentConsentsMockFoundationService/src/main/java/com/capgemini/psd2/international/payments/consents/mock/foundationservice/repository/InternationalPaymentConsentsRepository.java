package com.capgemini.psd2.international.payments.consents.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.capgemini.psd2.international.payments.consents.mock.foundationservice.domain.PaymentInstructionProposalInternational;

public interface InternationalPaymentConsentsRepository extends MongoRepository<PaymentInstructionProposalInternational, String> {
	/**
	 * Find by paymentInstructionProposalId
	 * @return the PaymentInstructionProposalInternational
	 */

	
	public PaymentInstructionProposalInternational findByPaymentInstructionProposalId (String paymentInstructionProposalId);
}
