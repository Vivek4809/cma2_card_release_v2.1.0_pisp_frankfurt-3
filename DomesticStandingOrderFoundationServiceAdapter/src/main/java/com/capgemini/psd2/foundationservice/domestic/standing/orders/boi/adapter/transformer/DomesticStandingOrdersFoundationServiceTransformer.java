package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.transformer;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.frequency.utility.FrequencyUtil;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Frequency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentInstructionStatusCode2;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentTransaction;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderResponse1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticStandingOrdersFoundationServiceTransformer {
	
	@Autowired
	private FrequencyUtil frequencyUtil;	
	
	public StandingOrderInstructionComposite transformDomesticStandingOrdersResponseFromAPIToFDForInsert(
			CustomDStandingOrderPOSTRequest standingOrderRequest) {
		
		StandingOrderInstructionComposite standingOrderInstructionComposite=new StandingOrderInstructionComposite();
		StandingOrderInstructionProposal standingOrderInstructionProposal= new StandingOrderInstructionProposal();
		
		//Initiation
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation()))
		{
			if(!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getNumberOfPayments()))
			{
				standingOrderInstructionProposal.setNumberOfPayments(Double.valueOf(standingOrderRequest.getData().getInitiation().getNumberOfPayments()));
			}
			standingOrderInstructionProposal.setPaymentInstructionProposalId(standingOrderRequest.getData().getConsentId());
			standingOrderInstructionProposal.setReference(standingOrderRequest.getData().getInitiation().getReference());
			standingOrderInstructionProposal.setFirstPaymentDateTime(standingOrderRequest.getData().getInitiation().getFirstPaymentDateTime());
			standingOrderInstructionProposal.setRecurringPaymentDateTime(standingOrderRequest.getData().getInitiation().getRecurringPaymentDateTime());
			standingOrderInstructionProposal.setFinalPaymentDateTime(standingOrderRequest.getData().getInitiation().getFinalPaymentDateTime());
		}
		
		//FirstPaymentAmount
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getFirstPaymentAmount()))
		{
			PaymentTransaction firstPaymentAmount=new PaymentTransaction();
			FinancialEventAmount financialEventAmount=new FinancialEventAmount();
			Currency transactionCurrency=new Currency();
			
			financialEventAmount.setTransactionCurrency(Double.valueOf(standingOrderRequest.getData().getInitiation().getFirstPaymentAmount().getAmount()));
			transactionCurrency.setIsoAlphaCode(standingOrderRequest.getData().getInitiation().getFirstPaymentAmount().getCurrency());
			
			firstPaymentAmount.setFinancialEventAmount(financialEventAmount);
			firstPaymentAmount.setTransactionCurrency(transactionCurrency);
			
			standingOrderInstructionProposal.setFirstPaymentAmount(firstPaymentAmount);
		}
		
		//FinalPaymentAmount
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getFinalPaymentAmount()))
		{
			PaymentTransaction finalPaymentAmount=new PaymentTransaction();
			FinancialEventAmount financialEventAmount=new FinancialEventAmount();
			Currency transactionCurrency=new Currency();
			
			financialEventAmount.setTransactionCurrency(Double.valueOf(standingOrderRequest.getData().getInitiation().getFinalPaymentAmount().getAmount()));
			transactionCurrency.setIsoAlphaCode(standingOrderRequest.getData().getInitiation().getFinalPaymentAmount().getCurrency());
			
			finalPaymentAmount.setFinancialEventAmount(financialEventAmount);
			finalPaymentAmount.setTransactionCurrency(transactionCurrency);
			
			standingOrderInstructionProposal.setFinalPaymentAmount(finalPaymentAmount);
		}
		
		//RecurringPaymentAmount
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getRecurringPaymentAmount()))
		{
			PaymentTransaction recurringPaymentAmount=new PaymentTransaction();
			FinancialEventAmount financialEventAmount1=new FinancialEventAmount();
			Currency transactionCurrency1=new Currency();
			
			financialEventAmount1.setTransactionCurrency(Double.valueOf(standingOrderRequest.getData().getInitiation().getRecurringPaymentAmount().getAmount()));
			transactionCurrency1.setIsoAlphaCode(standingOrderRequest.getData().getInitiation().getRecurringPaymentAmount().getCurrency());
			
			recurringPaymentAmount.setFinancialEventAmount(financialEventAmount1);
			recurringPaymentAmount.setTransactionCurrency(transactionCurrency1);
			
			standingOrderInstructionProposal.setRecurringPaymentAmount(recurringPaymentAmount);
		}
		
		//DebtorAccount
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getDebtorAccount()))
		{
			AuthorisingPartyAccount authorisingPartyAccount=new AuthorisingPartyAccount();
			authorisingPartyAccount.setSchemeName(standingOrderRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
			authorisingPartyAccount.setAccountIdentification(standingOrderRequest.getData().getInitiation().getDebtorAccount().getIdentification());
			authorisingPartyAccount.setAccountName(standingOrderRequest.getData().getInitiation().getDebtorAccount().getName());
			authorisingPartyAccount.setSecondaryIdentification(standingOrderRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification());
			
			standingOrderInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		}
		
		//CreditorAccount
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getCreditorAccount()))
		{
			ProposingPartyAccount proposingPartyAccount=new ProposingPartyAccount();
			proposingPartyAccount.setSchemeName(standingOrderRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
			proposingPartyAccount.setAccountIdentification(standingOrderRequest.getData().getInitiation().getCreditorAccount().getIdentification());
			proposingPartyAccount.setAccountName(standingOrderRequest.getData().getInitiation().getCreditorAccount().getName());
			proposingPartyAccount.setSecondaryIdentification(standingOrderRequest.getData().getInitiation().getCreditorAccount().getSecondaryIdentification());
			
			standingOrderInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		}
		
		// Frequency
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getFrequency())
				&& !NullCheckUtils
						.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getFirstPaymentDateTime())) {
			String paymentStartDate = standingOrderRequest.getData().getInitiation().getFirstPaymentDateTime();
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date firstPaymentDate = simpleDateFormat.parse(paymentStartDate);
				String frequencyCmaToProcessLayer = frequencyUtil
						.CmaToProcessLayer(standingOrderRequest.getData().getInitiation().getFrequency(), firstPaymentDate);
				standingOrderInstructionProposal.setFrequency(Frequency.fromValue(frequencyCmaToProcessLayer));
			} catch (ParseException pe) {
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
			}
		}
				
		//Risk
		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor=new PaymentInstrumentRiskFactor();
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk()))
		{
			paymentInstrumentRiskFactor.setPaymentContextCode(standingOrderRequest.getRisk().getPaymentContextCode().toString());
			paymentInstrumentRiskFactor.setMerchantCategoryCode(standingOrderRequest.getRisk().getMerchantCategoryCode());
			paymentInstrumentRiskFactor.setMerchantCustomerIdentification(standingOrderRequest.getRisk().getMerchantCustomerIdentification());
			
			standingOrderInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		}
		
		//DeliveryAddress
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk().getDeliveryAddress())) {
			
			List<String> addline=standingOrderRequest.getRisk().getDeliveryAddress().getAddressLine();
			
			Address counterpartyAddress=new Address();
			
			if(addline.size()>0)
			{
				counterpartyAddress.setFirstAddressLine(addline.get(0));
				
			}
			if(addline.size()>1)
			{
				counterpartyAddress.setSecondAddressLine(addline.get(1));	
			}
			counterpartyAddress.setGeoCodeBuildingName(standingOrderRequest.getRisk().getDeliveryAddress().getStreetName());
			counterpartyAddress.setGeoCodeBuildingNumber(standingOrderRequest.getRisk().getDeliveryAddress().getBuildingNumber());
			counterpartyAddress.setPostCodeNumber(standingOrderRequest.getRisk().getDeliveryAddress().getPostCode());
			counterpartyAddress.setThirdAddressLine(standingOrderRequest.getRisk().getDeliveryAddress().getTownName());
			counterpartyAddress.setFourthAddressLine(standingOrderRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
			if(!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk().getDeliveryAddress().getCountry()))
			{
				Country addressCountry=new Country();
				addressCountry.setIsoCountryAlphaTwoCode(standingOrderRequest.getRisk().getDeliveryAddress().getCountry());
				counterpartyAddress.setAddressCountry(addressCountry);
			}
			
			paymentInstrumentRiskFactor.setCounterPartyAddress(counterpartyAddress);
		}
		
		standingOrderInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		standingOrderInstructionComposite.setPaymentInstructionProposal(standingOrderInstructionProposal);
	
		return standingOrderInstructionComposite;
		}
	
	/********************************************************************
	***** Function Name		:	transformDomesticStandingOrdersResponseFromFDToAPIForInsert
	***** Arguments			:	T
	***** Return Type		:	DStandingOrderPOST201Response
	***** Use of Function	:	It is use to retrieve/Get data from FS for payment-instruction-id
	***** Creation Date		:	10-Feb-2019
	***** Modification Date	:	10-Feb-2019
	*********************************************************************/
	public <T> DStandingOrderPOST201Response transformDomesticStandingOrdersResponseFromFDToAPIForInsert(
			T standingOrderInstructionProposalResponse) {
		
		
		DStandingOrderPOST201Response dstandingOrderPOST201Response=new DStandingOrderPOST201Response();
		
		StandingOrderInstructionComposite standingOrderInstructionComposite = (StandingOrderInstructionComposite) standingOrderInstructionProposalResponse;
		
		OBWriteDataDomesticStandingOrderResponse1 oBWriteDataDomesticStandingOrderResponse1=new OBWriteDataDomesticStandingOrderResponse1();
		populateFDToAPIResponse(standingOrderInstructionComposite, oBWriteDataDomesticStandingOrderResponse1);
		
		PaymentInstructionStatusCode2 status =standingOrderInstructionComposite.getPaymentInstruction().getPaymentInstructionStatusCode();
		if(!NullCheckUtils.isNullOrEmpty(status)){
			if( (status.toString().equalsIgnoreCase("InitiationCompleted")) && (standingOrderInstructionComposite.getPaymentInstruction().getPaymentInstructionNumber() != null)) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.PASS;
				dstandingOrderPOST201Response.setProcessExecutionStatus(consentStatus);
			}else if( ((status.toString()).equalsIgnoreCase("InitiationFailed")) && (standingOrderInstructionComposite.getPaymentInstruction().getPaymentInstructionNumber()!= null)) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.FAIL;
				dstandingOrderPOST201Response.setProcessExecutionStatus(consentStatus);
			}
		}
		
		dstandingOrderPOST201Response.setData(oBWriteDataDomesticStandingOrderResponse1);
		
		return dstandingOrderPOST201Response;  
		
		}
	

	/********************************************************************
	***** Function Name		:	transformDomesticStandingOrderResponse
	***** Arguments			:	T
	***** Return Type		:	DStandingOrderPOST201Response
	***** Use of Function	:	It is use to retrieve/Get data from FS for payment-instruction-id
	***** Creation Date		:	10-Feb-2019
	***** Modification Date	:	10-Feb-2019
	*********************************************************************/
	public <T> DStandingOrderPOST201Response transformDomesticStandingOrderResponse(T standingOrderInstructionProposalResponse) 
	{
		DStandingOrderPOST201Response dstandingOrderPOST201Response=new DStandingOrderPOST201Response();
		
		StandingOrderInstructionComposite standingOrderInstructionComposite = (StandingOrderInstructionComposite) standingOrderInstructionProposalResponse;
		
		OBWriteDataDomesticStandingOrderResponse1 oBWriteDataDomesticStandingOrderResponse1=new OBWriteDataDomesticStandingOrderResponse1();
		
		populateFDToAPIResponse(standingOrderInstructionComposite, oBWriteDataDomesticStandingOrderResponse1);
		
		dstandingOrderPOST201Response.setData(oBWriteDataDomesticStandingOrderResponse1);
		
		return dstandingOrderPOST201Response;  
		
		
	}

	/**
	 * @param standingOrderInstructionComposite
	 * @param oBWriteDataDomesticStandingOrderResponse1
	 */
	private void populateFDToAPIResponse(StandingOrderInstructionComposite standingOrderInstructionComposite,
			OBWriteDataDomesticStandingOrderResponse1 oBWriteDataDomesticStandingOrderResponse1) {
		//Data
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstruction()))
			{
				oBWriteDataDomesticStandingOrderResponse1.setDomesticStandingOrderId(standingOrderInstructionComposite.getPaymentInstruction().getPaymentInstructionNumber());
				oBWriteDataDomesticStandingOrderResponse1.setConsentId(standingOrderInstructionComposite.getPaymentInstructionProposal().getPaymentInstructionProposalId());
				oBWriteDataDomesticStandingOrderResponse1.setCreationDateTime(String.valueOf(standingOrderInstructionComposite.getPaymentInstruction().getInstructionIssueDate()));
				oBWriteDataDomesticStandingOrderResponse1.setStatus(OBExternalStatus1Code.fromValue(String.valueOf(standingOrderInstructionComposite.getPaymentInstruction().getPaymentInstructionStatusCode())));
				oBWriteDataDomesticStandingOrderResponse1.setStatusUpdateDateTime(String.valueOf(standingOrderInstructionComposite.getPaymentInstruction().getInstructionStatusUpdateDateTime()));
			}
		
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal()))
		{
			oBWriteDataDomesticStandingOrderResponse1.setConsentId(standingOrderInstructionComposite.getPaymentInstructionProposal().getPaymentInstructionProposalId());
			
			//Charges
			List<OBCharge1> charges = new ArrayList<>();
			OBCharge1 oBCharge1 = new OBCharge1();
			OBCharge1Amount oBCharge1Amount = new OBCharge1Amount();
			List<PaymentInstructionCharge> paymentInstructionCharges = new ArrayList<>();
			
			//Amount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges())){
				
				paymentInstructionCharges = standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges();
				for (PaymentInstructionCharge charge : paymentInstructionCharges) {
					oBCharge1.setChargeBearer(OBChargeBearerType1Code.fromValue(String.valueOf(charge.getChargeBearer())));
					oBCharge1.setType(charge.getType());
					oBCharge1.setAmount(oBCharge1Amount);
					charges.add(oBCharge1);
				}
			}
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges())){
				oBCharge1Amount.setAmount(standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges().get(0).getAmount().getTransactionCurrency().toString());
				oBCharge1Amount.setCurrency(standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges().get(0).getCurrency().getIsoAlphaCode());
			}
	
			
			
			oBWriteDataDomesticStandingOrderResponse1.setCharges(charges);
			
			//Initiation
			OBDomesticStandingOrder1 initiation=new OBDomesticStandingOrder1();
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getNumberOfPayments()))
			{
				initiation.setNumberOfPayments(String.valueOf(standingOrderInstructionComposite.getPaymentInstructionProposal().getNumberOfPayments()));
			}
			initiation.setFrequency(String.valueOf(standingOrderInstructionComposite.getPaymentInstructionProposal().getFrequency()));
			initiation.setReference(standingOrderInstructionComposite.getPaymentInstructionProposal().getReference());
			initiation.setFirstPaymentDateTime(standingOrderInstructionComposite.getPaymentInstructionProposal().getFirstPaymentDateTime());
			initiation.setRecurringPaymentDateTime(standingOrderInstructionComposite.getPaymentInstructionProposal().getRecurringPaymentDateTime());
			initiation.setFinalPaymentDateTime(standingOrderInstructionComposite.getPaymentInstructionProposal().getFinalPaymentDateTime());
			
			//FirstPaymentAmount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getFirstPaymentAmount()))
			{
				OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount=new OBDomesticStandingOrder1FirstPaymentAmount();
				firstPaymentAmount.setAmount(BigDecimal.valueOf(standingOrderInstructionComposite.getPaymentInstructionProposal().getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency()).toPlainString());
				firstPaymentAmount.setCurrency(standingOrderInstructionComposite.getPaymentInstructionProposal().getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
			
				initiation.setFirstPaymentAmount(firstPaymentAmount);
			}
			
			//RecurringPaymentAmount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getRecurringPaymentAmount()))
			{	
				OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount=new OBDomesticStandingOrder1RecurringPaymentAmount();
				recurringPaymentAmount.setAmount(BigDecimal.valueOf(standingOrderInstructionComposite.getPaymentInstructionProposal().getRecurringPaymentAmount().getFinancialEventAmount().getTransactionCurrency()).toPlainString());
				recurringPaymentAmount.setCurrency(standingOrderInstructionComposite.getPaymentInstructionProposal().getRecurringPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
				
				initiation.setRecurringPaymentAmount(recurringPaymentAmount);
			}
			
			//FinalPaymentAmount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getFinalPaymentAmount()))
			{
				OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount=new OBDomesticStandingOrder1FinalPaymentAmount();
				finalPaymentAmount.setAmount(BigDecimal.valueOf(standingOrderInstructionComposite.getPaymentInstructionProposal().getFinalPaymentAmount().getFinancialEventAmount().getTransactionCurrency()).toPlainString());
				finalPaymentAmount.setCurrency((standingOrderInstructionComposite.getPaymentInstructionProposal().getFinalPaymentAmount().getTransactionCurrency().getIsoAlphaCode()));
				
				initiation.setFinalPaymentAmount(finalPaymentAmount);
			}
			
			//Frequency
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getFirstPaymentDateTime()) && !NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getFrequency())) {	
				String paymentStartDate=standingOrderInstructionComposite.getPaymentInstructionProposal().getFirstPaymentDateTime();
				try {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); 
					Date firstPaymentDate= simpleDateFormat.parse(paymentStartDate);
					String frequencyProcessLayerToCma=frequencyUtil.processLayerToCma(standingOrderInstructionComposite.getPaymentInstructionProposal().getFrequency().toString(), firstPaymentDate);
					initiation.setFrequency(frequencyProcessLayerToCma);
					}catch(ParseException pe) {
						throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
					}
			}
			//DebtorAccount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getAuthorisingPartyAccount()))
			{
				OBCashAccountDebtor3 accountDebtor3=new OBCashAccountDebtor3();
				accountDebtor3.setSchemeName(standingOrderInstructionComposite.getPaymentInstructionProposal().getAuthorisingPartyAccount().getSchemeName());
				accountDebtor3.setIdentification(standingOrderInstructionComposite.getPaymentInstructionProposal().getAuthorisingPartyAccount().getAccountIdentification());
				accountDebtor3.setName(standingOrderInstructionComposite.getPaymentInstructionProposal().getAuthorisingPartyAccount().getAccountName());
				accountDebtor3.setSecondaryIdentification(standingOrderInstructionComposite.getPaymentInstructionProposal().getAuthorisingPartyAccount().getSecondaryIdentification());
				
				initiation.setDebtorAccount(accountDebtor3);
			}
			
			//CreditorAccount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getProposingPartyAccount()))
			{
				OBCashAccountCreditor2 creditorAccount=new OBCashAccountCreditor2();
				creditorAccount.setSchemeName(standingOrderInstructionComposite.getPaymentInstructionProposal().getProposingPartyAccount().getSchemeName());
				creditorAccount.setIdentification(standingOrderInstructionComposite.getPaymentInstructionProposal().getProposingPartyAccount().getAccountIdentification());
				creditorAccount.setName(standingOrderInstructionComposite.getPaymentInstructionProposal().getProposingPartyAccount().getAccountName());
				creditorAccount.setSecondaryIdentification(standingOrderInstructionComposite.getPaymentInstructionProposal().getProposingPartyAccount().getSecondaryIdentification());
				
				initiation.setCreditorAccount(creditorAccount);
			}
			oBWriteDataDomesticStandingOrderResponse1.setInitiation(initiation);
		}
	} 

}
