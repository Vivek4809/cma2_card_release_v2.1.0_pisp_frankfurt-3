
package com.capgemini.psd2.pisp.domestic.payments.routing.adapter.routing;

import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;

public interface DomesticPaymentsAdapterFactory {

	public DomesticPaymentsAdapter getDomesticPaymentsAdapterInstance(String coreSystemName);
}
