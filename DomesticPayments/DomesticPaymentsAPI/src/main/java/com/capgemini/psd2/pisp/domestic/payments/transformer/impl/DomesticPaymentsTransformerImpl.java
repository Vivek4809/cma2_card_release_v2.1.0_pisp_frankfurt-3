package com.capgemini.psd2.pisp.domestic.payments.transformer.impl;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionTransformer;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("domesticPaymentsTransformer")
public class DomesticPaymentsTransformerImpl
		implements PaymentSubmissionTransformer<PaymentDomesticSubmitPOST201Response, CustomDPaymentsPOSTRequest> {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	public PaymentDomesticSubmitPOST201Response updateMetaAndLinks(
			PaymentDomesticSubmitPOST201Response submissionResponse, String methodType) {
		if (submissionResponse.getLinks() == null)
			submissionResponse.setLinks(new PaymentSetupPOSTResponseLinks());
		submissionResponse.getLinks().setSelf(PispUtilities.populateLinks(
				submissionResponse.getData().getDomesticPaymentId(), methodType, reqHeaderAtrributes.getSelfUrl()));

		if (submissionResponse.getMeta() == null)
			submissionResponse.setMeta(new PaymentSetupPOSTResponseMeta());
		return submissionResponse;
	}

	@Override
	public PaymentDomesticSubmitPOST201Response paymentSubmissionResponseTransformer(
			PaymentDomesticSubmitPOST201Response paymentSubmissionResponse,
			Map<String, Object> paymentsPlatformResourceMap, String methodType) {

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);

		/* Adding Prefix uk.obie. in creditor scheme in response */
		String creditorAccountScheme = paymentSubmissionResponse.getData().getInitiation().getCreditorAccount()
				.getSchemeName();
		if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(creditorAccountScheme)) {
			paymentSubmissionResponse.getData().getInitiation().getCreditorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(creditorAccountScheme));
		}

		/* Adding Prefix uk.obie. in debtor scheme in response */
		if (!NullCheckUtils.isNullOrEmpty(paymentSubmissionResponse.getData().getInitiation().getDebtorAccount())) {
			String debtorAccountScheme = paymentSubmissionResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();
			if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
				paymentSubmissionResponse.getData().getInitiation().getDebtorAccount()
						.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
			}
		}

		if (methodType.equals(RequestMethod.POST.toString())) {
			paymentSubmissionResponse.getData().setCreationDateTime(paymentsPlatformResource.getCreatedAt());
			paymentSubmissionResponse.getData().setStatus(
					OBTransactionIndividualStatus1Code.valueOf(paymentsPlatformResource.getStatus().toUpperCase()));
			paymentSubmissionResponse.getData()
					.setStatusUpdateDateTime(paymentsPlatformResource.getStatusUpdateDateTime());
			if (paymentConsentsPlatformResource.getTppDebtorDetails().equalsIgnoreCase(Boolean.FALSE.toString())) {
				paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);
			}
			/*
			 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
			 * TPP.
			 */
			if (Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorDetails())
					&& !Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorNameDetails()))
				paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().setName(null);

		} else {
			paymentSubmissionResponse.getData().setCreationDateTime(paymentsPlatformResource.getCreatedAt());
			OBTransactionIndividualStatus1Code status = OBTransactionIndividualStatus1Code
					.valueOf(paymentsPlatformResource.getStatus().toUpperCase());
			String statusUpdateDateTime = NullCheckUtils.isNullOrEmpty(
					paymentsPlatformResource.getStatusUpdateDateTime()) ? paymentsPlatformResource.getUpdatedAt()
							: paymentsPlatformResource.getStatusUpdateDateTime();

			if (paymentsPlatformResource.getStatus().equals(OBTransactionIndividualStatus1Code.PENDING.toString())) {
				if (paymentSubmissionResponse.getData()
						.getStatus() == OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED) {
					status = paymentSubmissionResponse.getData().getStatus();
					updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
				}
				if(!NullCheckUtils.isNullOrEmpty(paymentSubmissionResponse.getData().getMultiAuthorisation())) {
				if (paymentSubmissionResponse.getData().getMultiAuthorisation()
						.getStatus() == OBExternalStatus2Code.AUTHORISED) {
					status = OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTINPROCESS;
					statusUpdateDateTime = PispUtilities.getCurrentDateInISOFormat();
					updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
				} else if (paymentSubmissionResponse.getData().getMultiAuthorisation()
						.getStatus() == OBExternalStatus2Code.REJECTED) {
					status = OBTransactionIndividualStatus1Code.REJECTED;
					statusUpdateDateTime = PispUtilities.getCurrentDateInISOFormat();
					updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
				}
				}
			}
			paymentSubmissionResponse.getData().setStatus(status);
			paymentSubmissionResponse.getData().setStatusUpdateDateTime(statusUpdateDateTime);

			/*
			 * SIT Defect #2447 fix Submission GET should not have debtor details if not
			 * provided at time of setup : 04-03-2019
			 */

			/*
			 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent details do not have
			 * to send in response. debtorFlag would be set as Null DebtorAgent is not
			 * available in CMA3
			 */
			if (!Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorDetails()))
				paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);

			/*
			 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
			 * TPP.
			 */
			if (Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorDetails())
					&& !Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorNameDetails()))
				paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().setName(null);
		}

		updateMetaAndLinks(paymentSubmissionResponse, methodType);
		return paymentSubmissionResponse;
	}

	private void updatePaymentsResource(PaymentsPlatformResource paymentsPlatformResource,
			OBTransactionIndividualStatus1Code status, String statusUpdateDateTime) {
		paymentsPlatformResource.setStatus(status.toString());
		paymentsPlatformResource.setStatusUpdateDateTime(statusUpdateDateTime);
		paymentsPlatformAdapter.updatePaymentsPlatformResource(paymentsPlatformResource);
	}

	@Override
	public CustomDPaymentsPOSTRequest paymentSubmissionRequestTransformer(
			CustomDPaymentsPOSTRequest paymentSubmissionRequest) {
		return paymentSubmissionRequest;
	}
}
