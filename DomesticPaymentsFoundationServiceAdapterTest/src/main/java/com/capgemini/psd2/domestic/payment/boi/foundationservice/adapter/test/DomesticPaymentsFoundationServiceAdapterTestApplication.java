package com.capgemini.psd2.domestic.payment.boi.foundationservice.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.domestic.payments.foundationservice.boi.adapter.DomesticPaymentsFoundationServiceAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDomestic1;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class DomesticPaymentsFoundationServiceAdapterTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomesticPaymentsFoundationServiceAdapterTestApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestDomesticPaymentsFoundationServiceAdapter
{
	
	@Autowired
	private DomesticPaymentsFoundationServiceAdapter domesticPaymentsFoundationServiceAdapter;
	
	@RequestMapping("/testDomesticPayment")
	public PaymentDomesticSubmitPOST201Response retrieveDomesticPayment()
	{
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers= new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("1234567");
		customPaymentStageIdentifiers.setPaymentSubmissionId("123");
		customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		Map<String,String> params=new HashMap<String ,String>();
	    params.put("consentFlowType", "AISP");
	    params.put("x-channel-id", "BOL");
	    params.put("x-user-id", "BOI999");
	    params.put("X-BOI-PLATFORM", "platform");
	    params.put("x-fapi-interaction-id", "87654321");
		
		return domesticPaymentsFoundationServiceAdapter.retrieveStagedDomesticPayments(customPaymentStageIdentifiers, params);
	}
	
	
	@RequestMapping(value = "/testDomesticPaymentSubmissionExecute", method = RequestMethod.POST)
	public ResponseEntity<PaymentDomesticSubmitPOST201Response> executeDomesticPayment() {
		
		CustomDPaymentsPOSTRequest DomesticPaymentsRequest1 = new CustomDPaymentsPOSTRequest();
		OBWriteDomestic1 oBWriteDomestic1 = new OBWriteDomestic1();
		OBWriteDataDomestic1 oBWriteDataDomestic1 = new OBWriteDataDomestic1();
		OBDomestic1 oBDomestic1 = new OBDomestic1();
		OBRisk1 oBRisk1 = new OBRisk1();
		oBWriteDataDomestic1.setConsentId("3f0992c4-6739-4791-b867-e4dadd863fb9");
		oBDomestic1.setInstructionIdentification("ABDDCF");
		oBDomestic1.setEndToEndIdentification("DEMO USER");
		OBDomestic1InstructedAmount  oBDomestic1InstructedAmount = new OBDomestic1InstructedAmount();
		oBDomestic1InstructedAmount.setAmount("2.222");
		oBDomestic1InstructedAmount.setCurrency("EUR");
		oBDomestic1.setInstructedAmount(oBDomestic1InstructedAmount);
		OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
		oBCashAccountCreditor2.setSchemeName("UK.OBIE.IBAN");
		oBCashAccountCreditor2.setIdentification("GB26MIDL40051512345674");
		oBCashAccountCreditor2.setName("Test user");
		oBCashAccountCreditor2.setSecondaryIdentification("0002");
		oBDomestic1.setCreditorAccount(oBCashAccountCreditor2);
		OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
		oBRemittanceInformation1.setReference("FRESCO-101");
		oBRemittanceInformation1.setUnstructured("Internal ops code 5120101");
		oBDomestic1.setRemittanceInformation(oBRemittanceInformation1);
		oBWriteDataDomestic1.setInitiation(oBDomestic1);
		
		OBExternalPaymentContext1Code oBExternalPaymentContext1Code = OBExternalPaymentContext1Code.ECOMMERCEGOODS;
		oBRisk1.setPaymentContextCode(oBExternalPaymentContext1Code);
		oBRisk1.setMerchantCategoryCode("5967");
		oBRisk1.setMerchantCustomerIdentification("053598653254");
		
		DomesticPaymentsRequest1.setData(oBWriteDataDomestic1);
		DomesticPaymentsRequest1.setRisk(oBRisk1);
		Map<String,String> params=new HashMap<String ,String>();
		params.put("X-BOI-CHANNEL", "BOL");
		params.put("X-BOI-USER", "BOI999");
		params.put("X-CORRELATION-ID","abcdefghijklm");
		params.put("X-BOI-PLATFORM", "platform");
		params.put("x-fapi-interaction-id", "87654321");
		params.put("consentFlowType", "AISP");
		
		Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap = new HashMap<String, OBTransactionIndividualStatus1Code>();
		
		PaymentDomesticSubmitPOST201Response submissionResponse = domesticPaymentsFoundationServiceAdapter.processDomesticPayments(DomesticPaymentsRequest1, paymentStatusMap, params);
		
		return new ResponseEntity<>(submissionResponse, HttpStatus.CREATED);
		
	}

}