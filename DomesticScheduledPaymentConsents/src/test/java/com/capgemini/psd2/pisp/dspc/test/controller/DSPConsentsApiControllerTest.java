package com.capgemini.psd2.pisp.dspc.test.controller;

import static org.mockito.Matchers.anyString;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.dspc.controller.DSPConsentsApiController;
import com.capgemini.psd2.pisp.dspc.service.DSPConsentsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPConsentsApiControllerTest {

	@Mock
	private DSPConsentsService service;

	@Mock
	ObjectMapper objectMapper;

	@InjectMocks
	private DSPConsentsApiController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testObjectMapper() {
		controller.getObjectMapper();
	}

	@Test
	public void testRequest() {
		controller.getRequest();
	}

	@Test
	public void testCreateDomesticScheduledPaymentConsents() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();

		@SuppressWarnings("serial")
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createDomesticScheduledPaymentConsentsResource(request)).thenReturn(response);
		controller.createDomesticScheduledPaymentConsents(request, xFapiFinancialId, authorization, xIdempotencyKey,
				xJwsSignature, null, null, null, null);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateDomesticScheduledPaymentConsentsException() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();

		String xFapiFinancialId = "1";
		String authorization = "2";
		String xIdempotencyKey = "3";
		String xJwsSignature = "4";

		Mockito.when(service.createDomesticScheduledPaymentConsentsResource(request)).thenThrow(PSD2Exception.class);
		controller.createDomesticScheduledPaymentConsents(request, xFapiFinancialId, authorization, xIdempotencyKey,
				xJwsSignature, null, null, null, null);

	}

	@Test
	public void testGetDomesticScheduledPaymentConsentsConsentId() {
		CustomDSPConsentsPOSTResponse response = new CustomDSPConsentsPOSTResponse();

		@SuppressWarnings("serial")
		DefaultSerializerProvider p = new DefaultSerializerProvider() {

			@Override
			public DefaultSerializerProvider createInstance(SerializationConfig config, SerializerFactory jsf) {
				return null;
			}
		};
		objectMapper.setSerializerProvider(p);

		String xFapiFinancialId = "1";
		String authorization = "2";
		String domesticScheduledPaymentId = "123456";

		Mockito.when(service.retrieveDomesticScheduledPaymentConsentsResource(anyString())).thenReturn(response);
		controller.getDomesticScheduledPaymentConsentsConsentId(domesticScheduledPaymentId, xFapiFinancialId, authorization,
				null, null, null, null);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetDomesticScheduledPaymentConsentsConsentIdExcception() {
		String xFapiFinancialId = "1";
		String authorization = "2";
		String domesticScheduledPaymentId = "123456";
		Mockito.when(service.retrieveDomesticScheduledPaymentConsentsResource(anyString()))
				.thenThrow(PSD2Exception.class);
		controller.getDomesticScheduledPaymentConsentsConsentId(domesticScheduledPaymentId, xFapiFinancialId, authorization,
				null, null, null, null);
	}

	@After
	public void tearDown() {
		service = null;
		controller = null;
	}

}
