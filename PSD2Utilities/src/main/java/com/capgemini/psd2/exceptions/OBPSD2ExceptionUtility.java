package com.capgemini.psd2.exceptions;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.yaml.snakeyaml.Yaml;

@Component
public class OBPSD2ExceptionUtility {
	
	@Value("${spring.cloud.config.uri:null}")
	private String configServerUrl;
	
	@Value("${spring.profiles:null}")
	private String profile;
	
	private RestTemplate restTemplate=new RestTemplate();
	
	private Map<String,Map<String,String>> errorMessageMaps=new HashMap<>();
	
	public static final Map<String,String> genericErrorMessages=new HashMap<>();
	
	public static final Map<String,String> specificErrorMessages=new HashMap<>();
	
	@PostConstruct
	public void init() {
		updateErrorMessageMap();
	}

	private void updateErrorMessageMap() {
		String finalUrl= configServerUrl + "/" + "errorMessageMapping-" + profile + ".yml";
		Object value = restTemplate.getForObject(finalUrl, String.class);
		Yaml yaml=new Yaml();
		errorMessageMaps=(Map<String,Map<String,String>>)yaml.load(value.toString());
		genericErrorMessages.putAll(errorMessageMaps.get("genericErrorMessagesMap"));
		specificErrorMessages.putAll(errorMessageMaps.get("specifcErrorMessagesMap"));
		
	}

}


