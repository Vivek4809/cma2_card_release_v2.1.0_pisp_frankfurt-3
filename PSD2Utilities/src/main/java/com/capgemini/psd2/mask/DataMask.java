/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.mask;

import java.util.Map;

/**
 * The Interface DataMask.
 */
public interface DataMask {
	
	/**
	 * Mask M response.
	 *
	 * @param <T> the generic type
	 * @param input the input
	 * @param method the method
	 * @return the t
	 */
	public <T> T maskMResponse(T input, String method);
	
	/**
	 * Mask M request log.
	 *
	 * @param <T> the generic type
	 * @param input the input
	 * @param method the method
	 * @return the t
	 */
	public <T> T maskMRequestLog(T input, String method);
	
	/**
	 * Mask M response log.
	 *
	 * @param <T> the generic type
	 * @param input the input
	 * @param method the method
	 * @return the t
	 */
	public <T> T maskMResponseLog(T input, String method);
	
	/**
	 * Mask response.
	 *
	 * @param <T> the generic type
	 * @param input the input
	 * @param method the method
	 * @return the t
	 */
	public <T> T maskResponse(T input, String method);
	
	/**
	 * Mask request log.
	 *
	 * @param <T> the generic type
	 * @param input the input
	 * @param method the method
	 * @return the t
	 */
	public <T> T maskRequestLog(T input, String method);
	
	/**
	 * Mask response log.
	 *
	 * @param <T> the generic type
	 * @param input the input
	 * @param method the method
	 * @return the t
	 */
	public <T> T maskResponseLog(T input, String method);
	
	/**
	 * Mask.
	 *
	 * @param <T> the generic type
	 * @param input the input
	 * @param rules the rules
	 * @param maskOutput the mask output
	 * @return the t
	 */
	public <T> T mask(T input, Map<String,String> rules, MaskOutput maskOutput);
	

	/**
	 * Mask.
	 *
	 * @param <T> the generic type
	 * @param input the input
	 * @param method the method
	 * @return the String
	 */
	public <T> String maskResponseGenerateString(T input, String method);
}
