/*******************************************************************************

 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.psd2.actuator.trace;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.trace.InMemoryTraceRepository;
import org.springframework.boot.actuate.trace.Trace;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.utilities.JSONUtilities;

@Component
public class LoggingTraceRepository implements TraceRepository {
	
	/** The api id. */
	@Value("${spring.application.name}")
	private String apiId;

	private static final Logger LOG = LoggerFactory.getLogger(LoggingTraceRepository.class);
	private final TraceRepository delegate = new InMemoryTraceRepository();

	/* (non-Javadoc)
	 * @see org.springframework.boot.actuate.trace.TraceRepository#add(java.util.Map)
	 */
	@Override
	public void add(Map<String, Object> traceInfo) {
						
		traceInfo.put("apiId", apiId);
		LOG.info(JSONUtilities.getJSONOutPutFromObject(traceInfo));
		this.delegate.add(traceInfo);
	}

	/* (non-Javadoc)
	 * @see org.springframework.boot.actuate.trace.TraceRepository#findAll()
	 */
	@Override
	public List<Trace> findAll() {
		return delegate.findAll();
	}
}