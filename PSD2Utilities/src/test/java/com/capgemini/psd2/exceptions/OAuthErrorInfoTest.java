package com.capgemini.psd2.exceptions;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

public class OAuthErrorInfoTest {
	
	@InjectMocks
	OAuthErrorInfo oauthErrorInfo;
	
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		oauthErrorInfo.setStatusCode("status");
		oauthErrorInfo.setError("error");
		oauthErrorInfo.setError_description("error_description");
	
	}
	
	@Test
	public void getError(){
		ReflectionTestUtils.setField(oauthErrorInfo, "sendErrorPayload", Boolean.TRUE);
		oauthErrorInfo.getError();
	}
	
	@Test
	public void getError_description(){
		ReflectionTestUtils.setField(oauthErrorInfo, "sendErrorPayload", Boolean.TRUE);
		oauthErrorInfo.getError_description();
	}
	
	@Test
	public void setError_description(){
		 Map<String, String> map = new HashMap<>();
		 map.put("statusCode", "statusCode");
		ReflectionTestUtils.setField(oauthErrorInfo, "errorMap", map);
		ReflectionTestUtils.setField(oauthErrorInfo, "statusCode", "statusCode");
		oauthErrorInfo.setError_description("error_description");
	}
	
	@Test
	public void setError_description1(){
		 Map<String, String> map = new HashMap<>();
		ReflectionTestUtils.setField(oauthErrorInfo, "errorMap", map);
		ReflectionTestUtils.setField(oauthErrorInfo, "statusCode", "statusCode");
		oauthErrorInfo.setError_description("error_description");
	}
	
	/*@Test
	public void test() {
		OAuthErrorInfo errorInfo = new OAuthErrorInfo("500");
		errorInfo.setDetailErrorMessage("detail error message");
		errorInfo.setErrorCode("error code");
		errorInfo.setErrorMessage("errorMessage");
		errorInfo.setJti("jti");
		errorInfo.setOauthServerTokenRefreshURL("htps://localhost:9001/oAuthServer");
		errorInfo.setError("error");
		errorInfo.setError_description("error description");
		errorInfo.setStatusCode("500");
		assertEquals("detail error message", errorInfo.getDetailErrorMessage());
		assertEquals("error code", errorInfo.getErrorCode());
		assertEquals("errorMessage", errorInfo.getErrorMessage());
		assertEquals("jti", errorInfo.getJti());
		assertEquals("htps://localhost:9001/oAuthServer", errorInfo.getOauthServerTokenRefreshURL());
	    assertEquals("error description", errorInfo.getError_description());
	    assertEquals("error", errorInfo.getError());
	    assertEquals("500", errorInfo.getStatusCode());
	}
	
	@Test
	public void testConstructor(){
		OAuthErrorInfo errorInfo = new OAuthErrorInfo("101", "error", "error_description");
		assertEquals("error", errorInfo.getError());
		assertEquals("error_description",errorInfo.getError_description());
		assertEquals("101", errorInfo.getStatusCode());
	}*/
	
	@Test
	public void statusCodeTest(){
		
		assertTrue(oauthErrorInfo.getStatusCode()== "status");
	}
	

}
