package com.capgemini.psd2.account.standingorder.routing.adapter.routing;


import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;

@FunctionalInterface
public interface AccountStandingOrderAdapterFactory 
{
	/**
	 * Gets the adapter instance.
	 *
	 * @param coreSystemName the core system name
	 * @return the adapter instance
	 */
	public AccountStandingOrdersAdapter getAdapterInstance(String adapterName);
}
