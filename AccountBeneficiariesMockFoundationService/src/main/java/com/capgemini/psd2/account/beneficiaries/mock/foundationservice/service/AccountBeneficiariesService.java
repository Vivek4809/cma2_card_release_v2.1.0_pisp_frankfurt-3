package com.capgemini.psd2.account.beneficiaries.mock.foundationservice.service;

import com.capgemini.psd2.account.beneficiaries.mock.foundationservice.domain.PartiesPaymentBeneficiariesresponse;

public interface AccountBeneficiariesService {

	public PartiesPaymentBeneficiariesresponse getBeneficiaries(String userId);

}
