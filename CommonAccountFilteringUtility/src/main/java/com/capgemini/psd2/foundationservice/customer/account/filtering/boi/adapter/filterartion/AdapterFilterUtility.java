package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.CreditCardAccnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;

@Component
public class AdapterFilterUtility {

	// public AccountMapping prevalidateAccounts(Accnts accGetResponse,
	// AccountMapping accountMapping) {
	//
	// List<Accnt> masterAccounts = (List<Accnt>) accGetResponse.getAccount();
	// List<CreditCardAccnt> masterCreditAccounts = (List<CreditCardAccnt>)
	// accGetResponse.getCreditCardAccount();
	// List<AccountDetails> accDetails = accountMapping.getAccountDetails();
	// List<AccountDetails> filteredAccountMapping = new
	// ArrayList<AccountDetails>();
	// boolean prevalidateResult = false;
	//
	// for (AccountDetails accDetail : accDetails) {
	// String creditCardNumber=accDetail.getAccount().getIdentification();
	//
	// String nsc = accDetail.getAccountNSC();
	// String num = accDetail.getAccountNumber();
	// for (Accnt masterAcc : masterAccounts) {
	//
	// if (masterAcc.getAccountNumber().equals(num)
	// && masterAcc.getAccountNSC().equals(nsc)) {
	// filteredAccountMapping.add(accDetail);
	// prevalidateResult = true;
	// }
	// }
	//
	// for (CreditCardAccnt masterCreditAcc : masterCreditAccounts) {
	//
	// if (masterCreditAcc.getCreditCardNumber().equals(creditCardNumber)
	// && masterCreditAcc.getPlApplId().equals(num)) {
	// filteredAccountMapping.add(accDetail);
	// prevalidateResult = true;
	// }
	// }
	//
	// }
	// if (!prevalidateResult) {
	// throw
	// AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
	// }
	//
	// accountMapping.setAccountDetails(filteredAccountMapping);
	// return accountMapping;
	// }

	public AccountMapping prevalidateAccounts(DigitalUserProfile accGetResponse, AccountMapping accountMapping) {

		List<AccountEntitlements> masterAccounts = (List<AccountEntitlements>) accGetResponse.getAccountEntitlements();
		List<AccountDetails> accDetails = accountMapping.getAccountDetails();
		List<AccountDetails> filteredAccountMapping = new ArrayList<AccountDetails>();
		boolean prevalidateResult = false;

		for (AccountDetails accDetail : accDetails) {

			String creditCardNumber = accDetail.getAccount().getIdentification();
			String nsc = accDetail.getAccountNSC();
			String num = accDetail.getAccountNumber();

			for (AccountEntitlements masterAcc : masterAccounts) {

				if (masterAcc.getAccount().getSourceSystemAccountType()
						.equals(SourceSystemAccountType.CURRENT_ACCOUNT)) {
					if (masterAcc.getAccount().getAccountNumber().equals(num) && masterAcc.getAccount()
							.getCurrentAccountInformation().getParentNationalSortCodeNSCNumber().equals(nsc)) {
						filteredAccountMapping.add(accDetail);
						prevalidateResult = true;
					}
				}

				else if (masterAcc.getAccount().getSourceSystemAccountType()
						.equals(SourceSystemAccountType.SAVINGS_ACCOUNT)) {
					if (masterAcc.getAccount().getAccountNumber().equals(num) && masterAcc.getAccount()
							.getSavingsAccountInformation().getParentNationalSortCodeNSCNumber().equals(nsc)) {
						filteredAccountMapping.add(accDetail);
						prevalidateResult = true;
					}
				}

				else if (masterAcc.getAccount().getSourceSystemAccountType()
						.equals(SourceSystemAccountType.CREDIT_CARD)) {
					if (masterAcc.getAccount().getAccountNumber().equals(num)
							&& masterAcc.getAccount().getCreditCardAccountInformation().getCard()
									.getMaskedCardPANNumber().equals(creditCardNumber)) {
						filteredAccountMapping.add(accDetail);
						prevalidateResult = true;
					}
				}

			}
		}
		if (!prevalidateResult) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
		}

		accountMapping.setAccountDetails(filteredAccountMapping);
		return accountMapping;
	}

	public DigitalUserProfile prevalidateJurisdiction(DigitalUserProfile digiUserProfile, String jurisdictionType) {

		DigitalUserProfile filteredAccounts = new DigitalUserProfile();

		List<AccountEntitlements> listAccountEntitlement = digiUserProfile.getAccountEntitlements();
		List<AccountEntitlements> accountList = new ArrayList<>();
		for (AccountEntitlements accnt : listAccountEntitlement) {
			if (accnt.getAccount().getAccountBrand().getBrandCode() != null) {
				if (jurisdictionType.contains(accnt.getAccount().getAccountBrand().getBrandCode().toString())) {
					AccountEntitlements accntEntitlment = new AccountEntitlements();
					accntEntitlment.setAccount(accnt.getAccount());
					accntEntitlment.setEntitlements(accnt.getEntitlements());
					accountList.add(accntEntitlment);

				}
			}
		}
		filteredAccounts.setAccountEntitlements(accountList);
		filteredAccounts.setDigitalUser(digiUserProfile.getDigitalUser());
		filteredAccounts.setPartyEntitlements(digiUserProfile.getPartyEntitlements());
		filteredAccounts.setPartyInformation(digiUserProfile.getPartyInformation());
		return filteredAccounts;
	}

}