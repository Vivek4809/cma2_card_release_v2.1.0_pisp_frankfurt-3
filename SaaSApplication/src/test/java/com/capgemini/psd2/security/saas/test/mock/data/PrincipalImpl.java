package com.capgemini.psd2.security.saas.test.mock.data;

import java.security.Principal;

public class PrincipalImpl implements Principal{
	
	@Override
	public String toString() {
		return "abc";
	}
	
	@Override
	public String getName() {
		return "abc";
	}

}
