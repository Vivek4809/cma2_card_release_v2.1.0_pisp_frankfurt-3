package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;

public interface DSPConsentsFoundationRepository
		extends MongoRepository<CustomDSPConsentsPOSTResponse, String> {

	public CustomDSPConsentsPOSTResponse findOneByDataConsentId(String consentId);
}
