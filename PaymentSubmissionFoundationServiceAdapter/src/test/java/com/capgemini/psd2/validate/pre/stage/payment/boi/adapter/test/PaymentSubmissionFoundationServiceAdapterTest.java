package com.capgemini.psd2.validate.pre.stage.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.ValidationViolation;
import com.capgemini.psd2.adapter.exceptions.ValidationViolations;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.PaymentSubmissionFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.client.PaymentSubmissionFoundationServiceClient;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.utility.PaymentSubmissionFoundationServiceUtility;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionFoundationServiceAdapterTest {
	
	@InjectMocks
	private PaymentSubmissionFoundationServiceAdapter adapter;
	
	@Mock
	private PaymentSubmissionFoundationServiceUtility paymentSubmissionFoundationServiceUtility;

	@Mock
	private PaymentSubmissionFoundationServiceClient paymentSubmissionFoundationServiceClient;

	@Mock
	private AdapterUtility adapterUtility;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test(expected = AdapterException.class)
	public void testExecutePaymentSubmission(){
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		Map<String, String> params = new HashMap<>();
		HttpHeaders headers = new HttpHeaders();
		ValidationPassed validationPassed = new ValidationPassed();
		PaymentInstruction instruction = new PaymentInstruction();
		PaymentSubmissionExecutionResponse executionResponse = new PaymentSubmissionExecutionResponse();
		ReflectionTestUtils.setField(adapter, "executePaymentBaseURL", "http://localhost:9085/payment-web-service/services/executePayment");
		
		when(paymentSubmissionFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		when(paymentSubmissionFoundationServiceUtility. transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(instruction);
		when(paymentSubmissionFoundationServiceClient.executePaymentSubmission(anyObject(), anyObject(), anyObject(), anyObject())).thenReturn(validationPassed);
		when(paymentSubmissionFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(executionResponse);
		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation validationViolation = new ValidationViolation();
		validationViolation.setErrorCode("123");
		validationViolation.setErrorText("test");
		validationViolations.getValidationViolation().add(validationViolation);
		PaymentSubmissionExecutionResponse response = adapter.executePaymentSubmission(paymentSubmissionPOSTRequest, params);
		assertNotNull(response);
		/*
		 * 
		 */
		ErrorInfo errorInfo = new ErrorInfo();
		validationViolation.setErrorCode("test");
		validationViolation.setErrorText("test^test");
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		Map<String, String> map = new HashMap<>();
		map.put("test", null);
		when(paymentSubmissionFoundationServiceClient.executePaymentSubmission(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("Hey", errorInfo, validationViolations));
		when(adapterUtility.getThrowableError()).thenReturn(map);
		adapter.executePaymentSubmission(paymentSubmissionPOSTRequest, params);
		/*
		 * 
		 */
		map.put("test", "test");
		adapter.executePaymentSubmission(paymentSubmissionPOSTRequest, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testExecutePaymentSubmissionTechnicalErrorException(){
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		Map<String, String> params = new HashMap<>();
		HttpHeaders headers = new HttpHeaders();
		ErrorInfo errorInfo = new ErrorInfo();
		PaymentInstruction instruction = new PaymentInstruction();
		ValidationViolations validationViolations = new ValidationViolations();
		ValidationViolation validationViolation = new ValidationViolation();
		PaymentSubmissionExecutionResponse executionResponse = new PaymentSubmissionExecutionResponse();
		ReflectionTestUtils.setField(adapter, "executePaymentBaseURL", "http://localhost:9085/payment-web-service/services/executePayment");
		
		validationViolation.setErrorCode("123");
		validationViolation.setErrorText("test");
		errorInfo.setErrorCode("test");
		errorInfo.setErrorMessage("test");
		validationViolations.getValidationViolation().add(validationViolation);
		when(paymentSubmissionFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		when(paymentSubmissionFoundationServiceUtility. transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(instruction);
		when(paymentSubmissionFoundationServiceClient.executePaymentSubmission(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(new AdapterException("Hey", errorInfo, validationViolations) );
		when(paymentSubmissionFoundationServiceUtility.transformResponseFromFSToAPI(anyObject())).thenReturn(executionResponse);
		
		adapter.executePaymentSubmission(paymentSubmissionPOSTRequest, params);
	}
	
	@Test(expected = AdapterException.class)
	public void tearExecutePaymentSubmissionThrowTechError(){
	
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		Map<String, String> params = new HashMap<>();
		HttpHeaders headers = new HttpHeaders();
		PaymentInstruction instruction = new PaymentInstruction();
		ReflectionTestUtils.setField(adapter, "executePaymentBaseURL", "http://localhost:9085/payment-web-service/services/executePayment");
		
		when(paymentSubmissionFoundationServiceUtility.createRequestHeaders(anyObject(), anyObject(), anyObject())).thenReturn(headers);
		when(paymentSubmissionFoundationServiceUtility. transformRequestFromAPIToFS(anyObject(), anyObject())).thenReturn(instruction);
		when(paymentSubmissionFoundationServiceClient.executePaymentSubmission(anyObject(), anyObject(), anyObject(), anyObject())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONNECTION_ERROR));
		
		adapter.executePaymentSubmission(paymentSubmissionPOSTRequest, params);
	}
}
