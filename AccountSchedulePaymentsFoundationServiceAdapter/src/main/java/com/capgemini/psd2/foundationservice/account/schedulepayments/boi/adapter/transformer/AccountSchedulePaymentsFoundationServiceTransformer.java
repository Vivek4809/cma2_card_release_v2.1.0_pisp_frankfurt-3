/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification4;
import com.capgemini.psd2.aisp.domain.OBCashAccount3;
import com.capgemini.psd2.aisp.domain.OBExternalScheduleType1Code;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1Data;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.constants.AccountSchedulePaymentsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.OneOffPaymentInstruction;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PartiesOneOffPaymentInstructionsresponse;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.RequestedOccurenceDateType;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;



/**
 * The Class AccountSchedulePaymentsFoundationServiceTransformer.
 */
@Component
public class AccountSchedulePaymentsFoundationServiceTransformer {

	/** The psd2 validator. */
	@Autowired
	private PSD2Validator psd2Validator;

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	public <T> PlatformAccountSchedulePaymentsResponse transformSchedulePayments(T inputBalanceObj,
			Map<String, String> params) {

		OBReadScheduledPayment1 oBReadScheduledPayment1 = new OBReadScheduledPayment1();
		OBReadScheduledPayment1Data data = new OBReadScheduledPayment1Data();
		PartiesOneOffPaymentInstructionsresponse partiesAccountsAccountNumberOnceOffPaymentScheduleScheduleItemsResponse = (PartiesOneOffPaymentInstructionsresponse) inputBalanceObj;
		List<OneOffPaymentInstruction> scheduleItemBaseTypeList = partiesAccountsAccountNumberOnceOffPaymentScheduleScheduleItemsResponse
				.getOneOffPaymentInstructionsList();

		for (OneOffPaymentInstruction scheduleItemBaseType : scheduleItemBaseTypeList) {

			OBScheduledPayment1 response = new OBScheduledPayment1();
			OBActiveOrHistoricCurrencyAndAmount oBScheduledPayment1InstructedAmount = new OBActiveOrHistoricCurrencyAndAmount();

			response.setAccountId(params.get(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_ID));

			if (!NullCheckUtils
					.isNullOrEmpty(scheduleItemBaseType.getPaymentInstruction().getPaymentInstructionNumber()))
				response.setScheduledPaymentId(
						scheduleItemBaseType.getPaymentInstruction().getPaymentInstructionNumber());
			
				response.setScheduledPaymentDateTime(
						timeZoneDateTimeAdapter.parseDateCMA(scheduleItemBaseType.getRequestedOccurenceDate()));
			 

			getScheduledType(scheduleItemBaseType.getRequestedOccurenceDateType(), response.getScheduledType(), response);
			
			String spInsAmount = null;
			spInsAmount = BigDecimal.valueOf(Double.valueOf(scheduleItemBaseType.getPaymentAmount().getTransactionCurrency().toString())).toPlainString();
			if (!spInsAmount.contains("."))
				spInsAmount = spInsAmount + ".0";
			
			oBScheduledPayment1InstructedAmount.setAmount(spInsAmount);
			oBScheduledPayment1InstructedAmount
					.setCurrency(scheduleItemBaseType.getTransactionCurrency().getIsoAlphaCode());
			response.setInstructedAmount(oBScheduledPayment1InstructedAmount);
			response.setReference(
					scheduleItemBaseType.getPaymentInstructionToCounterPartyBasic().get(0).getCounterpartyReferenceText());

			// First Check SORTCODE ACCOUNT NUMBER
			 if (!NullCheckUtils.isNullOrEmpty(scheduleItemBaseType.getPaymentInstructionToCounterPartyBasic().get(0)
					.getCounterpartyAccountNumber()) && !NullCheckUtils.isNullOrEmpty(scheduleItemBaseType.getPaymentInstructionToCounterPartyBasic().get(0)
									.getCounterpartyNationalSortCodeNSCNumber())) {
				 OBCashAccount3 oBScheduledPayment1CreditorAccount = new OBCashAccount3();
				oBScheduledPayment1CreditorAccount.setSchemeName(AccountSchedulePaymentsFoundationServiceConstants.UK_OBIE_SORTCODEACCOUNTNUMBER);
				// Checking Identification
				String nscAcctNum = scheduleItemBaseType.getPaymentInstructionToCounterPartyBasic().get(0)
						.getCounterpartyNationalSortCodeNSCNumber();

				String counterAccountNumber = scheduleItemBaseType.getPaymentInstructionToCounterPartyBasic().get(0)
						.getCounterpartyAccountNumber();

				String nscNum = nscAcctNum.substring(0, nscAcctNum.length());
				String accountNumber = counterAccountNumber.substring(0, counterAccountNumber.length());

				if ((null != nscNum && nscNum.length() == 6)
						&& (null != accountNumber && accountNumber.length() == 8)) {
					oBScheduledPayment1CreditorAccount.setIdentification(nscNum + accountNumber);

				} else {
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
				}
				response.setCreditorAccount(oBScheduledPayment1CreditorAccount);

			}
			
           //Then Check IBAN if not present SORTCODE ACCOUNT NUMBER
			 else if (!NullCheckUtils.isNullOrEmpty(scheduleItemBaseType.getPaymentInstructionToCounterPartyBasic().get(0)
					.getCounterpartyInternationalBankAccountNumberIBAN()))
			{
				 OBCashAccount3 oBScheduledPayment1CreditorAccount = new OBCashAccount3();
				 OBBranchAndFinancialInstitutionIdentification4 oBScheduledPayment1CreditorAgent = new OBBranchAndFinancialInstitutionIdentification4();
				oBScheduledPayment1CreditorAgent.setSchemeName(AccountSchedulePaymentsFoundationServiceConstants.UK_OBIE_BICFI);
				oBScheduledPayment1CreditorAgent.setIdentification(scheduleItemBaseType
						.getPaymentInstructionToCounterPartyBasic().get(0).getCounterpartySWIFTBankIdentifierCodeBIC());
				response.setCreditorAgent(oBScheduledPayment1CreditorAgent);
				oBScheduledPayment1CreditorAccount.setSchemeName(AccountSchedulePaymentsFoundationServiceConstants.UK_OBIE_IBAN);
				oBScheduledPayment1CreditorAccount
						.setIdentification(scheduleItemBaseType.getPaymentInstructionToCounterPartyBasic().get(0)
								.getCounterpartyInternationalBankAccountNumberIBAN());
				oBScheduledPayment1CreditorAccount.setName(
						scheduleItemBaseType.getPaymentInstructionToCounterPartyBasic().get(0).getCounterpartyName());
				response.setCreditorAccount(oBScheduledPayment1CreditorAccount);
				
			}
				
			

			psd2Validator.validate(response);
			data.addScheduledPaymentItem(response);
		}
		PlatformAccountSchedulePaymentsResponse platformAccountSchedulePaymentsResponse = new PlatformAccountSchedulePaymentsResponse();
		oBReadScheduledPayment1.setData(data);
		platformAccountSchedulePaymentsResponse.setObReadScheduledPayment1(oBReadScheduledPayment1);
		
		if(NullCheckUtils.isNullOrEmpty(partiesAccountsAccountNumberOnceOffPaymentScheduleScheduleItemsResponse) || null==partiesAccountsAccountNumberOnceOffPaymentScheduleScheduleItemsResponse.getOneOffPaymentInstructionsList()|| NullCheckUtils.isNullOrEmpty(data.getScheduledPayment()))
		{
			data.setScheduledPayment(new ArrayList<>());
			oBReadScheduledPayment1.setData(data);
			platformAccountSchedulePaymentsResponse.setObReadScheduledPayment1(oBReadScheduledPayment1);
		}
		
		return platformAccountSchedulePaymentsResponse;

	}

	private void getScheduledType(RequestedOccurenceDateType fsScheduleType, OBExternalScheduleType1Code cmaScheduledTypeEnum,
			OBScheduledPayment1 response) {

		if ( String.valueOf(RequestedOccurenceDateType.ARRIVAL).equalsIgnoreCase(String.valueOf(fsScheduleType))){
			response.setScheduledType(OBExternalScheduleType1Code.ARRIVAL);

		}
		else if (String.valueOf(RequestedOccurenceDateType.EXECUTION).equalsIgnoreCase(String.valueOf(fsScheduleType))){
			response.setScheduledType(OBExternalScheduleType1Code.EXECUTION);

		}
		else{
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
		}

	}

}
