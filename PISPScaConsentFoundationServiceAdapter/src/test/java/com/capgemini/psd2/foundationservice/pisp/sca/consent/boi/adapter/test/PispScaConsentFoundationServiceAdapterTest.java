package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.test;

import static org.mockito.Matchers.any;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.PispScaConsentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.client.PispScaConsentFoundationServiceClient;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.delegate.PispScaConsentFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.transformer.PispScaConsentFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class PispScaConsentFoundationServiceAdapterTest {

	@InjectMocks
	private PispScaConsentFoundationServiceAdapter pispScaConsentFoundationServiceAdapter;

	@Mock
	private PispScaConsentFoundationServiceDelegate pispScaConsentFoundationServiceDelegate;

	@Mock
	private PispScaConsentFoundationServiceTransformer pispScaConsentFoundationServiceTransformer;

	@Mock
	private PispScaConsentFoundationServiceClient pispScaConsentFoundationServiceClient;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testPispScaConsentFoundationServiceAdapter() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		HttpHeaders httpHeaders = new HttpHeaders();

		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");

		Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(), any()))
		.thenReturn(httpHeaders);
		Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService(any(), any(),
				any())).thenReturn(new PaymentInstructionProposal());
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponse(any()))
		.thenReturn(new CustomConsentAppViewData());

		pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(stageIdentifiers, params);

	}
	@Test
	public void testPispScaConsentFoundationServiceAdapter6() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomConsentAppViewData customConsentAppViewData= new CustomConsentAppViewData();
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal= new ScheduledPaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		HttpHeaders httpHeaders = new HttpHeaders();

		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		customConsentAppViewData.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType());


		Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(),any()))
		.thenReturn(httpHeaders);
		Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentScheduledFoundationService(any(), any(),
				any())).thenReturn(scheduledPaymentInstructionProposal);
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseScheduled(any()))
		.thenReturn(customConsentAppViewData);

		pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(stageIdentifiers, params);

	}
	
	@Test
	public void testPispScaConsentFoundationServiceAdapter5() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomConsentAppViewData customConsentAppViewData= new CustomConsentAppViewData();
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal= new ScheduledPaymentInstructionProposal();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		HttpHeaders httpHeaders = new HttpHeaders();

		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		customConsentAppViewData.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType());


		Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(),any()))
		.thenReturn(httpHeaders);
		Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURLForStandingOrders(any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationServiceForStandingOrders(any(), any(),
				any())).thenReturn( new StandingOrderInstructionProposal());
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseForStandingOrders(any(), any()))
		.thenReturn(customConsentAppViewData);

		pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(stageIdentifiers, params);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterForParamsNull() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(stageIdentifiers, null);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterStageIdentifiersForNull() {

		Map<String, String> params = new HashMap<>();
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(null, params);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterStageIdentifiersIdForNull() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		stageIdentifiers.setPaymentConsentId(null);
		pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(stageIdentifiers, params);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterStageIdentifiersVersionForNull() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		stageIdentifiers.setPaymentConsentId("1230");
		stageIdentifiers.setPaymentSetupVersion(null);
		pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(stageIdentifiers, params);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapter1() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, params);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapter2() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(stageIdentifiers, updateData, params);

	}

	@Test
	public void testPispScaConsentFoundationServiceAdapter3() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		preAuthAdditionalInfo.setAccountBic("jhgeuy");
		pispScaConsentFoundationServiceAdapter.validatePreAuthorisation(selectedDebtorDetails, preAuthAdditionalInfo,
				stageIdentifiers, params);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceAdapterforstanding() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		preAuthAdditionalInfo.setAccountBic("jhgeuy");
		pispScaConsentFoundationServiceAdapter.validatePreAuthorisation(selectedDebtorDetails, preAuthAdditionalInfo,
				stageIdentifiers, params);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceAdapterforscheduled() {

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		preAuthAdditionalInfo.setAccountBic("jhgeuy");
		pispScaConsentFoundationServiceAdapter.validatePreAuthorisation(selectedDebtorDetails, preAuthAdditionalInfo,
				stageIdentifiers, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterForUpdate1() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(null, updateData, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterForUpdate5() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId(null);
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(stageIdentifiers, updateData, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterForUpdate6() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("1234");
		stageIdentifiers.setPaymentSetupVersion(null);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(stageIdentifiers, updateData, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterForUpdate2() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(stageIdentifiers, null, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterForUpdate3() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(stageIdentifiers, updateData, null);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceAdapterForUpdate4() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(), any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService(any(), any(),
				any())).thenReturn(new PaymentInstructionProposal());
		
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformPispScaConsentResponseForUpdate(any(), any(), any())).thenReturn(new PaymentInstructionProposal());
		Mockito.when(pispScaConsentFoundationServiceDelegate.putPaymentFoundationServiceURL(any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/c9067c82-0d95-4b4c-86ff-b06e88a6b363/payments-process-api/1.0.20/group-payments/p/payments-service/v1.0/domestic/payment-instruction-proposals/1234");
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(stageIdentifiers, updateData, params);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceAdapterForUpdate7() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentScheduledFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentScheduledFoundationService(any(), any(),
				any())).thenReturn(new ScheduledPaymentInstructionProposal());
		
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformPispScaConsentScheduledResponseForUpdate(any(), any(), any())).thenReturn(new ScheduledPaymentInstructionProposal ());
		Mockito.when(pispScaConsentFoundationServiceDelegate.putPaymentScheduledFoundationServiceURL(any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/c9067c82-0d95-4b4c-86ff-b06e88a6b363/payments-process-api/1.0.20/group-payments/p/payments-service/v1.0/domestic/payment-instruction-proposals/1234");
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(stageIdentifiers, updateData, params);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceAdapterForUpdate8() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createDomesticStandingOrderConsentRequestHeaders(any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getDomesticStandingOrderConsentScheduledFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationServiceForStandingOrders(any(), any(),
				any())).thenReturn(new StandingOrderInstructionProposal());
		
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformDomesticStandingOrderConsentResponseForUpdate(any(), any())).thenReturn(new StandingOrderInstructionProposal());
		Mockito.when(pispScaConsentFoundationServiceDelegate.putDomesticStandingOrderFoundationServiceURL(any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/c9067c82-0d95-4b4c-86ff-b06e88a6b363/payments-process-api/1.0.20/group-payments/p/payments-service/v1.0/domestic/payment-instruction-proposals/1234");
		pispScaConsentFoundationServiceAdapter.updatePaymentStageData(stageIdentifiers, updateData, params);
	}
	
	
	@Test
	public void testPispScaConsentFoundationServiceAdapterForFraud() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService(any(), any(),
				any())).thenReturn(new PaymentInstructionProposal());
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformFraudSystemDomesticPaymentStagedData(any())).thenReturn(new CustomFraudSystemPaymentData());
		pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, params);
}
	
	@Test
	public void testPispScaConsentFoundationServiceAdapterForFraudSchedule() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeadersScheduled(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaScheduleConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService2(any(), any(),
				any())).thenReturn(new ScheduledPaymentInstructionProposal());
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformFraudSystemDomesticScheduledPaymentStagedData(any())).thenReturn(new CustomFraudSystemPaymentData());
		pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, params);
}
	
	
	@Test
	public void testPispScaConsentFoundationServiceAdapterForFraudStanding() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeadersForStandingOrders(any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURLForStandingOrders(any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationServiceForStandingOrders(any(), any(),
				any())).thenReturn(new StandingOrderInstructionProposal());
		Mockito.when(pispScaConsentFoundationServiceTransformer.transformFraudSystemDomesticStandingOrderPaymentStagedData(any())).thenReturn(new CustomFraudSystemPaymentData());
		pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, params);
}
	
	
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterFornull() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		Map<String, String> params = new HashMap<>();
		pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, null);
	}
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterFornull1() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(null);
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		Map<String, String> params = new HashMap<>();
		pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, params);
	}
	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceAdapterFornull2() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("1234");
		stageIdentifiers.setPaymentSetupVersion(null);
		Map<String, String> params = new HashMap<>();
		pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, params);
	}
	
	@Test
	public void testretrieveConsentAppStagedViewDataForValidate(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService(any(), any(),any())).thenReturn(new PaymentInstructionProposal());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidate(stageIdentifiers, params);
	
	}
	@Test(expected = AdapterException.class)
	public void testretrieveConsentAppStagedViewDataForValidatefornull(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService(any(), any(),any())).thenReturn(new PaymentInstructionProposal());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidate(stageIdentifiers, null);
	
	}
	@Test(expected = AdapterException.class)
	public void testretrieveConsentAppStagedViewDataForValidatefornull2(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeaders(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService(any(), any(),any())).thenReturn(new PaymentInstructionProposal());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidate(null, params);
	
	}
	@Test
	public void testretrieveConsentAppStagedViewDataForValidateScheduled(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeadersScheduled(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaScheduleConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService2(any(), any(),any())).thenReturn(new ScheduledPaymentInstructionProposal ());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidateScheduled(stageIdentifiers, params);
	
	}
	@Test(expected = AdapterException.class)
	public void testretrieveConsentAppStagedViewDataForValidateScheduledfornull(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeadersScheduled(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaScheduleConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService2(any(), any(),any())).thenReturn(new ScheduledPaymentInstructionProposal ());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidateScheduled(stageIdentifiers, null);
	
	}
	
	@Test(expected = AdapterException.class)
	public void testretrieveConsentAppStagedViewDataForValidateScheduledfornull2(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeadersScheduled(any(),any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaScheduleConsentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService2(any(), any(),any())).thenReturn(new ScheduledPaymentInstructionProposal ());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidateScheduled(null, params);
	
	}
	@Test
	public void testretrieveConsentAppStagedViewDataForValidateStandingOrder(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeadersStandingOrder(any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURLForStandingOrders(any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationServiceForStandingOrders(any(), any(),any())).thenReturn(new StandingOrderInstructionProposal());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidateStandingOrder(stageIdentifiers, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testretrieveConsentAppStagedViewDataForValidateStandingOrdernull(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeadersStandingOrder(any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURLForStandingOrders(any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationServiceForStandingOrders(any(), any(),any())).thenReturn(new StandingOrderInstructionProposal());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidateStandingOrder(stageIdentifiers, null);
	}
	
	@Test(expected = AdapterException.class)
	public void testretrieveConsentAppStagedViewDataForValidateStandingOrdernull2(){
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(pispScaConsentFoundationServiceDelegate.createPispScaConsentRequestHeadersStandingOrder(any())).thenReturn(httpHeaders);
	    Mockito.when(pispScaConsentFoundationServiceDelegate.getPispScaConsentFoundationServiceURLForStandingOrders(any()))
		.thenReturn(
				"http://localhost:9093/fs-domesticpaymentconsent-service/services/domesticpaymentconsent/v2.0/domestic/payment-instruction-proposals/1234");
		Mockito.when(pispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationServiceForStandingOrders(any(), any(),any())).thenReturn(new StandingOrderInstructionProposal());
	pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewDataForValidateStandingOrder(null, params);
	}
}