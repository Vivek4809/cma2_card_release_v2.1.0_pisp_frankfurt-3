/*
 * Payment Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * The type of the response
 */
//@JsonAdapter(Type.Adapter.class)
public enum Type {
  
  CRITICAL("CRITICAL"),
  
  ERROR("ERROR"),
  
  WARNING("WARNING"),
  
  INFORMATION("INFORMATION"),
  
  AUDIT("AUDIT"),
  
  SUCCESSFUL("SUCCESSFUL");

  private String value;

  Type(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }
  @JsonCreator
  public static Type fromValue(String text) {
    for (Type b : Type.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }

	/*
	 * public static class Adapter extends TypeAdapter<Type> {
	 * 
	 * @Override public void write(final JsonWriter jsonWriter, final Type
	 * enumeration) throws IOException { jsonWriter.value(enumeration.getValue()); }
	 * 
	 * @Override public Type read(final JsonReader jsonReader) throws IOException {
	 * String value = jsonReader.nextString(); return
	 * Type.fromValue(String.valueOf(value)); } }
	 */}

