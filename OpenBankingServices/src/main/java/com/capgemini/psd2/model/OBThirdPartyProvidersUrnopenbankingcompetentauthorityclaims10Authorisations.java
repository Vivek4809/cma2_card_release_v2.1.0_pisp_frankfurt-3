package com.capgemini.psd2.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations   {
  @JsonProperty("Active")
  private Boolean active = null;

  @JsonProperty("MemberState")
  private String memberState = null;

  @JsonProperty("Psd2Role")
  private String psd2Role = null;

  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations active(Boolean active) {
    this.active = active;
    return this;
  }

   /**
   * Indicator to show if this claim is active
   * @return active
  **/
  @ApiModelProperty(value = "Indicator to show if this claim is active")


  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations memberState(String memberState) {
    this.memberState = memberState;
    return this;
  }

   /**
   * Member State giving the authorisation
   * @return memberState
  **/
  @ApiModelProperty(value = "Member State giving the authorisation")


  public String getMemberState() {
    return memberState;
  }

  public void setMemberState(String memberState) {
    this.memberState = memberState;
  }

  public OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations psd2Role(String psd2Role) {
    this.psd2Role = psd2Role;
    return this;
  }

   /**
   * Psd2 Role in which institution authorised
   * @return psd2Role
  **/
  @ApiModelProperty(value = "Psd2 Role in which institution authorised")


  public String getPsd2Role() {
    return psd2Role;
  }

  public void setPsd2Role(String psd2Role) {
    this.psd2Role = psd2Role;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations = (OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations) o;
    return Objects.equals(this.active, obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations.active) &&
        Objects.equals(this.memberState, obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations.memberState) &&
        Objects.equals(this.psd2Role, obThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations.psd2Role);
  }

  @Override
  public int hashCode() {
    return Objects.hash(active, memberState, psd2Role);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations {\n");
    
    sb.append("    active: ").append(toIndentedString(active)).append("\n");
    sb.append("    memberState: ").append(toIndentedString(memberState)).append("\n");
    sb.append("    psd2Role: ").append(toIndentedString(psd2Role)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

