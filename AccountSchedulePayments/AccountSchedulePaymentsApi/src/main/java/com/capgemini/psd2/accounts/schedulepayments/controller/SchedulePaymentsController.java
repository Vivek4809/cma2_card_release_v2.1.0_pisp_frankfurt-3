package com.capgemini.psd2.accounts.schedulepayments.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.accounts.schedulepayments.service.SchedulePaymentService;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment1;

@RestController
public class SchedulePaymentsController {
	
	@Autowired
	private SchedulePaymentService schedulePaymentService;	

	@GetMapping(value="/accounts/{accountId}/scheduled-payments",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadScheduledPayment1 retrieveSchedulePayments(@PathVariable("accountId") String accountId) {
		return schedulePaymentService.retrieveSchedulePayments(accountId);
	}
}
