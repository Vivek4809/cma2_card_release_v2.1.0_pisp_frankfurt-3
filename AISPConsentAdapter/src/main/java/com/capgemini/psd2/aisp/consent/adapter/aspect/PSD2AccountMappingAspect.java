/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.psd2.aisp.consent.adapter.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aspect.PSD2AspectUtils;


/**
 * The Class PSD2Aspect.
 */
@Component
@Aspect
public class PSD2AccountMappingAspect {


	@Autowired
	private PSD2AspectUtils aspectUtils;

	
	/**
	 * Arround logger advice controller.
	 *
	 * @param proceedingJoinPoint the proceeding join point
	 * @return the object
	 */
	@Around("(execution(* com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapterImpl.retrieveAccountMappingDetails(..)))")
	public Object arroundLoggerAdviceController(ProceedingJoinPoint proceedingJoinPoint) {
		return aspectUtils.methodPayloadAdvice(proceedingJoinPoint);
	} 



}
