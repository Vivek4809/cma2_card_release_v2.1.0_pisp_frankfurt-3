package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client.AccountBeneficiariesFoundationServiceClientImpl;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiary;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.BeneficiaryAccount;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.ExternalPaymentBeneficiaryIndicator;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.OperationalOrganisationUnit;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PaymentBeneficiaryLocaleTypeCode;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesFoundationServiceClientImplTest {
	@InjectMocks
	private AccountBeneficiariesFoundationServiceClientImpl accountBeneficiariesFoundationServiceClientImpl;
	@Mock
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	@Mock
	private DataMask dataMask;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRestTransportForAccountBeneficiaryFS() {
		RequestInfo requestInfo = new RequestInfo();

		PartiesPaymentBeneficiariesresponse partiesPaymentBeneficiariesresponse = new PartiesPaymentBeneficiariesresponse();
		List<Beneficiary> paymentBeneficiaryList = new ArrayList<Beneficiary>();
		Beneficiary paymentBeneficiary = new Beneficiary();
		BeneficiaryAccount beneficiaryAccount = new BeneficiaryAccount();
		OperationalOrganisationUnit operationalOrganisationUnit = new OperationalOrganisationUnit();
		Address addressReference = new Address();
		Country addressCountry = new Country();

		addressReference.setGeoCodeBuildingName("New Century House");
		addressReference.setFirstAddressLine("Mayor Street Lower");
		addressReference.setSecondAddressLine("International Financial Services Centre");
		addressReference.setThirdAddressLine("Dublin 1");

		addressCountry.setIsoCountryAlphaTwoCode("IE");
		addressCountry.setCountryName("Republic of Ireland");
		addressReference.setAddressCountry(addressCountry);

		operationalOrganisationUnit.setAddressReference(addressReference);

		beneficiaryAccount.setAccountName("ABC Corporation");
		beneficiaryAccount.setAccountNumber("24512786");
		beneficiaryAccount.setParentNationalSortCodeNSCNumber("902127");
		beneficiaryAccount.setInternationalBankAccountNumberIBAN("GB58BOFI90212724512786");
		beneficiaryAccount.setSwiftBankIdentifierCodeBIC("BOFIGB2BXXX");
		beneficiaryAccount.setOperationalOrganisationUnit(operationalOrganisationUnit);

		paymentBeneficiary.setPaymentBeneficiaryIdentifier("1234");
		paymentBeneficiary.setPaymentBeneficiaryLabel("ACME Co. Invoices");
		paymentBeneficiary.setPaymentBeneficiaryNarrativeText("October Invoices");
		paymentBeneficiary.setPaymentBeneficiaryBankName("Bank of Ireland");
		paymentBeneficiary.setPaymentBeneficiaryLocaleTypeCode(PaymentBeneficiaryLocaleTypeCode.SEPA);
		paymentBeneficiary.setExternalPaymentBeneficiaryIndicator(ExternalPaymentBeneficiaryIndicator.N);
		paymentBeneficiary.setBeneficiaryAccount(beneficiaryAccount);

		paymentBeneficiaryList.add(paymentBeneficiary);

		partiesPaymentBeneficiariesresponse.setPaymentBeneficiaryList(paymentBeneficiaryList);
		partiesPaymentBeneficiariesresponse.setTotal(1d);

		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceClientImpl, "maskBeneficiariesResponse",
				true);
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		queryParams.add("nsc", "123456");
		queryParams.add("accountNumber", "12345678");
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		requestInfo.setUrl("http://localhost:9087/fs-abt-service/services/user/12345678/beneficiaries");
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject()))
				.thenReturn(partiesPaymentBeneficiariesresponse);
		Mockito.when(dataMask.maskResponse(anyObject(), anyObject())).thenReturn(partiesPaymentBeneficiariesresponse);
		// maskResponse is true
		PartiesPaymentBeneficiariesresponse response = accountBeneficiariesFoundationServiceClientImpl
				.restTransportForAccountBeneficiaryFS(requestInfo, PartiesPaymentBeneficiariesresponse.class,httpHeaders,queryParams);
		assertThat(response).isEqualTo(partiesPaymentBeneficiariesresponse);
		// maskResponse is false
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceClientImpl, "maskBeneficiariesResponse",
				false);
		response = accountBeneficiariesFoundationServiceClientImpl.restTransportForAccountBeneficiaryFS(requestInfo,
				PartiesPaymentBeneficiariesresponse.class, httpHeaders,queryParams);
		assertThat(response).isEqualTo(partiesPaymentBeneficiariesresponse);
	}
}		