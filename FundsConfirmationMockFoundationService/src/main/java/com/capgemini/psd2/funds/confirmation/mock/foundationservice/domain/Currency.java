package com.capgemini.psd2.funds.confirmation.mock.foundationservice.domain;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

/**
 * Currency object
 */
@ApiModel(description = "Currency object")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-07T14:38:09.445+05:30")

public class Currency   {
  @JsonProperty("isoAlphaCode")
  private String isoAlphaCode = null;

  @JsonProperty("currencyName")
  private String currencyName = null;

  public Currency isoAlphaCode(String isoAlphaCode) {
    this.isoAlphaCode = isoAlphaCode;
    return this;
  }

  /**
   * Get isoAlphaCode
   * @return isoAlphaCode
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^[A-Z]{3}$") 
  public String getIsoAlphaCode() {
    return isoAlphaCode;
  }

  public void setIsoAlphaCode(String isoAlphaCode) {
    this.isoAlphaCode = isoAlphaCode;
  }

  public Currency currencyName(String currencyName) {
    this.currencyName = currencyName;
    return this;
  }

  /**
   * Get currencyName
   * @return currencyName
  **/
  @ApiModelProperty(value = "")


  public String getCurrencyName() {
    return currencyName;
  }

  public void setCurrencyName(String currencyName) {
    this.currencyName = currencyName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Currency currency = (Currency) o;
    return Objects.equals(this.isoAlphaCode, currency.isoAlphaCode) &&
        Objects.equals(this.currencyName, currency.currencyName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(isoAlphaCode, currencyName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Currency {\n");
    
    sb.append("    isoAlphaCode: ").append(toIndentedString(isoAlphaCode)).append("\n");
    sb.append("    currencyName: ").append(toIndentedString(currencyName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

