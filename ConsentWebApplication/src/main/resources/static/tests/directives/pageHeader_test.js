"use strict";
describe("ConsentApp.pageHeader_test", function() {
    beforeEach(module("consentTemplates", "consentApp"));
    var compile, scope, element;

    beforeEach(inject(function($rootScope, $compile) {
        scope = $rootScope.$new();
        element = angular.element("<page-header></page-header>");
        $compile(element)(scope);
        scope.$digest();
    }));

    describe("test template", function() {
        it("should have placeholder for logo in template", function() {
            var result = element[0].querySelectorAll(".navbar-brand");
            expect(result.length).toEqual(1);
        });
    });
});