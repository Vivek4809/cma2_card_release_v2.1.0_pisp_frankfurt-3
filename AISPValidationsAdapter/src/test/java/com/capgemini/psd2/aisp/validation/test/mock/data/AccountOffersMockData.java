package com.capgemini.psd2.aisp.validation.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount;
import com.capgemini.psd2.aisp.domain.OBOffer1;
import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.domain.OBReadOffer1Data;

public class AccountOffersMockData {

	public static OBReadOffer1 getMockExpectedAccountOffersResponse() {
		OBReadOffer1Data obReadOffer1Data = new OBReadOffer1Data();
		List<OBOffer1> offerList = new ArrayList<>();

		OBOffer1 offer1 = new OBOffer1();
		offer1.setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");
		offer1.setOfferId("Offer1");
		OBActiveOrHistoricCurrencyAndAmount amount = new OBActiveOrHistoricCurrencyAndAmount();
		amount.setAmount("0.12345");
		amount.setCurrency("GBP");
		
		OBActiveOrHistoricCurrencyAndAmount fee = new OBActiveOrHistoricCurrencyAndAmount();
		fee.setAmount("9011.12345");
		fee.setCurrency("GBP");
		
	
		offer1.setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");
		offer1.setOfferId("Offer1");
		offer1.setAmount(amount);
		offer1.setFee(fee);
		offer1.setStartDateTime("2018-05-08T07:24:39.113Z");
		offer1.setEndDateTime("2018-05-08T07:24:39.113Z");
		offerList.add(offer1);

		obReadOffer1Data.setOffer(offerList);
		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
		obReadOffer1.setData(obReadOffer1Data);

		Links links = new Links();
		obReadOffer1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);

		obReadOffer1.setMeta(meta);
		return obReadOffer1;
	}
}
