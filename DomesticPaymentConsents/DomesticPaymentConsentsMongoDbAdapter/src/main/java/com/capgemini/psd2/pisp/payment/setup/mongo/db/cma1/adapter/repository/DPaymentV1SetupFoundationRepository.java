package com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain.CustomDomesticPaymentSetupCma1POSTResponse;

public interface DPaymentV1SetupFoundationRepository extends MongoRepository<CustomDomesticPaymentSetupCma1POSTResponse, String>{

	  public CustomDomesticPaymentSetupCma1POSTResponse findOneByDataPaymentId(String paymentId );	
}
