package com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.DPaymentConsentsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("dPaymentConsentsStagingRoutingAdapter")
public class DPaymentConsentsStagingRoutingAdapterImpl implements DomesticPaymentStagingAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(DPaymentConsentsStagingRoutingAdapterImpl.class);

	private DomesticPaymentStagingAdapter beanInstance;

	@Autowired
	private LoggerUtils loggerUtils;

	/** The factory. */
	@Autowired
	private DPaymentConsentsAdapterFactory factory;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The default adapter. */
	@Value("${app.paymentSetupStagingAdapter}")
	private String paymentSetupStagingAdapter;

	@Override
	public CustomDPaymentConsentsPOSTResponse processDomesticPaymentConsents(
			CustomDPaymentConsentsPOSTRequest paymentSetupRequest, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.processDomesticPaymentConsents()",
				loggerUtils.populateLoggerData("processDomesticPaymentConsents"));

		CustomDPaymentConsentsPOSTResponse x = getDomesticRoutingAdapter().processDomesticPaymentConsents(
				paymentSetupRequest, stageIdentifiers, addHeaderParams(params), successStatus, failureStatus);

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"tenantId\":{}}",
				"com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.processDomesticPaymentConsents()",
				loggerUtils.populateLoggerData("processDomesticPaymentConsents"),
				JSONUtilities.getJSONOutPutFromObject(reqHeaderAtrributes.getTenantId()));
		return x;
	}

	@Override
	public CustomDPaymentConsentsPOSTResponse retrieveStagedDomesticPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentSetupInfo, Map<String, String> params) {
		return getDomesticRoutingAdapter().retrieveStagedDomesticPaymentConsents(customPaymentSetupInfo,
				addHeaderParams(params));
	}

	private DomesticPaymentStagingAdapter getDomesticRoutingAdapter() {
		if (beanInstance == null)
			beanInstance = factory.getDomesticPaymentSetupStagingInstance(paymentSetupStagingAdapter);
		return beanInstance;
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>();

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}
}
