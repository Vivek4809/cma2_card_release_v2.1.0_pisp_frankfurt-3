package com.capgemini.psd2.pisp.international.payments.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.InternationalPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.international.payments.comparator.InternationalPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.international.payments.service.InternationalPaymentsService;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class InternationalPaymentsServiceImpl implements InternationalPaymentsService {

	@Autowired
	private PaymentSubmissionProcessingAdapter<CustomIPaymentsPOSTRequest, PaymentInternationalSubmitPOST201Response> paymentsProcessingAdapter;

	@Autowired
	@Qualifier("iPaymentsRoutingAdapter")
	private InternationalPaymentsAdapter iPaymentsAdapter;

	@Autowired
	@Qualifier("iPaymentConsentsStagingRoutingAdapter")
	private InternationalPaymentStagingAdapter iPaymentConsentsAdapter;

	@Autowired
	private InternationalPaymentsPayloadComparator iPaymentsComparator;

	@Value("${cmaVersion}")
	private String cmaVersion;
	
	@Override
	public PaymentInternationalSubmitPOST201Response createInternationalPaymentsResource(
			CustomIPaymentsPOSTRequest paymentRequest) {

		PaymentInternationalSubmitPOST201Response paymentsFoundationResponse = null;
		CustomIPaymentConsentsPOSTResponse consentsFoundationResource = null;
		PaymentConsentsPlatformResource paymentConsentPlatformResource = null;
		PaymentsPlatformResource paymentsPlatformResource = null;

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter.preSubmissionProcessing(
				paymentRequest, populatePlatformDetails(), paymentRequest.getData().getConsentId().trim());

		paymentConsentPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);
		paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);

		consentsFoundationResource = internationalPayLoadCompare(paymentRequest, paymentConsentPlatformResource,
				paymentRequest.getData().getConsentId().trim());

		if (paymentsPlatformResource == null) {
			paymentsPlatformResource = paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					paymentRequest.getData().getConsentId(), cmaVersion,
					PaymentTypeEnum.INTERNATIONAL_PAY);
			paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.INCOMPLETE)) {
			paymentRequest = populateInternationalStagedProcessingFields(paymentRequest, consentsFoundationResource);
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap = new HashMap<>();
			paymentStatusMap.put(PaymentConstants.INITIAL, OBTransactionIndividualStatus1Code.PENDING);
			paymentStatusMap.put(PaymentConstants.FAIL, OBTransactionIndividualStatus1Code.REJECTED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AUTH,
					OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTINPROCESS);
			paymentStatusMap.put(PaymentConstants.PASS_M_AWAIT, OBTransactionIndividualStatus1Code.PENDING);
			paymentStatusMap.put(PaymentConstants.PASS_M_REJECT, OBTransactionIndividualStatus1Code.REJECTED);
			paymentRequest.setCreatedOn(paymentsPlatformResource.getCreatedAt());
			paymentsFoundationResponse = iPaymentsAdapter.processPayments(paymentRequest, paymentStatusMap, null);
		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.COMPLETED)) {
			CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
			identifiers.setPaymentSetupVersion(cmaVersion);
			identifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
			identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
			identifiers.setPaymentSubmissionId(paymentsPlatformResource.getSubmissionId());
			paymentsFoundationResponse = iPaymentsAdapter.retrieveStagedPaymentsResponse(identifiers, null);

		}

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		/* Fix for SIT issue #2272 
		 * Tpp providing Blank remittance info, and FS is returning null,
		 * Thus in such case returning same Blank remittance info to TPP.
		 * 27.02.2019 */
		OBRemittanceInformation1 requestRemittanceInfo = paymentRequest.getData().getInitiation().getRemittanceInformation();
		paymentsFoundationResponse.getData().getInitiation().setRemittanceInformation(requestRemittanceInfo);
		
		return paymentsProcessingAdapter.postPaymentProcessFlows(paymentsPlatformResourceMap,
				paymentsFoundationResponse, paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getInternationalPaymentId(), multiAuthStatus,
				HttpMethod.POST.toString());
	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails() {
		CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails = new CustomPaymentSetupPlatformDetails();
		customPaymentSetupPlatformDetails.setSetupCmaVersion(cmaVersion);
		customPaymentSetupPlatformDetails.setPaymentType(PaymentTypeEnum.INTERNATIONAL_PAY);
		return customPaymentSetupPlatformDetails;
	}

	private CustomIPaymentConsentsPOSTResponse internationalPayLoadCompare(CustomIPaymentsPOSTRequest submissionRequest,
			PaymentConsentsPlatformResource paymentConsentPlatformResource, String consentId) {

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentConsentId(consentId);
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);

		CustomIPaymentConsentsPOSTResponse paymentSetupFoundationResponse = iPaymentConsentsAdapter
				.retrieveStagedInternationalPaymentConsents(identifiers, null);

		if (iPaymentsComparator.comparePaymentDetails(paymentSetupFoundationResponse, submissionRequest,
				paymentConsentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));
		return paymentSetupFoundationResponse;
	}

	private CustomIPaymentsPOSTRequest populateInternationalStagedProcessingFields(
			CustomIPaymentsPOSTRequest submissionRequest,
			CustomIPaymentConsentsPOSTResponse paymentSetupFoundationResponse) {
		/*
		 * If TPP has not sent DebtorAccount.Name then product will set the name
		 * details from staging record which is sent by FS
		 */
		if (submissionRequest.getData().getInitiation().getDebtorAccount() != null && NullCheckUtils
				.isNullOrEmpty(submissionRequest.getData().getInitiation().getDebtorAccount().getName())) {

			submissionRequest.getData().getInitiation().getDebtorAccount()
					.setName(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount().getName());
		}
		/*
		 * If debtor details are not provided by TPP then Platform will set
		 * debtor details received from FS. Same will be sent to FS for
		 * Pre-SubmissionValidation and Account Permission
		 */

		if (submissionRequest.getData().getInitiation().getDebtorAccount() == null) {
			submissionRequest.getData().getInitiation()
					.setDebtorAccount(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount());
		}
		submissionRequest.setFraudSystemResponse(paymentSetupFoundationResponse.getFraudScore());
		return submissionRequest;
	}

	@Override
	public PaymentInternationalSubmitPOST201Response retrieveInternationalPaymentsResource(String submissionId) {

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter
				.prePaymentProcessGETFlows(submissionId, PaymentTypeEnum.INTERNATIONAL_PAY);

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
		identifiers.setPaymentSubmissionId(submissionId);

		PaymentInternationalSubmitPOST201Response paymentsFoundationResponse = iPaymentsAdapter
				.retrieveStagedPaymentsResponse(identifiers, null);

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return paymentsProcessingAdapter.postPaymentProcessFlows(paymentsPlatformResourceMap,
				paymentsFoundationResponse, paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getInternationalPaymentId(), multiAuthStatus,
				HttpMethod.GET.toString());
	}

}