package com.capgemini.psd2.pisp.international.payments.test.mockdata;

import java.util.HashMap;
import java.util.Map;

import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.status.PaymentConstants;

public class IPResponseTransformerImplTestMockdata {

	// Map<String, Object> paymentsPlatformResourceMap;
	public static PaymentInternationalSubmitPOST201Response paymentSubmissionResponse;

	public static Map<String, Object> getpaymentsPlatformResourceMap() {

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentsPlatformResource value = new PaymentsPlatformResource();
		value.setPaymentConsentId("123456");
		value.setStatus("Pending");
		paymentsPlatformResourceMap.put("submission", value);

		return paymentsPlatformResourceMap;
	}

	public static Map<String, Object> getpaymentsPlatformResourceMap1() {

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentsPlatformResource value = new PaymentsPlatformResource();
		value.setPaymentConsentId("123456");
		value.setStatus("AcceptedSettlementCompleted");
		paymentsPlatformResourceMap.put("submission", value);

		return paymentsPlatformResourceMap;
	}

	public static Map<String, Object> getpaymentsPlatformResourceMap2() {

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentsPlatformResource value = new PaymentsPlatformResource();
		value.setPaymentConsentId("123456");
		value.setStatus("AcceptedSettlementInProcess");
		
		
		paymentsPlatformResourceMap.put("submission", value);

		return paymentsPlatformResourceMap;
	}
	
	public static Map<String, Object> getpaymentsPlatformResourceMap3() {

		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentsPlatformResource value = new PaymentsPlatformResource();
		value.setPaymentConsentId("123456");
		value.setStatus("AcceptedSettlementInProcess");
		value.setStatusUpdateDateTime("31/01/2019");
		
		PaymentConsentsPlatformResource vid=new PaymentConsentsPlatformResource();
		vid.setTppDebtorDetails("false");
		paymentsPlatformResourceMap.put("submission", value);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, vid);

		return paymentsPlatformResourceMap;
	}

	public static PaymentInternationalSubmitPOST201Response getPOST201response() {

		paymentSubmissionResponse = new PaymentInternationalSubmitPOST201Response();

		OBWriteDataInternationalResponse1 data = new OBWriteDataInternationalResponse1();
		data.setStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED);
		data.setConsentId("1234");
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBCashAccountDebtor3 debtor = new OBCashAccountDebtor3();
		debtor.setSchemeName("IBAN");
		creditorAccount.setSchemeName("IBAN");
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtor);
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multiAuthorisation);
		paymentSubmissionResponse.setData(data);
		return paymentSubmissionResponse;

	}
	
	
	public static PaymentInternationalSubmitPOST201Response getPOST201responseMetaandLinks() {

		paymentSubmissionResponse = new PaymentInternationalSubmitPOST201Response();

		OBWriteDataInternationalResponse1 data = new OBWriteDataInternationalResponse1();
		data.setStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED);
		data.setConsentId("1234");
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBCashAccountDebtor3 debtor = new OBCashAccountDebtor3();
		debtor.setSchemeName("IBAN");
		creditorAccount.setSchemeName("IBAN");
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtor);
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multiAuthorisation);
		paymentSubmissionResponse.setData(data);
		paymentSubmissionResponse.setLinks(new PaymentSetupPOSTResponseLinks());
		paymentSubmissionResponse.setMeta(new PaymentSetupPOSTResponseMeta());
		return paymentSubmissionResponse;

	}
	
	
	public static PaymentInternationalSubmitPOST201Response getPOST201responseDetbtandCredScehemmismatch() {

		paymentSubmissionResponse = new PaymentInternationalSubmitPOST201Response();

		OBWriteDataInternationalResponse1 data = new OBWriteDataInternationalResponse1();
		data.setStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED);
		data.setConsentId("1234");
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBCashAccountDebtor3 debtor = new OBCashAccountDebtor3();
		debtor.setSchemeName("IBAN1");
		creditorAccount.setSchemeName("IBAN1");
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtor);
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multiAuthorisation);
		paymentSubmissionResponse.setData(data);
		return paymentSubmissionResponse;

	}
	
	public static PaymentInternationalSubmitPOST201Response getPOST201response1() {

		paymentSubmissionResponse = new PaymentInternationalSubmitPOST201Response();

		OBWriteDataInternationalResponse1 data = new OBWriteDataInternationalResponse1();
		data.setStatus(OBTransactionIndividualStatus1Code.PENDING);
		data.setConsentId("1234");
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBCashAccountDebtor3 debtor = new OBCashAccountDebtor3();
		debtor.setSchemeName("IBAN");
		creditorAccount.setSchemeName("IBAN");
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtor);
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multiAuthorisation);
		paymentSubmissionResponse.setData(data);
		return paymentSubmissionResponse;

	}
	
	public static PaymentInternationalSubmitPOST201Response getPOST201responseDebtor() {

		paymentSubmissionResponse = new PaymentInternationalSubmitPOST201Response();

		OBWriteDataInternationalResponse1 data = new OBWriteDataInternationalResponse1();
		data.setConsentId("1234");
		OBInternational1 initiation = new OBInternational1();
		OBCashAccountDebtor3 debtor = new OBCashAccountDebtor3();
		debtor.setSchemeName("IBAN");
		initiation.setDebtorAccount(debtor);
		data.setInitiation(initiation);
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multiAuthorisation);
		paymentSubmissionResponse.setData(data);
		return paymentSubmissionResponse;

	}

}
