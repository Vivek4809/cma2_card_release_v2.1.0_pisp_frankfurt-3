package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.DomesticStandingOrdersConsentsFoundationServiceAdapterApplication;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.client.DomesticStandingOrdersConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.delegate.DomesticStandingOrdersConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.transformer.DomesticStandingOrdersConsentsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringRunner.class)
public class DomesticStandingOrdersConsentsFoundationServiceDelegateTest {

	@InjectMocks
	DomesticStandingOrdersConsentsFoundationServiceDelegate delegate;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceTransformer transformer;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceClient client;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceAdapterApplication adapter;
	
	@Mock
	private RestClientSync restClient;
	
	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Context loads.
	 */

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testStandingOrderConsentFoundationServiceDelegateCreate() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partySourceId", "partysource");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");

		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();

		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");

		delegate.createPaymentRequestHeadersForProcess(params);
	}
	
	@Test
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlCreate() {

		String version = "v2.0";
		delegate.postPaymentFoundationServiceURL(version);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlCreate1() {

		String version = null;
		delegate.postPaymentFoundationServiceURL(version);

	}
	
	@Test
	public void testStandingOrderConsentFoundationServiceDelegateRetrive() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partySourceId", "partysource");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");

		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();

		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");

		delegate.createPaymentRequestHeadersForGet( params);
	}
	
	@Test
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlRetrive() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.getPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlRetrive1() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.getPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlRetrive2() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.getPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}
}
