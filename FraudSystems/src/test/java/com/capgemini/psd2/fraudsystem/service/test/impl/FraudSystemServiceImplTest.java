package com.capgemini.psd2.fraudsystem.service.test.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.fraudsystem.policy.impl.FraudSystemStoragetPolicy;
import com.capgemini.psd2.fraudsystem.policy.impl.FraudSystemUsagePolicy;
import com.capgemini.psd2.fraudsystem.request.handler.FraudSystemRequestAttributes;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl;
import com.capgemini.psd2.fraudsystem.service.impl.FraudSystemServiceImpl;

public class FraudSystemServiceImplTest {

	@Mock
	private FraudSystemRequestHandlerImpl fraudSystemDataHandler;

	@Mock
	private FraudSystemRequestAttributes fraudSystemRequestAttributes;

	@Mock
	private FraudSystemAdapter fraudSystemAdapter;

	@Mock
	private FraudSystemStoragetPolicy fraudSystemStoragetPolicy;

	@Mock
	private FraudSystemUsagePolicy fraudSystemUsagePolicy;

	@InjectMocks
	FraudSystemServiceImpl fraudSystemServiceImpl = new FraudSystemServiceImpl();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	PSD2CustomerInfo custInfo = new PSD2CustomerInfo();

	@Test
	public void processServiceTest() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("authenticationStatus", "loginSuccess");
		map.put("pageType", FraudSystemConstants.CONSENT_PAGE);
		map.put("consentStatus", FraudSystemConstants.CONSENT_SUCCESS);
		map.put("flowType", FraudSystemConstants.PISP_FLOW);
		Map<String, Map<String, Object>> fraudnetRequest = new HashMap<String, Map<String, Object>>();
		String fraudnetResponse = "dummy";
		when(fraudSystemDataHandler.captureContactInfo(map)).thenReturn(custInfo);
		doNothing().when(fraudSystemDataHandler).captureDeviceEventInfo(map);
		doNothing().when(fraudSystemDataHandler).captureFinanacialAccountsInfo(custInfo, map);
		doNothing().when(fraudSystemDataHandler).captureTransferInfo(map);
		when(fraudSystemRequestAttributes.getFraudSystemRequest()).thenReturn(fraudnetRequest);
		when(fraudSystemAdapter.retrieveFraudScore(fraudnetRequest)).thenReturn(fraudnetResponse);
		assertEquals(fraudnetResponse, fraudSystemServiceImpl.callFraudSystemService(map));
	}

	@Test
	public void processServiceExceptionTest() {
		Map<String, Object> map = null;
		doNothing().when(fraudSystemDataHandler).captureDeviceEventInfo(map);
		assertNull(fraudSystemServiceImpl.callFraudSystemService(map));
	}

	@Test
	public void processServiceIfBranchTest() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("authenticationStatus", "loginFail");
		map.put("pageType", "fail");
		map.put("consentStatus", "fail");
		map.put("flowType", "fail");
		fraudSystemServiceImpl.callFraudSystemService(map);
	}

}
