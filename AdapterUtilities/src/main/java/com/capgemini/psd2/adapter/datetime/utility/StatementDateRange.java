package com.capgemini.psd2.adapter.datetime.utility;

import java.util.Date;

public class StatementDateRange {

	private Date consentExpiryDateTime;
	private Date transactionFromDateTime;
	private Date transactionToDateTime;
	private Date requestDateTime;
	private Date filterFromDate;
	private Date filterToDate;
	private Date newFilterFromDate;
	private Date newFilterToDate;
	private boolean emptyResponse;
	
	public Date getConsentExpiryDateTime() {
		return consentExpiryDateTime;
	}
	public void setConsentExpiryDateTime(Date consentExpiryDateTime) {
		this.consentExpiryDateTime = consentExpiryDateTime;
	}
	public Date getTransactionFromDateTime() {
		return transactionFromDateTime;
	}
	public void setTransactionFromDateTime(Date transactionFromDateTime) {
		this.transactionFromDateTime = transactionFromDateTime;
	}
	public Date getTransactionToDateTime() {
		return transactionToDateTime;
	}
	public void setTransactionToDateTime(Date transactionToDateTime) {
		this.transactionToDateTime = transactionToDateTime;
	}
	public Date getRequestDateTime() {
		return requestDateTime;
	}
	public void setRequestDateTime(Date requestDateTime) {
		this.requestDateTime = requestDateTime;
	}
	public Date getFilterFromDate() {
		return filterFromDate;
	}
	public void setFilterFromDate(Date filterFromDate) {
		this.filterFromDate = filterFromDate;
	}
	public Date getFilterToDate() {
		return filterToDate;
	}
	public void setFilterToDate(Date filterToDate) {
		this.filterToDate = filterToDate;
	}
	public Date getNewFilterFromDate() {
		return newFilterFromDate;
	}
	public void setNewFilterFromDate(Date newFilterFromDate) {
		this.newFilterFromDate = newFilterFromDate;
	}
	public Date getNewFilterToDate() {
		return newFilterToDate;
	}
	public void setNewFilterToDate(Date newFilterToDate) {
		this.newFilterToDate = newFilterToDate;
	}
	public boolean isEmptyResponse() {
		return emptyResponse;
	}
	public void setEmptyResponse(boolean emptyResponse) {
		this.emptyResponse = emptyResponse;
	}

	
	
	
}
