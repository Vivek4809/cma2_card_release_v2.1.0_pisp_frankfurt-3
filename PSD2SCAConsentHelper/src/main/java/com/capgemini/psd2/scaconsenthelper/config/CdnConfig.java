package com.capgemini.psd2.scaconsenthelper.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;

@Configuration
public class CdnConfig {

     private static String cdnBaseURL;
	
	private static String libsHashCode;
	
	private static String templateHashCode;
	
	private static String appHashCode;
	
	private static String buildVersion;
	
	@Value("${cdn.baseURL}")
	public  void setCdnBaseURL(String cdnBaseURL) {
		CdnConfig.cdnBaseURL = cdnBaseURL;
	}
	@Value("${cdn.libsHashCode}")
	public  void setLibsHashCode(String libsHashCode) {
		CdnConfig.libsHashCode = libsHashCode;
	}
	@Value("${cdn.templateHashCode}")
	public  void setTemplateHashCode(String templateHashCode) {
		CdnConfig.templateHashCode = templateHashCode;
	}
	@Value("${cdn.appHashCode}")
	public  void setAppHashCode(String appHashCode) {
		CdnConfig.appHashCode = appHashCode;
	}
	@Value("${cdn.buildVersion}")
	public  void setBuildVersion(String buildVersion) {
		CdnConfig.buildVersion = buildVersion;
	}
	
	public static Map<String,String> populateCdnAttributes(){
		Map<String,String> map = new HashMap<>();
		map.put("cdnBaseURL",cdnBaseURL);
		map.put("libsHashCode",PSD2SecurityConstants.CDN_SHA_256+libsHashCode);
		map.put("templateHashCode",PSD2SecurityConstants.CDN_SHA_256+templateHashCode);
		map.put("appHashCode",PSD2SecurityConstants.CDN_SHA_256+appHashCode);
		map.put("buildVersion",buildVersion);
		return map;
	}
}
