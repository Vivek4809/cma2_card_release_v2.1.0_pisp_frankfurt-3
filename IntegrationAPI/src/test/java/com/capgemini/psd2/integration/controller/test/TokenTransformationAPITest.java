package com.capgemini.psd2.integration.controller.test;


import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.config.TokenTransformationConfigBean;
import com.capgemini.psd2.integration.controller.TokenTransformationAPI;
import com.capgemini.psd2.integration.dtos.TPPInformationDTO;
import com.capgemini.psd2.integration.service.TPPInformationService;
import com.capgemini.psd2.integration.service.TokenTransformationService;
import com.capgemini.psd2.token.TPPInformation;

public class TokenTransformationAPITest {
	@Mock
	private TokenTransformationConfigBean configBean;
	
	@Mock
	private TokenTransformationService tokenTransformationService;
	
	@Mock
	private TPPInformationService tppInformationService;
	
	@Mock 
	private HttpServletRequest request;
	
	@InjectMocks
	public TokenTransformationAPI controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);	
	}
	
	@Test(expected = PSD2Exception.class)
	public void transformTokenNullClientIdTest(){
		controller.transformToken("1234");
	}
	
	@Test(expected=PSD2Exception.class)
	public void transformTokenNotNullClientIdScopeNullTest(){
		when(request.getHeader("client_id")).thenReturn("abc");
		when(request.getHeader("scope")).thenReturn("open accounts");
		controller.transformToken("1234");
	}
	
	@Test
	public void transformScopeAccountTest(){
		TPPInformationDTO tppInformationDTO = new TPPInformationDTO();
		Set<String> tppRoles = new HashSet<>();
		TPPInformation tppInformation = new TPPInformation();
		tppInformation.setTppLegalEntityName("abc");
		tppInformation.setTppRegisteredId("123");
		tppInformation.setTppRoles(tppRoles);
		tppInformationDTO.setTppInformation(tppInformation);
		Mockito.doNothing().when(tokenTransformationService).transformAISPToken(anyObject(), anyObject());
		Mockito.when(tppInformationService.findTPPInformation(anyObject())).thenReturn(tppInformationDTO);
		when(request.getHeader("client_id")).thenReturn("abc");
		when(request.getHeader("scope")).thenReturn("openid accounts");
		controller.transformToken("1234");
	}
	
	@Test
	public void transformScopePaymentTest(){
		TPPInformationDTO tppInformationDTO = new TPPInformationDTO();
		Set<String> tppRoles = new HashSet<>();
		List<String> list = new ArrayList<>();
		list.add("scope");
		TPPInformation tppInformation = new TPPInformation();
		tppInformation.setTppLegalEntityName("abc");
		tppInformation.setTppRegisteredId("123");
		tppInformation.setTppRoles(tppRoles);
		tppInformationDTO.setTppInformation(tppInformation);
		Mockito.doNothing().when(tokenTransformationService).transformAISPToken(anyObject(), anyObject());
		Mockito.when(tppInformationService.findTPPInformation(anyObject())).thenReturn(tppInformationDTO);
		Mockito.when(configBean.getServiceparams()).thenReturn(list);
		when(request.getHeader("client_id")).thenReturn("abc");
		when(request.getHeader("scope")).thenReturn("openid accounts");
		controller.transformToken("1234");
	}
	}
	
