package com.capgemini.psd2.integration.utilities;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import org.apache.commons.codec.binary.Base64;
import javax.xml.bind.DatatypeConverter;

public class X509CertificateUtils {

	private static CertificateFactory certificateFactory = null;

	private X509CertificateUtils() {

	}

	static {
		try {
			certificateFactory = CertificateFactory.getInstance("X509");
		} catch (CertificateException e) {
			e.printStackTrace(System.err);
		}
	}

	public static X509Certificate buildX509Certificate(String certificate) throws CertificateException {
		
		byte [] decoded = Base64.decodeBase64(certificate.replaceAll("-----BEGIN CERTIFICATE-----", "").replaceAll("-----END CERTIFICATE-----", ""));
		InputStream in = new ByteArrayInputStream(decoded);
		return (X509Certificate) certificateFactory.generateCertificate(in);
	}

	public static String extractSha256Thumbprint(X509Certificate xCert)
			throws NoSuchAlgorithmException, CertificateEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] encodedCert = xCert.getEncoded();
		md.update(encodedCert);
		byte[] digest = md.digest();
		String digestHex = DatatypeConverter.printHexBinary(digest);
		return digestHex.toLowerCase();
	}

}
