package com.capgemini.psd2.account.request.routing.adapter.routing;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;

public interface AccountRequestAdapterFactory {

	public AccountRequestAdapter getAdapterInstance(String coreSystemName); 
}
