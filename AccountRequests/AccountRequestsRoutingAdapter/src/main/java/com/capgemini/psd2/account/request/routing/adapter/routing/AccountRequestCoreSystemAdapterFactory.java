package com.capgemini.psd2.account.request.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;

@Component
public class AccountRequestCoreSystemAdapterFactory implements ApplicationContextAware, AccountRequestAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public AccountRequestAdapter getAdapterInstance(String adapterName) {
		return (AccountRequestAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {

		this.applicationContext = applicationContext;
	}
}
