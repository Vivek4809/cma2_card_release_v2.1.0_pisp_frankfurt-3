package com.capgemini.psd2.mongo.db.config;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

@Component
public class AccountAwsObject {

	
	public S3Object getAccountAwsObjectS3Object(AmazonS3 s3client,String awsS3BucketName,String fileName){
			
		S3Object fullObject = s3client.getObject(new GetObjectRequest(awsS3BucketName,fileName ));
	
		return fullObject;
	}
	
	public AmazonS3 getAccountAwsClient(BasicAWSCredentials awsCredentials,String awsRegion){
			
		 AmazonS3 s3client=AmazonS3ClientBuilder
				.standard()
				.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
				.withRegion(awsRegion)
				.build();
		return s3client;
	}
	
}
