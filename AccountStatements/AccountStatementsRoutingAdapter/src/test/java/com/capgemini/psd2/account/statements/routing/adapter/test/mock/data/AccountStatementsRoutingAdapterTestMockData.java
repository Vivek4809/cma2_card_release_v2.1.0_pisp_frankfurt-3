package com.capgemini.psd2.account.statements.routing.adapter.test.mock.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.capgemini.psd2.account.statements.mongo.db.adapter.constants.AccountStatementMongoDbAdapterConstants;
import com.capgemini.psd2.aisp.domain.OBReadStatement1;
import com.capgemini.psd2.aisp.domain.OBReadStatement1Data;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3;
import com.capgemini.psd2.aisp.domain.OBReadTransaction3Data;
import com.capgemini.psd2.aisp.domain.OBStatement1;
import com.capgemini.psd2.aisp.domain.OBTransaction3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;

public class AccountStatementsRoutingAdapterTestMockData {


	public static PlatformAccountStatementsResponse getMockStatementGETResponse() {
		PlatformAccountStatementsResponse platformAccountStatementsResponse = new PlatformAccountStatementsResponse();
		OBReadStatement1Data data = new OBReadStatement1Data();
		List<OBStatement1> statement = new ArrayList<>();
		data.setStatement(statement);
		OBReadStatement1 oBReadStatement1 = new OBReadStatement1();
		oBReadStatement1.setData(data);
		platformAccountStatementsResponse.setoBReadStatement1(oBReadStatement1);
		return platformAccountStatementsResponse;
	
}
	public static PlatformAccountTransactionResponse getMockTransactionGETResponse() {
	PlatformAccountTransactionResponse platformAccountTransactionResponse = new PlatformAccountTransactionResponse();
	OBReadTransaction3Data data = new OBReadTransaction3Data();
	List<OBTransaction3> transaction = new ArrayList<>();
	data.setTransaction(transaction);
	OBReadTransaction3 oBReadTransaction2 = new OBReadTransaction3();
	oBReadTransaction2.setData(data);
	platformAccountTransactionResponse.setoBReadTransaction3(oBReadTransaction2);
	return platformAccountTransactionResponse;

}
	
	public static PlatformAccountSatementsFileResponse getMockFileGETResponse() {
		PlatformAccountSatementsFileResponse platformAccountSatementsFileResponse = new PlatformAccountSatementsFileResponse();
		
		platformAccountSatementsFileResponse.setFileName("local.pdf");
		platformAccountSatementsFileResponse.setFileByteArray(null);

		return platformAccountSatementsFileResponse;

	}
	
	
	
	
}	