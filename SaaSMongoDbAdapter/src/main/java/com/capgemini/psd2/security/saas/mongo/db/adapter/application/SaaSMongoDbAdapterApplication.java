package com.capgemini.psd2.security.saas.mongo.db.adapter.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaaSMongoDbAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaaSMongoDbAdapterApplication.class, args);
	}
}
