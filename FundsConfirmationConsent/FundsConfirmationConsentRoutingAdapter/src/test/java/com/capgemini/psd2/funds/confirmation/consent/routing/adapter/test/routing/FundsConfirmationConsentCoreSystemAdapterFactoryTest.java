package com.capgemini.psd2.funds.confirmation.consent.routing.adapter.test.routing;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.funds.confirmation.consent.routing.adapter.routing.FundsConfirmationConsentCoreSystemAdapterFactory;

public class FundsConfirmationConsentCoreSystemAdapterFactoryTest {

	@Mock
	private ApplicationContext context;
	
	/** The account information core system adapter factory. */
	@InjectMocks
	private FundsConfirmationConsentCoreSystemAdapterFactory obj;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	@Test
	public void testGetAdapterInstance() {
		FundsConfirmationConsentAdapter adapter=new FundsConfirmationConsentAdapter() {

			@Override
			public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsentPOSTResponse(
					OBFundsConfirmationConsentDataResponse1 data1) {
				return null;
			}

			@Override
			public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsentPOSTResponse(String consentId) {
				return null;
			}

			@Override
			public void removeFundsConfirmationConsent(String consentId,String dsf) {
				
			}

			@Override
			public OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentResponse(String consentId,
					OBExternalRequestStatus1Code statusEnum) {
				return null;
			}
			
		};

		obj.setApplicationContext(context);
		when(context.getBean(anyString())).thenReturn(null);
		obj.getAdapterInstance("abc");

		
	}
}
