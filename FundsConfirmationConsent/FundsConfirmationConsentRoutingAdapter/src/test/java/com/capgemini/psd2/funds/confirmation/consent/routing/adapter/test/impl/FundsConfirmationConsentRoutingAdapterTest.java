package com.capgemini.psd2.funds.confirmation.consent.routing.adapter.test.impl;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.funds.confirmation.consent.routing.adapter.impl.FundsConfirmationConsentRoutingAdapter;
import com.capgemini.psd2.funds.confirmation.consent.routing.adapter.routing.FundsConfirmationConsentAdapterFactory;

public class FundsConfirmationConsentRoutingAdapterTest {

	@Mock
	private FundsConfirmationConsentAdapterFactory fundsConfirmationConsentAdapterFactory;
	
	@InjectMocks
	private FundsConfirmationConsentRoutingAdapter fundsConfirmationConsentRoutingAdapter;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreateFundsConfirmationConsentPOSTResponse() {
		FundsConfirmationConsentAdapter adapter=new FundsConfirmationConsentAdapter() {

			@Override
			public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsentPOSTResponse(
					OBFundsConfirmationConsentDataResponse1 data1) {
				return null;
			}

			@Override
			public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsentPOSTResponse(String consentId) {
				return null;
			}

			@Override
			public void removeFundsConfirmationConsent(String consentId,String client) {
				
			}

			@Override
			public OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentResponse(String consentId,
					OBExternalRequestStatus1Code statusEnum) {
				return null;
			}	
	};
	when(fundsConfirmationConsentAdapterFactory.getAdapterInstance(anyString())).thenReturn(adapter);
	fundsConfirmationConsentRoutingAdapter.createFundsConfirmationConsentPOSTResponse(null);
	fundsConfirmationConsentRoutingAdapter.getFundsConfirmationConsentPOSTResponse("12345");
	fundsConfirmationConsentRoutingAdapter.removeFundsConfirmationConsent("1234","rtr");
	fundsConfirmationConsentRoutingAdapter.updateFundsConfirmationConsentResponse("1234", OBExternalRequestStatus1Code.AUTHORISED);
}
}