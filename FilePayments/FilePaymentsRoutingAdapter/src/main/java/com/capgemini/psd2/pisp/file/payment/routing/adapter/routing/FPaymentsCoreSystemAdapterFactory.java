package com.capgemini.psd2.pisp.file.payment.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.pisp.adapter.FilePaymentsAdapter;

@Component
public class FPaymentsCoreSystemAdapterFactory implements ApplicationContextAware, FPaymentsAdapterFactory{

	private ApplicationContext applicationContext;
	
	@Override
	public FilePaymentsAdapter getFilePaymentsStagingInstance(String adapterName) {
		return (FilePaymentsAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

}
