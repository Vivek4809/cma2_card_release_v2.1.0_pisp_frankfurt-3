package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.transformer.DomesticScheduledPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.utilities.NullCheckUtils;
@Component
public class DomesticScheduledPaymentConsentsFoundationServiceDelegate {

	@Autowired
	private DomesticScheduledPaymentConsentsFoundationServiceTransformer domesticPaymentConsentsFoundationServiceTransformer;
	
	@Value("${foundationService.transactionReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactionReqHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;
	
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;
	
	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourcesystem;
	
	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;
	
	@Value("${foundationService.domesticScheduledPaymentConsentBaseURL}")
	private String domesticScheduledPaymentConsentBaseURL;
	
	@Value("${foundationService.apiChannelCode:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceId;
	
	@Value("${foundationService.domesticPaymentConsentSetupVersion}")
	private String domesticScheduledPaymentConsentSetupVersion;
	
	@Value("${foundationService.apiChannelBrand:#{X-API-CHANNEL-BRAND}}")
	private String apiChannelBrand;
	
	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;
	
	public HttpHeaders createPaymentRequestHeaders(Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
		httpHeaders.add(transactionReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		}
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}
		httpHeaders.add(sourcesystem,"PSD2API");
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
		httpHeaders.add(apiChannelCode, (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase());
		}
		httpHeaders.add(systemApiVersion, "3.0");
		
		//Channel Brand
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
			httpHeaders.add(apiChannelBrand, BrandCode3.NIGB.toString());
		}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
			httpHeaders.add(apiChannelBrand, BrandCode3.ROI.toString());
		}
	}	
	
		return httpHeaders;
	}
	
	public String getPaymentFoundationServiceURL(String paymentInstuctionProposalId){
		return domesticScheduledPaymentConsentBaseURL + "/" + "v" +domesticScheduledPaymentConsentSetupVersion + "/domestic/scheduled/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

	public HttpHeaders createPaymentRequestHeadersPost(Map<String, String> params) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
		httpHeaders.add(apiChannelCode, (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase());
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
		httpHeaders.add(transactionReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		
		}
		httpHeaders.add(sourcesystem, "PSD2API");
		
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
			httpHeaders.add(apiChannelBrand, BrandCode3.NIGB.toString());
		}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
			httpHeaders.add(apiChannelBrand, BrandCode3.ROI.toString());
		}
	}
		httpHeaders.add(systemApiVersion, "3.0");
		
		return httpHeaders;
	}

	public String postPaymentFoundationServiceURL() {
		
		return domesticScheduledPaymentConsentBaseURL + "/" + "v"+domesticScheduledPaymentConsentSetupVersion   + "/domestic/scheduled/payment-instruction-proposals";
	}

	public ScheduledPaymentInstructionProposal transformDomesticConsentResponseFromAPIToFDForInsert(
			CustomDSPConsentsPOSTRequest paymentConsentsRequest,Map<String, String> params) {
		
		return domesticPaymentConsentsFoundationServiceTransformer.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
	}



}
