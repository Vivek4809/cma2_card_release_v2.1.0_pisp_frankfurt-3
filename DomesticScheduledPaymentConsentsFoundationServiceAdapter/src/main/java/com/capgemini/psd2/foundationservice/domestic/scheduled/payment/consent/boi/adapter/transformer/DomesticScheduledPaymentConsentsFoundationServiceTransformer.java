package com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.Channel;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ChannelCode;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPermissions2Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticScheduledPaymentConsentsFoundationServiceTransformer {
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Value("${foundationService.endToEndIdentificationMockValue}")
	private String endToEndIdentificationMockValue;
	

	

	public <T> GenericPaymentConsentResponse transformDomesticConsentResponseFromFDToAPIForInsert(
			T paymentInstructionProposalResponse) {
		GenericPaymentConsentResponse consentResponse = new GenericPaymentConsentResponse();

		List<OBCharge1> charges = new ArrayList<>();
		ScheduledPaymentInstructionProposal paymentInstructionProposal = (ScheduledPaymentInstructionProposal) paymentInstructionProposalResponse;
		List<PaymentInstructionCharge> paymentInstructionCharges;
		OBCharge1Amount oBCharge1Amount = new OBCharge1Amount();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getCharges())) {
			
			oBCharge1Amount.setAmount(
					paymentInstructionProposal.getCharges().get(0).getAmount().getTransactionCurrency().toString());
			oBCharge1Amount.setCurrency(paymentInstructionProposal.getCharges().get(0).getCurrency().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getCharges())) {
			paymentInstructionCharges = paymentInstructionProposal.getCharges();
			for (PaymentInstructionCharge charge : paymentInstructionCharges) {
				OBCharge1 oBCharge1 = new OBCharge1();
				oBCharge1.setChargeBearer(OBChargeBearerType1Code.fromValue(String.valueOf(charge.getChargeBearer())));
				oBCharge1.setType(charge.getType());
				oBCharge1.setAmount(oBCharge1Amount);
				charges.add(oBCharge1);
			}
		}
		consentResponse.setConsentId(paymentInstructionProposal.getPaymentInstructionProposalId());
		if (!charges.isEmpty())
			consentResponse.setCharges(charges);
		ProposalStatus status = paymentInstructionProposal.getProposalStatus();

		if (!NullCheckUtils.isNullOrEmpty(status)) {
			if (((status.toString()).equals("Rejected"))
					&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalId()))) {
				ProcessConsentStatusEnum consentStatus = ProcessConsentStatusEnum.FAIL;
				consentResponse.setProcessConsentStatusEnum(consentStatus);
			} else if (((status.toString()).equals("AwaitingAuthorisation"))
					&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalId()))) {
				ProcessConsentStatusEnum consentStatus = ProcessConsentStatusEnum.PASS;
				consentResponse.setProcessConsentStatusEnum(consentStatus);
			}
		}
		return consentResponse;
	}

	public ScheduledPaymentInstructionProposal transformDomesticConsentResponseFromAPIToFDForInsert(
			CustomDSPConsentsPOSTRequest paymentConsentsRequest,Map<String, String> params)  {
		ScheduledPaymentInstructionProposal instructionProposal2 = new ScheduledPaymentInstructionProposal();
		
		//---As per FS requirement set Id as blank instead of null.---
		instructionProposal2.setPaymentInstructionProposalId("");

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData())) {
			instructionProposal2.setPermission(paymentConsentsRequest.getData().getPermission().toString());
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation()))
				instructionProposal2.setInstructionReference(
						paymentConsentsRequest.getData().getInitiation().getInstructionIdentification());
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getEndToEndIdentification())){
				instructionProposal2.setInstructionEndToEndReference(
						paymentConsentsRequest.getData().getInitiation().getEndToEndIdentification());
			}else{
				instructionProposal2.setInstructionEndToEndReference(endToEndIdentificationMockValue);
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getLocalInstrument()))
				instructionProposal2.setInstructionLocalInstrument(
						paymentConsentsRequest.getData().getInitiation().getLocalInstrument());
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime())) {
				instructionProposal2.setRequestedExecutionDateTime(paymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime()); 
			}

			Channel channel = new Channel();
			if (!NullCheckUtils
					.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
				
				channel.setChannelCode(ChannelCode.fromValue((params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase()));
			}else {
				channel.setChannelCode(ChannelCode.API);
			}
			
			if (!NullCheckUtils
					.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
				
				if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
					channel.setBrandCode(BrandCode3.NIGB);
				}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
					channel.setBrandCode(BrandCode3.ROI);
				}	
				
			}

			instructionProposal2.setChannel(channel);
		
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getInstructedAmount())) {
				Currency transactionCurrency = new Currency();
				transactionCurrency.setIsoAlphaCode(
						paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency());
				FinancialEventAmount financialEventAmount = new FinancialEventAmount();
				financialEventAmount.setTransactionCurrency(Double.parseDouble(
						paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getAmount()));
				instructionProposal2.setFinancialEventAmount(financialEventAmount);
				instructionProposal2.setTransactionCurrency(transactionCurrency);
			}
			
			//---Creditor------
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount())) {
				ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
				proposingPartyAccount.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
				proposingPartyAccount.setAccountIdentification(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification());
				proposingPartyAccount.setAccountName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getName());
				proposingPartyAccount.setSecondaryIdentification(paymentConsentsRequest.getData().getInitiation()
						.getCreditorAccount().getSecondaryIdentification());
				instructionProposal2.setProposingPartyAccount(proposingPartyAccount);
			}
			//---Debtor	------
			
			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getDebtorAccount())) {
				AuthorisingPartyAccount account = new AuthorisingPartyAccount();
				account.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
				account.setAccountIdentification(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification());
				account.setAccountName(paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getName());
				account.setSecondaryIdentification(paymentConsentsRequest.getData().getInitiation().getDebtorAccount()
						.getSecondaryIdentification());
				instructionProposal2.setAuthorisingPartyAccount(account);
			}

			// Setting CreditorPostalAddress from API to FD
			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress())) {
				PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();

				boolean isInt=false;
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressType())) {
					proposingPartyPostalAddress.setAddressType(String.valueOf(
							paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getAddressType())); 
					isInt=true;
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getDepartment())) {
					proposingPartyPostalAddress.setDepartment(
							paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getDepartment()); 
					isInt=true;
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getSubDepartment())) {
					proposingPartyPostalAddress.setSubDepartment(
							paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getSubDepartment());
					isInt=true;
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getStreetName())) {
					proposingPartyPostalAddress.setGeoCodeBuildingName(
							paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getStreetName());
					isInt=true;
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getBuildingNumber())) {
					proposingPartyPostalAddress.setGeoCodeBuildingNumber(paymentConsentsRequest.getData().getInitiation()
							.getCreditorPostalAddress().getBuildingNumber());
					isInt=true;
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getPostCode())) {
					proposingPartyPostalAddress.setPostCodeNumber(
							paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getPostCode());
					isInt=true;
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getTownName())) {
					proposingPartyPostalAddress.setTownName(
							paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getTownName());
					isInt=true;
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation()
						.getCreditorPostalAddress().getCountrySubDivision())) {
					proposingPartyPostalAddress.setCountrySubDivision(paymentConsentsRequest.getData().getInitiation()
							.getCreditorPostalAddress().getCountrySubDivision());
					isInt=true;
				}
				List<String> addressLine = paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress()
						.getAddressLine();
				if (!NullCheckUtils.isNullOrEmpty(addressLine) && !addressLine.isEmpty()) {
					proposingPartyPostalAddress.setAddressLine(paymentConsentsRequest.getData().getInitiation()
							.getCreditorPostalAddress().getAddressLine());
					isInt = true;
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getCountry())) {
					Country addressCountry = new Country();
					addressCountry.setIsoCountryAlphaTwoCode(paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress().getCountry());
					proposingPartyPostalAddress.setAddressCountry(addressCountry);
					isInt=true;
				}

				if(isInt)
					instructionProposal2.setProposingPartyPostalAddress(proposingPartyPostalAddress);
			} 


			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getRemittanceInformation())) {
				instructionProposal2.setAuthorisingPartyUnstructuredReference(
						paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured());
				instructionProposal2.setAuthorisingPartyReference(
						paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getReference());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getAuthorisation())) {
				
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getAuthorisation().getAuthorisationType()))
				
					instructionProposal2.setAuthorisationType(AuthorisationType.fromValue(
							paymentConsentsRequest.getData().getAuthorisation().getAuthorisationType().toString()));
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime()))
				{
					instructionProposal2.setAuthorisationDatetime(paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime()); 
				}
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk())) {
				PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();
				paymentInstructionRiskFactorReference.setPaymentContextCode(
						String.valueOf(paymentConsentsRequest.getRisk().getPaymentContextCode()));
				paymentInstructionRiskFactorReference
						.setMerchantCategoryCode(paymentConsentsRequest.getRisk().getMerchantCategoryCode());
				paymentInstructionRiskFactorReference.setMerchantCustomerIdentification(
						paymentConsentsRequest.getRisk().getMerchantCustomerIdentification());
				instructionProposal2.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);

				// Setting DeliveryAddress value from API to FD
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress())) {

					List<String> addline = paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine();

					Address counterpartyAddress = new Address();

					if (!NullCheckUtils.isNullOrEmpty(addline) && !addline.isEmpty()) {
						counterpartyAddress.setFirstAddressLine(addline.get(0));
					}
					if (!NullCheckUtils.isNullOrEmpty(addline) &&  addline.size() > 1) {
						counterpartyAddress.setSecondAddressLine(addline.get(1));
					}
					if (!NullCheckUtils
							.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getStreetName())) {
						counterpartyAddress.setGeoCodeBuildingName(
								paymentConsentsRequest.getRisk().getDeliveryAddress().getStreetName());
					}
					if (!NullCheckUtils
							.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber())) {
						counterpartyAddress.setGeoCodeBuildingNumber(
								paymentConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber());
					}
					if (!NullCheckUtils
							.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getPostCode())) {
						counterpartyAddress
								.setPostCodeNumber(paymentConsentsRequest.getRisk().getDeliveryAddress().getPostCode());
					}
					if (!NullCheckUtils
							.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getTownName())) {
						counterpartyAddress.setThirdAddressLine(
								paymentConsentsRequest.getRisk().getDeliveryAddress().getTownName());
					}
					if (!NullCheckUtils.isNullOrEmpty(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision())) {
						counterpartyAddress.setFourthAddressLine(
								paymentConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
					}


					if (!NullCheckUtils
							.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getCountry())) {
						Country addressCountry = new Country();
						addressCountry.setIsoCountryAlphaTwoCode(
								paymentConsentsRequest.getRisk().getDeliveryAddress().getCountry());
						counterpartyAddress.setAddressCountry(addressCountry);
					}

					paymentInstructionRiskFactorReference.setCounterPartyAddress(counterpartyAddress);
				}

				instructionProposal2.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
			}
			
			instructionProposal2.setProposalCreationDatetime(paymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime()); 

		}
		return instructionProposal2;
	}

	public <T> CustomDSPConsentsPOSTResponse transformDomesticScheduledPaymentResponse(T inputBalanceObj) {
		// Root Element
		CustomDSPConsentsPOSTResponse obcustomDSPConsentsPOSTResponse = new CustomDSPConsentsPOSTResponse();
		ScheduledPaymentInstructionProposal paymentIns = (ScheduledPaymentInstructionProposal) inputBalanceObj;
		OBWriteDataDomesticScheduledConsentResponse1 oBWriteDataDomesticScheduledConsentResponse1 = new OBWriteDataDomesticScheduledConsentResponse1();

		oBWriteDataDomesticScheduledConsentResponse1.setConsentId(paymentIns.getPaymentInstructionProposalId());

		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposalCreationDatetime())) {
			oBWriteDataDomesticScheduledConsentResponse1.setCreationDateTime(paymentIns.getProposalCreationDatetime());
		}

		oBWriteDataDomesticScheduledConsentResponse1
				.setStatus(OBExternalConsentStatus1Code.fromValue(String.valueOf(paymentIns.getProposalStatus())));
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposalStatusUpdateDatetime())) {
			oBWriteDataDomesticScheduledConsentResponse1
					.setStatusUpdateDateTime(paymentIns.getProposalStatusUpdateDatetime());
		}

		oBWriteDataDomesticScheduledConsentResponse1
				.setPermission(OBExternalPermissions2Code.fromValue(paymentIns.getPermission()));

		// Set OBDomestic1
		OBDomestic1 obDomestic1 = new OBDomestic1();
		obDomestic1.setInstructionIdentification(paymentIns.getInstructionReference());
		obDomestic1.setEndToEndIdentification(paymentIns.getInstructionEndToEndReference());
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getInstructionLocalInstrument())) {
			obDomestic1.setLocalInstrument(paymentIns.getInstructionLocalInstrument());
		}

		// charges
		List<OBCharge1> obCharge1List = new ArrayList<>();
		OBCharge1 obCharge1 = new OBCharge1();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getCharges()) && !paymentIns.getCharges().isEmpty()) {
			List<PaymentInstructionCharge> charges = paymentIns.getCharges();
			for (PaymentInstructionCharge charge : charges) {

				OBCharge1Amount oBCharge1Amount = new OBCharge1Amount();
				obCharge1.setChargeBearer(OBChargeBearerType1Code.fromValue(String.valueOf(charge.getChargeBearer())));
				obCharge1.setType(charge.getType());
				oBCharge1Amount.setAmount(String.valueOf(charge.getAmount().getTransactionCurrency()));
				oBCharge1Amount.setCurrency(charge.getCurrency().getIsoAlphaCode());
				obCharge1.setAmount(oBCharge1Amount);
				obCharge1List.add(obCharge1);
			}
		}

		if (!obCharge1List.isEmpty())
			oBWriteDataDomesticScheduledConsentResponse1.setCharges(obCharge1List);

		// initiation
		OBDomesticScheduled1 oBDomesticScheduled1 = new OBDomesticScheduled1();
		oBDomesticScheduled1.setInstructionIdentification(paymentIns.getInstructionReference());
		oBDomesticScheduled1.setEndToEndIdentification(paymentIns.getInstructionEndToEndReference());
		oBDomesticScheduled1.setLocalInstrument(paymentIns.getInstructionLocalInstrument());
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getRequestedExecutionDateTime())) {
			
			oBDomesticScheduled1.setRequestedExecutionDateTime(paymentIns.getRequestedExecutionDateTime());
		}
		// InstructedAmount
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getFinancialEventAmount())) {
			OBDomestic1InstructedAmount oBDomestic1InstructedAmount = new OBDomestic1InstructedAmount();
			oBDomestic1InstructedAmount
					.setAmount(String.valueOf(paymentIns.getFinancialEventAmount().getTransactionCurrency()));
			oBDomestic1InstructedAmount.setCurrency(paymentIns.getTransactionCurrency().getIsoAlphaCode());
			oBDomesticScheduled1.setInstructedAmount(oBDomestic1InstructedAmount);
		}

		// DebtorAccount
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyAccount())) {
			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
			oBCashAccountDebtor3.setSchemeName(paymentIns.getAuthorisingPartyAccount().getSchemeName());
			oBCashAccountDebtor3.setIdentification(paymentIns.getAuthorisingPartyAccount().getAccountIdentification());
			oBCashAccountDebtor3.setName(paymentIns.getAuthorisingPartyAccount().getAccountName());
			oBCashAccountDebtor3
					.setSecondaryIdentification(paymentIns.getAuthorisingPartyAccount().getSecondaryIdentification());
			oBDomesticScheduled1.setDebtorAccount(oBCashAccountDebtor3);
		}

		// CreditorAccount
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyAccount())) {
			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			oBCashAccountCreditor2.setSchemeName(paymentIns.getProposingPartyAccount().getSchemeName());
			oBCashAccountCreditor2.setIdentification(paymentIns.getProposingPartyAccount().getAccountIdentification());
			oBCashAccountCreditor2.setName(paymentIns.getProposingPartyAccount().getAccountName());
			oBCashAccountCreditor2
					.setSecondaryIdentification(paymentIns.getProposingPartyAccount().getSecondaryIdentification());
			oBDomesticScheduled1.setCreditorAccount(oBCashAccountCreditor2);
		}
		// CreditorPostalAddress
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress())) {
			OBPostalAddress6 oBPostalAddress6 = new OBPostalAddress6();
			
			boolean isInit = false;
			
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getAddressType()))
			{
				oBPostalAddress6.setAddressType(
					OBAddressTypeCode.fromValue(paymentIns.getProposingPartyPostalAddress().getAddressType()));
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getDepartment()))
			{
				oBPostalAddress6.setDepartment(paymentIns.getProposingPartyPostalAddress().getDepartment());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getSubDepartment()))
			{
				oBPostalAddress6.setSubDepartment(paymentIns.getProposingPartyPostalAddress().getSubDepartment());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getGeoCodeBuildingName()))
			{
				oBPostalAddress6.setStreetName(paymentIns.getProposingPartyPostalAddress().getGeoCodeBuildingName());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getGeoCodeBuildingNumber()))
			{
				oBPostalAddress6.setBuildingNumber(paymentIns.getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getPostCodeNumber()))
			{
				oBPostalAddress6.setPostCode(paymentIns.getProposingPartyPostalAddress().getPostCodeNumber());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getTownName()))
			{
				oBPostalAddress6.setTownName(paymentIns.getProposingPartyPostalAddress().getTownName());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getCountrySubDivision()))
			{
				oBPostalAddress6.setCountrySubDivision(paymentIns.getProposingPartyPostalAddress().getCountrySubDivision());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getAddressCountry())&&(!NullCheckUtils.isNullOrEmpty(paymentIns.getProposingPartyPostalAddress().getAddressCountry().getIsoCountryAlphaTwoCode()))) {
				
					oBPostalAddress6.setCountry(paymentIns.getProposingPartyPostalAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
					isInit = true;
				}
			List<String> addressLine = paymentIns.getProposingPartyPostalAddress().getAddressLine();
			if (!NullCheckUtils.isNullOrEmpty(addressLine) && !addressLine.isEmpty()) {
				oBPostalAddress6.setAddressLine(paymentIns.getProposingPartyPostalAddress().getAddressLine());
				isInit = true;
			}

			if (isInit)
				oBDomesticScheduled1.setCreditorPostalAddress(oBPostalAddress6);
		}

		// RemittanceInformation
	if ((!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 obRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInit = false;
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyReference())) {
				obRemittanceInformation1.setReference(paymentIns.getAuthorisingPartyReference());
				isInit = true;
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisingPartyUnstructuredReference())) {
				obRemittanceInformation1.setUnstructured(paymentIns.getAuthorisingPartyUnstructuredReference());
				isInit = true;
			}
			if (isInit)
				oBDomesticScheduled1.setRemittanceInformation(obRemittanceInformation1);
		}
		// Authorization
		OBAuthorisation1 obAuthorisation1 = new OBAuthorisation1();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisationType())) {
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisationType())) {
				obAuthorisation1.setAuthorisationType(
						OBExternalAuthorisation1Code.fromValue(paymentIns.getAuthorisationType().toString()));
			}
			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getAuthorisationDatetime())) {
				obAuthorisation1.setCompletionDateTime(paymentIns.getAuthorisationDatetime());
			}
			oBWriteDataDomesticScheduledConsentResponse1.setAuthorisation(obAuthorisation1);
		}

		// Risk
		OBRisk1 obRisk1 = new OBRisk1();
		if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference())) {
			obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.fromValue(
					String.valueOf(paymentIns.getPaymentInstructionRiskFactorReference().getPaymentContextCode())));
			obRisk1.setMerchantCategoryCode(
					paymentIns.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode());
			obRisk1.setMerchantCustomerIdentification(
					paymentIns.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());

			// DeliveryAddress
			OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
			List<String> addressList = new ArrayList<>();
			if (!NullCheckUtils
				    .isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())) {

			if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
					.getCounterPartyAddress().getFirstAddressLine())) {
				addressList.add(paymentIns.getPaymentInstructionRiskFactorReference().getCounterPartyAddress()
						.getFirstAddressLine());

				}
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
						.getCounterPartyAddress().getSecondAddressLine())) {
					addressList.add(paymentIns.getPaymentInstructionRiskFactorReference().getCounterPartyAddress()
							.getSecondAddressLine());
				}

				if (!NullCheckUtils.isNullOrEmpty(addressList) && !addressList.isEmpty()) {
					obRisk1DeliveryAddress.setAddressLine(addressList);
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
						.getCounterPartyAddress().getGeoCodeBuildingName())) {
					obRisk1DeliveryAddress.setStreetName(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getGeoCodeBuildingName());
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
						.getCounterPartyAddress().getGeoCodeBuildingNumber())) {
					obRisk1DeliveryAddress.setBuildingNumber(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getGeoCodeBuildingNumber());
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
						.getCounterPartyAddress().getPostCodeNumber())) {
					obRisk1DeliveryAddress.setPostCode(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getPostCodeNumber());
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
						.getCounterPartyAddress().getThirdAddressLine())) {
					obRisk1DeliveryAddress.setTownName(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getThirdAddressLine());
				}
				StringBuilder countrySubDivision = new StringBuilder();
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
						.getCounterPartyAddress().getFourthAddressLine())) {
					countrySubDivision.append(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getFourthAddressLine());
					obRisk1DeliveryAddress.setCountrySubDivision(countrySubDivision.toString());
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentIns.getPaymentInstructionRiskFactorReference()
						.getCounterPartyAddress().getFifthAddressLine())) {
					countrySubDivision.append(paymentIns.getPaymentInstructionRiskFactorReference()
							.getCounterPartyAddress().getFifthAddressLine());
					obRisk1DeliveryAddress.setCountrySubDivision(countrySubDivision.toString());
				}
				obRisk1DeliveryAddress.setCountry(paymentIns.getPaymentInstructionRiskFactorReference()
						.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());

				obRisk1.setDeliveryAddress(obRisk1DeliveryAddress);
			}
			obcustomDSPConsentsPOSTResponse.setRisk(obRisk1);
		}

		// final response setup
		oBWriteDataDomesticScheduledConsentResponse1.setInitiation(oBDomesticScheduled1);
		
		obcustomDSPConsentsPOSTResponse.setData(oBWriteDataDomesticScheduledConsentResponse1);

		return obcustomDSPConsentsPOSTResponse;

	}
}