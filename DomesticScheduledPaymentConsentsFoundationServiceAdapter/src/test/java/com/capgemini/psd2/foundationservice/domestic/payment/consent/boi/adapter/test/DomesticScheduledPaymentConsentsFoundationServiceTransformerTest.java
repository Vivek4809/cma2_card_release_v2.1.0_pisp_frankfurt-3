package com.capgemini.psd2.foundationservice.domestic.payment.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.consent.boi.adapter.transformer.DomesticScheduledPaymentConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPermissions2Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticScheduledPaymentConsentsFoundationServiceTransformerTest {
	
	@InjectMocks
	DomesticScheduledPaymentConsentsFoundationServiceTransformer transformer;
	
	@Mock
	private PSD2Validator psd2Validator;
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void  testtransformDomesticConsentResponseFromFDToAPIForInsert(){
		Map<String, String> params= new HashMap<>();
		
		
		ScheduledPaymentInstructionProposal paymentInstructionProposal = new ScheduledPaymentInstructionProposal();
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		Amount amount = new Amount();
		Currency currency = new Currency(); 
		
		currency.setIsoAlphaCode("pata nai");
		amount.setTransactionCurrency(22.0);
		
		charge.setAmount(amount);
		charge.setCurrency(currency);
		charges.add(charge);
		
		paymentInstructionProposal.setCharges(charges);
		paymentInstructionProposal.setPaymentInstructionProposalId("paymentInstructionProposalId");
		paymentInstructionProposal.setProposalStatus(ProposalStatus.AwaitingAuthorisation);
		transformer.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposal);
		
	}
	
	@Test
	public void  testtransformDomesticConsentResponseFromFDToAPIForInsert1(){
		Map<String, String> params= new HashMap<>();
		
		
		ScheduledPaymentInstructionProposal paymentInstructionProposal = new ScheduledPaymentInstructionProposal();
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		Amount amount = new Amount();
		Currency currency = new Currency(); 
		
		currency.setIsoAlphaCode("pata nai");
		amount.setTransactionCurrency(22.0);
		
		charge.setAmount(amount);
		charge.setCurrency(currency);
		charges.add(charge);
		
		paymentInstructionProposal.setCharges(charges);
		paymentInstructionProposal.setPaymentInstructionProposalId("paymentInstructionProposalId");
		paymentInstructionProposal.setProposalStatus(ProposalStatus.Rejected);
		transformer.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposal);
		
	}
	@Test
	public void testtransformDomesticPaymentResponse1(){
		CustomDSPConsentsPOSTRequest  paymentConsentsRequest=new CustomDSPConsentsPOSTRequest();
		
		Map<String, String> params= new HashMap<>();
		OBWriteDataDomesticScheduledConsent1 data = new OBWriteDataDomesticScheduledConsent1();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		initiation.setRequestedExecutionDateTime("2018-12-30T15:25:30-05:00");;
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("ftgfy");
		remittanceInformation.setUnstructured("unstructured");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("hgh");
		creditorAccount.setName("vg0");
		creditorAccount.setSchemeName("cfg");
		creditorAccount.setSecondaryIdentification("fgg");
		
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("12323");
		debtorAccount.setName("erer");
		debtorAccount.setSchemeName("wedfdf");
		debtorAccount.setSecondaryIdentification("dfdfd");

		List<String> addressLine = new ArrayList<>();
		addressLine.add("sxs");
		addressLine.add("sxsxc");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1234");
		instructedAmount.setCurrency("EUR");
		initiation.setDebtorAccount(debtorAccount);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setInstructionIdentification("tuyht");
		initiation.setLocalInstrument("tfh");
		initiation.setEndToEndIdentification("cg");
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		creditorPostalAddress.setAddressType(OBAddressTypeCode.BUSINESS);
		creditorPostalAddress.setDepartment("wedfdf");
		creditorPostalAddress.setSubDepartment("wedfdf");
		creditorPostalAddress.setStreetName("wedfdf");
		creditorPostalAddress.setBuildingNumber("wedfdf");
		creditorPostalAddress.setPostCode("wedfdf");
		creditorPostalAddress.setTownName("wedfdf");
		creditorPostalAddress.setCountrySubDivision("wedfdf");
		creditorPostalAddress.setCountry("wedfdf");
		creditorPostalAddress.setAddressLine(addressLine);
		initiation.setCreditorPostalAddress(creditorPostalAddress );
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		authorisation.setCompletionDateTime("2018-12-30T15:25:30-05:00");
		OBRisk1 obRisk1=new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
		deliveryAddress.setCountry("India");
		deliveryAddress.setTownName("Pune");
		deliveryAddress.setAddressLine(addressLine );
		deliveryAddress.setStreetName("dcfdcs");
		deliveryAddress.buildingNumber("54212");
		deliveryAddress.setPostCode("sdcfsd");
		deliveryAddress.setCountrySubDivision("fvb5d4fb54");
		obRisk1.setDeliveryAddress(deliveryAddress);
		data.setAuthorisation(authorisation);
		data.setPermission(OBExternalPermissions2Code.CREATE);
		data.setInitiation(initiation);
		paymentConsentsRequest.setData(data);
		paymentConsentsRequest.setRisk(obRisk1);

		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("tenant_id","BOIROI");
		transformer.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
	}
	@Test
	public void testtransformDomesticPaymentResponse4(){
		CustomDSPConsentsPOSTRequest  paymentConsentsRequest=new CustomDSPConsentsPOSTRequest();
		
		Map<String, String> params= new HashMap<>();
		OBWriteDataDomesticScheduledConsent1 data = new OBWriteDataDomesticScheduledConsent1();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		initiation.setRequestedExecutionDateTime("2018-12-30T15:25:30-05:00");;
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("ftgfy");
		remittanceInformation.setUnstructured("unstructured");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setIdentification("hgh");
		creditorAccount.setName("vg0");
		creditorAccount.setSchemeName("cfg");
		creditorAccount.setSecondaryIdentification("fgg");
		
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification("12323");
		debtorAccount.setName("erer");
		debtorAccount.setSchemeName("wedfdf");
		debtorAccount.setSecondaryIdentification("dfdfd");

		List<String> addressLine = new ArrayList<>();
		addressLine.add("sxs");
		addressLine.add("sxsxc");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1234");
		instructedAmount.setCurrency("EUR");
		initiation.setDebtorAccount(debtorAccount);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setInstructionIdentification("tuyht");
		initiation.setLocalInstrument("tfh");
		initiation.setEndToEndIdentification(null);
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		creditorPostalAddress.setAddressType(OBAddressTypeCode.BUSINESS);
		creditorPostalAddress.setDepartment("wedfdf");
		creditorPostalAddress.setSubDepartment("wedfdf");
		creditorPostalAddress.setStreetName("wedfdf");
		creditorPostalAddress.setBuildingNumber("wedfdf");
		creditorPostalAddress.setPostCode("wedfdf");
		creditorPostalAddress.setTownName("wedfdf");
		creditorPostalAddress.setCountrySubDivision("wedfdf");
		creditorPostalAddress.setCountry("wedfdf");
		creditorPostalAddress.setAddressLine(addressLine);
		initiation.setCreditorPostalAddress(creditorPostalAddress );
		OBAuthorisation1 authorisation=new OBAuthorisation1();
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.SINGLE);
		authorisation.setCompletionDateTime("2018-12-30T15:25:30-05:00");
		OBRisk1 obRisk1=new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
		deliveryAddress.setCountry("India");
		deliveryAddress.setTownName("Pune");
		deliveryAddress.setAddressLine(addressLine );
		deliveryAddress.setStreetName("dcfdcs");
		deliveryAddress.buildingNumber("54212");
		deliveryAddress.setPostCode("sdcfsd");
		deliveryAddress.setCountrySubDivision("fvb5d4fb54");
		obRisk1.setDeliveryAddress(deliveryAddress);
		data.setAuthorisation(authorisation);
		data.setPermission(OBExternalPermissions2Code.CREATE);
		data.setInitiation(initiation);
		paymentConsentsRequest.setData(data);
		paymentConsentsRequest.setRisk(obRisk1);
		
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("tenant_id","BOIROI");
		params.put("X-BOI-CHANNEL", "BoiChannel");
		
		
		
		transformer.transformDomesticConsentResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
	}
	@Test
	public void testtransformDomesticPaymentResponse(){
	ScheduledPaymentInstructionProposal inputBalanceObj = new ScheduledPaymentInstructionProposal();
	inputBalanceObj.setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setInstructionReference("boi");
	inputBalanceObj.setInstructionLocalInstrument("card");
	FinancialEventAmount amt = new FinancialEventAmount();
	amt.setTransactionCurrency(23.00);
	inputBalanceObj.setFinancialEventAmount(amt);
	Currency currency = new Currency();
	currency .isoAlphaCode("EU");
	inputBalanceObj.setTransactionCurrency(currency);
	AuthorisingPartyAccount act = new AuthorisingPartyAccount();
	act.setAccountName("nam");
	act.setAccountNumber("345");
	act.setSchemeName("debit");
	act.setSecondaryIdentification("678");
	inputBalanceObj.setAuthorisingPartyAccount(act);
	ProposingPartyAccount ppa = new ProposingPartyAccount();
	ppa.setAccountName("nam");
	ppa.setAccountNumber("987");
	ppa.setSchemeName("credit");
	ppa.setSecondaryIdentification("456");
	inputBalanceObj.setProposingPartyAccount(ppa);
	PaymentInstructionPostalAddress add = new PaymentInstructionPostalAddress();
	Country cntry = new Country();
	cntry.setCountryName("absjhab");
	add .setAddressCountry(cntry);
	List<String> line = new ArrayList<>();
	line.add("Talwade");
	add.setAddressLine(line);
	add.setAddressType("POSTAL");
	List<PaymentInstructionCharge> charges = new ArrayList<>();
	PaymentInstructionCharge charge = new PaymentInstructionCharge();
	PaymentInstructionCharge charge1 = new PaymentInstructionCharge();
	charge.setType("credit");
	charge1.setType("debit");
	Currency cur = new Currency();
	cur.setIsoAlphaCode("EUR");
	charge.setCurrency(cur);
	charge1.setCurrency(cur);
	Amount amount = new Amount();
	amount.setTransactionCurrency(4567.00);
	charge.setAmount(amount);
	charge1.setAmount(amount);
	charge.setChargeBearer(ChargeBearer.BorneByCreditor);
	charge1.setChargeBearer(ChargeBearer.BorneByDebtor);
	charges.add(charge);
	charges.add(charge1);
	inputBalanceObj.setCharges(charges);
	inputBalanceObj.setProposingPartyPostalAddress(add);
	inputBalanceObj.setAuthorisationType(AuthorisationType.Any);
	OffsetDateTime date = OffsetDateTime.now();
	inputBalanceObj.setAuthorisationDatetime("2018-10-20T00:00:00+01:00");
	inputBalanceObj.setProposalCreationDatetime("2018-10-20T00:00:00+01:00");
	inputBalanceObj.setProposalStatusUpdateDatetime("2018-10-20T00:00:00+01:00");
	inputBalanceObj.setRequestedExecutionDateTime("2018-10-20T00:00:00+01:00");
	PaymentInstrumentRiskFactor ref = new PaymentInstrumentRiskFactor();
	Address counterPartyAddress = new Address();
	counterPartyAddress.setAddressCountry(cntry );
	counterPartyAddress.setFifthAddressLine("absjhab");
	counterPartyAddress.setFirstAddressLine("absjhab");
	counterPartyAddress.setFourthAddressLine("absjhab");
	counterPartyAddress.setGeoCodeBuildingName("absjhab");
	counterPartyAddress.setGeoCodeBuildingNumber("absjhab");
	counterPartyAddress.setPostCodeNumber("absjhab");
	counterPartyAddress.setSecondAddressLine("absjhab");
	counterPartyAddress.setThirdAddressLine("absjhab");
	ref.setCounterPartyAddress(counterPartyAddress );
	ref.setMerchantCategoryCode("absjhab");
	ref.setMerchantCustomerIdentification("absjhab");
	ref.setPaymentContextCode("absjhab");
	inputBalanceObj.setPaymentInstructionRiskFactorReference(ref );
	Map<String, String> params = new HashMap<>();
	inputBalanceObj.setAuthorisingPartyReference("sdsfscf");
	inputBalanceObj.setAuthorisingPartyUnstructuredReference("dfvsds");
	CustomDSPConsentsPOSTResponse response = transformer.transformDomesticScheduledPaymentResponse(inputBalanceObj);
	assertNotNull(response);  
	}

	@Test
	public void testtransformDomesticPaymentResponse2(){
	ScheduledPaymentInstructionProposal inputBalanceObj = new ScheduledPaymentInstructionProposal();
	inputBalanceObj .setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setInstructionReference("boi");
	FinancialEventAmount amt = new FinancialEventAmount();
	amt.setTransactionCurrency(23.00);
	inputBalanceObj.setFinancialEventAmount(amt);
	Currency currency = new Currency();
	currency .isoAlphaCode("EU");
	inputBalanceObj.setTransactionCurrency(currency);
	AuthorisingPartyAccount act = new AuthorisingPartyAccount();
	act.setAccountName("nam");
	act.setAccountNumber("345");
	act.setSchemeName("debit");
	act.setSecondaryIdentification("678");
	inputBalanceObj.setAuthorisingPartyAccount(act);
	ProposingPartyAccount ppa = new ProposingPartyAccount();
	ppa.setAccountName("nam");
	ppa.setAccountNumber("987");
	ppa.setSchemeName("credit");
	ppa.setSecondaryIdentification("456");
	inputBalanceObj.setProposingPartyAccount(ppa);
	PaymentInstructionPostalAddress add = new PaymentInstructionPostalAddress();
	add .setTownName("pune");
	add.setDepartment("A");
	add.setCountrySubDivision("MH");
	add.setSubDepartment("ABC");
	add.setPostCodeNumber("600789");
	add.setGeoCodeBuildingName("capg");
	add.setGeoCodeBuildingNumber("1521");
	Country country = new Country();
	country .setIsoCountryAlphaTwoCode("IND");
	add.setAddressCountry(country);
	List<String> line = new ArrayList<>();
	line.add("Talwade");
	add.setAddressLine(line);
	add.setAddressType("POSTAL");
	inputBalanceObj.setProposingPartyPostalAddress(add);
	Map<String, String> params = new HashMap<>();
	CustomDSPConsentsPOSTResponse response = transformer.transformDomesticScheduledPaymentResponse(inputBalanceObj);
	assertNotNull(response);;  
	}


}
