package com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.service;

import com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.domain.AuthenticationParameters;


public interface SCAAuthenticationRetrieveService {

	public AuthenticationParameters retrieveAuthentication(String digitalUserId);

}
