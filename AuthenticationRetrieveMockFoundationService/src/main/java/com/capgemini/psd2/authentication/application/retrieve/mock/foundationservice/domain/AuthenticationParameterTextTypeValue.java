/*
 * Authentication Service API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.domain;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;

/**
 * AuthenticationParameterTextTypeValue
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-10-14T18:22:40.133+05:30")
public class AuthenticationParameterTextTypeValue {
  @SerializedName("type")
  private Type2 type = null;

  @SerializedName("value")
  private String value = null;

  public AuthenticationParameterTextTypeValue type(Type2 type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")
  public Type2 getType() {
    return type;
  }

  public void setType(Type2 type) {
    this.type = type;
  }

  public AuthenticationParameterTextTypeValue value(String value) {
    this.value = value;
    return this;
  }

   /**
   * The Value of Authentication Parameter Text Corresponding to the Type
   * @return value
  **/
  @ApiModelProperty(value = "The Value of Authentication Parameter Text Corresponding to the Type")
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = (AuthenticationParameterTextTypeValue) o;
    return Objects.equals(this.type, authenticationParameterTextTypeValue.type) &&
        Objects.equals(this.value, authenticationParameterTextTypeValue.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, value);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AuthenticationParameterTextTypeValue {\n");
    
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    value: ").append(toIndentedString(value)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

