/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.account.balance.mock.foundationservice.domain.Balanceresponse;

/**
 * The Interface AccountBalanceRepository.
 */
public interface AccountBalanceRepository extends MongoRepository<Balanceresponse, String> {

	/**
	 * Find by nsc and account number.
	 *
	 * @param nsc
	 *            the nsc
	 * @param accountNumber
	 *            the account number
	 * @return the accnt
	 */

	public Balanceresponse findByAccountId(String accountId);

}
