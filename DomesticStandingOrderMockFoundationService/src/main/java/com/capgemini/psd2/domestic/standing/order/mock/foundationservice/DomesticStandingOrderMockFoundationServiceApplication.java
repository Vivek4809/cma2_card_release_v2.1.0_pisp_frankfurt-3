package com.capgemini.psd2.domestic.standing.order.mock.foundationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@SpringBootApplication
public class DomesticStandingOrderMockFoundationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomesticStandingOrderMockFoundationServiceApplication.class, args);
	}
}

