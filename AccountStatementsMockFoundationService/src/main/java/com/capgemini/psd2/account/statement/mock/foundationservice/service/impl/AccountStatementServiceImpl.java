/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.statement.mock.foundationservice.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.statement.mock.foundationservice.raml.domain.StatementObject;
import com.capgemini.psd2.account.statement.mock.foundationservice.raml.domain.Statementsresponse;
import com.capgemini.psd2.account.statement.mock.foundationservice.repository.AccountStatementRepository;
import com.capgemini.psd2.account.statement.mock.foundationservice.service.AccountStatementService;
//import com.capgemini.psd2.account.statement.mock.foundationservice.service.StatementsResponse;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
//import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.GroupByDate;
//import com.capgemini.psd2.mock.foundationservice.account.transactions.domain.Transaction;


/**
 * The Class AccountStatementsServiceImpl.
 */
@Service
public class AccountStatementServiceImpl implements AccountStatementService {

	/** The repository. */
	@Autowired
	private AccountStatementRepository repository;

	@Override
	public Statementsresponse retrieveAccountStatements(String accountId, String FromBookingDateTime, String ToBookingDateTime) throws Exception {
		Statementsresponse statementsResponse = repository.findByAccountId(accountId);
		
		if(null == statementsResponse)
		{
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.STATEMENT_NOT_FOUND_CS_STL);
		}
		
		if(statementsResponse.getStatementsList().isEmpty())
			return statementsResponse;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date FromBookingDate = null;
		Date ToBookingDate = null;
        try {
        	FromBookingDate = formatter.parse(FromBookingDateTime);
        	ToBookingDate = formatter.parse(ToBookingDateTime);
        }catch(ParseException e) {
        	System.out.println(e); 
        }
        
        Statementsresponse response = new Statementsresponse();
        response.setTotal(statementsResponse.getTotal());
        List<StatementObject> groupByDateList = new ArrayList<StatementObject>(); 
        for(StatementObject baseType : statementsResponse.getStatementsList()){
        	Date postedDate = formatter.parse(baseType.getDocumentIssuanceDate());  
        	if((postedDate.compareTo(FromBookingDate)==0 || postedDate.after(FromBookingDate)) && (postedDate.compareTo(ToBookingDate)==0 || postedDate.before(ToBookingDate))) {
				groupByDateList.add(baseType);
			}
        }
        statementsResponse.getStatementsList().clear();
        for(StatementObject baseType : groupByDateList){
        	statementsResponse.addStatementsListItem(baseType);
        }
        Collections.sort(statementsResponse.getStatementsList(), new Comparator<StatementObject>() {
			  public int compare(StatementObject o1, StatementObject o2) {
			      return o2.getDocumentIssuanceDate().compareTo(o1.getDocumentIssuanceDate());
			  }
	
		});
        
     
        
		return statementsResponse;

	}

	public byte[] retrivePdf(String accountId, String statementId) throws IOException {
		InputStream in = null;
		byte[] byteArray = null;
		try{
			in = this.getClass().getResourceAsStream("/Statements.pdf");
			byteArray = IOUtils.toByteArray(in);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
            //release resource, if any
			in.close();
        }
		
		return byteArray;
	}

}
