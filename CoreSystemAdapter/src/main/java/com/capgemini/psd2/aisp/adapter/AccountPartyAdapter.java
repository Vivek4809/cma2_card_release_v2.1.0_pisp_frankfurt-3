package com.capgemini.psd2.aisp.adapter;

import java.util.Map;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountPartyResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public interface AccountPartyAdapter {
	
		
		/**
		 * Retrieve account Party.
		 *
		 * @param accountMapping the account mapping
		 * @param params the params
		 * @return the PartyGETResponse
		 */
		public PlatformAccountPartyResponse retrieveAccountParty(AccountMapping accountMapping, Map<String, String> params);
	}



