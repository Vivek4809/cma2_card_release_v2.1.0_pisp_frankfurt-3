package com.capgemini.psd2.pisp.stage.domain;

public class CustomFraudSystemPaymentData {

	private String paymentType;
	private String transferAmount;
	private String transferCurrency;
	private String transferTime;
	private String transferMemo;
	private CreditorDetails creditorDetails;

	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the transferAmount
	 */
	public String getTransferAmount() {
		return transferAmount;
	}

	/**
	 * @param transferAmount
	 *            the transferAmount to set
	 */
	public void setTransferAmount(String transferAmount) {
		this.transferAmount = transferAmount;
	}

	/**
	 * @return the transferCurrency
	 */
	public String getTransferCurrency() {
		return transferCurrency;
	}

	/**
	 * @param transferCurrency
	 *            the transferCurrency to set
	 */
	public void setTransferCurrency(String transferCurrency) {
		this.transferCurrency = transferCurrency;
	}

	/**
	 * @return the transferTime
	 */
	public String getTransferTime() {
		return transferTime;
	}

	/**
	 * @param transferTime
	 *            the transferTime to set
	 */
	public void setTransferTime(String transferTime) {
		this.transferTime = transferTime;
	}

	/**
	 * @return the transferMemo
	 */
	public String getTransferMemo() {
		return transferMemo;
	}

	/**
	 * @param transferMemo
	 *            the transferMemo to set
	 */
	public void setTransferMemo(String transferMemo) {
		this.transferMemo = transferMemo;
	}


	public CreditorDetails getCreditorDetails() {
		return creditorDetails;
	}

	public void setCreditorDetails(CreditorDetails creditorDetails) {
		this.creditorDetails = creditorDetails;
	}

	@Override
	public String toString() {
		return "CustomFraudSystemPaymentData [paymentType=" + paymentType + ", transferAmount=" + transferAmount
				+ ", transferCurrency=" + transferCurrency + ", transferTime=" + transferTime + ", transferMemo="
				+ transferMemo + ", creditorDetails=" + creditorDetails + "]";
	}

}
