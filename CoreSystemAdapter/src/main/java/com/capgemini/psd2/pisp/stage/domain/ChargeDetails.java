package com.capgemini.psd2.pisp.stage.domain;

import java.util.List;

import javax.validation.Valid;

import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChargeDetails {

	@JsonProperty("Charges")
	@Valid
	private List<OBCharge1> chargesList = null;

	@Valid
	public List<OBCharge1> getChargesLis() {
		return chargesList;
	}

	public void setChargesList(List<OBCharge1> charges) {
		this.chargesList = charges;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ChargeDetails [charges=" + chargesList + "]";
	}
}
