package com.capgemini.psd2.pisp.domain;

import java.util.List;

import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;

public class GenericPaymentsResponse {

	private String submissionId;
	/*
	 * creationDateTime, status, statusUpdateDateTime : Fields will be populated
	 * from product end
	 */
	private String expectedExecutionDateTime;
	private String expectedSettlementDateTime;
	private List<OBCharge1> charges;
	private OBMultiAuthorisation1 multiAuthorisation;
	private ProcessExecutionStatusEnum processExecutionStatusEnum;
	private OBExchangeRate2 exchangeRateInformation;

	public String getSubmissionId() {
		return submissionId;
	}

	public void setSubmissionId(String submissionId) {
		this.submissionId = submissionId;
	}

	public OBMultiAuthorisation1 getMultiAuthorisation() {
		return multiAuthorisation;
	}

	public void setMultiAuthorisation(OBMultiAuthorisation1 multiAuthorisation) {
		this.multiAuthorisation = multiAuthorisation;
	}

	public String getExpectedExecutionDateTime() {
		return expectedExecutionDateTime;
	}

	public void setExpectedExecutionDateTime(String expectedExecutionDateTime) {
		this.expectedExecutionDateTime = expectedExecutionDateTime;
	}

	public String getExpectedSettlementDateTime() {
		return expectedSettlementDateTime;
	}

	public void setExpectedSettlementDateTime(String expectedSettlementDateTime) {
		this.expectedSettlementDateTime = expectedSettlementDateTime;
	}

	public List<OBCharge1> getCharges() {
		return charges;
	}

	public void setCharges(List<OBCharge1> charges) {
		this.charges = charges;
	}

	public ProcessExecutionStatusEnum getProcessExecutionStatusEnum() {
		return processExecutionStatusEnum;
	}

	public void setProcessExecutionStatusEnum(ProcessExecutionStatusEnum processExecutionStatusEnum) {
		this.processExecutionStatusEnum = processExecutionStatusEnum;
	}

	public OBExchangeRate2 getExchangeRateInformation() {
		return exchangeRateInformation;
	}

	public void setExchangeRateInformation(OBExchangeRate2 exchangeRateInformation) {
		this.exchangeRateInformation = exchangeRateInformation;
	}

	@Override
	public String toString() {
		return "GenericPaymentsResponse [submissionId=" + submissionId + ", expectedExecutionDateTime="
				+ expectedExecutionDateTime + ", expectedSettlementDateTime=" + expectedSettlementDateTime
				+ ", charges=" + charges + ", multiAuthorisation=" + multiAuthorisation
				+ ", processExecutionStatusEnum=" + processExecutionStatusEnum + ", exchangeRateInformation="
				+ exchangeRateInformation + "]";
	}

}
