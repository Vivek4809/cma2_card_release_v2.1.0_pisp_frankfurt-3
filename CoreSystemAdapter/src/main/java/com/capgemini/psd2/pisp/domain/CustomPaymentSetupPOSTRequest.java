package com.capgemini.psd2.pisp.domain;

public class CustomPaymentSetupPOSTRequest extends OBWriteDomesticConsent1 {
	private String paymentStatus;

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@Override
	public String toString() {
		return "CustomPaymentSetupPOSTRequest [paymentStatus=" + paymentStatus + ", toString()=" + super.toString()
				+ "]";
	}

}
