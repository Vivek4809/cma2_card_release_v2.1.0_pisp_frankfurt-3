package com.capgemini.psd2.pisp.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DPaymentsRetrieveGetRequest {
	private String domesticPaymentId;

	@NotNull
	@Size(min=1,max=40)
	public String getDomesticPaymentId() {
		return domesticPaymentId;
	}

	public void setDomesticPaymentId(String domesticPaymentId) {
		this.domesticPaymentId = domesticPaymentId;
	}
	

	

}
