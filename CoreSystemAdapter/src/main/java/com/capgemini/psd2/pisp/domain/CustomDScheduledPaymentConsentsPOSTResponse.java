package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection = "scheduledPaymentSetupFoundationResources")
public class CustomDScheduledPaymentConsentsPOSTResponse extends OBWriteDomesticScheduledConsentResponse1 {

	@Id
	private String id;
	private String scheduledPaymentSubmissionId;
	private OBTransactionIndividualStatus1Code scheduledPaymentSubmissionStatus;
	private String scheduledPaymentSubmissionStatusUpdateDateTime;
	private Object fraudScore;
	private ProcessConsentStatusEnum consentPorcessStatus;

	/*
	 * private Object fraudnetResponse; private String paymentCharges; private
	 * String exchangeRate; private String setupVersion; private String
	 * paymentType;
	 */

	/*
	 * private String payerCurrency; private String payerJurisdiction; private
	 * String accountNumber; private String accountNSC; private String
	 * accountBic; private String accountIban; private String accountPan;
	 */

	@JsonIgnore
	public String getId() {
		return id;
	}

	@JsonIgnore
	public ProcessConsentStatusEnum getConsentPorcessStatus() {
		return consentPorcessStatus;
	}

	public void setConsentPorcessStatus(ProcessConsentStatusEnum consentPorcessStatus) {
		this.consentPorcessStatus = consentPorcessStatus;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonIgnore
	public String getPaymentSubmissionId() {
		return scheduledPaymentSubmissionId;
	}

	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.scheduledPaymentSubmissionId = paymentSubmissionId;
	}
	
	/*
	 * @JsonIgnore public String getPaymentCharges() { return paymentCharges; }
	 * 
	 * public void setPaymentCharges(String paymentCharges) {
	 * this.paymentCharges = paymentCharges; }
	 * 
	 * @JsonIgnore public String getExchangeRate() { return exchangeRate; }
	 * 
	 * public void setExchangeRate(String exchangeRate) { this.exchangeRate =
	 * exchangeRate; }
	 */

	// @JsonIgnore
	// public String getPayerCurrency() {
	// return payerCurrency;
	// }
	//
	// public void setPayerCurrency(String payerCurrency) {
	// this.payerCurrency = payerCurrency;
	// }
	//
	// @JsonIgnore
	// public String getPayerJurisdiction() {
	// return payerJurisdiction;
	// }
	//
	// public void setPayerJurisdiction(String payerJurisdiction) {
	// this.payerJurisdiction = payerJurisdiction;
	// }
	//
	// @JsonIgnore
	// public String getAccountNumber() {
	// return accountNumber;
	// }
	//
	// public void setAccountNumber(String accountNumber) {
	// this.accountNumber = accountNumber;
	// }
	//
	// @JsonIgnore
	// public String getAccountNSC() {
	// return accountNSC;
	// }
	//
	// public void setAccountNSC(String accountNSC) {
	// this.accountNSC = accountNSC;
	// }
	//
	// @JsonIgnore
	// public String getAccountBic() {
	// return accountBic;
	// }
	//
	// public void setAccountBic(String accountBic) {
	// this.accountBic = accountBic;
	// }
	//
	// @JsonIgnore
	// public String getAccountIban() {
	// return accountIban;
	// }
	//
	// public void setAccountIban(String accountIban) {
	// this.accountIban = accountIban;
	// }
	//
	// /**
	// * @return the accountPan
	// */
	// public String getAccountPan() {
	// return accountPan;
	// }
	//
	// /**
	// * @param accountPan the accountPan to set
	// */
	// public void setAccountPan(String accountPan) {
	// this.accountPan = accountPan;
	// }

	/*
	 * @JsonIgnore public Object getFraudnetResponse() { return
	 * fraudnetResponse; }
	 * 
	 * public void setFraudnetResponse(Object fraudnetResponse) {
	 * this.fraudnetResponse = fraudnetResponse; }
	 */

	@JsonIgnore
	public OBTransactionIndividualStatus1Code getPaymentSubmissionStatus() {
		return scheduledPaymentSubmissionStatus;
	}

	public void setPaymentSubmissionStatus(OBTransactionIndividualStatus1Code paymentSubmissionStatus) {
		this.scheduledPaymentSubmissionStatus = paymentSubmissionStatus;
	}

	@JsonIgnore
	public String getPaymentSubmissionStatusUpdateDateTime() {
		return scheduledPaymentSubmissionStatusUpdateDateTime;
	}

	public void setPaymentSubmissionStatusUpdateDateTime(String paymentSubmissionStatusUpdateDateTime) {
		this.scheduledPaymentSubmissionStatusUpdateDateTime = paymentSubmissionStatusUpdateDateTime;
	}


	/**
	 * @return the fraudScore
	 */
	@JsonIgnore
	public Object getFraudScore() {
		return fraudScore;
	}

	/**
	 * @param fraudScore the fraudScore to set
	 */
	public void setFraudScore(Object fraudScore) {
		this.fraudScore = fraudScore;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CustomDScheduledPaymentSetupPOSTResponse [id=" + id + ", paymentSubmissionId=" + scheduledPaymentSubmissionId
				+ ", paymentSubmissionStatus=" + scheduledPaymentSubmissionStatus + ", paymentSubmissionStatusUpdateDateTime="
				+ scheduledPaymentSubmissionStatusUpdateDateTime + ", fraudScore=" + fraudScore + "]";
	}

	/*
	 * @JsonIgnore public String getSetupVersion() { return setupVersion; }
	 * 
	 * public void setSetupVersion(String setupVersion) { this.setupVersion =
	 * setupVersion; }
	 * 
	 * @JsonIgnore public String getPaymentType() { return paymentType; }
	 * 
	 * public void setPaymentType(String paymentType) { this.paymentType =
	 * paymentType; }
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */

}
