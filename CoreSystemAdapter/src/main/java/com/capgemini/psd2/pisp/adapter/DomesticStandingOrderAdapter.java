package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface DomesticStandingOrderAdapter {
	// Called from Domestic Payments API to process payment
	public DStandingOrderPOST201Response processDomesticStandingOrder(CustomDStandingOrderPOSTRequest request,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params);

	// Called from Domestic Payments API to retrieve payment
	public DStandingOrderPOST201Response retrieveStagedDomesticStandingOrder(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);
}
