package com.capgemini.psd2.pisp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "internationalPaymentSetupPlatformResources")
public class IPaymentConsentsPlatformResource {

	@Id
	private String id;	
	private String paymentId;
	private String status;
	private String statusUpdateDateTime;
	private String createdAt;
	private String updatedAt;
	private String endToEndIdentification;
	private String tppDebtorDetails;
	private String tppDebtorNameDetails;
	private String instructionIdentification;
	private String idempotencyKey;
	private String tppCID;
	private String idempotencyRequest;
	private String tppLegalEntityName;
	
	public String getIdempotencyRequest() {
		return idempotencyRequest;
	}
	public void setIdempotencyRequest(String idempotencyRequest) {
		this.idempotencyRequest = idempotencyRequest;
	}
	
	public String getIdempotencyKey() {
		return idempotencyKey;
	}
	public void setIdempotencyKey(String idempotencyKey) {
		this.idempotencyKey = idempotencyKey;
	}
	public String getTppCID() {
		return tppCID;
	}
	public void setTppCID(String tppCID) {
		this.tppCID = tppCID;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusUpdateDateTime() {
		return statusUpdateDateTime;
	}
	public void setStatusUpdateDateTime(String statusUpdateDateTime) {
		this.statusUpdateDateTime = statusUpdateDateTime;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;	
	}
	public String getEndToEndIdentification() {
		return endToEndIdentification;
	}
	public void setEndToEndIdentification(String endToEndIdentification) {
		this.endToEndIdentification = endToEndIdentification;
	}
		
	public String getInstructionIdentification() {
		return instructionIdentification;
	}
	public void setInstructionIdentification(String instructionIdentification) {
		this.instructionIdentification = instructionIdentification;
	}
	
	
	public String getTppDebtorDetails() {
		return tppDebtorDetails;
	}
	public void setTppDebtorDetails(String tppDebtorDetails) {
		this.tppDebtorDetails = tppDebtorDetails;
	}
	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}
	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}
	
	public String getTppDebtorNameDetails() {
		return tppDebtorNameDetails;
	}
	public void setTppDebtorNameDetails(String tppDebtorNameDetails) {
		this.tppDebtorNameDetails = tppDebtorNameDetails;
	}
	@Override
	public String toString() {
		return "PaymentSetupResource [id=" + id + ", paymentId=" + paymentId + ", status=" + status + ", createdAt="
				+ createdAt + ", updatedAt=" + updatedAt + ", endToEndIdentification=" + endToEndIdentification
				+ ", tppDebtorDetails=" + tppDebtorDetails + ", tppDebtorNameDetails="
						+ tppDebtorNameDetails + ", instructionIdentification=" + instructionIdentification
				+ ", idempotencyKey=" + idempotencyKey + ", tppCID=" + tppCID + ", idempotencyRequest="
				+ idempotencyRequest + ", tppLegalEntityName=" + tppLegalEntityName + "]";
	}
	
	
}
