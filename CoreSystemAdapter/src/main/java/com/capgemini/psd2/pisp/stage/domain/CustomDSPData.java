package com.capgemini.psd2.pisp.stage.domain;

public class CustomDSPData {
	
	private String RequestedExecutionDateTime;

	public String getRequestedExecutionDateTime() {
		return RequestedExecutionDateTime;
	}

	public void setRequestedExecutionDateTime(String requestedExecutionDateTime) {
		RequestedExecutionDateTime = requestedExecutionDateTime;
	}

	@Override
	public String toString() {
		return "CustomDSPData [RequestedExecutionDateTime=" + RequestedExecutionDateTime + "]";
	}
}
