package com.capgemini.psd2.cisp.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * OBFundsConfirmationConsentDataResponse1
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-11-05T15:52:27.305+05:30")

@Document(collection = "#{@CollectionNameLocator.getFundsSetupCollectionName()}")
public class OBFundsConfirmationConsentDataResponse1   {
	@Id
	@JsonProperty("ConsentId")
	private String consentId = null;
	
	@JsonProperty("CreationDateTime")
	private String creationDateTime = null;

	@JsonProperty("Status")
	private OBExternalRequestStatus1Code status = null;

	@JsonProperty("StatusUpdateDateTime")
	private String statusUpdateDateTime = null;

	@JsonProperty("ExpirationDateTime")
	private String expirationDateTime = null;

	@JsonProperty("DebtorAccount")
	private OBCashAccountDebtor3 debtorAccount = null;

	@JsonIgnore
	private String tenantId = null;

	@JsonIgnore
	private String cmaVersion;

	@JsonIgnore
	private String tppCID = null;

	@JsonIgnore
	private String tppLegalEntityName  = null;

	public OBFundsConfirmationConsentDataResponse1 consentId(String consentId) {
		this.consentId = consentId;
		return this;
	}

	/**
	 * Unique identification as assigned to identify the funds confirmation consent resource.
	 * @return consentId
	 **/
	@ApiModelProperty(required = true, value = "Unique identification as assigned to identify the funds confirmation consent resource.")
	@NotNull

	@Size(min=1,max=128)
	public String getConsentId() {
		return consentId;
	}

	public void setConsentId(String consentId) {
		this.consentId = consentId;
	}

	public OBFundsConfirmationConsentDataResponse1 creationDateTime(String string) {
		this.creationDateTime = string;
		return this;
	}

	/**
	 * Date and time at which the resource was created. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
	 * @return creationDateTime
	 **/
	@ApiModelProperty(required = true, value = "Date and time at which the resource was created. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
	@NotNull

	@Valid

	public String getCreationDateTime() {
		return creationDateTime;
	}

	public void setCreationDateTime(String creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public OBFundsConfirmationConsentDataResponse1 status(OBExternalRequestStatus1Code status) {
		this.status = status;
		return this;
	}

	/**
	 * Get status
	 * @return status
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public OBExternalRequestStatus1Code getStatus() {
		return status;
	}

	public void setStatus(OBExternalRequestStatus1Code status) {
		this.status = status;
	}

	public OBFundsConfirmationConsentDataResponse1 statusUpdateDateTime(String statusUpdateDateTime) {
		this.statusUpdateDateTime = statusUpdateDateTime;
		return this;
	}

	/**
	 * Date and time at which the resource status was updated. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
	 * @return statusUpdateDateTime
	 **/
	@ApiModelProperty(required = true, value = "Date and time at which the resource status was updated. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
	@NotNull

	@Valid

	public String getStatusUpdateDateTime() {
		return statusUpdateDateTime;
	}

	public void setStatusUpdateDateTime(String string) {
		this.statusUpdateDateTime = string;
	}

	public OBFundsConfirmationConsentDataResponse1 expirationDateTime(String expirationDateTime) {
		this.expirationDateTime = expirationDateTime;
		return this;
	}

	/**
	 * Specified date and time the funds confirmation authorisation will expire. If this is not populated, the authorisation will be open ended. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
	 * @return expirationDateTime
	 **/
	@ApiModelProperty(value = "Specified date and time the funds confirmation authorisation will expire. If this is not populated, the authorisation will be open ended. All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")

	@Valid

	public String getExpirationDateTime() {
		return expirationDateTime;
	}

	public void setExpirationDateTime(String expirationDateTime) {
		this.expirationDateTime = expirationDateTime;
	}

	public OBFundsConfirmationConsentDataResponse1 debtorAccount(OBCashAccountDebtor3 debtorAccount) {
		this.debtorAccount = debtorAccount;
		return this;
	}

	/**
	 * Get debtorAccount
	 * @return debtorAccount
	 **/
	@ApiModelProperty(required = true, value = "")
	@NotNull

	@Valid

	public OBCashAccountDebtor3 getDebtorAccount() {
		return debtorAccount;
	}

	public void setDebtorAccount(OBCashAccountDebtor3 debtorAccount) {
		this.debtorAccount = debtorAccount;
	}


	/**
	 * Get tenantId
	 * @return tenantId
	 **/
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OBFundsConfirmationConsentDataResponse1 obFundsConfirmationConsentDataResponse1 = (OBFundsConfirmationConsentDataResponse1) o;
		return Objects.equals(this.consentId, obFundsConfirmationConsentDataResponse1.consentId) &&
				Objects.equals(this.creationDateTime, obFundsConfirmationConsentDataResponse1.creationDateTime) &&
				Objects.equals(this.status, obFundsConfirmationConsentDataResponse1.status) &&
				Objects.equals(this.statusUpdateDateTime, obFundsConfirmationConsentDataResponse1.statusUpdateDateTime) &&
				Objects.equals(this.expirationDateTime, obFundsConfirmationConsentDataResponse1.expirationDateTime) &&
				Objects.equals(this.debtorAccount, obFundsConfirmationConsentDataResponse1.debtorAccount) &&
				Objects.equals(this.tenantId, obFundsConfirmationConsentDataResponse1.tenantId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(consentId, creationDateTime, status, statusUpdateDateTime, expirationDateTime, debtorAccount, tenantId);
	}

	@Override
	public String toString() {
		return "OBFundsConfirmationConsentDataResponse1 [consentId=" + consentId + ", creationDateTime="
				+ creationDateTime + ", status=" + status + ", statusUpdateDateTime=" + statusUpdateDateTime
				+ ", expirationDateTime=" + expirationDateTime + ", debtorAccount=" + debtorAccount + ", tenantId="
				+ tenantId + ", cmaVersion=" + cmaVersion + ", tppCID=" + tppCID + ", tppLegalEntityName="
				+ tppLegalEntityName + "]";
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

	public String getTppCID()
	{
		return tppCID;
	}

	public void setTppCID(String tppCID) {

		this.tppCID= tppCID;

	}

	public String getTppLegalEntityName()
	{
		return tppLegalEntityName;
	}


	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;

	}

	public String getCmaVersion() {
		return cmaVersion;
	}

	public void setCmaVersion(String cmaVersion) {
		this.cmaVersion = cmaVersion;
	}



}

