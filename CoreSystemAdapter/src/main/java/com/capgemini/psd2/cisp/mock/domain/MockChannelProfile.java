package com.capgemini.psd2.cisp.mock.domain;

import com.capgemini.psd2.consent.domain.PSD2Account;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class MockChannelProfile extends PSD2Account {

	@JsonIgnore
	private String psuId;

	public String getPsuId() { 
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

}
