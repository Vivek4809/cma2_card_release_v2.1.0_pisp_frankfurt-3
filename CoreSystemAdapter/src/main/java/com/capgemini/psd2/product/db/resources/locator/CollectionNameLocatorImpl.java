package com.capgemini.psd2.product.db.resources.locator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("collectionNameLocator")
//@Order(Ordered.HIGHEST_PRECEDENCE)
public class CollectionNameLocatorImpl implements CollectionNameLocator {

	@Autowired
	private MultiTenancyRequestBean requestBean;

	@Override
	public String getAccountAccessSetupCollectionName() {
		return (requestBean.getTenantId().equals("BOIUK")) ? CollectionNameConstants.UK_AISP_SETUP_RES
				: requestBean.getTenantId().concat(CollectionNameConstants.AISP_SETUP_RES_SUFFIX);
	}

	@Override
	public String getAispConsentCollectionName() {
		return (requestBean.getTenantId().equals("BOIUK")) ? CollectionNameConstants.UK_AISP_CONSENT_RES
				: requestBean.getTenantId().concat(CollectionNameConstants.AISP_CONSENT_RES_SUFFIX);
	}

	@Override
	public String getPaymentSetupCollectionName() {
		return (requestBean.getTenantId().equals("BOIUK")) ? CollectionNameConstants.UK_PISP_SETUP_RES
				: requestBean.getTenantId().concat(CollectionNameConstants.PISP_SETUP_RES_SUFFIX);
	}

	@Override
	public String getPaymentSubmissionCollectionName() {
		return (requestBean.getTenantId().equals("BOIUK")) ? CollectionNameConstants.UK_PISP_SUB_RES
				: requestBean.getTenantId().concat(CollectionNameConstants.PISP_SUB_RES_SUFFIX);
	}

	@Override
	public String getPispConsentCollectionName() {
		return (requestBean.getTenantId().equals("BOIUK")) ? CollectionNameConstants.UK_PISP_CONSENT_RES
				: requestBean.getTenantId().concat(CollectionNameConstants.PISP_CONSENT_RES_SUFFIX);
	}

	@Override
	public String getFundsSetupCollectionName() {
		return (requestBean.getTenantId().equals("BOIUK")) ? CollectionNameConstants.UK_CISP_SETUP_RES
				: requestBean.getTenantId().concat(CollectionNameConstants.CISP_SETUP_RES_SUFFIX);
	}

	@Override
	public String getCispConsentCollectionName() {
		return (requestBean.getTenantId().equals("BOIUK")) ? CollectionNameConstants.UK_CISP_CONSENT_RES
				: requestBean.getTenantId().concat(CollectionNameConstants.CISP_CONSENT_RES_SUFFIX);
	}

	@Override
	public String getCPNPCollectionName() {
		return (requestBean.getTenantId().equals("BOIUK")) ? CollectionNameConstants.UK_CPNP_RES
				: requestBean.getTenantId().concat(CollectionNameConstants.CPNP_RES_SUFFIX);
	}

}
