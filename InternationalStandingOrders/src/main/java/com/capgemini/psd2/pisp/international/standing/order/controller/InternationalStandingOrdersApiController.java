package com.capgemini.psd2.pisp.international.standing.order.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIStandingOrderPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalStandingOrderResponse1;
import com.capgemini.psd2.pisp.international.standing.order.service.InternationalStandingOrdersService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-11T16:32:00.031+05:30")

@Controller
public class InternationalStandingOrdersApiController implements InternationalStandingOrdersApi {

	@Autowired
	InternationalStandingOrdersService service;
    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public InternationalStandingOrdersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

	/*@Override   
	public ResponseEntity<CustomIStandingOrderPOSTResponse> createInternationalStandingOrders(
			@ApiParam(value = "Default", required = true) @Valid @RequestBody OBWriteInternationalStandingOrder1 request,
			@ApiParam(value = "The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.", required = true) @RequestHeader(value = "x-fapi-financial-id", required = true) String xFapiFinancialId,
			@ApiParam(value = "An Authorisation Token as per https://tools.ietf.org/html/rfc6750", required = true) @RequestHeader(value = "Authorization", required = true) String authorization,
			@ApiParam(value = "Every request will be processed only once per x-idempotency-key.  The Idempotency Key will be valid for 24 hours.", required = true) @RequestHeader(value = "x-idempotency-key", required = true) String xIdempotencyKey,
			@ApiParam(value = "A detached JWS signature of the body of the payload.", required = true) @RequestHeader(value = "x-jws-signature", required = true) String xJwsSignature,
			@ApiParam(value = "The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC") @RequestHeader(value = "x-fapi-customer-last-logged-time", required = false) String xFapiCustomerLastLoggedTime,
			@ApiParam(value = "The PSU's IP address if the PSU is currently logged in with the TPP.") @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
			@ApiParam(value = "An RFC4122 UID used as a correlation id.") @RequestHeader(value = "x-fapi-interaction-id", required = false) String xFapiInteractionId,
			@ApiParam(value = "Indicates the user-agent that the PSU is using.") @RequestHeader(value = "x-customer-user-agent", required = false) String xCustomerUserAgent)  {
		CustomIStandingOrderPOSTRequest customRequest = new CustomIStandingOrderPOSTRequest();
		customRequest.setData(request.getData());
		customRequest.setRisk(request.getRisk());
		
		return new ResponseEntity<>(service.createInternationalStandingOrderResource(customRequest), HttpStatus.CREATED);
	}*/

	@Override
	public ResponseEntity<OBWriteInternationalStandingOrderResponse1> getInternationalStandingOrdersInternationalStandingOrderPaymentId(
			@ApiParam(value = "InternationalPaymentId", required = true) @PathVariable("InternationalPaymentId") String internationalPaymentId,
			@ApiParam(value = "The unique id of the ASPSP to which the request is issued. The unique id will be issued by OB.", required = true) @RequestHeader(value = "x-fapi-financial-id", required = true) String xFapiFinancialId,
			@ApiParam(value = "An Authorisation Token as per https://tools.ietf.org/html/rfc6750", required = true) @RequestHeader(value = "Authorization", required = true) String authorization,
			@ApiParam(value = "The time when the PSU last logged in with the TPP.  All dates in the HTTP headers are represented as RFC 7231 Full Dates. An example is below:  Sun, 10 Sep 2017 19:43:31 UTC") @RequestHeader(value = "x-fapi-customer-last-logged-time", required = false) String xFapiCustomerLastLoggedTime,
			@ApiParam(value = "The PSU's IP address if the PSU is currently logged in with the TPP.") @RequestHeader(value = "x-fapi-customer-ip-address", required = false) String xFapiCustomerIpAddress,
			@ApiParam(value = "An RFC4122 UID used as a correlation id.") @RequestHeader(value = "x-fapi-interaction-id", required = false) String xFapiInteractionId,
			@ApiParam(value = "Indicates the user-agent that the PSU is using.") @RequestHeader(value = "x-customer-user-agent", required = false) String xCustomerUserAgent) {
		return new ResponseEntity<>(service.retrieveInternationalStandingOrderResource(internationalPaymentId),HttpStatus.OK);
	}

	@Override
	public ResponseEntity<CustomIStandingOrderPOSTResponse> createInternationalStandingOrders(
			@RequestBody OBWriteInternationalStandingOrder1 request, String xFapiFinancialId,
			String authorization, String xIdempotencyKey, String xJwsSignature, String xFapiCustomerLastLoggedTime,
			String xFapiCustomerIpAddress, String xFapiInteractionId, String xCustomerUserAgent) {
		CustomIStandingOrderPOSTRequest customRequest = new CustomIStandingOrderPOSTRequest();
		customRequest.setData(request.getData());
		customRequest.setRisk(request.getRisk());
		
		return new ResponseEntity<>(service.createInternationalStandingOrderResource(customRequest), HttpStatus.CREATED);}

}
