package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The transaction information from the account of the end user used in the
 * originating event\&quot;
 */
@ApiModel(description = "The transaction information from the account of the end user used in the originating event\"")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaction {
	@SerializedName("paymentInstruction")
	private PaymentInstruction paymentInstruction;

	@SerializedName("counterParty")
	private List<CounterParty> counterParty = new ArrayList<CounterParty>();

	@SerializedName("boiCurrencyCode")
	private String boiCurrencyCode;

	@SerializedName("financialEventAmount")
	private FinancialEventAmount financialEventAmount;

	@SerializedName("occurenceDate")
	private String occurenceDate;

	@SerializedName("transactionDecription")
	private String transactionDecription;

	public Transaction paymentInstruction(PaymentInstruction paymentInstruction) {
		this.paymentInstruction = paymentInstruction;
		return this;
	}

	/**
	 * Get paymentInstruction
	 * 
	 * @return paymentInstruction
	 **/
	@ApiModelProperty(required = true, value = "")
	public PaymentInstruction getPaymentInstruction() {
		return paymentInstruction;
	}

	public void setPaymentInstruction(PaymentInstruction paymentInstruction) {
		this.paymentInstruction = paymentInstruction;
	}

	public Transaction counterParty(List<CounterParty> counterParty) {
		this.counterParty = counterParty;
		return this;
	}

	public Transaction addCounterPartyItem(CounterParty counterPartyItem) {
		this.counterParty.add(counterPartyItem);
		return this;
	}

	/**
	 * The details of the counterparty to the financial Event.
	 * 
	 * @return counterParty
	 **/
	@ApiModelProperty(required = true, value = "The details of the counterparty to the financial Event.")
	public List<CounterParty> getCounterParty() {
		return counterParty;
	}

	public void setCounterParty(List<CounterParty> counterParty) {
		this.counterParty = counterParty;
	}

	public Transaction boiCurrencyCode(String boiCurrencyCode) {
		this.boiCurrencyCode = boiCurrencyCode;
		return this;
	}

	/**
	 * Get boiCurrencyCode
	 * 
	 * @return boiCurrencyCode
	 **/
	@ApiModelProperty(required = true, value = "")
	public String getBoiCurrencyCode() {
		return boiCurrencyCode;
	}

	public void setBoiCurrencyCode(String boiCurrencyCode) {
		this.boiCurrencyCode = boiCurrencyCode;
	}

	public Transaction financialEventAmount(FinancialEventAmount financialEventAmount) {
		this.financialEventAmount = financialEventAmount;
		return this;
	}

	/**
	 * Get financialEventAmount
	 * 
	 * @return financialEventAmount
	 **/
	@ApiModelProperty(required = true, value = "")
	public FinancialEventAmount getFinancialEventAmount() {
		return financialEventAmount;
	}

	public void setFinancialEventAmount(FinancialEventAmount financialEventAmount) {
		this.financialEventAmount = financialEventAmount;
	}

	public Transaction occurenceDate(String occurenceDate) {
		this.occurenceDate = occurenceDate;
		return this;
	}

	/**
	 * The Occurrence Date is the date that someone did something which started
	 * a transaction in motion
	 * 
	 * @return occurenceDate
	 **/
	@ApiModelProperty(value = "The Occurrence Date is the date that someone did something which started a transaction in motion")
	public String getOccurenceDate() {
		return occurenceDate;
	}

	public void setOccurenceDate(String occurenceDate) {
		this.occurenceDate = occurenceDate;
	}

	public Transaction transactionDecription(String transactionDecription) {
		this.transactionDecription = transactionDecription;
		return this;
	}

	/**
	 * Description of the Payment / Transaction
	 * 
	 * @return transactionDecription
	 **/
	@ApiModelProperty(value = "Description of the Payment  / Transaction")
	public String getTransactionDecription() {
		return transactionDecription;
	}

	public void setTransactionDecription(String transactionDecription) {
		this.transactionDecription = transactionDecription;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Transaction transaction = (Transaction) o;
		return Objects.equals(this.paymentInstruction, transaction.paymentInstruction)
				&& Objects.equals(this.counterParty, transaction.counterParty)
				&& Objects.equals(this.boiCurrencyCode, transaction.boiCurrencyCode)
				&& Objects.equals(this.financialEventAmount, transaction.financialEventAmount)
				&& Objects.equals(this.occurenceDate, transaction.occurenceDate)
				&& Objects.equals(this.transactionDecription, transaction.transactionDecription);
	}

	@Override
	public int hashCode() {
		return Objects.hash(paymentInstruction, counterParty, boiCurrencyCode, financialEventAmount, occurenceDate,
				transactionDecription);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Transaction {\n");

		sb.append("    paymentInstruction: ").append(toIndentedString(paymentInstruction)).append("\n");
		sb.append("    counterParty: ").append(toIndentedString(counterParty)).append("\n");
		sb.append("    boiCurrencyCode: ").append(toIndentedString(boiCurrencyCode)).append("\n");
		sb.append("    financialEventAmount: ").append(toIndentedString(financialEventAmount)).append("\n");
		sb.append("    occurenceDate: ").append(toIndentedString(occurenceDate)).append("\n");
		sb.append("    transactionDecription: ").append(toIndentedString(transactionDecription)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
