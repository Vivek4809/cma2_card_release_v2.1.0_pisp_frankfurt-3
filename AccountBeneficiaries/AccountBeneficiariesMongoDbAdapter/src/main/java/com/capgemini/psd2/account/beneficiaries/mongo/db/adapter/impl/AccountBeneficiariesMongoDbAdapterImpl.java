/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.repository.AccountBeneficiaryRepository;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.domain.OBBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary2Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountBeneficiaryCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;


/**
 * The Class AccountBeneficiariesMongoDbAdaptorImpl.
 */
@Component
public class AccountBeneficiariesMongoDbAdapterImpl implements AccountBeneficiariesAdapter {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountBeneficiariesMongoDbAdapterImpl.class);
	
	@Autowired
	private AccountBeneficiaryRepository repository;
	
	@Autowired
	SandboxValidationUtility utility;
	
	@Override
	public PlatformAccountBeneficiariesResponse retrieveAccountBeneficiaries(AccountMapping accountMapping,
			Map<String, String> params) {
		PlatformAccountBeneficiariesResponse platformBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary2 oBReadBeneficiary2 = new OBReadBeneficiary2();
		List<AccountBeneficiaryCMA2> mockBeneficiary = new ArrayList<>();
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));
		
		try {
			if(utility.isValidPsuId(accountMapping.getPsuId())){
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			mockBeneficiary = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.CONNECTION_ERROR));
		}
		
		List<OBBeneficiary2> beneficiaryList = new ArrayList<>();	
		
		if (null!=mockBeneficiary && !mockBeneficiary.isEmpty()) {
			for (AccountBeneficiaryCMA2 beneficiary : mockBeneficiary) {
				beneficiary.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
				beneficiaryList.add(beneficiary);
			}
		}
		
		OBReadBeneficiary2Data oBReadBeneficiary2Data = new OBReadBeneficiary2Data();
		oBReadBeneficiary2Data.setBeneficiary(beneficiaryList);
		oBReadBeneficiary2.setData(oBReadBeneficiary2Data);
		platformBeneficiariesResponse.setoBReadBeneficiary2(oBReadBeneficiary2);
		return platformBeneficiariesResponse;
	}
}
