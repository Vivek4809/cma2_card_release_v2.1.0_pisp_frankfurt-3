package com.capgemini.psd2.account.beneficiaries.routing.adapter.test.adapter;

import java.util.Map;

import com.capgemini.psd2.account.beneficiaries.routing.adapter.test.mock.data.AccountBeneficiariesRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountBeneficiariesTestRoutingAdapter implements AccountBeneficiariesAdapter {

	@Override
	public PlatformAccountBeneficiariesResponse retrieveAccountBeneficiaries(AccountMapping accountMapping,
			Map<String, String> params) {
		return AccountBeneficiariesRoutingAdapterTestMockData.getMockBeneficiariesGETResponse();
	}

}
