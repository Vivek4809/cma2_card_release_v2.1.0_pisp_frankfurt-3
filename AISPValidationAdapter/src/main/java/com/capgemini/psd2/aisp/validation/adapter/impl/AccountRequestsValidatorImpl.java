package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.constants.CommonAccountValidations;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountRequestsValidator")
@ConfigurationProperties("app")
public class AccountRequestsValidatorImpl implements
AISPCustomValidator<OBReadConsentResponse1> {

@Autowired
private PSD2Validator psd2Validator;

@Value("${app.regex.paymentId:#{null}}")
private String paymentIdRegexValidator;

@Value("${app.swaggerValidations.response:#{true}}")
private Boolean resValidationEnabled;

@Autowired
private CommonAccountValidations commonAccountValidations;

@Override
public boolean validateAccountsResponse(OBReadConsentResponse1 t) {
// TODO Auto-generated method stub
return false;
}

}