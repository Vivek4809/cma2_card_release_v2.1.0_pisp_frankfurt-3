package com.capgemini.psd2.identitymanagement.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.token.store.jwk.JwkException;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.identitymanagement.constants.IdentityManagementConstants;
import com.capgemini.psd2.identitymanagement.models.CustomClientDetails;
import com.capgemini.psd2.identitymanagement.models.LoginCredentials;
import com.capgemini.psd2.identitymanagement.models.TppAuthorities;
import com.capgemini.psd2.identitymanagement.services.TppInfoMgmtService;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Service("tppInfoMgmtService")
@ConditionalOnProperty(name = "app.isSandBoxProfile", havingValue = "true", matchIfMissing = false)
@ConfigurationProperties("clientAuthorities")
public class TppInfoMgmtServiceMuleImpl implements TppInfoMgmtService {

	@Autowired
	@Qualifier("restClientIdManagement")
	private RestClientSync restClient;
	
	@Value("${mule.loginUrl}")
	private String loginUrl;
	@Value("${mule.username}")
	private String username;
	@Value("${mule.password}")
	private String password;
	@Value("${mule.clientDetailsUrl}")
	private String clientDetailsUrl;
	
	private List<String> authorities; 
	
	
	public String restTransportForAuthenticationApplication(RequestInfo reqInfo, LoginCredentials loginCredentials, Class<String> response, HttpHeaders headers) {
		return restClient.callForPost(reqInfo, loginCredentials, response, headers);
	}

	@Override
	public CustomClientDetails findTppInfo(String clientId){
		return findTPPDetail(clientId);
	}
	
	private CustomClientDetails findTPPDetail(String clientId){
		String token = getBearerToken();
		Map<String,Object> clientDetails = getClientDetails(clientId, token);
		
		CustomClientDetails customClientDetails = populateCustomClientDetails(clientDetails);
		
		return customClientDetails;
	}
	
	private String getBearerToken() {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(loginUrl);
		LoginCredentials loginCredentials = new LoginCredentials(username , password);
		Map<String,String> token =  restClient.callForPost(requestInfo, loginCredentials, Map.class, null);
		return token.get(IdentityManagementConstants.RES_MULE_ACCESS_TOKEN);
	}

	private Map<String,Object> getClientDetails(String clientId, String token) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(clientDetailsUrl + clientId);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(IdentityManagementConstants.AUTHORIZATION_HEADER, IdentityManagementConstants.BEARER +" "+ token);
		Map<String,Object> clientDetails =  restClient.callForGet(requestInfo, Map.class, httpHeaders);
		return clientDetails;
	}

	private CustomClientDetails populateCustomClientDetails(Map<String,Object> clientDetails) {
		CustomClientDetails customClientDetails = null;
		try{
			String clientId = (String)clientDetails.get(IdentityManagementConstants.RES_MULE_CLIENT_ID);
			String secret = (String)clientDetails.get(IdentityManagementConstants.RES_MULE_CLIENT_SECRET);
			List<String> redirectUriList = (ArrayList<String>)clientDetails.get(IdentityManagementConstants.RES_MULE_REDIRECT_URI);
			String legalEntityName = (String)clientDetails.get(IdentityManagementConstants.RES_MULE_CLIENT_NAME);
			
			Collection<GrantedAuthority> tppAuthorities = new ArrayList<>();
			
			for(String grantAuthority : authorities){
				tppAuthorities.add(new TppAuthorities(grantAuthority));
			}
					
			Set<String> redirectUriSet = new HashSet<String>(redirectUriList);
			
			customClientDetails = new CustomClientDetails(clientId, secret
					, redirectUriSet, tppAuthorities, 
					false, legalEntityName, null);
			
			return customClientDetails;
		}
		catch(Exception e){
			throw new JwkException(e.getMessage());
		}
	}
	
	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientDetailsUrl() {
		return clientDetailsUrl;
	}

	public void setClientDetailsUrl(String clientDetailsUrl) {
		this.clientDetailsUrl = clientDetailsUrl;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

}