package com.capgemini.psd2.identitymanagement.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.idmanagement.handlers.IdmanagementExceptionHandlerImpl;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@Configuration
public class IdManagementRestClientConfig {

	/**
	 * Rest client sync impl.
	 *
	 * @return the rest client sync
	 */
	@Bean(name="restClientIdManagement")
	public RestClientSync restClientSyncImpl(){
		return new RestClientSyncImpl(idManagementExceptionHandlerImpl());
	}
	
	
	/**
	 * Exception handler impl.
	 *
	 * @return the exception handler
	 */
	@Bean
	public IdmanagementExceptionHandlerImpl idManagementExceptionHandlerImpl(){
		return new IdmanagementExceptionHandlerImpl();
	}
}
