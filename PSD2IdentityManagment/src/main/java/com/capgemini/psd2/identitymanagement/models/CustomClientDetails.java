package com.capgemini.psd2.identitymanagement.models;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.provider.ClientDetails;

public class CustomClientDetails implements ClientDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String clientId;
	private String clientSecret;
	private Set<String> registeredRedirectUri;
	private Collection<GrantedAuthority> authorities;
	private Integer accessTokenValiditySeconds; 
	private Integer refreshTokenValiditySeconds; 
	private boolean autoApprove; 
	private boolean tppBlocked;
	private String legalEntityName;
	private String registeredId;
	private boolean secretRequired; 
	private Set<String> resourceIds;
	private Set<String> scopes; 
	private transient Map<String,List<Object>> scopeClaims; 
	private Set<String> authorizedGrantTypes; 
	private transient Map<String, Object> additionalInformation;
	
	
	public CustomClientDetails(String clientId,String clientSecret,Set<String> registeredRedirectUri,Collection<GrantedAuthority> authorities,boolean tppBlocked,String legalEntityName,String registeredId) {
		this.clientId=clientId;
		this.clientSecret = clientSecret;
		this.registeredRedirectUri = registeredRedirectUri;
		this.authorities = authorities;
		this.tppBlocked = tppBlocked;
		this.registeredId = registeredId;
		this.legalEntityName = legalEntityName;
	}
	
	
	public CustomClientDetails(){
		
	}
	
	public void setScopes(Set<String> scopes) {
		this.scopes = scopes;
	}

	public void setAuthorizedGrantTypes(Set<String> authorizedGrantTypes) {
		this.authorizedGrantTypes = authorizedGrantTypes;
	}
	
	public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
		this.accessTokenValiditySeconds = accessTokenValiditySeconds;
	}

	public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
		this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
	}
	
	
	
	@Override
	public String getClientId() {
		return clientId;
	}
	
	public void setTppBlocked(boolean tppBlocked) {
		this.tppBlocked = tppBlocked;
	}


	public boolean isTppBlocked() {
		return tppBlocked;
	}
	
	
	@Override
	public Set<String> getResourceIds() {
		return resourceIds;
	}

	public void setResourceIds(Set<String> resourceIds) {
		this.resourceIds = resourceIds;
	}


	@Override
	public boolean isSecretRequired() {
		return secretRequired;
	}

	@Override
	public String getClientSecret() {
		return clientSecret;
	}

	@Override
	public boolean isScoped() {
		return false;
	}

	@Override
	public Set<String> getScope() {
		return scopes;
	}

	@Override
	public Set<String> getAuthorizedGrantTypes() {
		return authorizedGrantTypes;
	}
	
	public void setRegisteredRedirectUri(Set<String> registeredRedirectUri) {
		this.registeredRedirectUri = registeredRedirectUri;
	}


	@Override
	public Set<String> getRegisteredRedirectUri() {
		return registeredRedirectUri;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public Integer getAccessTokenValiditySeconds() {
		return accessTokenValiditySeconds;
	}

	@Override
	public Integer getRefreshTokenValiditySeconds() {
		return refreshTokenValiditySeconds;
	}

	@Override
	public boolean isAutoApprove(String scope) {
		return autoApprove;
	}

	@Override
	public Map<String, Object> getAdditionalInformation() {
		return additionalInformation;
	}

	public String getLegalEntityName() {
		return legalEntityName;
	}

	public void setLegalEntityName(String legalEntityName) {
		this.legalEntityName = legalEntityName;
	}

	public String getRegisteredId() {
		return registeredId;
	}

	public void setRegisteredId(String registeredId) {
		this.registeredId = registeredId;
	}

	public Map<String, List<Object>> getScopeClaims() {
		return scopeClaims;
	}

	public void setScopeClaims(Map<String, List<Object>> scopeClaims) {
		this.scopeClaims = scopeClaims;
	}
}