package com.capgemini.psd2.identitymanagement.test.models;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import com.capgemini.psd2.identitymanagement.models.CustomClientDetails;

public class CustomClientDetailsTest {

	private CustomClientDetails customClientDetails;
	private CustomClientDetails custClientDetails;
	
	@Before
	public void setUp() throws Exception {
		customClientDetails = new CustomClientDetails();
		customClientDetails.setAccessTokenValiditySeconds(12);
		Set<String> authorizedGrantTypes = new HashSet<>();
		authorizedGrantTypes.add("AISP");
		customClientDetails.setAuthorizedGrantTypes(authorizedGrantTypes );
		customClientDetails.setLegalEntityName("Moneywise");
		customClientDetails.setRefreshTokenValiditySeconds(12);
		customClientDetails.setRegisteredId("Moneywise");
		Set<String> registeredRedirectUri = new HashSet<>();
		registeredRedirectUri.add("/test");
		customClientDetails.setRegisteredRedirectUri(registeredRedirectUri );
		Set<String> resourceIds = new HashSet<>();
		resourceIds.add("resource");
		customClientDetails.setResourceIds(resourceIds );
		Map<String, List<Object>> scopeClaims = new HashMap<>();
		List<Object> scope = new ArrayList<>();
		scope.add("accounts");
		scopeClaims.put("claims",scope);
		customClientDetails.setScopeClaims(scopeClaims);
		Set<String> scopes = new HashSet<>();
		scopes.add("accounts");
		customClientDetails.setScopes(scopes);
		Collection<GrantedAuthority> authorities = new HashSet<>();
		GrantedAuthority GrantedAuthority = null;
		authorities.add(GrantedAuthority );
	     custClientDetails = new CustomClientDetails("Moneywise", "Moneywise", registeredRedirectUri, authorities  , Boolean.TRUE, "Moneywise", "Moneywise_Wealth"); 
	    custClientDetails.setTppBlocked(true);
	}
	
	@Test
	public void test(){
	    assertEquals(12,customClientDetails.getAccessTokenValiditySeconds().intValue());
	    assertEquals("Moneywise", custClientDetails.getClientId());
		assertFalse(custClientDetails.getAuthorities().isEmpty());
		assertTrue(customClientDetails.getAuthorizedGrantTypes().contains("AISP"));
		assertEquals("Moneywise",custClientDetails.getClientSecret());
		assertEquals("Moneywise",customClientDetails.getLegalEntityName());
		assertEquals(12,customClientDetails.getRefreshTokenValiditySeconds().intValue());
		assertEquals("Moneywise",customClientDetails.getRegisteredId());
		assertTrue(customClientDetails.getRegisteredRedirectUri().contains("/test"));
		assertFalse(customClientDetails.getResourceIds().isEmpty());
		assertTrue(customClientDetails.getScope().contains("accounts"));
		assertTrue(customClientDetails.getScopeClaims().containsKey("claims"));
	    assertTrue(custClientDetails.isTppBlocked());
	    assertNotNull(custClientDetails.isAutoApprove("scope"));
	    assertFalse(custClientDetails.isSecretRequired());
	    assertFalse(custClientDetails.isScoped());
	}
	
	
}
