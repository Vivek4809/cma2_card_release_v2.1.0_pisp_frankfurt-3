/*package com.capgemini.psd2.security.test.models;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.identitymanagement.models.CustomClientDetails;
import com.capgemini.psd2.security.models.AuthorizedTokenDTO;
import com.capgemini.psd2.security.validation.ConsentTypeCodeEnum;

public class AuthorizedTokenDTOTest {

	private AuthorizedTokenDTO authorizedTokenDTO;

	@Before
	public void setUp() throws Exception {
		authorizedTokenDTO = new AuthorizedTokenDTO();
		authorizedTokenDTO.setFlowType("AISP");
		Map<String, String> params =new HashMap<>();
		params.put(ConsentTypeCodeEnum.AccountRequestId.name(),"123445");
		authorizedTokenDTO.setParamsMap(params);
		authorizedTokenDTO.setSetupParamName("AccountRequestId");
		CustomClientDetails customClientDetails = new CustomClientDetails();
		customClientDetails.setLegalEntityName("Moneywise");
		authorizedTokenDTO.setCustomClientDetails(customClientDetails);
	}

	@Test
	public void testGetter(){
		assertEquals("AISP",authorizedTokenDTO.getFlowType());
		assertEquals("123445",authorizedTokenDTO.getParamsMap().get(ConsentTypeCodeEnum.AccountRequestId.name()));
	    assertEquals("Moneywise", authorizedTokenDTO.getCustomClientDetails().getLegalEntityName());
		assertEquals("AccountRequestId",authorizedTokenDTO.getSetupParamName());
	}



}
*/