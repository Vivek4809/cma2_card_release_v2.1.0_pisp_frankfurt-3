package com.capgemini.psd2.security.test.aspect;

import static org.mockito.Mockito.doReturn;

import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.security.aspects.PSD2SecurityAspect;
import com.capgemini.psd2.security.aspects.PSD2SecurityAspectUtils;

@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2SecurityAspectTest {

	@InjectMocks
	private PSD2SecurityAspect aspect = new PSD2SecurityAspect();
	@Mock
	private PSD2SecurityAspectUtils aspectUtils;
	@Mock
	private ProceedingJoinPoint proceedingJoinPoint;


	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testArroundLoggerAdviceService(){
		doReturn(new Object()).when(aspectUtils).methodAdvice(proceedingJoinPoint);
		aspect.arroundLoggerAdviceService(proceedingJoinPoint);
	}
	
	@Test
	public void testArroundLoggerAdviceController(){
		doReturn(new Object()).when(aspectUtils).methodAdvice(proceedingJoinPoint);
		aspect.arroundLoggerAdviceController(proceedingJoinPoint);
	}
	

}
