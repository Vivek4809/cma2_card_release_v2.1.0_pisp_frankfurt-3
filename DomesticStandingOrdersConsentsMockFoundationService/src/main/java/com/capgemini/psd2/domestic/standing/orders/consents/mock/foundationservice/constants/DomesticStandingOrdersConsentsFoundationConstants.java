package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.constants;

public class DomesticStandingOrdersConsentsFoundationConstants {
	
	public static final String RECORD_NOT_FOUND = "Standing Order Instruction Proposal Record Not Found";
	
	public static final String REFERENCE = "PISPreference";
	
}
