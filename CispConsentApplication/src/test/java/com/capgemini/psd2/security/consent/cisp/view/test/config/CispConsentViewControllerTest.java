/*package com.capgemini.psd2.security.consent.cisp.view.test.config;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBAccount2;
import com.capgemini.psd2.aisp.domain.OBAccount2Account;
import com.capgemini.psd2.aisp.domain.OBAccount2Servicer;
import com.capgemini.psd2.aisp.domain.OBReadAccount2;
import com.capgemini.psd2.aisp.domain.OBReadAccount2Data;
import com.capgemini.psd2.cisp.domain.OBCashAccountDebtor2;
import com.capgemini.psd2.cisp.domain.OBCashAccountDebtor2.SchemeNameEnum;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentDataResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.cisp.view.controllers.CispConsentViewController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;

public class CispConsentViewControllerTest {
	


	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;

	@Mock
	private UIStaticContentUtilityController uiController;

	@Mock
	private RequestHeaderAttributes requestHeaders;

	@Mock
	private CispConsentCreationDataHelper consentCreationDataHelper;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private DataMask dataMask;
	
	@InjectMocks
	private CispConsentViewController cispConsentViewController;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testConsentView() throws NamingException {
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(consentCreationDataHelper.findExistingConsentAccounts(anyString())).thenReturn(getCustomerAccountInfo());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model=new HashMap<>();
		
		Object tppInfo=new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		cispConsentViewController.consentView(model);
	}
	
	@Test
	public void testConsentViewElseTest() throws NamingException {
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		pickupDataModel.setUserId("userId");
		requestHeaders.setCorrelationId("correlationId");
		pickupDataModel.setChannelId("ChannelId");
		
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model=new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data = new OBFundsConfirmationConsentDataResponse1();
		setUpResponseData.setData(data);
		OBCashAccountDebtor2 debtorAccount = new OBCashAccountDebtor2();
		data.setDebtorAccount(debtorAccount);
		data.getDebtorAccount().setSchemeName(SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		
		Object tppInfo=new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId())).thenReturn(setUpResponseData);
		cispConsentViewController.consentView(model);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testConsentViewElseNotNullTest() throws NamingException {
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		pickupDataModel.setUserId("userId");
		requestHeaders.setCorrelationId("correlationId");
		pickupDataModel.setChannelId("ChannelId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model=new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data= new OBFundsConfirmationConsentDataResponse1();
		data.setConsentId("consentId");
		setUpResponseData.setData(data);
		OBCashAccountDebtor2 debtorAccount = new OBCashAccountDebtor2();
		debtorAccount.setIdentification("identification");
		com.capgemini.psd2.cisp.domain.OBCashAccountDebtor2.SchemeNameEnum val =com.capgemini.psd2.cisp.domain.OBCashAccountDebtor2.SchemeNameEnum.SORTCODEACCOUNTNUMBER;
		debtorAccount.setSchemeName(val );
		setUpResponseData.getData().setDebtorAccount(debtorAccount);
		Object tppInfo=new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId())).thenReturn(setUpResponseData);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), pickupDataModel.getIntentTypeEnum().getIntentType(),
					requestHeaders.getCorrelationId(), pickupDataModel.getChannelId(), "schemeName")).thenReturn(getCustomerAccountInfo());
		when(tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId())).thenReturn("tppInformationObj");
		cispConsentViewController.consentView(model);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testConsentViewElseNotNull1Test() throws NamingException {
		
		requestHeaders.setCorrelationId("correlationId");
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model=new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data =new OBFundsConfirmationConsentDataResponse1();
		data.setConsentId("consentId");
		setUpResponseData.setData(data);
		OBCashAccountDebtor2 debtorAccount = new OBCashAccountDebtor2();
		debtorAccount.setIdentification("identification");
		debtorAccount.setSchemeName(com.capgemini.psd2.cisp.domain.OBCashAccountDebtor2.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
		setUpResponseData.getData().setDebtorAccount(debtorAccount );
		Object tppInfo=new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId())).thenReturn(setUpResponseData);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), pickupDataModel.getIntentTypeEnum().getIntentType(),
					requestHeaders.getCorrelationId(), pickupDataModel.getChannelId(), "schemeName")).thenReturn(getCustomerAccountInfo());
		BasicAttributes tppInformationObj = new BasicAttributes();
		when(tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId())).thenReturn(tppInformationObj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), pickupDataModel.getIntentTypeEnum().getIntentType(),
					requestHeaders.getCorrelationId(), pickupDataModel.getChannelId(),setUpResponseData.getData().getDebtorAccount().getSchemeName().toString())).thenReturn(getCustomerAccountInfo());
		cispConsentViewController.consentView(model);
	}
	
	@Test
	public void testConsentViewElseNotNull2Test() throws NamingException {
		
		requestHeaders.setCorrelationId("correlationId");
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model=new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentDataResponse1 data =new OBFundsConfirmationConsentDataResponse1();
		data.setConsentId("consentId");
		setUpResponseData.setData(data);
		OBCashAccountDebtor2 debtorAccount = new OBCashAccountDebtor2();
		debtorAccount.setIdentification("IBAN");
		debtorAccount.setSecondaryIdentification("1234");
		debtorAccount.setSchemeName(com.capgemini.psd2.cisp.domain.OBCashAccountDebtor2.SchemeNameEnum.IBAN);
		setUpResponseData.getData().setDebtorAccount(debtorAccount );
		Object tppInfo=new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId())).thenReturn(setUpResponseData);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), pickupDataModel.getIntentTypeEnum().getIntentType(),
					requestHeaders.getCorrelationId(), pickupDataModel.getChannelId(), "schemeName")).thenReturn(getCustomerAccountInfo());
		BasicAttributes tppInformationObj = new BasicAttributes();
		when(tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId())).thenReturn(tppInformationObj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), pickupDataModel.getIntentTypeEnum().getIntentType(),
					requestHeaders.getCorrelationId(), pickupDataModel.getChannelId(),setUpResponseData.getData().getDebtorAccount().getSchemeName().toString())).thenReturn(getCustomerAccountInfo());
		cispConsentViewController.consentView(model);
	}
	
	public static OBReadAccount2 getCustomerAccountInfo() {
		OBReadAccount2 mockOBReadAccount2 = new OBReadAccount2();
		List<OBAccount2> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(SchemeNameEnum.IBAN.name(), "IBAN");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "78910111");
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123456");

		acct.setAdditionalInformation(additionalInformation);
		// acct.setHashedValue();
		//acct.setAccountType("savings");
		OBAccount2Servicer servicer = new OBAccount2Servicer();
		servicer.setIdentification("identification");
		List<OBAccount2Account> accountList= new ArrayList<>();
		OBAccount2Account account = new OBAccount2Account();
		account.setIdentification("identification");
		account.setSchemeName("UK.OBIE.IBAN");
		accountList.add(account);
		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		// accnt.setHashedValue();
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		//accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount2Data data2 = new OBReadAccount2Data();
		data2.setAccount(accountData);
		account.setSchemeName("UK.OBIE.IBAN");
		Map<String, String> additionalInformation1 = new HashMap<>();
		additionalInformation1.put(SchemeNameEnum.IBAN.name(), "IBAN");
		additionalInformation1.put(PSD2Constants.ACCOUNT_NUMBER, "78910111");
		additionalInformation1.put(PSD2Constants.ACCOUNT_NSC, "123456");

		accnt.setAdditionalInformation(additionalInformation1);
		mockOBReadAccount2.setData(data2);

		return mockOBReadAccount2;
	}
	
	
	
	@Test
	public void homePageTest(){
		
		Map<String, Object> model=new HashMap<>();
		when(request.getRequestURI()).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm/home");
		when(request.getQueryString()).thenReturn("Dummy Query");
		ReflectionTestUtils.setField(cispConsentViewController, "edgeserverhost", "edgeserverhost");
		ReflectionTestUtils.setField(cispConsentViewController, "applicationName", "applicationName");
		cispConsentViewController.homePage(model);
	}
	
	
	
}
*/