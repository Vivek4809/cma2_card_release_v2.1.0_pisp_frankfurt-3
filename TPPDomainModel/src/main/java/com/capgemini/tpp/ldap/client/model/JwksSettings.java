package com.capgemini.tpp.ldap.client.model;

public class JwksSettings {

	private String jwksUrl;
	private String jwks;

	public String getJwksUrl() { 
		return this.jwksUrl; 
	}

	public void setJwksUrl(String jwksUrl) { 
		this.jwksUrl = jwksUrl; 
	}

	public String getJwks() { 
		return this.jwks; 
	}

	public void setJwks(String jwks) { 
		this.jwks = jwks; 
	}	
}
