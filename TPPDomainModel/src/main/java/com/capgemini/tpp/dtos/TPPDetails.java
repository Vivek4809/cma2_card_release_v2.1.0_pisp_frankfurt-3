package com.capgemini.tpp.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "applicationName", "applicationDescription", "clientId", "redirectUris", "termOfServiceURI",
		"policyURI", "JWKSEndPoint","file" })
public class TPPDetails {

	@JsonProperty("applicationName")
	private String applicationName;
	@JsonProperty("applicationDescription")
	private String applicationDescription;
	@JsonProperty("clientId")
	private String clientId;
	@JsonProperty("redirectUris")
	private List<String> redirectUris;
	@JsonProperty("termOfServiceURI")
	private String termOfServiceURI;
	@JsonProperty("policyURI")
	private String policyURI;
	@JsonProperty("JWKSEndPoint")
	private String jwksEndPoint;
	@JsonProperty("file")
	private String file;

	public void setFile(String file) {
		this.file = file;
	}
	public String getFile(String file){
		return file;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		if (applicationName!=null)
			this.applicationName = applicationName;
	}

	public String getApplicationDescription() {
		return applicationDescription;
	}

	public void setApplicationDescription(String applicationDescription) {
		if (applicationDescription!=null)
			this.applicationDescription = applicationDescription;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		if (clientId!=null)
			this.clientId = clientId;
	}

	public List<String> getRedirectUris() {
		return redirectUris;
	}

	public void setRedirectUris(List<String> redirectUris) {
		if (redirectUris != null && !redirectUris.isEmpty())
			this.redirectUris = redirectUris;
	}

	public String getTermOfServiceURI() {
		return termOfServiceURI;
	}

	public void setTermOfServiceURI(String termOfServiceURI) {
		if (termOfServiceURI!=null)
			this.termOfServiceURI = termOfServiceURI;
	}

	public String getPolicyURI() {
		return policyURI;
	}

	public void setPolicyURI(String policyURI) {
		if (policyURI!=null)
			this.policyURI = policyURI;
	}

	public String getJWKSEndPoint() {
		return jwksEndPoint;
	}

	public void setJWKSEndPoint(String jWKSEndPoint) {
		if (jWKSEndPoint!=null)
			this.jwksEndPoint = jWKSEndPoint;
	}
	
	
}