package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Information about the results of a password update operation
 */
@ApiModel(description = "Information about the results of a password update operation")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class PasswordUpdateResponse   {
  /**
   * Gets or Sets schemas
   */
  public enum SchemasEnum {
    PASSWORDUPDATERESPONSE("urn:pingidentity:scim:api:messages:2.0:PasswordUpdateResponse");

    private String value;

    SchemasEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SchemasEnum fromValue(String text) {
      for (SchemasEnum b : SchemasEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("schemas")
  private List<SchemasEnum> schemas = new ArrayList<SchemasEnum>();

  @JsonProperty("generatedPassword")
  private String generatedPassword = null;

  public PasswordUpdateResponse schemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
    return this;
  }

  public PasswordUpdateResponse addSchemasItem(SchemasEnum schemasItem) {
    this.schemas.add(schemasItem);
    return this;
  }

   /**
   * Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.
   * @return schemas
  **/
  @ApiModelProperty(required = true, value = "Contains a list of one or more URIs that indicate included SCIM schemas that are used to indicate the attributes contained within a resource.")
  @NotNull


  public List<SchemasEnum> getSchemas() {
    return schemas;
  }

  public void setSchemas(List<SchemasEnum> schemas) {
    this.schemas = schemas;
  }

  public PasswordUpdateResponse generatedPassword(String generatedPassword) {
    this.generatedPassword = generatedPassword;
    return this;
  }

   /**
   * The password generated during the password update operation.
   * @return generatedPassword
  **/
  @ApiModelProperty(value = "The password generated during the password update operation.")


  public String getGeneratedPassword() {
    return generatedPassword;
  }

  public void setGeneratedPassword(String generatedPassword) {
    this.generatedPassword = generatedPassword;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PasswordUpdateResponse passwordUpdateResponse = (PasswordUpdateResponse) o;
    return Objects.equals(this.schemas, passwordUpdateResponse.schemas) &&
        Objects.equals(this.generatedPassword, passwordUpdateResponse.generatedPassword);
  }

  @Override
  public int hashCode() {
    return Objects.hash(schemas, generatedPassword);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PasswordUpdateResponse {\n");
    
    sb.append("    schemas: ").append(toIndentedString(schemas)).append("\n");
    sb.append("    generatedPassword: ").append(toIndentedString(generatedPassword)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

