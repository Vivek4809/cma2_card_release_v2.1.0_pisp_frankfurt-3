package com.capgemini.tpp.muleapi.schema;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "audit", "masterOrganizationId","organizationId","id","apiVersionId","name","description","limits","status","autoApprove","applicationCount" })
public class Tier {

	@JsonProperty("audit")
	private Audit audit;
	@JsonProperty("masterOrganizationId")
	private String masterOrganizationId;
	@JsonProperty("organizationId")
	private String organizationId;
	@JsonProperty("id")
	private int id;
	@JsonProperty("apiVersionId")
	private int apiVersionId;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
 	private String description;
	@JsonProperty("limits")
 	private ArrayList<Limit> limits;
	@JsonProperty("status")
 	private String status;
	@JsonProperty("autoApprove")
 	private Boolean autoApprove;
	@JsonProperty("applicationCount")
 	private int applicationCount;
	public Audit getAudit() {
		return audit;
	}
	public void setAudit(Audit audit) {
		this.audit = audit;
	}
	public String getMasterOrganizationId() {
		return masterOrganizationId;
	}
	public void setMasterOrganizationId(String masterOrganizationId) {
		this.masterOrganizationId = masterOrganizationId;
	}
	public String getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getApiVersionId() {
		return apiVersionId;
	}
	public void setApiVersionId(int apiVersionId) {
		this.apiVersionId = apiVersionId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ArrayList<Limit> getLimits() {
		return limits;
	}
	public void setLimits(ArrayList<Limit> limits) {
		this.limits = limits;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getAutoApprove() {
		return autoApprove;
	}
	public void setAutoApprove(Boolean autoApprove) {
		this.autoApprove = autoApprove;
	}
	public int getApplicationCount() {
		return applicationCount;
	}
	public void setApplicationCount(int applicationCount) {
		this.applicationCount = applicationCount;
	}
}
