package com.capgemini.psd2.adapter.security.custom.domain;

import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;



public class CustomAuthenticationServiceGetResponse {

	private Boolean sessionInitiationFailureIndicator;
	
	private AuthenticationParameters authenticationParameters;

	public AuthenticationParameters getAuthenticationParameters() {
		return authenticationParameters;
	}

	public void setAuthenticationParameters(AuthenticationParameters authenticationParameters) {
		this.authenticationParameters = authenticationParameters;
	}

	public Boolean getSessionInitiationFailureIndicator() {
		return sessionInitiationFailureIndicator;
	}

	public void setSessionInitiationFailureIndicator(Boolean sessionInitiationFailureIndicator) {
		this.sessionInitiationFailureIndicator = sessionInitiationFailureIndicator;
	}

	
}
