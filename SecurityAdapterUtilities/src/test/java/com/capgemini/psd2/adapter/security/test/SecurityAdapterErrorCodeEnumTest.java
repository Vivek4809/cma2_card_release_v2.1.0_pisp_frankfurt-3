package com.capgemini.psd2.adapter.security.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.adapter.exceptions.security.SecurityAdapterErrorCodeEnum;

/*
 * The Class SecurityAdapterErrorCodeEnumTest.
 */
public class SecurityAdapterErrorCodeEnumTest {

	 /*The security adapter error code enum. */
	private SecurityAdapterErrorCodeEnum technicalError;
	private SecurityAdapterErrorCodeEnum badRequest;
	private SecurityAdapterErrorCodeEnum noValidBrandId;
	private SecurityAdapterErrorCodeEnum noValidChannelsFound;
	private SecurityAdapterErrorCodeEnum authenticationFailure;
	private SecurityAdapterErrorCodeEnum invalidRequestSetupId;
	private SecurityAdapterErrorCodeEnum setupRecordWithInvalidIdStatus;
	
	/* Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		technicalError= SecurityAdapterErrorCodeEnum.TECHNICAL_ERROR;
		badRequest= SecurityAdapterErrorCodeEnum.BAD_REQUEST;
		noValidBrandId= SecurityAdapterErrorCodeEnum.NO_VALID_BRAND_ID;
		noValidChannelsFound= SecurityAdapterErrorCodeEnum.NO_VALID_CHANNELS_FOUND;
		authenticationFailure= SecurityAdapterErrorCodeEnum.AUTHENTICATION_FAILURE_ERROR_LOGIN;
		invalidRequestSetupId= SecurityAdapterErrorCodeEnum.INVALID_REQUEST_SETUP_ID;
		setupRecordWithInvalidIdStatus= SecurityAdapterErrorCodeEnum.SETUP_RECORD_WITH_INVALID_STATUS;
	}
	
	/*
	 * Test.
	 */
	 
	@Test
	public void technicalErrorTest(){
		assertEquals("800", technicalError.getErrorCode()); 
		assertEquals("Technical Error. Please try again later", technicalError.getErrorMessage()); 
		assertEquals("500", technicalError.getStatusCode()); 
	}
	
	@Test
	public void badRequestTest(){
		assertEquals("801", badRequest.getErrorCode()); 
		assertEquals("Bad request.Please check your request", badRequest.getErrorMessage()); 
		assertEquals("400", badRequest.getStatusCode()); 
	}
	
	@Test
	public void noValidBrandIdTest(){
		assertEquals("802", noValidBrandId.getErrorCode()); 
		assertEquals("No valid Brand id provided in request", noValidBrandId.getErrorMessage()); 
		assertEquals("400", noValidBrandId.getStatusCode()); 
	}
	
	@Test
	public void noValidChannelsFoundTest(){
		assertEquals("803", noValidChannelsFound.getErrorCode()); 
		assertEquals("No valid channel found", noValidChannelsFound.getErrorMessage()); 
		assertEquals("400", noValidChannelsFound.getStatusCode()); 
	}
	
	@Test
	public void authenticationFailureTest(){
		assertEquals("804", authenticationFailure.getErrorCode()); 
		assertEquals("Authentication failed", authenticationFailure.getErrorMessage()); 
		assertEquals("401", authenticationFailure.getStatusCode()); 
	}
	
	@Test
	public void invalidRequestSetupIdTest(){
		assertEquals("805", invalidRequestSetupId.getErrorCode()); 
		assertEquals("No such record exist against the requested request id", invalidRequestSetupId.getErrorMessage()); 
		assertEquals("404", invalidRequestSetupId.getStatusCode()); 
	}
	
	
	@Test
	public void setupRecordWithInvalidIdStatusTest(){
		assertEquals("806", setupRecordWithInvalidIdStatus.getErrorCode()); 
		assertEquals("Setup record is having invalid status & does not comply with current flow", setupRecordWithInvalidIdStatus.getErrorMessage()); 
		assertEquals("400", setupRecordWithInvalidIdStatus.getStatusCode()); 
	}
}
