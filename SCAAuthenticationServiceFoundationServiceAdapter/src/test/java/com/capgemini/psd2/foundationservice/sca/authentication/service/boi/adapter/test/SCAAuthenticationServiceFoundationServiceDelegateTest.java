package com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.delegate.SCAAuthenticationServiceFoundationServiceDelegate;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAAuthenticationServiceFoundationServiceDelegateTest {

	@InjectMocks
	private SCAAuthenticationServiceFoundationServiceDelegate delegate;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateRequestHeadersActual() {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(AdapterSecurityConstants.USER_HEADER, "test");
		params.put(AdapterSecurityConstants.CHANNELID_HEADER, "test");
		params.put(AdapterSecurityConstants.CORRELATIONID_HEADER, "test");
		  params.put("x-api-channel-brand", "abghgc");
		  params.put("x-api-channel-code", "abghgc");
 
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "sourceUserInReqHeader", "X-API-SOURCE-USER");
		ReflectionTestUtils.setField(delegate, "sourceSystemInReqHeader", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "trasanctionIdInReqHeader", "X-API-TRANSANCTION-ID");
		ReflectionTestUtils.setField(delegate, "apiCorrelationIdInReqHeader", "X-API-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "apichannelCode", "X-API-Channel-Code");
		ReflectionTestUtils.setField(delegate, "apichannelBrand", "X-API-Channel-Brand");

		RequestInfo requestInfo = new RequestInfo();
		httpHeaders = delegate.createRequestHeaders(requestInfo, params);

		assertNotNull(httpHeaders);
	}

	@Test
	public void testpostAuthenticationFoundationServiceURL() {

		String str = delegate.postAuthenticationFoundationServiceURL();
		assertNotNull(str);

	}

}