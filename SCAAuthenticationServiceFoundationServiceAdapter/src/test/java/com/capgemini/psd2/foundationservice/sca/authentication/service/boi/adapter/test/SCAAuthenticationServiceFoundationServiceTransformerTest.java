package com.capgemini.psd2.foundationservice.sca.authentication.service.boi.adapter.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.domain.AuthenticationMethodCode1;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameterTextTypeValue;
import com.capgemini.psd2.adapter.security.domain.AuthenticationSystemCode;
import com.capgemini.psd2.adapter.security.domain.ChannelCode;
import com.capgemini.psd2.adapter.security.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.adapter.security.domain.DeviceStatusCode;
import com.capgemini.psd2.adapter.security.domain.DeviceType;
import com.capgemini.psd2.adapter.security.domain.DigitalUser;
import com.capgemini.psd2.adapter.security.domain.DigitalUser13;
import com.capgemini.psd2.adapter.security.domain.DigitalUserSession;
import com.capgemini.psd2.adapter.security.domain.EventType1;
import com.capgemini.psd2.adapter.security.domain.LoginResponse;
import com.capgemini.psd2.adapter.security.domain.SecureAccessKeyPositionValue;
import com.capgemini.psd2.adapter.security.domain.SelectedDeviceReference;
import com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.transformer.SCAAuthenticationServiceFoundationServiceTransformer;

public class SCAAuthenticationServiceFoundationServiceTransformerTest {

	
	@InjectMocks
	private SCAAuthenticationServiceFoundationServiceTransformer scaAuthenticationServiceFoundationServiceTransformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testtransformAuthenticationServiceResponse() {

		LoginResponse loginResponse = new LoginResponse();

		DigitalUser digitalUser = new DigitalUser();
		digitalUser.setDigitalUserIdentifier("hkjh");
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserLockedOutIndicator(true);
		loginResponse.setDigitalUser(digitalUser);

		DigitalUserSession digitalUserSession = new DigitalUserSession();
		digitalUserSession.setSessionInitiationFailureIndicator(false);
		digitalUserSession.setSessionInitiationFailureReasonCode("jhgj");
		//digitalUserSession.setSessionStartDateTime("9-8-2019");
		loginResponse.setDigitalUserSession(digitalUserSession);

		scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceResponse(loginResponse);
	}
/*
	@Test
	public void testtransformAuthenticationServiceRequest() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserIdentifier("uigi89689");

		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEncryptedCustomerObject("uytuy9067957");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest,params);

	}

	@Test
	public void testtransformAuthenticationServiceRequest1() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserIdentifier("uigi89689");

		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEncryptedCustomerObject("uytuy9067957");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceRequest(null);

	}

	@Test
	public void testtransformAuthenticationServiceRequest3() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserIdentifier("uigi89689");

		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEncryptedCustomerObject("uytuy9067957");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest);

	}

	@Test
	public void testtransformAuthenticationServiceRequest4() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserIdentifier("uigi89689");

		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PIN);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEncryptedCustomerObject("uytuy9067957");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest);

	}

	@Test
	public void testtransformAuthenticationServiceRequest5() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserIdentifier("uigi89689");

		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.PUSH_NOTIFICATION);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEncryptedCustomerObject("uytuy9067957");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer
				.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest);

	}
	
	@Test
	public void testtransformAuthenticationServiceRequest8() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserIdentifier("uigi89689");

		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEncryptedCustomerObject("uytuy9067957");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		//secureAccessKeyPositionValue.setPosition(5.0);
		secureAccessKeyPositionValue.setValue("5-8-2019");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest);

	}
	
	@Test
	public void testtransformAuthenticationServiceRequest9() {

		CustomAuthenticationServicePostRequest customAuthenticationServicePostRequest = new CustomAuthenticationServicePostRequest();

		DigitalUser13 digitalUser = new DigitalUser13();
		digitalUser.setAuthenticationSystemCode(AuthenticationSystemCode.B365);
		digitalUser.setChannelCode(ChannelCode.B365);
		digitalUser.setDigitalUserIdentifier("uigi89689");

		List<CustomerAuthenticationSession> list = new ArrayList<>();
		CustomerAuthenticationSession customerAuthenticationSession = new CustomerAuthenticationSession();
		customerAuthenticationSession.setAuthenticationMethodCode(AuthenticationMethodCode1.DATE_OF_BIRTH);
		customerAuthenticationSession.setAuthenticationSessionIdentifier("uiu77");
		customerAuthenticationSession.setEncryptedCustomerObject("uytuy9067957");
		customerAuthenticationSession.setEventType(EventType1.ACCOUNT_INFORMATION_CONSENT);

		SelectedDeviceReference selectedDeviceReference = new SelectedDeviceReference();
		selectedDeviceReference.setDeviceNameText("ijioj");
		selectedDeviceReference.setDeviceStatusCode(DeviceStatusCode.ACTIVE);
		selectedDeviceReference.setDeviceType(DeviceType.SOFT_TOKEN);
		customerAuthenticationSession.setSelectedDeviceReference(selectedDeviceReference);

		List<SecureAccessKeyPositionValue> list3 = new ArrayList<>();
		SecureAccessKeyPositionValue secureAccessKeyPositionValue = new SecureAccessKeyPositionValue();
		//secureAccessKeyPositionValue.setPosition(5.0);
		//secureAccessKeyPositionValue.setValue("5-8-2019");
		list3.add(secureAccessKeyPositionValue);
		customerAuthenticationSession.setSecureAccessKeyUsed(list3);

		List<AuthenticationParameterTextTypeValue> list2 = new ArrayList<>();
		AuthenticationParameterTextTypeValue authenticationParameterTextTypeValue = new AuthenticationParameterTextTypeValue();
		authenticationParameterTextTypeValue.setValue("ujo");
		list2.add(authenticationParameterTextTypeValue);
		customerAuthenticationSession.setAuthenticationParameterText(list2);

		list.add(customerAuthenticationSession);
		digitalUser.setCustomerAuthenticationSession(list);
		customAuthenticationServicePostRequest.setDigitalUser(digitalUser);

		scaAuthenticationServiceFoundationServiceTransformer.transformAuthenticationServiceRequest(customAuthenticationServicePostRequest);

	}*/
}
