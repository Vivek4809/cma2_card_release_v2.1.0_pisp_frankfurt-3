package com.capgemini.psd2.revoke.consent.routing.test.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.internal.apis.adapter.RevokeConsentAdapter;
import com.capgemini.psd2.revoke.consent.routing.impl.RevokeConsentRoutingAdapter;

public class RevokeConsentRoutingAdapterTest {


	@InjectMocks
	private RevokeConsentRoutingAdapter adapter;

	@Mock
	private ApplicationContext context;

	@Mock
	private RevokeConsentAdapter beanInstance;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void revokeConsentRequestTest(){
		ReflectionTestUtils.setField(adapter, "defaultAdapter", "mongoDbAdapter");
		adapter.revokeConsentRequest("12344", "1234452");
	}

	@Test
	public void setApplicationContextTest(){
		adapter.setApplicationContext(context);
	}
}
