package com.capgemini.psd2.RevokeConsent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.exceptions.PSD2ExceptionHandler;

@SpringBootApplication
@ComponentScan(basePackages = {
		"com.capgemini.psd2" }, excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = PSD2ExceptionHandler.class))
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@EnableEurekaClient
public class RevokeConsentApplication {

	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		context = SpringApplication.run(RevokeConsentApplication.class, args);
	}

}

